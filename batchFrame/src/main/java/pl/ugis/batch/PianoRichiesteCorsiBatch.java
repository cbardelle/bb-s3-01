package pl.ugis.batch;

import it.webegg.util.EncryptGoodbuy;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.core.Command;
import pl.ugis.batch.dao.BatchService;
import pl.ugis.batch.dao.LdapDAO;
import pl.ugis.batch.utility.CreateCSV;
import pl.ugis.batch.utility.CreateExcel;
import pl.ugis.batch.utility.EmailSender;
import pl.ugis.batch.utility.FtpClient;
import pl.ugis.batch.utility.LDAPUser;
import pl.ugis.batch.utility.MngmUtil;
import pl.ugis.batch.utility.PropertyFileReader;

public class PianoRichiesteCorsiBatch implements Command {

	static private PropertyFileReader pr = PropertyFileReader.getInstance();
	static Logger log = Logger.getLogger(PianoRichiesteCorsiBatch.class);

	public void execute() throws Exception,IllegalStateException 
	{
		try
		{
			String folder = pr.getProperty("folder_"+BatchMain.PIANO_RICHIESTE_CORSI);
			String outputType = pr.getProperty("outputType");
			String dateOutputFormat = pr.getProperty("dateOutputFormat");
			File file = null;
			log.debug("***   UserId: " + pr.getProperty("userId") +", URL: "+ pr.getProperty("URL") );
			
			int enrollmentsSize = BatchService.getEnrollmentsSize(); 
			int enrSize = enrollmentsSize/7500;
			int j=1;
			if(enrollmentsSize%7500!=0){ enrSize++; }
			Set<Integer> usersToCheckInLDAPSize = new HashSet<Integer>();
						
			for (int i = 1; i <= enrSize; i++) {

				if(StringUtils.equalsIgnoreCase(outputType, "xls")){
					file = new File(pr.getProperty("resources-path") + folder + pr.getProperty("fileName_"+BatchMain.PIANO_RICHIESTE_CORSI)+"-"+MngmUtil.today()+"_"+i+".xls");
				}else{
					file = new File(pr.getProperty("resources-path") + folder + pr.getProperty("fileName_"+BatchMain.PIANO_RICHIESTE_CORSI)+"-"+MngmUtil.today()+"_"+i+".csv");
				}
				
				String startPos = String.valueOf(j);
				String endPos = String.valueOf(j+7500);
				log.debug("***   Round: " + i +"/"+enrSize+", Start: "+ startPos +" End: "+ endPos );
				
				List<Enrollment> enrolments  = BatchService.getEnrollmentsForConsultaionWithManager(startPos, endPos);
	
				HashSet<String> usersToCheck = new HashSet<String>();
				for(Enrollment enr : enrolments)
				{
					//log.debug("enr.getNUM_ID() = " + enr.getNUM_ID());
					usersToCheck.add(enr.getNUM_ID().toUpperCase());
				}
	
				Map<String, LDAPUser> usersToCheckInLDAP = LdapDAO.getUsersData(usersToCheck);
				
				usersToCheckInLDAPSize.add(usersToCheckInLDAP.size());
				
				if(StringUtils.equalsIgnoreCase(outputType, "xls")){
					CreateExcel.createPianoRichiesteCorsiExcel(file, enrolments, usersToCheckInLDAP, dateOutputFormat);
				}else{
					CreateCSV.createPianoRichiesteCorsiCSV(file, enrolments, usersToCheckInLDAP, dateOutputFormat);
				}
				j=j+7500;
				
				String hostName = pr.getProperty("ftpHostnameEL");
				String userId = pr.getProperty("ftpUseridEL");
				String pw = pr.getProperty("ftpPasswordEL");
				String numMilliSeconds = pr.getProperty("ftpMilliWaitEL");
				String ftpFolder = pr.getProperty("ftpFolderEL");
	
				if(pw!=null)pw = EncryptGoodbuy.dencrypt(pw);
			
				if("true".equalsIgnoreCase(pr.getProperty("sendFileViaFtp")))
				{
					FtpClient ftpc = new FtpClient(hostName,userId,pw,numMilliSeconds,ftpFolder+folder);
					log.debug("Sending file no.: " + i);
					ftpc.sendFile(file);
				}
			}
			
			sendOKMail("PianoRichiesteCorsiBatch",enrollmentsSize,usersToCheckInLDAPSize,enrSize);
			
		}
		catch (IllegalStateException ie) 
		{
			log.error(ie.getMessage(),ie);
			throw ie;
		}
		catch (NumberFormatException ne)
		{
			log.error("Password isn't right coded.", ne);
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(), e);
			throw e;
		}		
	}
	
	static private void sendOKMail (String batchName, int enrollmentsSize, Set<Integer> usersToCheckInLDAPSize, int enrSize) {
 		EmailSender emailSender = new EmailSender();
		emailSender.setHostName(pr.getProperty("mail.host"));
		emailSender.setSubject("Batch: "+batchName+" OK");
		emailSender.setFromAddress(batchName+"@batchFrame.pl");
		Set<String> toAddresses = new HashSet<String>();
		
		if(pr.getProperty("mailReceiver1")!=null)
			toAddresses.add(pr.getProperty("mailReceiver1"));
		if(pr.getProperty("mailReceiver2")!=null)
			toAddresses.add(pr.getProperty("mailReceiver2"));
		if(pr.getProperty("mailReceiver3")!=null)
			toAddresses.add(pr.getProperty("mailReceiver3"));
		
		
		emailSender.setToAddresses(toAddresses);
		String msg = 
		 "<H2>Batch: "+batchName+" finished with no error</H2>"
		+"<p><b>Enrollments Size: </b>"+enrollmentsSize+"</p>"
		+"<p><b>Users from LDAP: </b>"+usersToCheckInLDAPSize.toString()+"</p>"
		+"<p><b>Sent "+enrSize+" files on Ftp";
			
		emailSender.setMessage(msg);
		
		try{
			emailSender.sendEmail();
		}catch (EmailException em){
			log.error("Can't send the email",em);	
		}
 		
 	}
}