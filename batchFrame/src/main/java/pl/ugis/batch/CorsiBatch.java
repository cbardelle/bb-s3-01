/*
 * Created on Mar 2, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch;

import it.webegg.util.EncryptGoodbuy;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.core.Command;
import pl.ugis.batch.dao.BatchService;
import pl.ugis.batch.utility.CreateExcel;
import pl.ugis.batch.utility.FtpClient;
import pl.ugis.batch.utility.MngmUtil;
import pl.ugis.batch.utility.PropertyFileReader;

/**
 * Weekly report with all enrollments sent to Metamorfosi from previous week � Corsi.xlc
 */
public class CorsiBatch implements Command {

	static private PropertyFileReader pr = PropertyFileReader.getInstance();
	static Logger log = Logger.getLogger(CorsiBatch.class);
	
	
	/* (non-Javadoc)
	 * @see pl.ugis.batch.utility.Command#execute()
	 */
	
	public void execute() throws Exception, IllegalStateException {
		
		try{
		
		List<Enrollment> enrolments = BatchService.getEnrollmentsSentToMetamrfosiFromLastWeek();
		
		String folder = pr.getProperty("folder_"+BatchMain.CORSI);
		
			
		File file = new File(pr.getProperty("resources-path")+folder+
				pr.getProperty("fileName_"+BatchMain.CORSI)+"-"+MngmUtil.today()+".xls");
		
		CreateExcel.createCorsiExcel(file,enrolments);
		
		String hostName = pr.getProperty("ftpHostnameEL");
		String userId = pr.getProperty("ftpUseridEL");
		String pw = pr.getProperty("ftpPasswordEL");
		
		if(pw!=null)
			pw = EncryptGoodbuy.dencrypt(pw);
		
		String numMilliSeconds = pr.getProperty("ftpMilliWaitEL");
		String ftpFolder = pr.getProperty("ftpFolderEL");
		
		if("true".equalsIgnoreCase(pr.getProperty("sendFileViaFtp"))){
			FtpClient ftpc = new FtpClient(hostName,userId,pw,numMilliSeconds,ftpFolder+folder);
			ftpc.sendFile(file);
		}
		}catch (IllegalStateException ie) {
			log.error(ie.getMessage(),ie);
			throw ie;
		}
		catch (NumberFormatException ne){
			log.error("Password isn't right coded.",ne);
		}
		catch (Exception e) {
			log.error(e.getMessage(),e);
			throw e;
		}
		
	}



	
}
