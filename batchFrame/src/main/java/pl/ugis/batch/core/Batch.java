
package pl.ugis.batch.core;

import org.apache.log4j.Logger;

import pl.ugis.batch.utility.Stopper;


/**
 * @author UV00022
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */


public class Batch {
	
	static public Logger log = Logger.getLogger(Batch.class);
	static public boolean executed = false;
	
	public static void execute(String id, Command command) throws Exception {
		
		//log.setResourceBundle(ResourceBundle.getBundle("fileCorsi"));
		//Category.getInstance("STDOUT").
		//log = Logger.getLogger(id);
		log.info("");log.info("");
		log.info("------------------- BATCH '"+id+"' STARTED------------------------");
		executed = true;
		
		Stopper stopper = new Stopper();
		stopper.start(id);
		command.execute();
		
		stopper.stop(true);
		log.info(" -------------------BATCH  '"+id+"' HAS BEEN FINISHED WITH SUCCESS---------");
		
	
	}

}
