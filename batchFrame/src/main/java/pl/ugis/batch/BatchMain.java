/*
 * Created on Aug 7, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

package pl.ugis.batch;


import java.util.HashSet;
import java.util.Set;

import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

import pl.ugis.batch.core.Batch;
import pl.ugis.batch.core.Command;
import pl.ugis.batch.utility.EmailSender;
import pl.ugis.batch.utility.PropertyFileReader;


public class BatchMain  {
	
public static final String FORNITORI_PREVISIONALE = "FornitoriPrevisionale";
public static final String CORSI= "Corsi";
public static final String FORNITORI_CONSUNTIVO = "FornitoriConsuntivo";
public static final String PIANO_RICHIESTE_CORSI = "PianoRichiesteCorsi";
public static final String STEP_CSV = "StepCSV";
public static final String FIX_TRABSP002 = "FixTransp002";

static boolean error = false;	
static boolean executed = false;
static Logger log = Logger.getLogger(BatchMain.class);
static private PropertyFileReader pr = PropertyFileReader.getInstance();	
		
public static void main(String[] args) {
	
	String batchName = "";
	
	try {
		
	
		for(String i:args ){
			
			batchName = i;
			
			try{
				Batch.execute(i, (Command)Class.forName(i).newInstance());
			
			}catch(ClassNotFoundException ce ){
				error = true;
				throw new IllegalArgumentException("Wrong argument. arg='"+i+"' It Should e.g. 'pl.ugis.batch.CorsiBatch' ");
			}
			
		
		}
		
		
		} catch (Exception e) {
			error = true;
			log.error("error "+e.getMessage(),e);
			sendAlertMail(batchName,e);
	
		} finally{
			if(error){
				log.info(" -------------------BATCH FRAME HAS BEEN FINISHED WITH ERROR-------");
			}
			
			System.exit(error ? 1 : 0);
			
		}
		
		
		
		
	}

 	static private void sendAlertMail (String batchName, Exception e) {
 		EmailSender emailSender = new EmailSender();
		emailSender.setHostName(pr.getProperty("mail.host"));
		emailSender.setSubject("Error in "+batchName);
		emailSender.setFromAddress(batchName+"@batchFrame.pl");
		Set<String> toAddresses = new HashSet<String>();
		
		if(pr.getProperty("mailReceiver1")!=null)
			toAddresses.add(pr.getProperty("mailReceiver1"));
		if(pr.getProperty("mailReceiver2")!=null)
			toAddresses.add(pr.getProperty("mailReceiver2"));
		if(pr.getProperty("mailReceiver3")!=null)
			toAddresses.add(pr.getProperty("mailReceiver3"));
		
		
		emailSender.setToAddresses(toAddresses);
		String msg = 
		 "<H1>Error occures in batch: "+batchName+"</H1>"
		+"<p><b>Please see the log:</b> C:/Batch/BatchFrame/log/batch.log</p>"
		+"<p><B>Error message:</B> "+e.getClass().getName()+" '"+e.getMessage()+"'</p>";
		
		StackTraceElement[] st =  e.getStackTrace();
		for (int i = 0;i<st.length;i++){
			msg+="<p><B>at:</B>"+st[i].getClassName() +"."+ st[i].getMethodName()+" <b>("+st[i].getClassName().substring(st[i].getClassName().lastIndexOf(".")+1)+".java:"+st[i].getLineNumber()+")</b></p>";
		}
		System.out.println("msg::"+msg);
		emailSender.setMessage(msg);
		
		try{
			emailSender.sendEmail();
		}catch (EmailException em){
			log.error("Can't send the email",em);	
		}
 		
 	}
 	
	

}
