/*
 * Created on May 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch.utility;

import java.io.StringWriter;

import org.apache.xml.serialize.*;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Element;

public class XMLUtils {
	
	@SuppressWarnings("deprecation")
	public static String serializeDOM(Element el) throws java.io.IOException {
		OutputFormat format = new OutputFormat();
		format.setIndenting(true);
		format.setPreserveSpace(true);
		format.setOmitXMLDeclaration(true);
		StringWriter stringOut = new StringWriter(); //Writer will be a String
		XMLSerializer serial = new XMLSerializer(stringOut, format);
		serial.asDOMSerializer(); // As a DOM Serializer

		serial.serialize(el);
		return stringOut.toString(); //Spit out DOM as a String
	}
	
	
	public static String serializeDOM(org.jdom.Element el){
		XMLOutputter xout = new XMLOutputter();
		return xout.outputString(el);
	}
	
}
