package pl.ugis.batch.utility;

public class LDAPDepartment 
{
	private String usdepartment;
	private String usdepartmentcode;
	private String usparentdepartmentcode;
	private String usmanager;
	private String usdepartmenttype;
	private String usdepartmenttypecode;
	
	public String getUsdepartment() {
		return usdepartment;
	}
	public void setUsdepartment(String usdepartment) {
		this.usdepartment = usdepartment;
	}
	public String getUsdepartmentcode() {
		return usdepartmentcode;
	}
	public void setUsdepartmentcode(String usdepartmentcode) {
		this.usdepartmentcode = usdepartmentcode;
	}
	public String getUsparentdepartmentcode() {
		return usparentdepartmentcode;
	}
	public void setUsparentdepartmentcode(String usparentdepartmentcode) {
		this.usparentdepartmentcode = usparentdepartmentcode;
	}
	public String getUsmanager() {
		return usmanager;
	}
	public void setUsmanager(String usmanager) {
		this.usmanager = usmanager;
	}
	public String getUsdepartmenttype() {
		return usdepartmenttype;
	}
	public void setUsdepartmenttype(String usdepartmenttype) {
		this.usdepartmenttype = usdepartmenttype;
	}
	public String getUsdepartmenttypecode() {
		return usdepartmenttypecode;
	}
	public void setUsdepartmenttypecode(String usdepartmenttypecode) {
		this.usdepartmenttypecode = usdepartmenttypecode;
	}
}
