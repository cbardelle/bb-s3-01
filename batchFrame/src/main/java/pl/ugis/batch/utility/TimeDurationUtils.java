/*
 * Created on Feb 26, 2009
 *
 */
package pl.ugis.batch.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

/**
 * Utility methods for handling duration strings.
 * 
 * @author UV00062
 *
 */
public class TimeDurationUtils {
	
	/**
	 * Utility class shouldn't be instantioned.
	 * 
	 */
	private TimeDurationUtils(){
		
	}
	
	
	/**
	 * Generate string with duration time in format:
	 * HH:mm
	 * 
	 * Hours number can be greater than 24, so if duration 
	 * equals 451800000 it will generate: "125:30"
	 * 
	 * If duration is lower than 0 it returns empty string.
	 * 
	 * @param duration
	 * @return
	 */
	public static String formatDuration(long duration) {
		String result = "";

		if (duration >= 0) {
			long hours = duration / (3600000); //milis_in_hour
			long minutes = (duration - hours * 3600000) / (1000 * 60); // milis_in_hour ... milis_in_minute
			result = ((hours < 10) ? "0" + hours : ""+hours) + ":"	+ ((minutes < 10) ? "0" + minutes : ""+minutes);
		}

		return result;
	}
	
	
	/**
	 * Parses string containing duration time and returns its value in miliseconds.
	 * Examples of input strings:
	 * <code>
	 * 08:12
	 * 009:32:001
	 * </code>
	 * It parses correctly such input strings as:
	 * <code>
	 * 09:155:300
	 * 0001234:5678
	 * </code>
	 * If input string is null, empty or not "well-formed", it returns -1.
	 * 
	 * 
	 * @param time input string
	 * @return duration time in miliseconds or -1 if input string has inproper value.
	 */
	public static long parseTimeString(String time){
		long result = -1;

		if (StringUtils.isEmpty(time)){
			return result;
		}
	
		long hoursNum = 0;
		long minutesNum = 0;
		long secondsNum = 0;
	
		//parse hour part
		int firstColon = time.indexOf(":");
		if (firstColon > 0) {
			hoursNum = NumberUtils.toInt( time.substring(0, firstColon), -1 );
			if(hoursNum < 0){
				//hours part doesn't have proper value
				return result;
			}
		} else {
			//cant find any colon inside the input string
			return result;
		}
	
		//parse minutes and seconds
		int secondColon = time.indexOf(":", firstColon+1);
		if(secondColon > 0) {
			minutesNum = NumberUtils.toLong( time.substring(firstColon+1, secondColon), -1 );
			secondsNum = NumberUtils.toLong( time.substring(secondColon+1), -1 );
		} else {
			minutesNum = NumberUtils.toLong( time.substring(firstColon+1), -1 ); 
		}
	
		if((minutesNum < 0) || (secondsNum < 0)){
			//minutes or seconds part doesn't have proper value
			return result;
		}
	
		//result in miliseconds
		result = (hoursNum * 3600L + minutesNum * 60L + secondsNum) * 1000L;

		return result;
	}
	
	/**
	 * Parses string containing duration time and returns its value in days.
	 * One day it is 7:30.
	 * Examples of input strings:
	 * <code>
	 * 04:00
	 * 07:30
	 * 15:00
	 * </code>
	 * It parses correctly such input strings as:
	 * <code>
	 * 0.5333333333333333
	 * 1
	 * 2.0
	 * </code>
	 * If input string is null, empty or not "well-formed", it returns -1.
	 * 
	 * 
	 * @param time input string
	 * @return duration time in miliseconds or -1 if input string has inproper value.
	 */
	public static double daysFromDuration(String duration){
		
		double oneDayInMiliSec = TimeDurationUtils.parseTimeString("7:30");
		double days = 0; 
		
		long durationinMiliSec = parseTimeString(duration);
		if(durationinMiliSec==-1)
			return -1;
			
		
		days = durationinMiliSec/oneDayInMiliSec;
		
		return days;
	}
	
	
	/**
	 * Generates string with suggested end hour of the course for
	 * courses no longer than 7.5h.
	 * It makes correction for courses longer than  4h.
	 * 
	 * @param duration duration of course
	 * @return string with suggested end hour
	 */
	public static String getSuggestedEndHour(long duration){
		String result = "17:00";
	
		if ( duration < (27000000)) { //miliseconds in 7.5h
		
			//lunch time correction
			if (duration > 14400000){ //miliseconds in 4h
				duration += 3600000; //miliseconds in 1h
			}
		
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 8);
			c.set(Calendar.MINUTE, 30);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			c.setTimeInMillis(c.getTimeInMillis() + duration);
		
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			result = sdf.format(c.getTime());
		}
	
		return result;
	}

}
