package pl.ugis.batch.utility;

import java.io.File;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;
import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.dao.LdapDAO;

public class CreateCSV {
	
	static Logger log = Logger.getLogger(CreateCSV.class);
	static NumberFormat nf = NumberFormat.getInstance();
	static String dFormat;
	static String toDay;
	
	 static public void createPianoRichiesteCorsiCSV(File file, List<Enrollment> enrollments, Map<String, LDAPUser> userToCorrectManagers, String dateOutputFormat) throws Exception,IllegalStateException{
			
		 	if(file == null || file.getName().startsWith("null"))
		 		throw new IllegalStateException("Wrong file name. See configAccessData.properties (fileName_*)");
		 	
		 	dFormat = dateOutputFormat;
		 	nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			toDay = parseDate(new Date());
			LDAPUser ldapUser;
			Map<String, LDAPDepartment> departmentCodesToDepartments=null;
			
			if(userToCorrectManagers.size()>0){
				departmentCodesToDepartments = LdapDAO.getDepartmentCodesToDepartments();
			}
			
		 	file.createNewFile();

		 	// Use FileWriter constructor that specifies open for appending
	        CSVWriter csvOutput = new CSVWriter(new FileWriter(file, true), ',');

		    String [] header = {
		    		"Request date", "Country selection", "Priority", "Didactics", "Method", "Subject Area 0 level",
		    		"Subject Area 1 level", "Subject Area 2 level", "Employee Id number", "Surname", "Name", "Training starting date",
		    		"Item ID", "Description", "Vendor", "Vendor training code", "Training source", "Duration - days",
		    		"Cost", "Direct manager ID number", "Direct manager Surname", "Direct manager Name", "Workplace", "Country",
		    		"UOG Code", "UOG Description", "Company", "Employee e-mail address", "Direct manager e-mail address", "Request Id-number",
		    		"Requester ID number", "Requester Surname", "Requester Name", "Category", "Group", "Skill",
		    		"Status", "Notes", "Approval/Rejection Date", "File date", 
		    		};
		    
		    csvOutput.writeNext(header);
		    
		    //Write data
		    String [] data;
		    for (Enrollment enrl : enrollments) {
		    	
		    	if(userToCorrectManagers.containsKey(enrl.getNUM_ID()))
				{
					ldapUser = userToCorrectManagers.get(enrl.getNUM_ID());

				//check for user's organizations, department, names
					ldapUser = LdapDAO.getUserFromLDAP(ldapUser.getUid());
					if(ldapUser.getUid() != null){
						enrl.setCompany(StringUtils.upperCase(ldapUser.getUsorganizationcode()));
						enrl.setDomainCode(StringUtils.upperCase(ldapUser.getUsdepartmentcode()));
						enrl.setDomainName(StringUtils.upperCase(ldapUser.getUsdepartment()));
						enrl.setLAST_NAME(StringUtils.upperCase(ldapUser.getSn()));
						enrl.setFIRST_NAME(StringUtils.upperCase(ldapUser.getGivenName()));
						enrl.setCountry(StringUtils.upperCase(ldapUser.getUscountry()));
					}

				//check for user's manager
					ldapUser = LdapDAO.getUserFromLDAP(ldapUser.getUsmanager());
					if(ldapUser.getUid() != null){
						enrl.setMANAGER_ID(ldapUser.getUid().toUpperCase());
						enrl.setMANAGER_FIRSTNAME(StringUtils.capitalize(StringUtils.lowerCase(ldapUser.getGivenName())));
						enrl.setMANAGER_LASTNAME(StringUtils.capitalize(StringUtils.lowerCase(ldapUser.getSn())));
						enrl.setMANAGER_EMAIL(StringUtils.lowerCase(ldapUser.getMail()));
					}
					
				//check for others departments manager
					ldapUser = LdapDAO.getUserDataFromLDAP(enrl.getNUM_ID(), departmentCodesToDepartments);
					enrl.setUSER_EMAIL(ldapUser.getMail());
					enrl.setFILIALE(ldapUser.getUsagency());
					enrl.setDomainManager(ldapUser.getUsdepartmenttypemngr());
					enrl.setCompetenceAreaCode(StringUtils.upperCase(ldapUser.getCompAreacode()));
					enrl.setCompetanceArea(StringUtils.upperCase(ldapUser.getCompArea()));
					enrl.setCompetanceAreaResp(StringUtils.upperCase(ldapUser.getCompAreaMngr()));
					enrl.setUnitCode(StringUtils.upperCase(ldapUser.getUnitCode()));
					enrl.setUnit(StringUtils.upperCase(ldapUser.getUnit()));
					enrl.setUnitResp(StringUtils.upperCase(ldapUser.getUnitMngr()));
					enrl.setCompetenceCenterCode(StringUtils.upperCase(ldapUser.getCompCenterCode()));
					enrl.setCompetenceCenter(StringUtils.upperCase(ldapUser.getCompCenter()));
					enrl.setCompetenceCenterResp(StringUtils.upperCase(ldapUser.getCompCenterMngr()));
				}
				
				//check for requester's data
				if(enrl.getRequester()!=null)
				{
					ldapUser = LdapDAO.getUserFromLDAP(enrl.getRequester().toUpperCase());
						if(ldapUser.getUid() != null){
						enrl.setRequesterSName(StringUtils.upperCase(ldapUser.getSn()));
						enrl.setRequesterName(StringUtils.upperCase(ldapUser.getGivenName()));
					}
						
				}
				
		    	data = new String [] { parseDate(enrl.getREQUEST_DATE()), enrl.getCustomField().getCountry(), enrl.getRequestPriority(), enrl.getDIDATTICA(), enrl.getTYPE(),enrl.getCustomField().getSubjArea1(),
		    		  				enrl.getCustomField().getSubjArea2(), enrl.getCustomField().getSubjArea3(), enrl.getNUM_ID(), enrl.getLAST_NAME(), enrl.getFIRST_NAME(), parseDate(enrl.getSTART_DATE()),
		    		  				enrl.getCATALOG_CODE(), enrl.getCOURSE_DESCRIPTION(), enrl.getCustomField().getSupplier(), enrl.getCustomField().getVendorTrainingCode(), enrl.getCATALOGENTRY_OID(), String.valueOf(enrl.getDAYS()==-1?"":Double.parseDouble(nf.format(enrl.getDAYS()).replace(',','.'))),
		    		  				String.valueOf(enrl.getCustomField().getCost()!=null?TextUtils.parseEnglishDouble(enrl.getCustomField().getCost(),0.00):""), enrl.getMANAGER_ID(), enrl.getMANAGER_LASTNAME(), enrl.getMANAGER_FIRSTNAME(), enrl.getFILIALE(), enrl.getCountry(),
		    		  				enrl.getDomainCode(), enrl.getDomainName(), enrl.getCompany(), enrl.getUSER_EMAIL(), enrl.getMANAGER_EMAIL(), enrl.getENROLLMENT_OID(),
		    		  				enrl.getRequester()==null?"":enrl.getRequester().toUpperCase(), enrl.getRequesterSName()==null?"":enrl.getRequesterSName().toUpperCase(), enrl.getRequesterName()==null?"":enrl.getRequesterName().toUpperCase(), enrl.getCATEGORY(), enrl.getGROUP(), enrl.getSKILL(),
		    		  				enrl.getSTATUS(), enrl.getNote(), parseDate(enrl.getApprRejDate()), String.valueOf(toDay) };

		      csvOutput.writeNext(data);
		    }
		    
		    csvOutput.close();

		    log.debug("Generated CSV : "+new Date());
	 }	 	

	 static String parseDate(Date date){
		 if(date==null){
			 return "";
		 }else{
			 return new SimpleDateFormat(dFormat.equalsIgnoreCase("dd/MM/yyyy")?"dd/MM/yyyy":dFormat).format(date);
		 }
	 }
}
