/*
 * Created on Mar 2, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch.utility;

import org.apache.log4j.Logger;

/**
 * @author UV00022
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */


public class Stopper {
	static Logger log = Logger.getLogger(Stopper.class);
	
	private long start;
	private String action;
	
	
	public void start(String id) {
		start(id, 1);
	}
	
	public void start(String action, int repeats) {
		start = System.currentTimeMillis();
		this.action = action;
	}
	
	public void start() {
		start = System.currentTimeMillis();
	}
	
	public long stop() {
		return System.currentTimeMillis() - start;
	}
	
	public long stop(boolean syso) {
		long duration = System.currentTimeMillis() - start;
		if (syso) {
			stop(duration);
		}
		return duration;
	}
	
	public void stop(long duration) {
		log.info("Execution time of " + action + ":\t" + duration + 
				"ms = "+duration*0.001+"s");
		
	}
}

