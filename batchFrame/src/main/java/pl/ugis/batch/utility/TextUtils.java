/*
 * Created on Mar 26, 2009
 *
 */
package pl.ugis.batch.utility;

import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

/**
 * @author UV00062
 *
 */
public class TextUtils {

	
	private TextUtils(){
		
	}
	
	
	public static String padZeroLeft(String value, int newLength) {
		if (value.length() != newLength) {
			String zeros = "";
			for (int i = 0; i < (newLength - value.length()); i++) {
				zeros += (String) "0";
			}
			value = (String) zeros + (String) value;
		}
		return value;
	}
	
	
	/**
	 * Replace some utf characters with they counterparts
	 * such as:
	 * <li> ' </li> 
	 * <li> " </li> 
	 * <li> [space] </li> 
	 * <li> - </li>
	 * 
	 * or removes some unallowed chars.
	 * 
	 *  
	 * @param s
	 * @return
	 */
	public static String replaceUncommonChars(String s){
		
		//replace with '
		s = StringUtils.replaceChars(s, (char)   18, '\'');
		s = StringUtils.replaceChars(s, (char)   39, '\'');
		s = StringUtils.replaceChars(s, (char)   96, '\'');
		s = StringUtils.replaceChars(s, (char)  180, '\'');
		s = StringUtils.replaceChars(s, (char)  768, '\'');
		s = StringUtils.replaceChars(s, (char)  769, '\'');
		s = StringUtils.replaceChars(s, (char)  900, '\'');
		s = StringUtils.replaceChars(s, (char) 8216, '\'');
		s = StringUtils.replaceChars(s, (char) 8217, '\'');
		s = StringUtils.replaceChars(s, (char) 8219, '\'');
		s = StringUtils.replaceChars(s, (char) 8242, '\'');
		s = StringUtils.replaceChars(s, (char)61455, '\'');
		s = StringUtils.replaceChars(s, (char)61449, '\'');
		
		//replace with "
		s = StringUtils.replaceChars(s, (char)  34, '"');
		s = StringUtils.replaceChars(s, (char) 733, '"');
		s = StringUtils.replaceChars(s, (char) 901, '"');
		s = StringUtils.replaceChars(s, (char)8220, '"');
		s = StringUtils.replaceChars(s, (char)8221, '"');
		s = StringUtils.replaceChars(s, (char)8243, '"');
		
		//replace with [space]
		s = StringUtils.replaceChars(s, '\t',       ' ');
		s = StringUtils.replaceChars(s, (char)8194, ' ');
		s = StringUtils.replaceChars(s, (char)8195, ' ');
		
		//replace with -
		s = StringUtils.replaceChars(s, (char)  22,  '-');
		s = StringUtils.replaceChars(s, (char) 172,  '-');
		s = StringUtils.replaceChars(s, (char) 173,  '-');
		s = StringUtils.replaceChars(s, (char)8211, '-');
		s = StringUtils.replaceChars(s, (char)8212, '-');
		s = StringUtils.replaceChars(s, (char)8213, '-');
		s = StringUtils.replaceChars(s, (char)8722, '-');
		s = StringUtils.replaceChars(s, (char)8254, '-');
		
		//remove
		s = StringUtils.remove(s, (char) 169);
		s = StringUtils.remove(s, (char) 167);
		s = StringUtils.remove(s, (char) 174);
		s = StringUtils.remove(s, (char) 182);
		s = StringUtils.remove(s, (char)8230);
		s = StringUtils.remove(s, (char)8482);
		
		return s;
	}


	/**
	 * Searches given array for matching string to stringToFind ( case 
	 * sensitive). If array contains such string it returns true. In other case
	 * or if stringToFind or array is null - returns false.
	 * 
	 * @param stringToFind
	 * @param templates
	 * @return
	 */
	public static boolean isOneOf(String stringToFind, String[] templates){
		if ((templates == null) || (templates.length == 0)){
			return false;
		}
	
		for (int i = 0; i < templates.length; i++) {
			if (StringUtils.equals(stringToFind, templates[i])){
				return true;
			}
		}
	
		return false;
	}
	
	
	/**
	 * Searches given array for matching string to stringToFind (ingores 
	 * case). If array contains such string it returns true. In other case
	 * or if stringToFind or array is null - returns false.
	 * 
	 * @param stringToFind
	 * @param templates
	 * @return
	 */
	public static boolean isOneOfIgnoreCase(String stringToFind, String[] templates){
		if ((templates == null) || (templates.length == 0)){
			return false;
		}

		for (int i = 0; i < templates.length; i++) {
			if (StringUtils.equalsIgnoreCase(stringToFind, templates[i])){
				return true;
			}
		}

		return false;
	}
	
	
	/**
	 * Parses string for presence of float value within, using locale for Italy.
	 * If given string doesn't contain proper value, it returns value 
	 * given in defaultValue parameter.
	 * 
	 * Examples of results:
	 * 
	 * <li> "2.8" : 28.0 </li>
	 * <li> "2.8" : 28.0 </li>
	 * <li> "2.8,5" : 28.5 </li>
	 * <li> "2.800,5" : 2800.5 </li>
	 * <li> "2.8.8" : 288.0 </li>
	 * <li> "2,8,8" : 2.8 </li>
	 * <li> "abc2,8" : {defaultValue} </li>
	 * <li> "a2,8bc" : {defaultValue} </li>
	 * <li> "2,8abc" : 2.8 </li>
	 * 
	 * @param s
	 * @param defaultValue
	 * @return
	 */
	public static float parseItalianFloat(String s, float defaultValue){
		float result = defaultValue; 
		
		
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		try {
			result =  nf.parse(s).floatValue();
		} catch (Exception e) {
			// nothing to do here
		}		
	
		return result;
	}
	
		
	
	public static double parseItalianDouble(String s, double defaultValue){
		double result = defaultValue; 
	
	
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		try {
			result =  nf.parse(s).doubleValue();
		} catch (Exception e) {
			// nothing to do here
		}		

		return result;
	}
	
	public static double parseItalianDouble(String s, double defaultValue,int minFractionalDigits){
		double result = defaultValue; 
	
	
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(minFractionalDigits);
		try {
			result =  nf.parse(s).doubleValue();
		} catch (Exception e) {
			// nothing to do here
		}		

		return result;
	}
	
	/**
		 * Parses string for presence of float value within, using locale for Italy.
		 * If given string doesn't contain proper value, it returns value 
		 * given in defaultValue parameter.
		 * 
		 * Examples of results:
		 * 
		 * <li> "2.8" : 2.8 </li>
		 * <li> "2800.80" : 2800.8 </li>
		 * @param s
		 * @param defaultValue
		 * @return
		 */
	
	public static double parseEnglishDouble(String s, double defaultValue){
		double result = defaultValue; 
		
		
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		try {
			result =  nf.parse(s).doubleValue();
		} catch (Exception e) {
			// nothing to do here
		}		
	
		return result;
	}
	
	public static double parseEnglishDouble(String s, double defaultValue,int minFractionalDigits){
		double result = defaultValue; 
		
		
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(minFractionalDigits);
		
		try {
			result =  nf.parse(s).doubleValue();
		} catch (Exception e) {
			// nothing to do here
		}		
	
		return result;
	}
	
	
	
	/**
	 * Parses string for presence of String Italian value into Englisg (Oracle) version using locale for English without ','.
	 * If given string doesn't contain proper value, it returns value 
	 * given in defaultValue parameter.
	 * 
	 * Examples of results:
	 * 
	 * <li> "2,8"	  :	"2.80"<li>
	 * <li> "445"	  :	"445"<li>
	 * <li> "445,3"	  :	"445.30"<li>
	 * <li> "4045,35" :	"4045.35"<li>
	 * <li> "4.045,35": "4045.35"<li>
	 * <li> "abc2,8"  : "{defaultValue}" </li>
	 * <li> "a2,8bc"  : "{defaultValue}" </li>
	 * <li> "2,8abc"  : "2.8" </li>
	 * 
	 * @param s
	 * @param defaultValue
	 * @return
	 */
	public static String parseItalianCurrency(String s, String defaultValue){
			String result = defaultValue; 
			
			
			NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
			try {
				
								
				float res =  nf.parse(s).floatValue();
				//if(NumberUtils.toInt(String.valueOf(s),-1)==-1){
				result = formatEnglishDobuble(res,2);
				//}else{
				//	result = formatEnglishDouble(res);
				//}
				
				
				
			} catch (Exception e) {
				// nothing to do here
			}		
	
			return result;
	}
	
	
	/**
	 * Parses string for presence of String English(Oracle) value into Italian version using locale for Italy.
	 * If given string doesn't contain proper value, it returns value 
	 * given in defaultValue parameter.
	 * 
	 * Examples of results:
	 * 
	 * <li> "28"		:	"28" </li>
	 * <li> "28.00"		:	"28,00" </li>
	 * <li> "2800"		:	"2.800" </li>
	 * <li> "445.30"	:	"445,30" </li>
	 * <li> "4045.35"	:	"4.045,35" </li>
	 * <li> "4,045.35"	:	"4.045,35" </li>
	 * <li> "abc2.8" 	: 	"{defaultValue}" </li>
	 * <li> "a2.8bc" 	: 	"{defaultValue}" </li>
	 * <li> "2.8abc" 	: 	"2,8" </li>
	 * 
	 * @param s
	 * @param defaultValue
	 * @return
	 */
	public static String parseEnglishCurrency(String s, String defaultValue){
		String result = defaultValue; 
		
		
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		try {
			
			float res =  nf.parse(s).floatValue();
			
			//if(NumberUtils.toInt(String.valueOf(s),-1)==-1){
			result = formatItalianDouble(res,2);
			//}else{
			//	result = formatItalianDouble(res);
			//}
			
			
		} catch (Exception e) {
			// nothing to do here
		}		
	
		return result;
	}
	
	//old
	public static String formatItalianFloat(float value){
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		nf.setMaximumFractionDigits(2);
		return nf.format(value);
	}
	
	public static String formatItalianDouble(double value){
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		nf.setMaximumFractionDigits(2);
		return nf.format(value);
	}
	
	public static String formatItalianDouble(double value,int minFractionDigits){
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(minFractionDigits);
		return nf.format(value);
	}
	
	public static String formatEnglishDouble(double value){
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(2);
		return nf.format(value).replaceAll("\\,","");
	}
	
	public static String formatEnglishDobuble(double value, int minFractionDigits){
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(minFractionDigits);
		return nf.format(value).replaceAll("\\,","");
	}





	public static String[] digDepartmentsFromDistinguieshedName(String data) {
		
		String[] result = null;
		result = StringUtils.split(StringUtils.substringBetween(data, "ou=",",o="), ",ou=");
		
		return result!=null?result:new String[0];
	}
	
}	