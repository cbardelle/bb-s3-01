/*
 * Created on Aug 13, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.StringTokenizer;


/*********************************************************/
/*The files simulation-message and cofig are always read */
/*The general file has to be set before by setJndi method*/
/*and then has to be called the method getGeneralProperty*/
/*********************************************************/


public class PropertyFileReader  
   {
   /**************************************************************************/
   /*********************** attributes ***************************************/
   /**************************************************************************/
   private static Properties generalProps = new Properties();
   private static Properties messageProps = new Properties();
   private static Properties configProps = null;
   private static String simulationfile = "";
   private static String configFile = "properties/configAccessData.properties";
   
   
   private final static PropertyFileReader ourInstance = new PropertyFileReader();
   
   public static PropertyFileReader getInstance(){
   	
   	return PropertyFileReader.ourInstance;
   }
   
   
   
   /**************************************************************************/
   /*************************** constructor **********************************/
   /**************************************************************************/
   private PropertyFileReader() 
   
      {
      init();
      }
   /***************************** init ***************************************/     	
   private void init()
  
      {
      //reload if necessary the config
      if (configProps == null)
         reloadFileConf();
      
      //read the location if message.properties
     // simulationfile = configProps.getProperty("configAccessData.properties");


      //reload if necessary the simulationfile
      int numSimul = messageProps.size();
      if (numSimul == 0)
         reloadFile(simulationfile, messageProps);
      }

   /**************************************************************************/
   /*************************** getConfigProperty ****************************/
   /**************************************************************************/
   public String getProperty(String key)
      {
      return configProps.getProperty(key);
      }

   /**************************************************************************/
   /*************************** getConfigProperty ****************************/
   /**************************************************************************/
   public String getProperty(String key, boolean flagReload)
      {
      if (flagReload)
         reloadFileConf();
      return configProps.getProperty(key);
      }

   /**************************************************************************/
   /*************************** getMessageSimulation *************************/
   /**************************************************************************/
   //called without flag the reload is true by default
   public String getMessageSimulation(String key) 
      {
 	  return getMessageSimulation(key ,true);
      }

   /**************************************************************************/
   /*************************** getMessageSimulation *************************/
   /**************************************************************************/
   public String getMessageSimulation(String key, boolean flagReload)
      {
      if (flagReload)
         reloadFile(simulationfile, messageProps);
 	  return messageProps.getProperty(key);
      }

   /**************************************************************************/
   /*************************** getGeneralProperty ***************************/
   /**************************************************************************/
   //called without flag the reload is true by default
   public String getGeneralProperty(String key, String file)
      {
      return getGeneralProperty(key, file, true);
      }

   /**************************************************************************/
   /*************************** getGeneralProperty ***************************/
   /**************************************************************************/
   public String getGeneralProperty(String key, String file, boolean flagReload)
      {
      if (flagReload)
         reloadFile(file, generalProps);
 	  return generalProps.getProperty(key);
      }
	public void forceReload() 
	   {
		this.reloadFileConf();
	   }
   /***************************** reloadFileConf *********************************/
   private void reloadFileConf()
      {
      try 
         {
         String fileNameWithPath = searchFile(configFile);
         configProps = new Properties();
         configProps.load(new FileInputStream(fileNameWithPath));
         }
      catch (Exception e)
         {
         System.out.println(e.getMessage());
         }      
      }

   /***************************** reloadFileConf *********************************/
   private void reloadFile(String file, Properties props)
      {
      try 
         {
         //Debug.write("Load the file:" + file, "PropertyFileReader");
         props.load(new FileInputStream(file));
         }
      catch (IOException e)
         {
         System.out.println(e.getMessage());
         }      
      catch (Exception e)
         {
         System.out.println(e.getMessage());
         }      
      }


   /***************************** searchFile **************************************/
	/**
	 * Cerca il file con nome specificato nel classPath
	 * @param nome del file da cercare.
	 * @return path del file
	 * @throw FileNotFoundException se il file non viene trovato.
	 */
	private static String 
	searchFile(String fileName) 
	throws FileNotFoundException 
	   {
	   String classPath = System.getProperty("java.class.path");
	   String classPathSeparator = System.getProperty("path.separator");
	   
	   classPath += classPathSeparator +  "." + classPathSeparator + "properties";
	   
	   StringTokenizer tok = new StringTokenizer(classPath, 
	   classPathSeparator, false);
	   File file;
	   File path;
	   while (tok.hasMoreTokens()) 
	      {
		  path = new File(tok.nextToken());
		  if (path.isDirectory()) 
		     {
			 file = new File(path.getAbsolutePath() + 
			 System.getProperty("file.separator") + fileName);
			 if (file.isFile()) 
				return file.getAbsolutePath();
			 }
		  }
	   throw new FileNotFoundException("file: " + fileName + " non trovato");
	   }      
   /*-----------------------------------------------------------------*/
   /*------------------------------ main (UNIT TEST) -----------------*/
   /*-----------------------------------------------------------------*/



   public static void
   main(String[] args) throws Exception 
      {
      PropertyFileReader pr = PropertyFileReader.getInstance();
      System.out.println(pr.getProperty("configAccessData.properties"));
//      System.out.println(pr.getConfigProperty("a", true));
//      System.out.println(pr.getGeneralProperty("service", "D:/buffer/config.properties"));
      System.out.println(pr.getMessageSimulation("a"));
      System.out.println(pr.getMessageSimulation("a", true));
      }
   }
