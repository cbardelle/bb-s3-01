package pl.ugis.batch.utility;

import java.io.File;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jxl.SheetSettings;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.bo.Supplier;
import pl.ugis.batch.dao.LdapDAO;

public class CreateExcel {
	
	static Logger log = Logger.getLogger(CreateExcel.class);
	static SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
	static NumberFormat nf = NumberFormat.getInstance();
	
	 static public void createPianoRichiesteCorsiExcel(File file, List<Enrollment> enrollments, Map<String, LDAPUser> userToCorrectManagers, String dateOutputFormat) throws Exception,IllegalStateException{
			
		 	if(file == null || file.getName().startsWith("null"))
		 		throw new IllegalStateException("Wrong file name. See configAccessData.properties (fileName_*)");
			
		 	WorkbookSettings workbookSettings = new WorkbookSettings();
		 	workbookSettings.setEncoding("UTF8");
		 	
			WritableWorkbook workbook = Workbook.createWorkbook(file,workbookSettings);  
			
			WritableSheet sheet = workbook.createSheet("PianoRichiesteCorsi", 0);
	
			SheetSettings settings = sheet.getSettings();
			settings.setVerticalFreeze(1);
	
			//Setting styles for cells
			WritableCellFormat headerFormat = new WritableCellFormat(
				new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false));
				headerFormat.setBackground(Colour.GRAY_25); 
			WritableCellFormat normalFormat = new WritableCellFormat(
				new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false)); 
			normalFormat.setShrinkToFit(false);

			WritableCellFormat dateFormat = new WritableCellFormat(
								new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false),
								new DateFormat(dateOutputFormat));
			
			dateFormat.setShrinkToFit(false);	
			
			WritableCellFormat currencyFormat = new WritableCellFormat( 
					new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false), 
					new jxl.write.NumberFormat("0.00")
				);
			currencyFormat.setShrinkToFit(false);
				
			//Setting header cells
			int colNum = 0;	
			
			sheet.setColumnView(colNum,15);
			Label label = new Label(colNum++, 0, "Request date",headerFormat);
			sheet.addCell(label);

			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Country selection",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Priority",headerFormat);
			sheet.addCell(label);
						
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Didactics",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,8);
			label = new Label(colNum++,0,"Method",headerFormat);
			sheet.addCell(label);	
			
			sheet.setColumnView(colNum,8);
			label = new Label(colNum++,0,"Subject Area 0 level",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,8);
			label = new Label(colNum++,0,"Subject Area 1 level",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,8);
			label = new Label(colNum++,0,"Subject Area 2 level",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Employee Id number",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Surname",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Name",headerFormat);
			sheet.addCell(label);
						
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Training starting date",headerFormat);
			sheet.addCell(label);
						
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Item ID",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,35);
			label = new Label(colNum++,0,"Description",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Vendor",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Vendor training code",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Training source",headerFormat);
			sheet.addCell(label);

			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Duration - days",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Cost",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,25);
			label = new Label(colNum++,0,"Direct manager ID number",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,25);
			label = new Label(colNum++,0,"Direct manager Surname",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,25);
			label = new Label(colNum++,0,"Direct manager Name",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Workplace",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Country",headerFormat);
			sheet.addCell(label);

			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"UOG Code",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"UOG Description",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"Company",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,40);
			label = new Label(colNum++,0,"Employee e-mail address",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,40);
			label = new Label(colNum++,0,"Direct manager e-mail address",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Request Id-number",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Requester ID number",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Requester Surname",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Requester Name",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"Category",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"Group",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"Skill",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Status",headerFormat);
			sheet.addCell(label);

			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Notes",headerFormat);
			sheet.addCell(label);

			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Approval/Rejection Date",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"File date",headerFormat);
			sheet.addCell(label);

			log.info("File: "+file.getName());
			log.info("Excel enrollments:"+enrollments.size());
			
			Iterator<Enrollment> it = enrollments.iterator();	
			Enrollment enrollment = null;
			String toDay = MngmUtil.todayItalianFormat();
			Map<String, LDAPDepartment> departmentCodesToDepartments=null;
			
			if(userToCorrectManagers.size()>0){
				departmentCodesToDepartments = LdapDAO.getDepartmentCodesToDepartments();
			}
			
			int i=0;
			LDAPUser ldapUser;
			while(it.hasNext()) {
				enrollment = (Enrollment)it.next();

				if(userToCorrectManagers.containsKey(enrollment.getNUM_ID()))
				{
					ldapUser = userToCorrectManagers.get(enrollment.getNUM_ID());

				//check for user's organizations, department, names
					ldapUser = LdapDAO.getUserFromLDAP(ldapUser.getUid());
					if(ldapUser.getUid() != null){
						enrollment.setCompany(StringUtils.upperCase(ldapUser.getUsorganizationcode()));
						enrollment.setDomainCode(StringUtils.upperCase(ldapUser.getUsdepartmentcode()));
						enrollment.setDomainName(StringUtils.upperCase(ldapUser.getUsdepartment()));
						enrollment.setLAST_NAME(StringUtils.upperCase(ldapUser.getSn()));
						enrollment.setFIRST_NAME(StringUtils.upperCase(ldapUser.getGivenName()));
						enrollment.setCountry(StringUtils.upperCase(ldapUser.getUscountry()));
					}

				//check for user's manager
					ldapUser = LdapDAO.getUserFromLDAP(ldapUser.getUsmanager());
					if(ldapUser.getUid() != null){
						enrollment.setMANAGER_ID(ldapUser.getUid().toUpperCase());
						enrollment.setMANAGER_FIRSTNAME(StringUtils.capitalize(StringUtils.lowerCase(ldapUser.getGivenName())));
						enrollment.setMANAGER_LASTNAME(StringUtils.capitalize(StringUtils.lowerCase(ldapUser.getSn())));
						enrollment.setMANAGER_EMAIL(StringUtils.lowerCase(ldapUser.getMail()));
					}
					
				//check for others departments manager
					ldapUser = LdapDAO.getUserDataFromLDAP(enrollment.getNUM_ID(), departmentCodesToDepartments);
					enrollment.setUSER_EMAIL(ldapUser.getMail());
					enrollment.setFILIALE(ldapUser.getUsagency());
					enrollment.setDomainManager(ldapUser.getUsdepartmenttypemngr());
					enrollment.setCompetenceAreaCode(StringUtils.upperCase(ldapUser.getCompAreacode()));
					enrollment.setCompetanceArea(StringUtils.upperCase(ldapUser.getCompArea()));
					enrollment.setCompetanceAreaResp(StringUtils.upperCase(ldapUser.getCompAreaMngr()));
					enrollment.setUnitCode(StringUtils.upperCase(ldapUser.getUnitCode()));
					enrollment.setUnit(StringUtils.upperCase(ldapUser.getUnit()));
					enrollment.setUnitResp(StringUtils.upperCase(ldapUser.getUnitMngr()));
					enrollment.setCompetenceCenterCode(StringUtils.upperCase(ldapUser.getCompCenterCode()));
					enrollment.setCompetenceCenter(StringUtils.upperCase(ldapUser.getCompCenter()));
					enrollment.setCompetenceCenterResp(StringUtils.upperCase(ldapUser.getCompCenterMngr()));
				}
				
				//check for requester's data
				if(enrollment.getRequester()!=null)
				{
					ldapUser = LdapDAO.getUserFromLDAP(enrollment.getRequester().toUpperCase());
						if(ldapUser.getUid() != null){
						enrollment.setRequesterSName(StringUtils.upperCase(ldapUser.getSn()));
						enrollment.setRequesterName(StringUtils.upperCase(ldapUser.getGivenName()));
					}
						
				}

				colNum = 0;	
								
				//Request date
				DateTime dateTime;
				if(enrollment.getREQUEST_DATE()!=null){
					dateTime = new DateTime(colNum++, i+1,enrollment.getREQUEST_DATE(),dateFormat);
					sheet.addCell(dateTime);
				}else{
					label = new Label(colNum++, i+1, "" , normalFormat);
					sheet.addCell(label);
				}
		
				//Country selection
				label = new Label(colNum++, i+1,enrollment.getCustomField().getCountry()==null? "" : enrollment.getCustomField().getCountry(), normalFormat);
				sheet.addCell(label);
				
				//Priority
				label = new Label(colNum++, i+1,enrollment.getRequestPriority()==null? "" : enrollment.getRequestPriority(), normalFormat);
				sheet.addCell(label);
								
				//Didattica
				label = new Label(colNum++, i+1,enrollment.getDIDATTICA()==null? "" : enrollment.getDIDATTICA(), normalFormat);
				sheet.addCell(label);
				
				//Method
				label = new Label(colNum++, i+1,enrollment.getTYPE()==null? "" : enrollment.getTYPE(), normalFormat);
				sheet.addCell(label);	
				
				//Subject Area 0 level
				label = new Label(colNum++, i+1,enrollment.getCustomField().getSubjArea1()==null? "" : enrollment.getCustomField().getSubjArea1(), normalFormat);
				sheet.addCell(label);
				
				//Subject Area 1 level
				label = new Label(colNum++, i+1,enrollment.getCustomField().getSubjArea2()==null? "" : enrollment.getCustomField().getSubjArea2(), normalFormat);
				sheet.addCell(label);
				
				//Subject Area 2 level
				label = new Label(colNum++, i+1,enrollment.getCustomField().getSubjArea3()==null? "" : enrollment.getCustomField().getSubjArea3(), normalFormat);
				sheet.addCell(label);
				
				//Employee Id number = NumInd
				label = new Label(colNum++, i+1,enrollment.getNUM_ID()==null? "" : enrollment.getNUM_ID(), normalFormat);
				sheet.addCell(label);
		
				//Cognome
				label = new Label(colNum++, i+1,enrollment.getLAST_NAME()==null? "---" : enrollment.getLAST_NAME(), normalFormat);
				sheet.addCell(label);
				
				//Nome
				label = new Label(colNum++, i+1,enrollment.getFIRST_NAME()==null? "---" : enrollment.getFIRST_NAME(), normalFormat);
				sheet.addCell(label);
							
				//Training starting date
				if(enrollment.getSTART_DATE()!=null){
					dateTime = new DateTime(colNum++, i+1,enrollment.getSTART_DATE(),dateFormat);
					sheet.addCell(dateTime);
				}else{
					label = new Label(colNum++, i+1, "" , normalFormat);
					sheet.addCell(label);
				}
								
				//Item ID
				label = new Label(colNum++, i+1,enrollment.getCATALOG_CODE()==null? "" : enrollment.getCATALOG_CODE(), normalFormat);
				sheet.addCell(label);
				
				//Descrizione
				label = new Label(colNum++, i+1,enrollment.getCOURSE_DESCRIPTION()==null? "" : enrollment.getCOURSE_DESCRIPTION(), normalFormat);
				sheet.addCell(label);
				
				//Vendor
				label = new Label(colNum++, i+1,enrollment.getCustomField().getSupplier()==null? "" : enrollment.getCustomField().getSupplier(), normalFormat);
				sheet.addCell(label);
				
				//Vendor training code
				label = new Label(colNum++, i+1,enrollment.getCustomField().getVendorTrainingCode()==null? "" : enrollment.getCustomField().getVendorTrainingCode(), normalFormat);
				sheet.addCell(label);
				
				//Training source
				label = new Label(colNum++, i+1,enrollment.getCATALOGENTRY_OID()==null? "" : enrollment.getCATALOGENTRY_OID(), normalFormat);
				sheet.addCell(label);
				
				//Durata
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				if(enrollment.getDAYS()==-1){
					Label cell = new Label(colNum++, i+1,"", normalFormat);
					sheet.addCell(cell);
				}else{
					Number cell;
					try{
						cell = new Number(colNum++, i+1,Double.parseDouble(nf.format(enrollment.getDAYS()).replace(',','.')));
					}catch (Exception e) {
						//no handle exception- getDAYS is null; set cell to 0
						cell = new Number(colNum++, i+1,Double.parseDouble("0"));
					}
					
					sheet.addCell(cell);
				}
				
				//Cost
				if(enrollment.getCustomField().getCost()!=null) {
					Number cell = new Number(colNum++, i+1, TextUtils.parseEnglishDouble(enrollment.getCustomField().getCost(),0.00),currencyFormat  );
					sheet.addCell(cell);
				}else{ 
					Label cell = new Label(colNum++, i+1, ""  );
					sheet.addCell(cell);
				}
				
				//Direct manager ID Number
				label = new Label(colNum++, i+1,enrollment.getMANAGER_ID()==null? "" : enrollment.getMANAGER_ID(), normalFormat);
				sheet.addCell(label);
				
				//Cognome Resp richieste
				label = new Label(colNum++, i+1,enrollment.getMANAGER_LASTNAME()==null? "" : enrollment.getMANAGER_LASTNAME(), normalFormat);
				sheet.addCell(label);
				
				//Nome Resp richieste
				label = new Label(colNum++, i+1,enrollment.getMANAGER_FIRSTNAME()==null? "" : enrollment.getMANAGER_FIRSTNAME(), normalFormat);
				sheet.addCell(label);
				
				//Filiale - Workplace
				label = new Label(colNum++, i+1,enrollment.getFILIALE()==null? "" : enrollment.getFILIALE(), normalFormat);
				sheet.addCell(label);
				
				//Country 
				label = new Label(colNum++, i+1,enrollment.getCountry()==null? "" : enrollment.getCountry(), normalFormat);
				sheet.addCell(label);

				//UOG Code
				label = new Label(colNum++, i+1,enrollment.getDomainCode()==null? "" : enrollment.getDomainCode(), normalFormat);
				sheet.addCell(label);
				
				//UOG Description
				label = new Label(colNum++, i+1,enrollment.getDomainName()==null? "" : enrollment.getDomainName(), normalFormat);
				sheet.addCell(label);
				
				//Company
				label = new Label(colNum++, i+1, enrollment.getCompany()==null? "" : enrollment.getCompany(), normalFormat);
				sheet.addCell(label);
				
				//Employee e-mail address
				label = new Label(colNum++, i+1, enrollment.getUSER_EMAIL()==null? "" : enrollment.getUSER_EMAIL(), normalFormat);
				sheet.addCell(label);
				
				//Direct manager e-mail address 
				label = new Label(colNum++, i+1, enrollment.getMANAGER_EMAIL()==null? "" : enrollment.getMANAGER_EMAIL(), normalFormat);
				sheet.addCell(label);
				
				//Request Id-number
				label = new Label(colNum++, i+1, enrollment.getENROLLMENT_OID()==null? "" :enrollment.getENROLLMENT_OID(), normalFormat);
				sheet.addCell(label);
				
				//Requester Id-number
				label = new Label(colNum++, i+1, enrollment.getRequester()==null? "" :enrollment.getRequester().toUpperCase(), normalFormat);
				sheet.addCell(label);
				
				//Requester Surname
				label = new Label(colNum++, i+1, enrollment.getRequesterSName()==null? "" :enrollment.getRequesterSName().toUpperCase(), normalFormat);
				sheet.addCell(label);
				
				//Requester Name
				label = new Label(colNum++, i+1, enrollment.getRequesterName()==null? "" :enrollment.getRequesterName().toUpperCase(), normalFormat);
				sheet.addCell(label);

				//Category
				label = new Label(colNum++, i+1, enrollment.getCATEGORY()==null? "" :enrollment.getCATEGORY(), normalFormat);
				sheet.addCell(label);
				
				//Group
				label = new Label(colNum++, i+1, enrollment.getGROUP()==null? "" :enrollment.getGROUP(), normalFormat);
				sheet.addCell(label);
				
				//Skill
				label = new Label(colNum++, i+1, enrollment.getSKILL()==null? "" :enrollment.getSKILL(), normalFormat);
				sheet.addCell(label);
				
				//Status
				label = new Label(colNum++, i+1, enrollment.getSTATUS()==null? "" :enrollment.getSTATUS(), normalFormat);
				sheet.addCell(label);
				
				//Notes
				label = new Label(colNum++, i+1, enrollment.getNote()==null? "" :enrollment.getNote(), normalFormat);
				sheet.addCell(label);
				
				//Approval/Rejection Date
				DateTime apprRejDate;
				if(enrollment.getApprRejDate()!=null){
					apprRejDate = new DateTime(colNum++, i+1,enrollment.getApprRejDate(),dateFormat);
					sheet.addCell(apprRejDate);
				}else{
					label = new Label(colNum++, i+1, "" , normalFormat);
					sheet.addCell(label);
				}
				
				//File date 
				label = new Label(colNum++, i+1, toDay, normalFormat);
				sheet.addCell(label);
				
				++i;
				
				if(i%100==0)System.out.println("i = " + i);
			}
			
			workbook.write();
			workbook.close();
		}
	 	 
	 static public void createSupplierWihCostExcel(File file,ArrayList<Supplier> suppliers) throws Exception, IllegalStateException{
			
		 	if(file == null || file.getName().startsWith("null"))
		 		throw new IllegalStateException("Wrong file name. See configAccessData.properties (fileName_*)");
			
			WritableWorkbook workbook = Workbook.createWorkbook(file);  
			WritableSheet sheet = workbook.createSheet("FornitoriConsuntivo", 0);
	
			SheetSettings settings = sheet.getSettings();
			settings.setVerticalFreeze(1);
	
	//		   Setting styles for cells
			WritableCellFormat headerFormat = new WritableCellFormat(
				new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false));
				headerFormat.setBackground(Colour.GRAY_25); 
			WritableCellFormat normalFormat = new WritableCellFormat(
				new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false)); 
			normalFormat.setShrinkToFit(false);
			
			
			WritableCellFormat dateFormat = new WritableCellFormat(
					new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false),
					new DateFormat("dd/MM/yyyy"));

			dateFormat.setShrinkToFit(false);	
			
			WritableCellFormat currencyFormat = new WritableCellFormat( 
					new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false), 
					new jxl.write.NumberFormat("0.00")
				);
			currencyFormat.setShrinkToFit(false);
			
	//	   Setting width of all columns		
			sheet.setColumnView(0,30);
			sheet.setColumnView(1,20);
			
			
	
	
	//	   Setting header cells
					
			Label label = new Label(0, 0, "Fornitore",headerFormat);
			sheet.addCell(label);
			
			label = new Label(1,0,"Costo",headerFormat);
			sheet.addCell(label);
			
			
			log.info("File: "+file.getName());
			log.info("Excel suppliers:"+suppliers.size());
			Iterator<Supplier> it = suppliers.iterator();	
			
			Supplier supplier = null;
			int i=0;
			while(it.hasNext()) {
				supplier = it.next();
				
				//Fornitore
				label = new Label(0, i+1,supplier.getSuplier()==null?"":supplier.getSuplier(), normalFormat);
				sheet.addCell(label);
		
				//Cost
				Number cell = new Number(1, i+1,supplier.getCost(),currencyFormat  );
				sheet.addCell(cell);
					
				
		
				++i;	
			}
			
	
			workbook.write();
			workbook.close();
			
	
		}
	 
	 
	 static public void createCorsiExcel(File file, List<Enrollment>  enrollments) throws Exception, IllegalStateException{
			
			if(file == null || file.getName().startsWith("null"))
		 		throw new IllegalStateException("Wrong file name. See configAccessData.properties (fileName_*)");
			
			
			WritableWorkbook workbook = Workbook.createWorkbook(file);  
			WritableSheet sheet = workbook.createSheet("PianoRichiesteCorsi", 0);
	
			SheetSettings settings = sheet.getSettings();
			settings.setVerticalFreeze(1);
	
	//		   Setting styles for cells
			WritableCellFormat headerFormat = new WritableCellFormat(
				new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false));
				headerFormat.setBackground(Colour.GRAY_25); 
				
			WritableCellFormat normalFormat = new WritableCellFormat(
					new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false)); 
			
			normalFormat.setShrinkToFit(false);
			
			WritableCellFormat dateFormat = new WritableCellFormat(
					new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false),
					new DateFormat("dd/MM/yyyy"));

			dateFormat.setShrinkToFit(false);	
			
			WritableCellFormat currencyFormat = new WritableCellFormat( 
					new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false), 
					new jxl.write.NumberFormat("0.00")
				);
			 
			currencyFormat.setShrinkToFit(false);

			
	//	   Setting header cells
			
			int colNum =0;		
			sheet.setColumnView(colNum,10);
			Label label = new Label(colNum++, 0, "Number Id",headerFormat);
			sheet.addCell(label);
			
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Cognome",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Nome",headerFormat);
			sheet.addCell(label);
			
			label = new Label(colNum++,0,"Stato",headerFormat);
			sheet.addCell(label);
			sheet.setColumnView(colNum,10);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++, 0,"Data",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,25);
			label = new Label(colNum++,0,"Codici",headerFormat);
			sheet.addCell(label);
						
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"Descrizione",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Tipo",headerFormat);
			sheet.addCell(label);
						
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Didattica",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,15);
			label = new Label(colNum++,0,"Espletamento",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,24);
			label = new Label(colNum++,0,"CodAreaTematica",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"DescrAreaTematica",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"Societ\u00E0",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,30);
			label = new Label(colNum++,0,"Sede",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,10);
			label = new Label(colNum++,0,"gg",headerFormat);
			sheet.addCell(label);
			
			sheet.setColumnView(colNum,20);
			label = new Label(colNum++,0,"Costo",headerFormat);
			sheet.addCell(label);
			
			log.info("File: "+file.getName());
			log.info("Excel enrollments:"+enrollments.size());
			Iterator<Enrollment> it = enrollments.iterator();	
			
			Enrollment enrollment = null;
			
			for(int i=0;it.hasNext();++i) {
				enrollment = (Enrollment)it.next();
				colNum=0;
				// as Manuela wanted:
				// "If the real duration is zero hours don�t insert the course in file 'corsi.xls' "
				if (enrollment.getRealDays() <= 0){
					continue;
				}
				
				Number number = new Number(colNum++, i+1,Integer.parseInt(enrollment.getUSER_ID()));
				sheet.addCell(number);
		
				label = new Label(colNum++, i+1,enrollment.getLAST_NAME()==null  ?"":enrollment.getLAST_NAME(), normalFormat);
				sheet.addCell(label);
				
				label = new Label(colNum++, i+1,enrollment.getFIRST_NAME()==null ?"":enrollment.getFIRST_NAME(), normalFormat);
				sheet.addCell(label);
		
				label = new Label(colNum++, i+1,"Fatto", normalFormat);
				sheet.addCell(label);
				
				
				//Data
				DateTime dateTime = new DateTime(colNum++, i+1,enrollment.getSTART_DATE(),dateFormat);
				sheet.addCell(dateTime);

				//Codice
				label = new Label(colNum++, i+1,enrollment.getCATALOG_CODE()==null? "" : enrollment.getCATALOG_CODE(), normalFormat);
				sheet.addCell(label);
				
				//Descrizione
				label = new Label(colNum++, i+1,enrollment.getCOURSE_TITLE()==null? "" : enrollment.getCOURSE_TITLE(), normalFormat);
				sheet.addCell(label);
				
				//Tipo
				label = new Label(colNum++, i+1,enrollment.getCustomField().getTipologia()==null? "" : enrollment.getCustomField().getTipologia(), normalFormat);
				sheet.addCell(label);
		
				//Didattica
				label = new Label(colNum++, i+1,enrollment.getDIDATTICA()==null? "" : enrollment.getDIDATTICA(), normalFormat);
				sheet.addCell(label);
				
				//Espletamento
				label = new Label(colNum++, i+1,enrollment.getTYPE()==null? "" : enrollment.getTYPE(), normalFormat);
				sheet.addCell(label);				
	
				//Area Tematica Code
				label = new Label(colNum++, i+1,enrollment.getAREA_TEMATICA_CODE()==null? "" : enrollment.getAREA_TEMATICA_CODE(), normalFormat);
				sheet.addCell(label);
				
				//Area Tematica Desc
				label = new Label(colNum++, i+1,enrollment.getCustomField().getAreaTematica()==null? "" : enrollment.getCustomField().getAreaTematica(), normalFormat);
				sheet.addCell(label);
				
				//Societa
				label = new Label(colNum++, i+1,enrollment.getCustomField().getSupplier()==null? "" : enrollment.getCustomField().getSupplier(), normalFormat);
				sheet.addCell(label);
				
				//Sede
				label = new Label(colNum++, i+1,enrollment.getCITY()==null? "" : enrollment.getCITY(), normalFormat);
				sheet.addCell(label);
				
				//gg
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				if(enrollment.getRealDays() <= 0){
					Label cell = new Label(colNum++, i+1,"", normalFormat);
					sheet.addCell(cell);
				}else{
					Number cell = new Number(colNum++, i+1,Double.parseDouble(nf.format(enrollment.getRealDays()).replace(',','.')));
					sheet.addCell(cell);
				}
				
				//Cost
				if(enrollment.getCustomField().getCost()!=null) {
					Number cell = new Number(colNum++, i+1, TextUtils.parseEnglishDouble(enrollment.getCustomField().getCost(),0.00) ,currencyFormat );
					sheet.addCell(cell);
					
				}else{ 
					Label cell = new Label(colNum++, i+1, ""  );
					sheet.addCell(cell);
					
				}	
				
			}
				
			workbook.write();
			workbook.close();
				
		}
	 
}