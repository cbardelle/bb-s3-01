/*
 * Created on Aug 13, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch.utility;




import java.sql.Connection;
import java.sql.DriverManager;



/*****************************************************************************/
/****************************** DBManager **************************************/
/*****************************************************************************/
public class DBManager
   {
   
   /**************************************************************************/
   /*********************** getConnection ************************************/
   /**************************************************************************/
   public Connection 
   getConnection() 
   throws Exception
      {
		
		 PropertyFileReader prop  = PropertyFileReader.getInstance();;
         String userid = prop.getProperty("userId");
	     String pw = prop.getProperty("pw");
         String className = prop.getProperty("className");
       	 String jdbcString = prop.getProperty("URL");

		 Class.forName(className).newInstance();
         Connection con = DriverManager.getConnection(jdbcString, userid, pw);
         return con;
       
      }


   /**************************************************************************/
   /****************************** main **************************************/
   /**************************************************************************/
   public static void main(String[] args)
      {
      try
         {
         Connection con = new DBManager().getConnection();
         System.out.println(con);
         }
      catch( Exception e )
         {
         System.err.println("Exception:" + e.getMessage());
         e.printStackTrace(System.err);
         }
      }
   }
