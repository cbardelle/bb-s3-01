package pl.ugis.batch.utility;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LdapResources  {

	public static java.lang.String getError ="";
	public static DirContext ctx = null;
	
	/**
	 * Constructor for ldapResources.
	 */
	public LdapResources() {
		super();
	}
	
	public static void connect (String server,String port,String user,String pwd) {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
	//	env.put("com.sun.jndi.ldap.connect.pool", "true");
		env.put(Context.PROVIDER_URL, "ldap://" + server + ":" + port);
		env.put(Context.SECURITY_PRINCIPAL, user);
		env.put(Context.SECURITY_AUTHENTICATION,"simple");
		env.put(Context.SECURITY_CREDENTIALS, pwd);
		env.put("com.sun.jndi.ldap.connect.timeout", "15000");
		env.put("com.sun.jndi.ldap.read.timeout", "15000");
		env.put("com.sun.jndi.ldap.connect.pool", "true");
		ctx = null;
		try {
				ctx = new InitialDirContext(env);
		} catch (NamingException e) {
				getError= e.getMessage();
				System.out.println("LDAP connect: errore connection: " +  e.getMessage());
		}
	}

	public static NamingEnumeration<SearchResult> search(String BaseDn, String filter) throws Exception  {
		
		NamingEnumeration<SearchResult> answer =null;
		try {
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			answer = ctx.search(BaseDn,filter, constraints );
		} catch (NamingException e) {
			getError=e.getMessage();
		}
		return answer;
	}

	public static NamingEnumeration<SearchResult> search(String BaseDn, String filter, String[] Attrs) throws Exception  {
		
		NamingEnumeration<SearchResult> answer =null;
		try {
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			constraints.setReturningAttributes(Attrs);
			answer = ctx.search(BaseDn,filter, constraints );
		} catch (NamingException e) {
			getError=e.getMessage();
		}
		return answer;
	}

	public static NamingEnumeration<SearchResult> search(String BaseDn, String filter, String[] Attrs, String SCOPE) throws Exception  {
	
		NamingEnumeration<SearchResult> answer =null;
		try {
			SearchControls constraints = new SearchControls();
			if (SCOPE.toUpperCase()=="SUB") {
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			} else {
				constraints.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			}
			constraints.setReturningAttributes(Attrs);
			answer = ctx.search(BaseDn,filter, constraints );
		} catch (NamingException e) {
			getError=e.getMessage();
		}
		return answer;
	}

	public static NamingEnumeration<SearchResult> search(String BaseDn, String filter, String SCOPE) throws Exception  {
		
		NamingEnumeration<SearchResult> answer =null;
		try {
			SearchControls constraints = new SearchControls();
			if (SCOPE.toUpperCase()=="SUB") {
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			} else {
				constraints.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			}
			answer = ctx.search(BaseDn,filter, constraints);
		} catch (NamingException e) {
			getError=e.getMessage();
		}
		return answer;
	}

	public static void disconnect() throws Exception {
		try {
			ctx.close();
		} catch (NamingException e) {
			getError=e.getMessage();
		}
	}


}

