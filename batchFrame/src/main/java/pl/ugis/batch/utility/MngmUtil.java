package pl.ugis.batch.utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import pl.ugis.batch.bo.Department;
import pl.ugis.batch.bo.Enrollment;

public class MngmUtil {
	static SimpleDateFormat formaterToDay = new SimpleDateFormat("yyyy.MM.dd");
	static SimpleDateFormat formaterToDayItalian = new SimpleDateFormat("dd-MM-yyyy");
	
	static public String today(){
		String today = formaterToDay.format(new Date());
		return today;
	}
	
	static public String todayItalianFormat()
	{
		String today = formaterToDayItalian.format(new Date());
		return today;
	}
	
	public void digDomainDepartment(Enrollment result, HashMap<String,Department> allDepartments){
		
		
		
		ArrayList<Department> depResult = new ArrayList<Department>();
		for ( String d:TextUtils.digDepartmentsFromDistinguieshedName(result.getDISTINGUISHED_NAME())){
			
			if(allDepartments!=null && allDepartments.get(d)!=null){
				Department dep = allDepartments.get(d);
				
				depResult.add(dep);
				
			}
			
			
		}
		result.setUserDepartments(depResult);
		
		for (Department userDep : result.getUserDepartments()){
			
			if(StringUtils.equalsIgnoreCase(userDep.getUsDepartmentType(), "DOMAIN")){
				result.setDomainCode(userDep.getUsDepartmentCode());
				result.setDomainName(userDep.getUsDepartment());
				result.setDomainManager(userDep.getUsManger());
				result.setDomainManagers(userDep.getManagers());
			}
		}
		
		
	}
}
