package pl.ugis.batch.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pl.ugis.batch.utility.LDAPDepartment;
import pl.ugis.batch.utility.LDAPUser;
import pl.ugis.batch.utility.LdapResources;
import pl.ugis.batch.utility.PropertyFileReader;

public class LdapDAO 
{
	static Logger log = Logger.getLogger(LdapDAO.class);
	
	private final static String ldapServer = PropertyFileReader.getInstance().getProperty("ldapServer");
	private final static String ldapUser = PropertyFileReader.getInstance().getProperty("ldapUser");
	private final static String ldapPass = PropertyFileReader.getInstance().getProperty("ldapPass");
	private final static String ldapPort = PropertyFileReader.getInstance().getProperty("ldapPort");
	
	static public Map<String, LDAPUser> getUserToCorrectManagersData(HashSet<String> userList) throws Exception
	{
		log.debug("Number of people with missing manager " + userList.size());
		Map<String, LDAPUser> users = new HashMap<String, LDAPUser>();

		try 
		{
			StringBuilder usersFiler = new StringBuilder("(|");
			for(String userID : userList)
			{
				usersFiler.append("(uid=" + userID + ")"); 
			}
			usersFiler.append(")");
			
			
			LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
			NamingEnumeration<SearchResult> ne = LdapResources.search("ou=People,dc=unicredito,dc=it", usersFiler.toString(), new String[] { "uid", "usmanager", "usdepartmentcode", "usparentdepartmentcode" });

			LDAPUser ldapUser = new LDAPUser();
			while (ne != null && ne.hasMoreElements()) 
			{
				SearchResult sr = ne.next();
				String uid = sr.getAttributes().get("uid") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("uid").get().toString());
				String usmanager = sr.getAttributes().get("usmanager") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usmanager").get().toString());
				String usdepartmentcode = sr.getAttributes().get("usdepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usdepartmentcode").get().toString());
				String usparentdepartmentcode = sr.getAttributes().get("usparentdepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usparentdepartmentcode").get().toString());

				ldapUser = new LDAPUser();
				ldapUser.setUid(uid);
				ldapUser.setUsmanager(usmanager);
				ldapUser.setUsdepartmentcode(usdepartmentcode);
				ldapUser.setUsparentdepartmentcode(usparentdepartmentcode);
				users.put(uid, ldapUser);
			}
			
			log.debug("Number of people with missing manager found in LDAP " + users.size());
		} 
		catch (Exception e) 
		{
			throw e;
		}
		finally
		{
			LdapResources.disconnect();
		}
		
		return users;
	}
	
	public static int setMissingManagersData(Map<String, LDAPUser> users, Map<String, LDAPDepartment> departmentCodesToDepartments)
	{
		int maxLoops = 10;
		int managersNotSet = 0;
		LDAPDepartment department;
		String managerUID;
		LDAPUser user;
		Iterator<String> LDAPUsersOids;
		
		do
		{
			LDAPUsersOids = users.keySet().iterator();
			while(LDAPUsersOids.hasNext())
			{
				user = users.get(LDAPUsersOids.next());
				
				if(( user.getUsmanager().trim().isEmpty() || user.getUsmanager().trim().equalsIgnoreCase(user.getUid().trim()) ) && user.isManagerSet() == false && user.isOmitUser() == false)
				{
					department = departmentCodesToDepartments.get(user.getUsparentdepartmentcode());
					
					if(department == null || department.getUsparentdepartmentcode().trim().isEmpty() || department.getUsparentdepartmentcode().trim().equals("."))
					{
						user.setOmitUser(true);
						user.setUsmanager("");
						managersNotSet++;
						log.debug("Can't set manager for user " + user.getUid() + " with usparentdepartmentcode " + user.getUsparentdepartmentcode());
						continue;
					}
					
					managerUID = department.getUsmanager();
	
					if(managerUID == null || managerUID.trim().isEmpty())
					{
						user.setUsparentdepartmentcode(department.getUsparentdepartmentcode());
					}
					else 
					{
						user.setUsmanager(managerUID);
					}
				}			
			}
			maxLoops--;
		}
		while(maxLoops >= 0 &&  managersNotSet > 0);
		
		return managersNotSet;
	}
	
	public static Map<String, LDAPDepartment> getDepartmentCodesToDepartments() throws Exception
	{
		Map<String, LDAPDepartment> departmentCodesToDepartments = new HashMap<String, LDAPDepartment>();
		String[] ldapDepartmentAttributes = new String[6];
		
		ldapDepartmentAttributes[0] = "usdepartment";
		ldapDepartmentAttributes[1] = "usdepartmentcode";
		ldapDepartmentAttributes[2] = "usdepartmenttype";
		ldapDepartmentAttributes[3] = "usparentdepartmentcode";
		ldapDepartmentAttributes[4] = "usmanager";
		ldapDepartmentAttributes[5] = "usdepartmenttypecode";
		
		String filter = "(usdepartmentcode=*)";
		String baseDN;
		if(ldapServer.equalsIgnoreCase("ugd.intranet.unicreditgroup.eu")||ldapServer.equalsIgnoreCase("ugdqa.intranet.unicreditgroup.eu")){
			baseDN = "ou=Org,dc=unicreditgroup,dc=eu";
		}else{
			baseDN = "ou=Org,dc=unicredito,dc=it";
		}
	    try
	    {
		    LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
		    NamingEnumeration<SearchResult> ldapDepartments = LdapResources.search(baseDN, filter, ldapDepartmentAttributes);
			
		    LDAPDepartment department;
			Attributes LDAPAttributes = null;
			while (ldapDepartments.hasMore()) 
			{
				department = new LDAPDepartment();
				LDAPAttributes = ((SearchResult)ldapDepartments.next()).getAttributes();
	
				department.setUsdepartment(getLDAPAttribute(LDAPAttributes,"usdepartment"));
				department.setUsdepartmentcode(getLDAPAttribute(LDAPAttributes,"usdepartmentcode"));
				department.setUsmanager(getLDAPAttribute(LDAPAttributes,"usmanager"));
				department.setUsparentdepartmentcode(getLDAPAttribute(LDAPAttributes,"usparentdepartmentcode"));
				department.setUsdepartmenttype(getLDAPAttribute(LDAPAttributes,"usdepartmenttype"));
				department.setUsdepartmenttypecode(getLDAPAttribute(LDAPAttributes,"usdepartmenttypecode"));
				
				departmentCodesToDepartments.put(department.getUsdepartmentcode(), department);
			}
			log.debug("departmentCodesToDepartments().size() = " + departmentCodesToDepartments.size());
	    }	
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			LdapResources.disconnect();
		}
			
		return departmentCodesToDepartments;
	}
	
	
	public static LDAPUser getUserFromLDAP(String userID) throws Exception  
	{
		LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
		NamingEnumeration<SearchResult> ne;
		if(ldapServer.equalsIgnoreCase("ugd.intranet.unicreditgroup.eu")||ldapServer.equalsIgnoreCase("ugdqa.intranet.unicreditgroup.eu")){
			ne = LdapResources.search("ou=People,dc=unicreditgroup,dc=eu", "(uid=" + userID + ")", new String[] { "uid", "usmanager", "usdepartmentcode", "usdepartment", "usparentdepartmentcode", "sn", "givenName", "mail", "usorganizationcode", "uscountry" });
		}else{
			ne = LdapResources.search("ou=People,dc=unicredito,dc=it", "(uid=" + userID + ")", new String[] { "uid", "usmanager", "usdepartmentcode", "usdepartment", "usparentdepartmentcode", "sn", "givenName", "mail", "usorganizationcode", "uscountry" });
		}
		
		LDAPUser ldapUser = new LDAPUser();
		while (ne != null && ne.hasMoreElements()) 
		{
			SearchResult sr = ne.next();
			String uid = sr.getAttributes().get("uid") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("uid").get().toString());
			String usmanager = sr.getAttributes().get("usmanager") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usmanager").get().toString());
			String usdepartmentcode = sr.getAttributes().get("usdepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usdepartmentcode").get().toString());
			String usdepartment = sr.getAttributes().get("usdepartment") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usdepartment").get().toString());
			String usparentdepartmentcode = sr.getAttributes().get("usparentdepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usparentdepartmentcode").get().toString());
			String sn = sr.getAttributes().get("sn") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("sn").get().toString());
			String givenName = sr.getAttributes().get("givenName") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("givenName").get().toString());
			String mail = sr.getAttributes().get("mail") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("mail").get().toString());
			String usorganizationcode = sr.getAttributes().get("usorganizationcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usorganizationcode").get().toString());
			String uscountry = sr.getAttributes().get("uscountry") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("uscountry").get().toString());
				
			ldapUser = new LDAPUser();
			ldapUser.setUid(uid);
			ldapUser.setUsmanager(usmanager);
			ldapUser.setUsdepartmentcode(usdepartmentcode);
			ldapUser.setUsdepartment(usdepartment);
			ldapUser.setUsparentdepartmentcode(usparentdepartmentcode);
			ldapUser.setGivenName(givenName);
			ldapUser.setSn(sn);
			ldapUser.setMail(mail);
			ldapUser.setUsorganizationcode(usorganizationcode);
			ldapUser.setUscountry(uscountry);
		}
		
		LdapResources.disconnect();
		
		return ldapUser;
	}

	private static String getLDAPAttribute(Attributes LDAPAttributes, String attributeName)
	{
		Attribute LDAPAttribute = LDAPAttributes.get(attributeName);
		String attributeValue = "";
		
		try {
			
			if(LDAPAttribute != null) {
				
				NamingEnumeration<?> attributes = LDAPAttribute.getAll();
				attributeValue = attributes.nextElement().toString().trim();
			}
			
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		return attributeValue;
	}

	public static Map<String, LDAPUser> getUsersData(
			HashSet<String> usersToCheckInLDAP) throws Exception {
		
		log.debug("Number of people to check " + usersToCheckInLDAP.size());
		Map<String, LDAPUser> users = new HashMap<String, LDAPUser>();

		try 
		{
			StringBuilder usersFiler = new StringBuilder("(|");
			for(String userID : usersToCheckInLDAP)
			{
				usersFiler.append("(uid=" + userID + ")"); 
			}
			usersFiler.append(")");
			
			LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
			NamingEnumeration<SearchResult> ne;
			if(ldapServer.equalsIgnoreCase("ugd.intranet.unicreditgroup.eu")||ldapServer.equalsIgnoreCase("ugdqa.intranet.unicreditgroup.eu")){
				ne = LdapResources.search("ou=People,dc=unicreditgroup,dc=eu", usersFiler.toString(), new String[] { "uid" });
			}else{
				ne = LdapResources.search("ou=People,dc=unicredito,dc=it", usersFiler.toString(), new String[] { "uid" });
			}
			
			LDAPUser ldapUser = new LDAPUser();
			while (ne != null && ne.hasMoreElements()) 
			{
				SearchResult sr = ne.next();
				String uid = sr.getAttributes().get("uid") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("uid").get().toString());
				
				uid=uid.toUpperCase();
				ldapUser = new LDAPUser();
				ldapUser.setUid(uid);

				users.put(uid, ldapUser);
			}
			log.debug("Number of people found in LDAP " + users.size());
		} 
		catch (Exception e) 
		{
			throw e;
		}
		finally
		{
			LdapResources.disconnect();
		}
		
		return users;
	
	}

	public static LDAPUser getUserDataFromLDAP(String userID, Map<String, LDAPDepartment> departmentCodesToDepartments) throws Exception
	{
		LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
		NamingEnumeration<SearchResult> ne;
		if(ldapServer.equalsIgnoreCase("ugd.intranet.unicreditgroup.eu")||ldapServer.equalsIgnoreCase("ugdqa.intranet.unicreditgroup.eu")){
			ne = LdapResources.search("ou=People,dc=unicreditgroup,dc=eu", "(uid=" + userID + ")", new String[] { "uid", "usmanager", "usdepartmentcode", "usparentdepartmentcode", "sn", "givenName", "mail", "usfulldepartmentcode", "usagency" });
		}else{
			ne = LdapResources.search("ou=People,dc=unicredito,dc=it", "(uid=" + userID + ")", new String[] { "uid", "usmanager", "usdepartmentcode", "usparentdepartmentcode", "sn", "givenName", "mail", "usfulldepartmentcode", "usagency" });
		}

		LDAPUser ldapUser = new LDAPUser();
		while (ne != null && ne.hasMoreElements()) 
		{
			SearchResult sr = ne.next();
			String uid = sr.getAttributes().get("uid") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("uid").get().toString());
			String usmanager = sr.getAttributes().get("usmanager") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usmanager").get().toString());
			String usdepartmentcode = sr.getAttributes().get("usdepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usdepartmentcode").get().toString());
			String usparentdepartmentcode = sr.getAttributes().get("usparentdepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usparentdepartmentcode").get().toString());
			String sn = sr.getAttributes().get("sn") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("sn").get().toString());
			String givenName = sr.getAttributes().get("givenName") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("givenName").get().toString());
			String mail = sr.getAttributes().get("mail") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("mail").get().toString());
			String usagency = sr.getAttributes().get("usagency") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usagency").get().toString());
			//
			String usorganizationcode = sr.getAttributes().get("usorganizationcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usorganizationcode").get().toString());
			String usfulldepartmentcode = sr.getAttributes().get("usfulldepartmentcode") == null ? "" : StringUtils.stripToEmpty(sr.getAttributes().get("usfulldepartmentcode").get().toString());
			String[] usdepartmentcodeTmp = usfulldepartmentcode.split("\\$");
			String[] retValues = new String[2];
			String domainCode="";
			String domainName="";
			String domainMngr = null;
			String compAreaCode="";
			String compAreaName="";
			String compAreaMngr="";
			String unitCode="";
			String unitName="";
			String unitMngr="";
			String compCenterCode="";
			String compCenterName="";
			String compCenterMngr="";
			//domain
			for(int i=0; i <= usdepartmentcodeTmp.length-1 ; i++)
			{
				LDAPDepartment departm = departmentCodesToDepartments.get(usdepartmentcodeTmp[i]);
				if(!(departm==null)){
					if(departm.getUsdepartmenttypecode().equalsIgnoreCase("DOM")){
						domainCode=usdepartmentcodeTmp[i];
						domainName=departm.getUsdepartment();
						domainMngr=departm.getUsmanager();
						break;
					}
			}}
			//Competence area
			retValues = findFirstDepartmentFromLDAP(usdepartmentcodeTmp,departmentCodesToDepartments,"CA");
			compAreaCode=retValues[0];
			compAreaName=retValues[1];
			compAreaMngr=retValues[2];
			//Unit
			retValues = findFirstDepartmentFromLDAP(usdepartmentcodeTmp,departmentCodesToDepartments,"UNI");
			unitCode=retValues[0];
			unitName=retValues[1];
			unitMngr=retValues[2];
			//Competence center
			retValues = findFirstDepartmentFromLDAP(usdepartmentcodeTmp,departmentCodesToDepartments,"CEO");
			compCenterCode=retValues[0];
			compCenterName=retValues[1];
			compCenterMngr=retValues[2];
			
			ldapUser = new LDAPUser();
			ldapUser.setUid(uid);
			ldapUser.setUsmanager(usmanager);
			ldapUser.setUsdepartmentcode(usdepartmentcode);
			ldapUser.setUsparentdepartmentcode(usparentdepartmentcode);
			ldapUser.setGivenName(givenName);
			ldapUser.setSn(sn);
			ldapUser.setMail(mail);
			ldapUser.setUsagency(usagency);
			ldapUser.setUsfulldepartmentcode(usfulldepartmentcode);
			ldapUser.setUsdepartmenttypecode(domainCode);
			ldapUser.setUsdepartment(domainName);
			ldapUser.setUsdepartmenttypemngr(domainMngr);
			ldapUser.setCompAreacode(compAreaCode);
			ldapUser.setCompArea(compAreaName);
			ldapUser.setCompAreaMngr(compAreaMngr);
			ldapUser.setUnitCode(unitCode);
			ldapUser.setUnit(unitName);
			ldapUser.setUnitMngr(unitMngr);
			ldapUser.setCompCenterCode(compCenterCode);
			ldapUser.setCompCenter(compCenterName);
			ldapUser.setCompCenterMngr(compCenterMngr);
			ldapUser.setUsorganizationcode(usorganizationcode);
		}
		LdapResources.disconnect();
		
		return ldapUser;
	}

	private static String[] findFirstDepartmentFromLDAP(String[] usdepartmentcodeTmp, Map<String, LDAPDepartment> departmentCodesToDepartments, String string) {
		String[] retVal = new String[3];
		for(int i=usdepartmentcodeTmp.length-1; i >= 0  ; i--)
		{
			LDAPDepartment departm = departmentCodesToDepartments.get(usdepartmentcodeTmp[i]);
				if(!(departm==null)){
					if(departm.getUsdepartmenttypecode().equalsIgnoreCase("CA")){
						retVal[0]=usdepartmentcodeTmp[i];
						retVal[1]=departm.getUsdepartment();
						retVal[2]=departm.getUsmanager();
						break;
				}}
		}
		
	return retVal;	
	}	
}