package pl.ugis.batch.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Department;
import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.bo.Supplier;
import pl.ugis.batch.bo.User;
import pl.ugis.batch.lms.BatchWebServices;
import pl.ugis.batch.utility.DBManager;
import pl.ugis.batch.utility.LdapResources;
import pl.ugis.batch.utility.PropertyFileReader;
import pl.ugis.batch.utility.TimeDurationUtils;

public class BatchService {

	static private Connection conn;

	static Logger log = Logger.getLogger(BatchService.class);

	private final static String ldapServer = PropertyFileReader.getInstance()
			.getProperty("ldapServer");

	private final static String ldapUser = PropertyFileReader.getInstance()
			.getProperty("ldapUser");

	private final static String ldapPass = PropertyFileReader.getInstance()
			.getProperty("ldapPass");

	private final static String ldapPort = PropertyFileReader.getInstance()
			.getProperty("ldapPort");

	private final static String ldapDN = PropertyFileReader.getInstance()
			.getProperty("ldapDN");
	
	private final static String requestIdOffset = PropertyFileReader.getInstance()
			.getProperty("requestIdOffset");

	static public List<Enrollment> getEnrollmentsForConsultaionWithManager(String startPos, String endPos)
			throws Exception {

		List<Enrollment> enrollments = null;

		try {
			log.debug("GETS CONNECTION");
			conn = new DBManager().getConnection();
			startPos = String.valueOf((Integer.parseInt(startPos)+Integer.parseInt(requestIdOffset)));
			endPos = String.valueOf((Integer.parseInt(endPos)+Integer.parseInt(requestIdOffset)));
			log.debug("RequestIdOffset set to: "+requestIdOffset);
			enrollments = BatchDAO.getEnrollmentsFromLTP(conn, startPos, endPos);

		} catch (Exception e) {
			throw e;

		} finally {
			if (conn != null)

				try {
					conn.close();
				} catch (Exception e2) {

					log
							.error("close connection problem,"
									+ e2.getMessage(), e2);
					throw e2;
				}
		}

		return enrollments;

	}

	static public ArrayList<Supplier> getSumOfCostForSuppliersSentToMetamorfosiInCurrentYear()
			throws Exception {

		ArrayList<Supplier> suppliers = null;

		try {
			log.debug("GETS CONNECTION");
			conn = new DBManager().getConnection();

			suppliers = BatchDAO
					.getSumOfCostForSuppliersSentToMetamorfosiInCurrentYear(conn);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;

		} finally {
			if (conn != null)

				try {
					// conn.setAutoCommit(true);
					conn.close();
				} catch (Exception e2) {

					log
							.error("close connection problem,"
									+ e2.getMessage(), e2);
					throw e2;
				}
		}

		return suppliers;

	}

	static public ArrayList<Supplier> getSumOfCostForSuppliersFromCurrentMonth()
			throws Exception {

		ArrayList<Supplier> suppliers = null;

		try {
			log.debug("GETS CONNECTION");
			conn = new DBManager().getConnection();

			suppliers = BatchDAO.getSumOfCostForSuppliersFromCurrentMonth(conn);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;

		} finally {
			if (conn != null)

				try {
					// conn.setAutoCommit(true);
					conn.close();
				} catch (Exception e2) {

					log
							.error("close connection problem,"
									+ e2.getMessage(), e2);
					throw e2;
				}
		}

		return suppliers;

	}

	static public List<Enrollment> getEnrollmentsSentToMetamrfosiFromLastWeek()
			throws Exception {

		List<Enrollment> enrollments = null;

		try {
			log.debug("GETS CONNECTION");
			conn = new DBManager().getConnection();

			enrollments = BatchDAO
					.getEnrollmentsSentToMetamrfosiFromLastWeek(conn);

			Iterator<Enrollment> it = enrollments.iterator();
			
			while (it.hasNext()) {
				Enrollment enr = (Enrollment) it.next();

				enr.setDAYS(TimeDurationUtils.daysFromDuration(enr
						.getCustomField().getDuration()));

			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;

		} finally {
			if (conn != null)

				try {
					conn.close();
				} catch (Exception e2) {

					e2.printStackTrace();
					log
							.error("close connection problem,"
									+ e2.getMessage(), e2);
					throw e2;
				}
		}

		return enrollments;

	}

	static public List<Enrollment> getWrongEnrollmentsFromTRASP002()
			throws Exception {

		List<Enrollment> enrollments = null;

		try {
			log.debug("GETS CONNECTION");
			conn = new DBManager().getConnection();

			enrollments = BatchDAO.getWrongEnrollmentsFromTRASP002(conn);

			LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);

			for (Enrollment e : enrollments) {

				NamingEnumeration<SearchResult> ne = LdapResources.search(ldapDN, "(uid="+ e.getUSER_ID()+")", new String[] { "uscf" });

				int counter = 0;
				while (ne != null && ne.hasMoreElements()) {

					counter++;
					SearchResult sr = (SearchResult) ne.next();

					if (sr.getAttributes().get("uscf") != null) {

						Attribute uscf = sr.getAttributes().get("uscf");

						e.setCf(String.valueOf(uscf).replaceAll("uscf:","").trim());
						log.debug("uscf=" + e.getCf());
					}
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;

		} finally {
			if (conn != null)

				try {
					conn.close();
				} catch (Exception e2) {

					e2.printStackTrace();
					log
							.error("close connection problem,"
									+ e2.getMessage(), e2);
					throw e2;
				}
		}

		return enrollments;

	}
	
	public static User getUserInfo(String userID) throws Exception
	{
		User user = new User(userID);
		String mail, uscountry, usmanager;
		
		try 
		{
			LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
			NamingEnumeration<SearchResult> ne = LdapResources.search("ou=People,dc=unicredito,dc=it", "(uid=" + userID + ")", new String[] { "mail", "uscountry", "usmanager" });
			
			int counter = 0;
			while (ne != null && ne.hasMoreElements()) {
	
				counter++;
				SearchResult sr = (SearchResult) ne.next();
	
				mail	  = sr.getAttributes().get("mail") == null? "":sr.getAttributes().get("mail").get().toString();
				uscountry = sr.getAttributes().get("uscountry") == null? "":sr.getAttributes().get("uscountry").get().toString();
				usmanager = sr.getAttributes().get("usmanager") == null? "":sr.getAttributes().get("usmanager").get().toString();
				
				user.setMail(StringUtils.stripToEmpty(mail));
				user.setUscountry(StringUtils.stripToEmpty(uscountry));
				user.setUsmanager(StringUtils.stripToEmpty(usmanager));
			}
		}
		catch (Exception e)
		{
			log.debug("USER_ID = " + userID );
			log.error(e.getMessage(), e);
			throw e;
		}
		finally 
		{
		}	
		
		return user;
	}
	
	static public HashMap<String, Department> getDepartments(Connection conn) throws Exception {

		HashMap<String,Department> usDepartments = null;
		
		try {
			
			LdapResources.connect(ldapServer, ldapPort, ldapUser, ldapPass);
	
			NamingEnumeration<SearchResult> ne = LdapResources.search("ou=Org,dc=unicredito,dc=it", "(|(usorganizationcode=US)(usorganizationcode=UO))", new String[] { "usdepartmentcode","usdepartment","usdepartmenttype","usmanager" });
			
			
			int counter = 0;
			usDepartments = new HashMap<String, Department>();
			while (ne != null && ne.hasMoreElements()) {
	
				counter++;
				SearchResult sr = (SearchResult) ne.next();
	
				
				String usdepartmentcode = sr.getAttributes().get("usdepartmentcode")==null?"":StringUtils.stripToEmpty(sr.getAttributes().get("usdepartmentcode").get().toString());
				String usdepartment = sr.getAttributes().get("usdepartment")==null?"":StringUtils.stripToEmpty(sr.getAttributes().get("usdepartment").get().toString());
				String usdepartmenttype = sr.getAttributes().get("usdepartmenttype")==null?"":StringUtils.stripToEmpty(sr.getAttributes().get("usdepartmenttype").get().toString());
				String usmanager = sr.getAttributes().get("usmanager")==null?"":StringUtils.stripToEmpty(sr.getAttributes().get("usmanager").get().toString());
				
				
				
				
				
				Department department = new Department(usdepartmentcode,usdepartment,usdepartmenttype,usmanager);
				String commonNames="";
				
				if(StringUtils.isNotBlank(usmanager) && StringUtils.equalsIgnoreCase(usdepartmenttype,"DOMAIN") ){
					
					ArrayList<User> managers = new ArrayList<User>();
					
					String[] users = usmanager.split(",");
					for(String userId : users){
						String commonName = BatchDAO.getCommonName(conn, userId);
						managers.add(new User(userId,commonName));
						commonNames+=commonName+",";
						
					}
					
					department.setManagers(managers);
				}
				
				usDepartments.put(usdepartmentcode,department );
				
				log.info("usdepartmentcode:"+usdepartmentcode+",usdepartmenttype:"+usdepartmenttype+",usmanager:"+usmanager+",DomainName:"+commonNames);
			}
	
	} catch (Exception e) {
		log.error(e.getMessage(), e);
		throw e;
	
	}  finally {
		
	}
	
	return usDepartments;
	
	}


	static public void setResultByWebServiceAndInsertIntoConsuntivazione(List<Enrollment> enrollments) throws Exception {

		int maxTryCount = 20;
		int tryTimeout = 20000;
		try {
			conn = new DBManager().getConnection();
			BatchWebServices ws = new BatchWebServices();
			
			for(Enrollment e:enrollments){
				
				if(e.getScore()==null) continue;
				
				String newScore = String.valueOf((Double.parseDouble(e.getScore())*100)); 
				
				log.info("Setting score for ROWNUM:"+e.getROWNUM()+" userId: "+e.getUSER_ID()+" enrollmentOid: "+e.getOID());
				ws.SetResult(e.getOID(),newScore ,e.getSTATUS());
				
				boolean sucess = false;
					
				for(int i=0;i<maxTryCount;i++){
					if (BatchDAO.TRASP002hasBeenFixed(e.getOID(), conn)) {
						BatchDAO.insertIntoConsuntivazione(e.getCf(), conn);
						log.info("Try "+i +" finished with success for "+e.getOID());
						sucess = true;
						break;
					}else{
						log.info("Try "+i +" finished with failed for "+e.getOID());
						Thread.sleep(tryTimeout);
					}
				}
				if (!sucess){
					log.error("Failed to insert into Consuntivazione for ROWNUM: "+e.getROWNUM()+" enrollment: "+e.getOID());
				}
				
			}

		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw ex;

		} finally {
			if (conn != null)

				try {
					conn.close();
				} catch (Exception e2) {

					log.error("close connection problem,"+ e2.getMessage(), e2);
					throw e2;
				}
		}

	
	}

	

	static public List<Enrollment> getStepEnrolments(String startDate)
			throws Exception {

		List<Enrollment> enrollments = null;

		try {
			log.debug("GETS CONNECTION");
			conn = new DBManager().getConnection();

			enrollments = BatchDAO.getStepEnrolments(conn, startDate);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;

		} finally {
			if (conn != null)

				try {
					conn.close();
				} catch (Exception e2) {

					e2.printStackTrace();
					log
							.error("close connection problem,"
									+ e2.getMessage(), e2);
					throw e2;
				}
		}

		return enrollments;

	}

	static public int getEnrollmentsSize()
		throws Exception {

	int enrollmentsSize = 0;

	try {
		conn = new DBManager().getConnection();
		enrollmentsSize = BatchDAO.getEnrollmentsSize(conn, requestIdOffset);
	
	} catch (Exception e) {
		throw e;
	
	} finally {
		if (conn != null)
	
			try {
				conn.close();
			} catch (Exception e2) {
	
				log
						.error("close connection problem,"
								+ e2.getMessage(), e2);
				throw e2;
			}
	}

	return enrollmentsSize;
	}

}