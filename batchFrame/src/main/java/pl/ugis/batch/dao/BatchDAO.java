package pl.ugis.batch.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import pl.ugis.batch.bo.CustomField;
import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.bo.Supplier;
import pl.ugis.batch.utility.PropertyFileReader;

public class BatchDAO
{
	private static Logger log = Logger.getLogger(BatchDAO.class);
	static String DBSchemaLMS = PropertyFileReader.getInstance().getProperty("LMSname")+".";
	static String DBSchemaUCL = PropertyFileReader.getInstance().getProperty("UCLname")+".";
	static String trasp002_oid_to_fix = PropertyFileReader.getInstance().getProperty("trasp002_oid_to_fix");
	
protected static ArrayList<Enrollment> getEnrollmentsForConsultationWithManager(
	Connection conn)  
	throws SQLException, Exception
{
	
	ArrayList<Enrollment> enrollments = null;
	
	String query = 
		"SELECT  "
/*		+ " CASE when u.user_id is not null and is_number(substr(u.user_id,3))=1  " +
				"then TO_NUMBER(substr(u.user_id,3)) " +
				"else 0 " +
				"end as NUM_ID, "
*/
		+ "e.OID as ENROLLMENT_OID, "
		+ "CASE when u.user_id is not null " + 
			" then u.user_id " +
			" else '' " +
			" end as NUM_ID, "
				
		+"	nvl(( SELECT aer.REQUEST_DATE 	"
		+"		         FROM "+DBSchemaLMS+"approvalrequest a,  	"                                                     
		+"		              "+DBSchemaUCL+"APPROVED_ENROLL_REQUEST aer  	"                                                              
		+"		            WHERE a.enrollment_oid=e.oid        	"                                                     
		+"		              AND aer.ENROLLMENT_OID=e.oid ),'') AS REQUEST_DATE, "
		
		//+"	nvl(( SELECT ar.DATEREQUEST 	"
		//+"		         FROM "+DBSchemaLMS+"approvalrequest a,  	"                                                     
		//+"		              "+DBSchemaUCL+"approval_request ar  	"                                                              
		//+"		            WHERE a.enrollment_oid=e.oid        	"                                                     
		//+"		              AND ar.oid=a.oid ),'') AS REQUEST_DATE, "
		
		+ "	initcap(u.FIRST_NAME) AS FIRST_NAME, "
		+ "	initcap(u.LAST_NAME) AS LAST_NAME, "
		+ "	initcap(LOWER(u.DISPLAY_NAME)) AS COMMON_NAME, "
		+ " lower(u.EMAIL_ADDRESS) AS USER_EMAIL, "
		+ " usrDetails.dirCode as CompetenceAreaCode, "
		+ " usrDetails.dir as CompetanceArea, "
	    + " usrDetails.marketCode as UnitCode, "
		+ " usrDetails.market as Unit, "
		+ " usrDetails.agencyCode as CompetenceCenterCode, "
	    + " usrDetails.agency as CompetenceCenter, "
	    
	    + " usrDetails.DR_RESP_NAME as CompetanceAreaResp, "
	    + " usrDetails.M_RESP_NAME as UnitResp, "
	    + " usrDetails.DISTINGUISHED_NAME as DISTINGUISHED_NAME, "
	    + " initcap(LOWER(u.DESCRIPTION)) AS FILIALE, "
		+ " c.code AS CATALOG_CODE, "
		+ "	ct.title_lower AS COURSE_TITLE, "
		+ "	ct.DESCRIPTION AS COURSE_DESCRIPTION, "
//		+ "	a.TYPE AS TYPE, "
		+ " (SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE c.OID = cfvo.REF_OID	AND cfo.oid = cfvo.customfield_oid	AND UPPER(cfo.NAME) LIKE UPPER('Modalit_ di espletamento') ) AS TYPE, "
		+ " a.CODE_STATE as CODE_STATE,"
		+ "	o.oid AS OFFERING_OID, "
		+ "	a.application_code AS APPLICATION_CODE, "
		+ "	e.ENROLLDATE AS ENROLLDATE, "
		+ "	o.startdate AS START_DATE, "
		+ "	o.enddate AS END_DATE, "
//		+ "	a.CODE_STATE AS CODE_STATE, "
		+ "	substr(UPPER(u.MANAGER ),5,7) AS MANAGER_ID, "
		+ " lower(usrDetails.MANAGER_EMAIL) AS MANAGER_EMAIL, "
		+ "	usrDetails.MANAGER_FIRSTNAME, "
		+ "	usrDetails.MANAGER_LASTNAME, "
//		+ " (CASE WHEN ((SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE o.OID = cfvo.REF_OID	AND cfo.oid = cfvo.customfield_oid	AND UPPER(cfo.NAME) LIKE UPPER('Fornitore') )) = 'UGIS' THEN 'INTERNO' ELSE 'ESTERNO' END) AS DIDATTICA, "
		+ " (CASE WHEN ((SELECT UPPER(VAL) FROM "+ DBSchemaLMS+"customfield cfo, " + DBSchemaLMS + "customfield_value cfvo WHERE c.OID = cfvo.REF_OID	AND cfo.oid = cfvo.customfield_oid	AND UPPER(cfo.NAME) LIKE UPPER('Int/Est') )) = 'INTERNO' THEN 'INTERNAL' ELSE 'EXTERNAL' END) AS DIDATTICA, "
		+ " o.city AS CITY, "
		+ "	u.IS_MANAGER AS IS_MANAGER , "
		+ " EH.REQUIRESMANAGERAPPROVAL AS REQUIRESMANAGERAPPROVAL, "
		+ " EH.REQUIRESAPPROVERAPPROVAL AS REQUIRESAPPROVERAPPROVAL, "
		+ " C.OID AS CATALOGENTRY_OID, "
		+ " TO_NUMBER(offering_cost.cost_for_user_2(e.OID))  AS COST_FOR_USER, "
		+" (SELECT "
		+ "	VAL  "
		+ "FROM "
		+ DBSchemaLMS+"customfield cfo, "
		+ DBSchemaLMS+"customfield_value cfvo  "
		+ " "
		+ "WHERE "
		+ "    c.OID = cfvo.REF_OID "
		+ "    AND cfo.oid = cfvo.customfield_oid  " 
		+ " AND UPPER(cfo.name) like UPPER('Int/Est') ) AS IntEst,   "
		+" (SELECT "
		+ "	VAL  "
		+ "FROM "
		+ DBSchemaLMS+"customfield cfo, "
		+ DBSchemaLMS+"customfield_value cfvo  "
		+ " "
		+ "WHERE "
		+ "    o.OID = cfvo.REF_OID "
		+ "    AND cfo.oid = cfvo.customfield_oid  " 
		+ " AND UPPER(cfo.name) like UPPER('Fornitore') ) AS FORNITORE,  "
		
		+ " nvl(       "                                                                                                                         
		+ "	     (   SELECT 	nvl(SUM( "
     	+ "						case when status='A'  "
     	+ "						then 0 "
     	+ "						else round( ((t2 - t1) * 24 * 60) + ((t4 - t3) * 24 * 60), 2) "
     	+ "						END "
     	+ "						)/*in minutes*/ / 450/*minutes in 7.5h*/, 0)    "
		+ "            FROM elvis_days2                                                                                                        "
		+ "           WHERE user_id = e.user_oid                                                                                               "
		+ "             AND offering_id = e.offering_oid                                                                                       "
		+ "        GROUP BY (user_id, offering_id)                                                                                             " 
	    + "      ),                                                                                                                            " 
	    + "      0 ) AS realDays,                                                                                                              " 
		
		+" (SELECT "
		+ "	VAL  "
		+ "FROM "
		+ DBSchemaLMS+"customfield cfo, "
		+ DBSchemaLMS+"customfield_value cfvo  "
		+ " "
		+ "WHERE "
		+ "    c.OID = cfvo.REF_OID "
		+ "    AND cfo.oid = cfvo.customfield_oid  " 
		+ " AND UPPER(cfo.name) like UPPER('Tipo Corso') ) AS TIPOLOGIA,  "
		
		+" (SELECT "
		+ "	VAL  "
		+ "FROM "
		+ DBSchemaLMS+"customfield cfo, "
		+ DBSchemaLMS+"customfield_value cfvo  "
		+ " "
		+ "WHERE "
		+ "    c.OID = cfvo.REF_OID "
		+ "    AND cfo.oid = cfvo.customfield_oid  " 
		+ " AND UPPER(cfo.name) like UPPER('Durata') ) AS DURATION,  "
		+ " DECODE(e.state,1,'Pending',3,'Refused',100,'Approved',110,'Completed') as STATE "
//		+ " ad.NOTE "
		+ "FROM "
		+ "	( "
		+"	SELECT	"
		+"	     u1.oid userOid,	"
		+"	     u1.user_id userId,	"
		+"		 u1.DISTINGUISHED_NAME DISTINGUISHED_NAME,"
		+"	     initcap(LOWER(u2.DISPLAY_NAME      )) MANAGER_NAME,	"
		+"	     initcap(LOWER(u2.LAST_NAME      )) MANAGER_LASTNAME,	"
		+"	     initcap(LOWER(u2.FIRST_NAME     )) MANAGER_FIRSTNAME,	"
		+"		 u2.EMAIL_ADDRESS as MANAGER_EMAIL,	"	
		+"	     sm.DR_COD dirCode,	"
		+"	     initcap(LOWER(sm.DIREZIONE_REGIONALE      )) dir,	"
		+"	     sm.M_COD marketCode,	"
		+"	     initcap(LOWER(sm.MERCATO)) market,	"
		+"	     sm.AG_COD agencyCode,	"
		+"	     initcap(LOWER(sm.AGENZIA)) agency, 	"
		+"	     initcap(LOWER(sm.DR_RESP_NAME)) DR_RESP_NAME,	"
		+"	     initcap(LOWER(sm.M_RESP_NAME)) M_RESP_NAME 	"
		+"	    FROM  "+DBSchemaLMS + "USR u1	"
		+"	left join  "+DBSchemaLMS + "usr u2 on upper(u2.USER_ID) = substr(UPPER(u1.MANAGER),5,7)	"
		+"	left join "+DBSchemaUCL +"STATISTICHE_matricole sm  on  u1.ldap_id = lower(matricola)	"
		+"	   	WHERE u1.active = 1 	"
		+"	   	AND nvl(u2.active,1) = 1 	"
		+"	 )  usrDetails, "
		
		+ "	" + DBSchemaUCL + "APPROVED_ENROLL_REQUEST a  "
		+ "	JOIN " + DBSchemaLMS + "ENROLLMENT e  "
		+ "	ON e.OID= a.ENROLLMENT_OID  "
		+ "	AND e.OFFERING_OID = a.OFFERING_OID  "
		+ " "
		+ "	JOIN " + DBSchemaLMS + "USR u  "
		+ "	ON e.USER_OID = u.OID  "
		+ "	AND a.USER_OID = e.USER_OID  "
		+ " "
		+ "	JOIN " + DBSchemaLMS + "CATALOGENTRY C  "
		+ "	ON e.CATALOGENTRY_OID = C.OID  "
		+ " "
		+ "	JOIN " + DBSchemaLMS + "CATALOGENTRY_TEXT ct  "
		+ "	ON ct.CATALOGENTRY_OID = e.CATALOGENTRY_OID  "
		+ " "
		+ "	JOIN " + DBSchemaLMS + "OFFERING o  "
		+ "	ON e.OFFERING_OID = o.OID  "
	
		+ " JOIN " + DBSchemaLMS + "CUSTOMFIELD_VALUE cfv "
		+ " ON cfv.REF_OID= o.OID "
		+ "  "
		+ " JOIN " + DBSchemaLMS + "CUSTOMFIELD CF "
		+ " ON CF.OID=CFV.CUSTOMFIELD_OID "
		+ " AND CF.NAME LIKE 'Societ%' "
		+ " AND UPPER (CFV.VAL)  LIKE UPPER('US%') "
		
		+ " JOIN " + DBSchemaLMS + "ENROLLABLEHELPER EH "
		+ " ON EH.CATALOGENTRY_OID=C.OID "
		
//		+ " JOIN " + DBSchemaUCL + "APPROVED_ENROLL_REQ_DETAILS ad "
//		+ " ON ad.ENROLLMENT_OID=e.OID "
		
		+ " "
		+ "WHERE "
		+ "usrDetails.userOid = u.OID    "       
		+ "	AND o.STATUS = 2  "
		+ " AND u.ACTIVE = 1  "
		+ "	AND e.STATE IN (1,3,100,110) "
//		+ " and e.STATE <> 90 "
		+ " and a.CREATED_FROM = 'MD' "
		+ " and TO_CHAR(e.ENROLLDATE,'YYYY') = TO_CHAR(sysdate,'YYYY')  "
//		+ " and TO_CHAR(e.ENROLLDATE,'YYYY') = '2010' "
		+ " and u.ORGANIZATION in ( 'US','UO') "
//		+ " and upper(c.code) in ('APLACBI01-1', 'APLACBI02-1-1', 'APLEBI001-1', 'APLPOWBI001-1') "
		+ " order by REQUEST_DATE desc, USER_ID, CATALOG_CODE ";

	log.debug("QUERY:" + query);
	PreparedStatement ps = null;
	ResultSet rs = null;
	try 
	{
		log.debug("Please Wait...");	
		ps = conn.prepareStatement(query);
		
		
		rs = ps.executeQuery();
		enrollments = new ArrayList<Enrollment>();
		while (rs.next()) 
		{			
			Enrollment enrollment = new Enrollment();
			
			enrollment.setENROLLMENT_OID(rs.getString("ENROLLMENT_OID").trim());
			enrollment.setNUM_ID(rs.getString("NUM_ID").trim());
			enrollment.setREQUEST_DATE(rs.getDate("REQUEST_DATE"));
			enrollment.setCATALOG_CODE(rs.getString("CATALOG_CODE").trim());
			enrollment.setENROLLDATE(rs.getDate("ENROLLDATE"));
			enrollment.setSTART_DATE(rs.getDate("START_DATE"));
			enrollment.setEND_DATE(rs.getDate("END_DATE"));
			enrollment.setCOURSE_TITLE(rs.getString("COURSE_TITLE"));
			enrollment.setCOURSE_DESCRIPTION(rs.getString("COURSE_DESCRIPTION"));
			enrollment.setFIRST_NAME(rs.getString("FIRST_NAME"));
			enrollment.setLAST_NAME(rs.getString("LAST_NAME"));
			enrollment.setUSER_EMAIL(rs.getString("USER_EMAIL"));
			enrollment.setFILIALE(rs.getString("FILIALE"));
			enrollment.setMANAGER_ID(rs.getString("MANAGER_ID"));
			enrollment.setMANAGER_FIRSTNAME(rs.getString("MANAGER_FIRSTNAME"));
			enrollment.setMANAGER_LASTNAME(rs.getString("MANAGER_LASTNAME"));
			enrollment.setMANAGER_EMAIL(rs.getString("MANAGER_EMAIL"));
			enrollment.setTYPE(rs.getString("TYPE"));
			enrollment.setOFFERING_OID(rs.getString("OFFERING_OID"));
			enrollment.setCATALOGENTRY_OID(rs.getString("CATALOGENTRY_OID"));
			enrollment.setCITY(rs.getString("CITY"));			
			enrollment.setREQUIRES_MANAGER_APPROVAL(rs.getInt("REQUIRESMANAGERAPPROVAL"));
			enrollment.setREQUIRES_APPROVER_APPROVAL(rs.getInt("REQUIRESAPPROVERAPPROVAL"));
			enrollment.setDIDATTICA(rs.getString("DIDATTICA"));
			enrollment.setCODE_STATE(rs.getString("CODE_STATE"));			
			enrollment.setCompetenceAreaCode(rs.getString("CompetenceAreaCode"));
			enrollment.setCompetanceArea(rs.getString("CompetanceArea"));			
			enrollment.setUnitCode(rs.getString("UnitCode"));
			enrollment.setUnit(rs.getString("Unit"));			
			enrollment.setCompetenceCenterCode(rs.getString("CompetenceCenterCode"));
			enrollment.setCompetenceCenter(rs.getString("CompetenceCenter"));			
			enrollment.setCompetanceAreaResp(rs.getString("CompetanceAreaResp"));
			enrollment.setUnitResp(rs.getString("UnitResp"));			    
			enrollment.setState(rs.getString("STATE"));
//			enrollment.setNote(rs.getString("NOTE"));
			enrollment.setRealDays(rs.getDouble("realDays"));
			enrollment.setDISTINGUISHED_NAME(rs.getString("DISTINGUISHED_NAME"));
			
			CustomField cf = new CustomField();
			cf.setCost(rs.getString("COST_FOR_USER"));
			cf.setSupplier(rs.getString("FORNITORE"));
			cf.setTipologia(rs.getString("TIPOLOGIA"));
			cf.setSource(rs.getString("IntEst"));
			cf.setDuration(rs.getString("DURATION"));
			enrollment.setCustomField(cf);
			
			enrollments.add(enrollment);
		}
		
	}
	catch (SQLException sex) 
	{
		log.debug("SQLException: " + sex.getMessage());
		throw(sex);
	} 
	catch (Exception ex) 
	{
		log.debug("Exception: " + ex.getMessage());
		throw(ex);
	} 	  
	finally{
		if (rs != null)
			rs.close();
		if (ps != null)
			ps.close();
	} 
	return enrollments;
}

static public int getEnrolledUsersSize( String offeringOid, Connection conn ) throws  Exception,SQLException {
	
	int size = 0;
	StringBuffer query = new StringBuffer(); 

	query.append("SELECT " );
	query.append("	count(u.USER_ID) as USER_COUNT " ); 
	query.append("										" ); 
	query.append("FROM " + DBSchemaLMS + "USR u	" ); 
	query.append("										" ); 
	query.append("JOIN	" + DBSchemaUCL + "APPROVED_ENROLL_REQUEST aer	" );
	query.append("ON aer.USER_OID = u.OID ");
	query.append("										" ); 
	query.append("JOIN " + DBSchemaLMS + "ENROLLMENT e	" ); 
	query.append("ON e.USER_OID = u.OID ");
	query.append("AND aer.OFFERING_OID = e.OFFERING_OID ");
	query.append("and aer.ENROLLMENT_OID = e.OID ");
	query.append("									" ); 
	query.append("WHERE	" );
	query.append("u.ACTIVE = 1 	" );
	query.append("and e.STATE <> 90 ");
	query.append("and (e.STATE >= 100 or (e.STATE=1 AND aer.START_DATE = to_date( '31/12/2099', 'dd/MM/yyyy' ) ) ) ");
	query.append("and e.OFFERING_OID = ? "); 

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
		ps = conn.prepareStatement(query.toString());
		ps.setString(1,offeringOid);
		//log.debug(query.toString());
		
		rs = ps.executeQuery();
		
		
		if ( rs.next() ) {

			size = rs.getInt("USER_COUNT");

		}	
				
		}catch (SQLException sex) 
		{
			log.debug("SQLException in getEnrolledUsersSize: " + sex.getMessage());
			throw(sex);
		} 
		catch (Exception ex) 
		{
			log.debug("Exception in getEnrolledUsersSize: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}
		
		return size;
}

/**
 * 
 * @return getEnrollmentsForConsultationWithManager   
 * @throws SQLException
 */
protected static ArrayList<Supplier> getSumOfCostForSuppliersSentToMetamorfosiInCurrentYear(
	Connection conn)  
	throws SQLException, Exception
{
	
	ArrayList<Supplier> suppliers = null;
	
	String query = 
		"SELECT "
		+ "cfv1.VAL 				as supplier, "
		+ "SUM( TO_NUMBER ( offering_cost.cost_for_user(e.OID))  ) as cost "
		+ " "
		+ "FROM "
		+ DBSchemaUCL+"APPROVED_ENROLL_REQUEST a  "
		+ " "
		+ "	JOIN "+ DBSchemaLMS+"ENROLLMENT e  "
		+ "	ON e.OID= a.ENROLLMENT_OID  "
		+ "	AND e.OFFERING_OID = a.OFFERING_OID  "
		+ "	 "
		+ " JOIN "+ DBSchemaLMS+"CATALOGENTRY C  "
		+ "	ON e.CATALOGENTRY_OID = C.OID  "
		+ "	 "
		+ "  JOIN "+ DBSchemaLMS+"OFFERING o  "
		+ "	ON e.OFFERING_OID = o.OID  "
		+ " "
		

		+ " JOIN "+ DBSchemaUCL+"ELVIS_ANAG_MODULES elvis  "
		+ "	ON elvis.OFFERING_ID=o.OID "
		+ " "
		+ "	JOIN "+ DBSchemaLMS+"CUSTOMFIELD_VALUE cfv  "
		+ "	ON cfv.REF_OID= o.OID  "
		+ " "
		+ "	JOIN "+ DBSchemaLMS+"CUSTOMFIELD CF  "
		+ "	ON CF.OID=CFV.CUSTOMFIELD_OID  "
		+ "	AND CF.NAME LIKE 'Societ%'  "
		+ "	AND UPPER (CFV.VAL) LIKE UPPER('US%')  "
		+ " "
		+ " "
		+ " "
		+ " JOIN "+ DBSchemaLMS+"CUSTOMFIELD_VALUE cfv1  "
		+ "	ON cfv1.REF_OID=o.OID  "
		+ " "
		+ "	JOIN "+ DBSchemaLMS+"CUSTOMFIELD CF1  "
		+ "	ON CF1.OID=CFV1.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF1.NAME) LIKE UPPER('Fornitore%')  "
		+ " AND CFV1.VAL <> '-' "
		+ " AND CFV1.VAL is not null "
		+ " "
//TODO Dla pewnosci
		+ " JOIN "+ DBSchemaLMS+"CUSTOMFIELD_VALUE cfv2  "
		+ "	ON cfv2.REF_OID= o.OID  "
		+ " "
		+ "	JOIN "+ DBSchemaLMS+"CUSTOMFIELD CF2  "
		+ "	ON CF2.OID=CFV2.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF2.NAME) LIKE UPPER('Costo')  "

		+ " "
		+ "WHERE "
		+ "	o.STATUS = 2  "
		+ "	AND a.CODE_STATE IN (100,110,200,400) "
		+ " and e.STATE <> 90 "
		
		+ " and elvis.CREATION='Y' "
		+ " and to_char(elvis.SEND_TO_METAMORFOSI,'YYYY') = to_char(sysdate,'YYYY') "
		+ " "
		+ "GROUP BY cfv1.VAL "
		+ "order by supplier ";
		

	
	log.debug("QUERY:" + query);
	PreparedStatement ps = null;
	ResultSet rs = null;	
	try 
	{
		
		ps = conn.prepareStatement(query);
		rs = ps.executeQuery();
		
		log.debug("Please Wait");	
		suppliers = new ArrayList<Supplier>();
		while (rs.next()) 
		{			
			Supplier supplier = new Supplier();
			
			supplier.setSuplier(rs.getString("supplier"));
			supplier.setCost(rs.getFloat("cost"));
			
			
			suppliers.add(supplier);
		}
		
	}
	catch (SQLException sex) 
	{
		log.debug("SQLException: " + sex.getMessage());
		throw(sex);
	} 
	catch (Exception ex) 
	{
		log.debug("Exception: " + ex.getMessage());
		throw(ex);
	} 	  
	finally{
		if (rs != null)
			rs.close();
		if (ps != null)
			ps.close();
	}  
	return suppliers;
}


/**
 * 
 * @return getSumOfCostForSuppliersFromCurrentMonth   
 * @throws SQLException
 */
protected static ArrayList<Supplier> getSumOfCostForSuppliersFromCurrentMonth(
	Connection conn)  
	throws SQLException, Exception
{
	
	ArrayList<Supplier> suppliers = null;
	
	
		
String query =	
		"SELECT  "
		+ "costNotSum.supplier as supplier, "
		+ "SUM( costNotSum.cost ) AS cost  "
		+ " "
		+ "FROM  "
		+ "(SELECT "
		+ "	cfv1.VAL AS supplier, "
		+ "	TO_NUMBER( offering_cost.cost_for_user(e.OID))  AS cost, "
		+ " e.OID "
		+ "FROM "
		+ "	"+ DBSchemaUCL+"APPROVED_ENROLL_REQUEST a  "
		+ "	JOIN "+DBSchemaLMS+"ENROLLMENT e  "
		+ "	ON e.OID= a.ENROLLMENT_OID  "
		+ "	AND e.OFFERING_OID = a.OFFERING_OID  "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"CATALOGENTRY C  "
		+ "	ON e.CATALOGENTRY_OID = C.OID  "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"OFFERING o  "
		+ "	ON e.OFFERING_OID = o.OID  "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv  "
		+ "	ON cfv.REF_OID= o.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF  "
		+ "	ON CF.OID=CFV.CUSTOMFIELD_OID  "
		+ "	AND CF.NAME LIKE 'Societ%'  "
		+ "	AND UPPER (CFV.VAL) LIKE UPPER('US%')  "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv1  "
		+ "	ON cfv1.REF_OID=o.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF1  "
		+ "	ON CF1.OID=CFV1.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF1.NAME) LIKE UPPER('Fornitore%')  "
		+ "	AND CFV1.VAL <> '-'  "
		+ "	AND CFV1.VAL IS NOT NULL  "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv2  "
		+ "	ON cfv2.REF_OID= o.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF2  "
		+ "	ON CF2.OID=CFV2.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF2.NAME) LIKE UPPER('Costo')  "
		+ "		 "
		+ " JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv3  "
		+ "	ON cfv3.REF_OID= c.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF3  "
		+ "	ON CF3.OID=CFV3.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF3.NAME) LIKE UPPER('Int/Est')  "
		+ "WHERE "
		+ "	o.STATUS = 2  "
		+ "	AND UPPER(cfv3.VAL) = UPPER('Esterno')  "
		+ "	AND a.CODE_STATE IN (100,200,400)  "
		+ "	AND e.STATE <> 90  "
		+ "	AND TO_CHAR(o.STARTDATE,'YYYY') = TO_CHAR(sysdate,'YYYY')  "
		+ "	AND to_number(TO_CHAR(o.STARTDATE,'MM')) >= to_number(to_char(sysdate,'MM'))  "
		+ " "
		+ " "
		+ " "
		+ "UNION "
		+ " "
		+ "SELECT "
		+ "	cfv1.VAL AS supplier, "
		+ "	TO_NUMBER( cfv2.val)  AS cost, "
		+ " o.OID "
		+ "FROM "
		+ "	 "+DBSchemaLMS+"CATALOGENTRY c "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"OFFERING o  "
		+ "	ON c.OID = o.CATALOGENTRY_OID "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv  "
		+ "	ON cfv.REF_OID= o.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF  "
		+ "	ON CF.OID=CFV.CUSTOMFIELD_OID  "
		+ "	AND CF.NAME LIKE 'Societ%'  "
		+ "	AND UPPER (CFV.VAL) LIKE UPPER('US%')  "
		+ "	 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv1  "
		+ "	ON cfv1.REF_OID=o.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF1  "
		+ "	ON CF1.OID=CFV1.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF1.NAME) LIKE UPPER('Fornitore%')  "
		+ "	AND CFV1.VAL <> '-'  "
		+ "	AND CFV1.VAL IS NOT NULL  "
		+ "		 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv2  "
		+ "	ON cfv2.REF_OID= o.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF2  "
		+ "	ON CF2.OID=CFV2.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF2.NAME) LIKE UPPER('Costo')  "
		+ "	AND cfv2.val is not null "
		+ "	and is_number(cfv2.val)=1 "
		+ "		 "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD_VALUE cfv3  "
		+ "	ON cfv3.REF_OID= c.OID  "
		+ "	JOIN "+DBSchemaLMS+"CUSTOMFIELD CF3  "
		+ "	ON CF3.OID=CFV3.CUSTOMFIELD_OID  "
		+ "	AND UPPER(CF3.NAME) LIKE UPPER('Int/Est')  "
		+ " "
		+ "WHERE "
		+ "	o.STATUS = 2  "
		+ "	AND UPPER(cfv3.VAL) = UPPER('Interno')  "
		+ "	AND TO_CHAR(o.STARTDATE,'YYYY') = TO_CHAR(sysdate,'YYYY')  "
		+ "	AND to_number(TO_CHAR(o.STARTDATE,'MM')) >= to_number(to_char(sysdate,'MM')) "
		+ " "
		+ ") costNotSum "
		+ " "
		+ "GROUP BY "
		+ "	costNotSum.supplier "
		+ "ORDER BY "
		+ "	costNotSum.supplier ";
	
	log.debug("QUERY:" + query);
	PreparedStatement ps = null;
	ResultSet rs = null;	
	try 
	{
		
		ps = conn.prepareStatement(query);
		rs = ps.executeQuery();
		log.debug("Please Wait");	
		
		suppliers = new ArrayList<Supplier>();
		while (rs.next()) 
		{			
			Supplier supplier = new Supplier();
			
			supplier.setSuplier(rs.getString("supplier"));
			supplier.setCost(rs.getFloat("cost"));
			
			
			suppliers.add(supplier);
		}
		
	}
	catch (SQLException sex) 
	{
		log.debug("SQLException: " + sex.getMessage());
		throw(sex);
	} 
	catch (Exception ex) 
	{
		log.debug("Exception: " + ex.getMessage());
		throw(ex);
	} 	  
	finally{
		if (rs != null)
			rs.close();
		if (ps != null)
			ps.close();
	} 
	return suppliers;
}


protected static CustomField retrivenValuesFromCustomField(String offeringOid,String catalogEntryOid,Connection conn)  
		throws SQLException, Exception
	{
		
		CustomField cf = null;
		
		String query =
		"SELECT "
		+ "	NAME, "
		+ "	VAL  "
		+ "FROM "
		+ DBSchemaLMS+" offering o, "
		+ DBSchemaLMS+"customfield cfo, "
		+ DBSchemaLMS+"customfield_value cfvo  "
		+ " "
		+ "WHERE "
		+ "    o.OID = cfvo.REF_OID "
		+ "    AND cfo.oid = cfvo.customfield_oid  "
		+ "	AND o.OID= '"+ offeringOid+"' "
		+ " "
		+ "UNION "
		+ " "
		+ "SELECT "
		+ "	NAME, "
		+ "	VAL  "
		+ "FROM "
		+ " "
		+ DBSchemaLMS+"CATALOGENTRY c, "
		+ DBSchemaLMS+"customfield cfc, "
		+ DBSchemaLMS+"customfield_value cfvc  "
		+ "WHERE "
		+ "    c.OID = cfvc.REF_OID "
		+ "    AND cfc.oid = cfvc.customfield_oid  "
		+ "	AND c.OID= '"+catalogEntryOid+"' ";

		
		//log.debug("Query:" + query.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;	
		try 
		{
			
			ps = conn.prepareStatement(query.toString());
			
			
			rs = ps.executeQuery();
			cf = new CustomField();
			
			while ( rs.next() ) {
				
				String customFieldName = rs.getString( "NAME" ); 
				String customFieldValue = rs.getString( "VAL" );
				
				if ( customFieldName.equalsIgnoreCase( "GeFo_status" ) && customFieldValue != null ) {
					cf.setState( customFieldValue );
				}
				
				if ( customFieldName.equalsIgnoreCase( "Fornitore" ) && customFieldValue != null ) {
					cf.setSupplier( customFieldValue );
				}
				
				if ( customFieldName.startsWith( "Societ" ) && customFieldName.length() == 7 && customFieldValue != null ) {
					cf.setSocieta( customFieldValue );
				}
				if ( customFieldName.startsWith( "Costo" ) && customFieldValue != null ) {
					cf.setCost( customFieldValue );
				}	
				if ( customFieldName.startsWith( "Int/Est" ) && customFieldValue != null ) {
					cf.setSource( customFieldValue );
				}
			
				if ( customFieldName.equalsIgnoreCase( "Societ" ) && customFieldName.length()>7 && customFieldValue != null ) {
					//cf.setState( customFieldValue );
				}
				
				if ( customFieldName.startsWith( "Competenza" ) && customFieldName.length() == 7 && customFieldValue != null ) {
					//cf.setSocieta( customFieldValue );
				}
				if ( customFieldName.startsWith( "Area Tematica" ) && customFieldValue != null ) {
					//cf.setCost( customFieldValue );
				}	
				if ( customFieldName.startsWith( "DURATA" ) && customFieldValue != null ) {
					cf.setDuration( customFieldValue );
				}
				
				if ( customFieldName.startsWith( "Modalita di espletamento" ) && customFieldValue != null ) {
					//cf.setTipologia( customFieldValue );
				}
				if ( customFieldName.startsWith( "Ente Finanziatore" ) && customFieldValue != null ) {
					//cf.setTipologia( customFieldValue );
				}
				
				if ( customFieldName.startsWith( "Tipo Corso" ) && customFieldValue != null ) {
					cf.setTipologia( customFieldValue );
				}
				
					
			}		
			
		}
		catch (SQLException sex) 
		{
			log.debug("SQLException: " + sex.getMessage());
			throw(sex);
		} 
		catch (Exception ex) 
		{
			log.debug(" xception: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}  
		return cf;
	}	



protected static ArrayList<Enrollment> getEnrollmentsSentToMetamrfosiFromLastWeek(
	Connection conn)  
	throws SQLException, Exception
{
	
	ArrayList<Enrollment> enrollments = null;
	
	String query = 
		"select * from ("
		+" SELECT "
		+ "	CASE when u.user_id is not null and is_number(substr(u.user_id,3))=1  " +
				"then TO_NUMBER(substr(u.user_id,3)) " +
				"else 0 " +
				"end as USER_ID, "
		+ "	initcap(u.FIRST_NAME) AS FIRST_NAME, "
		+ "	initcap(u.LAST_NAME) AS LAST_NAME, "
		+ "	C.code AS CATALOG_CODE, "
		+ "	ct.title_lower AS COURSE_TITLE, "
		+ "	ct.DESCRIPTION AS COURSE_DESCRIPTION, "
		+ "	a.TYPE AS TYPE, "
		+ "	o.oid AS OFFERING_OID, "
		+ "	e.ENROLLDATE AS ENROLLDATE, "
		+ "	o.startdate AS START_DATE, "
		+ "	a.CODE_STATE AS CODE_STATE, "
		+ "	o.city AS CITY, "
		+ "	C.OID AS CATALOGENTRY_OID, "
		+ " TO_NUMBER(offering_cost.cost_for_user(e.OID))  AS COST_FOR_USER, "
		
		+ " nvl(       "                                                                                                                         
		+ "	     (   SELECT 	nvl(SUM( "
     	+ "						case when status='A'  "
     	+ "						then 0 "
     	+ "						else round( ((t2 - t1) * 24 * 60) + ((t4 - t3) * 24 * 60), 2) "
     	+ "						END "
     	+ "						)/*in minutes*/ / 450/*minutes in 7.5h*/, 0)    "
		+ "            FROM elvis_days2                                                                                                        "
		+ "           WHERE user_id = e.user_oid                                                                                               "
		+ "             AND offering_id = e.offering_oid                                                                                       "
		+ "        GROUP BY (user_id, offering_id)                                                                                             " 
	    + "      ),                                                                                                                            " 
	    + "      0 ) AS realDays,                                                                                                              "
		
		+ "	(SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE o.OID = cfvo.REF_OID	AND cfo.oid = cfvo.customfield_oid	AND UPPER(cfo.NAME) LIKE UPPER('Fornitore') ) AS FORNITORE, "
		+ "	(SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE C.OID = cfvo.REF_OID AND cfo.oid = cfvo.customfield_oid 	AND UPPER(cfo.NAME) LIKE UPPER('Tipo Corso') ) AS TIPOLOGIA, "
		+ "	(SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE C.OID = cfvo.REF_OID AND cfo.oid = cfvo.customfield_oid  AND UPPER(cfo.NAME) LIKE UPPER('Durata') ) AS DURATA , "
		+ " (SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE C.OID = cfvo.REF_OID AND cfo.oid = cfvo.customfield_oid  AND UPPER(cfo.NAME) LIKE UPPER('Area Tematica') ) AS AREA_TEMATICA_DESC,  "
		+ " (SELECT elvis_tematica.CD_AREA FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo,"+DBSchemaUCL+"ELVIS_AREE_TEMATICHE elvis_tematica WHERE C.OID = cfvo.REF_OID AND cfo.oid = cfvo.customfield_oid  AND UPPER(cfo.NAME) LIKE UPPER('Area Tematica') and UPPER(elvis_tematica.DESCR)=UPPER(VAL) ) AS AREA_TEMATICA_CODE,  "
		+ " (SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE c.OID = cfvo.REF_OID AND cfo.oid = cfvo.customfield_oid  AND UPPER(cfo.NAME) LIKE UPPER('Int/Est') ) AS IntEst,  "
		+ " elvis.SEND_TO_METAMORFOSI  AS SEND_TO_METAMORFOSI,  "
		+ " (CASE WHEN ((SELECT VAL FROM "+ DBSchemaLMS+"customfield cfo, "+ DBSchemaLMS+"customfield_value cfvo WHERE o.OID = cfvo.REF_OID	AND cfo.oid = cfvo.customfield_oid	AND UPPER(cfo.NAME) LIKE UPPER('Fornitore') )) = 'UGIS' THEN 'INTERNO' ELSE 'ESTERNO' END) AS DIDATTICA, "
		+ " EH.REQUIRESMANAGERAPPROVAL AS REQUIRESMANAGERAPPROVAL "
		
		+ "FROM "
		+ "	"+ DBSchemaUCL+"APPROVED_ENROLL_REQUEST a  "
		+ " "
		+ "    JOIN "+ DBSchemaLMS+"ENROLLMENT e  "
		+ "    ON e.OID= a.ENROLLMENT_OID  "
		+ "    AND e.OFFERING_OID = a.OFFERING_OID  "
		+ " "
		+ "    JOIN "+ DBSchemaLMS+"USR u  "
		+ "    ON e.USER_OID = u.OID  "
		+ "    AND a.USER_OID = e.USER_OID  "
		+ " "
		+ "    JOIN "+ DBSchemaLMS+"CATALOGENTRY C  "
		+ "    ON e.CATALOGENTRY_OID = C.OID  "
		+ " "
		+ "    JOIN "+ DBSchemaLMS+"CATALOGENTRY_TEXT ct  "
		+ "    ON ct.CATALOGENTRY_OID = e.CATALOGENTRY_OID  "
		+ " "
		+ "    JOIN "+ DBSchemaLMS+"OFFERING o  "
		+ "    ON e.OFFERING_OID = o.OID  "
		+ " "
		+ "    JOIN "+ DBSchemaLMS+"CUSTOMFIELD_VALUE cfv  "
		+ "    ON cfv.REF_OID= o.OID  "
		+ "    JOIN "+ DBSchemaLMS+"CUSTOMFIELD CF  "
		+ "    ON CF.OID=CFV.CUSTOMFIELD_OID  "
		+ "    AND CF.NAME LIKE 'Societ%'  "
		+ "    AND UPPER (CFV.VAL) LIKE UPPER('US%')  "
		+ " "
		+ " "
		+ "    JOIN "+ DBSchemaUCL+"ELVIS_ANAG_MODULES elvis "
		+ "    ON elvis.OFFERING_ID=o.OID "
		
		+ " "
		+ " JOIN " + DBSchemaLMS + "ENROLLABLEHELPER EH "
		+ " ON EH.CATALOGENTRY_OID=C.OID "
		+ " "
		+ "WHERE "
		+ "	o.STATUS = 2  "
		+ "	AND u.ACTIVE = 1  "
		+ "	AND a.CODE_STATE IN (100,110,200,400)  "
		+ " and e.STATE <> 90 "
		+ "	and elvis.SEND_TO_METAMORFOSI >= sysdate - 7  "
		+ " and elvis.CREATION='Y' "
		
		+" UNION "
		
	// people from societa U9 only from search base o=US,dc=unicredito,dc=it
		+ "SELECT "
		+ "	CASE "
		+ "		WHEN u.user_id IS NOT NULL "
		+ "		AND is_number(substr(u.user_id,3))=1 "
		+ "		THEN TO_NUMBER(substr(u.user_id,3)) "
		+ "		ELSE 0 "
		+ "	END AS USER_ID,"
		+ "	initcap(u.FIRST_NAME) AS FIRST_NAME,"
		+ "	initcap(u.LAST_NAME) AS LAST_NAME,"
		+ "	C.code AS CATALOG_CODE,"
		+ "	ct.title_lower AS COURSE_TITLE,"
		+ "	ct.DESCRIPTION AS COURSE_DESCRIPTION,"
		+ "	a.TYPE AS TYPE,"
		+ "	o.oid AS OFFERING_OID,"
		+ "	e.ENROLLDATE AS ENROLLDATE,"
		+ "	o.startdate AS START_DATE,"
		+ "	a.CODE_STATE AS CODE_STATE,"
		+ "	o.city AS CITY,"
		+ "	C.OID AS CATALOGENTRY_OID,"
		+ "	TO_NUMBER(offering_cost.cost_for_user(e.OID)) AS COST_FOR_USER,"
		+ "	nvl( (	SELECT	"
		+ "					nvl(SUM(	"
        + "						case when status='A'  "
        + "						then 0	"
        + "						else round( ((t2 - t1) * 24 * 60) + ((t4 - t3) * 24 * 60), 2) "
        + "						end	"
        + "					)/*in minutes*/ / 450/*minutes in 7.5h*/, 0) "
		+ "			FROM"
		+ "				elvis_days2 "
		+ "			WHERE"
		+ "				user_id = e.user_oid "
		+ "				AND offering_id = e.offering_oid "
		+ "			GROUP BY"
		+ "				(user_id,"
		+ "				offering_id) ), 0 ) AS realDays,"
		+ "	(	SELECT"
		+ "			VAL "
		+ "		FROM"
		+ "			"+ DBSchemaLMS+"customfield cfo,"
		+ "			"+ DBSchemaLMS+"customfield_value cfvo "
		+ "		WHERE"
		+ "			o.OID = cfvo.REF_OID "
		+ "			AND cfo.oid = cfvo.customfield_oid "
		+ "			AND UPPER(cfo.NAME) LIKE UPPER('Fornitore') "
		+ "	)"
		+ "	AS FORNITORE,"
		+ "	(	SELECT"
		+ "			VAL "
		+ "		FROM"
		+ "			"+ DBSchemaLMS+"customfield cfo,"
		+ "			"+ DBSchemaLMS+"customfield_value cfvo "
		+ "		WHERE"
		+ "			C.OID = cfvo.REF_OID "
		+ "			AND cfo.oid = cfvo.customfield_oid "
		+ "			AND UPPER(cfo.NAME) LIKE UPPER('Tipo Corso') "
		+ "	)"
		+ "	AS TIPOLOGIA,"
		+ "	(	SELECT"
		+ "			VAL "
		+ "		FROM"
		+ "			"+ DBSchemaLMS+"customfield cfo,"
		+ "			"+ DBSchemaLMS+"customfield_value cfvo "
		+ "		WHERE"
		+ "			C.OID = cfvo.REF_OID "
		+ "			AND cfo.oid = cfvo.customfield_oid "
		+ "			AND UPPER(cfo.NAME) LIKE UPPER('Durata') "
		+ "	)"
		+ "	AS DURATA ,"
		+ "	(	SELECT"
		+ "			VAL "
		+ "		FROM"
		+ "			"+ DBSchemaLMS+"customfield cfo,"
		+ "			"+ DBSchemaLMS+"customfield_value cfvo "
		+ "		WHERE"
		+ "			C.OID = cfvo.REF_OID "
		+ "			AND cfo.oid = cfvo.customfield_oid "
		+ "			AND UPPER(cfo.NAME) LIKE UPPER('Area Tematica') "
		+ "	)"
		+ "	AS AREA_TEMATICA_DESC,"
		+ "	(	SELECT"
		+ "			elvis_tematica.CD_AREA "
		+ "		FROM"
		+ "			"+ DBSchemaLMS+"customfield cfo,"
		+ "			"+ DBSchemaLMS+"customfield_value cfvo,"
		+ "			"+ DBSchemaUCL+"ELVIS_AREE_TEMATICHE elvis_tematica "
		+ "		WHERE"
		+ "			C.OID = cfvo.REF_OID "
		+ "			AND cfo.oid = cfvo.customfield_oid "
		+ "			AND UPPER(cfo.NAME) LIKE UPPER('Area Tematica') "
		+ "			AND UPPER(elvis_tematica.DESCR)=UPPER(VAL) "
		+ "	)"
		+ "	AS AREA_TEMATICA_CODE,"
		+ "	(	SELECT"
		+ "			VAL "
		+ "		FROM"
		+ "			"+ DBSchemaLMS+"customfield cfo,"
		+ "			"+ DBSchemaLMS+"customfield_value cfvo "
		+ "		WHERE"
		+ "			C.OID = cfvo.REF_OID "
		+ "			AND cfo.oid = cfvo.customfield_oid "
		+ "			AND UPPER(cfo.NAME) LIKE UPPER('Int/Est') "
		+ "	)"
		+ "	AS IntEst,"
		+ "	elvis.SEND_TO_METAMORFOSI AS SEND_TO_METAMORFOSI,"
		+ "	("
		+ "	CASE "
		+ "		WHEN ((	SELECT"
		+ "					VAL "
		+ "				FROM"
		+ "					"+ DBSchemaLMS+"customfield cfo,"
		+ "					"+ DBSchemaLMS+"customfield_value cfvo "
		+ "				WHERE"
		+ "					o.OID = cfvo.REF_OID "
		+ "					AND cfo.oid = cfvo.customfield_oid "
		+ "					AND UPPER(cfo.NAME) LIKE UPPER('Fornitore') "
		+ "		)"
		+ "		) = 'UGIS' "
		+ "		THEN 'INTERNO' "
		+ "		ELSE 'ESTERNO' "
		+ "	END) AS DIDATTICA,"
		+ "	EH.REQUIRESMANAGERAPPROVAL AS REQUIRESMANAGERAPPROVAL "
		+ "FROM"
		+ "        "+ DBSchemaUCL+"APPROVED_ENROLL_REQUEST a "
		+ "		JOIN "+ DBSchemaLMS+"ENROLLMENT e "
		+ "		ON e.OID= a.ENROLLMENT_OID "
		+ "		AND e.OFFERING_OID = a.OFFERING_OID "
		+ ""
		+ "		JOIN "+ DBSchemaLMS+"USR u "
		+ "		ON e.USER_OID = u.OID "
		+ "		AND a.USER_OID = e.USER_OID "
		+ "        "
		+ "		JOIN "+ DBSchemaLMS+"CATALOGENTRY C "
		+ "		ON e.CATALOGENTRY_OID = C.OID "
		+ "		JOIN "+ DBSchemaLMS+"CATALOGENTRY_TEXT ct "
		+ "		ON ct.CATALOGENTRY_OID = e.CATALOGENTRY_OID "
		+ "		JOIN "+ DBSchemaLMS+"OFFERING o "
		+ "		ON e.OFFERING_OID = o.OID "
		+ "		"
		+ "        JOIN "+ DBSchemaLMS+"CUSTOMFIELD_VALUE cfv "
		+ "		ON cfv.REF_OID= o.OID "
		+ "		"
		+ "        JOIN "+ DBSchemaLMS+"CUSTOMFIELD CF "
		+ "		ON CF.OID=CFV.CUSTOMFIELD_OID "
		+ "		AND CF.NAME LIKE 'Societ%' "
		+ "		AND UPPER (CFV.VAL) LIKE UPPER('U9%') "
		+ "		"
		+ "        JOIN "+ DBSchemaUCL+"ELVIS_ANAG_MODULES elvis "
		+ "		ON elvis.OFFERING_ID=o.OID "
		+ "		JOIN "+ DBSchemaLMS+"ENROLLABLEHELPER EH "
		+ "		ON EH.CATALOGENTRY_OID=C.OID "
		+ "WHERE"
		+ "	o.STATUS = 2 "
		+ "	AND u.ACTIVE = 1 "
		+ "	AND a.CODE_STATE IN (100,"
		+ "	110,"
		+ "	200,"
		+ "	400) "
		+ "	AND e.STATE <> 90 "
		+ "	AND elvis.SEND_TO_METAMORFOSI >= sysdate - 7 "
		+ "	AND elvis.CREATION='Y'"
		+ " AND u.DISTINGUISHED_NAME like '%o=US,dc=unicredito,dc=it' "
		+ " )";
	
	log.debug("QUERY:" + query);
	PreparedStatement ps = null;
	ResultSet rs = null;	
	try 
	{
		
		ps = conn.prepareStatement(query);
		
		
		rs = ps.executeQuery();
		enrollments = new ArrayList<Enrollment>();
		while (rs.next()) 
		{			
			Enrollment enrollment = new Enrollment();
			
			enrollment.setUSER_ID(rs.getString("USER_ID").trim());
			enrollment.setCATALOG_CODE(rs.getString("CATALOG_CODE").trim());
			enrollment.setENROLLDATE(rs.getDate("ENROLLDATE"));
			enrollment.setSTART_DATE(rs.getDate("START_DATE"));
			enrollment.setCOURSE_TITLE(rs.getString("COURSE_TITLE"));
			enrollment.setCOURSE_DESCRIPTION(rs.getString("COURSE_DESCRIPTION"));
			enrollment.setFIRST_NAME(rs.getString("FIRST_NAME"));
			enrollment.setLAST_NAME(rs.getString("LAST_NAME"));
			enrollment.setTYPE(rs.getString("TYPE"));
			enrollment.setOFFERING_OID(rs.getString("OFFERING_OID"));
			enrollment.setCATALOGENTRY_OID(rs.getString("CATALOGENTRY_OID"));
			enrollment.setCITY(rs.getString("CITY"));
			enrollment.setCODE_STATE(rs.getString("CODE_STATE"));
			enrollment.setSEND_TO_METAMORFOSI(rs.getDate("SEND_TO_METAMORFOSI"));
			enrollment.setDIDATTICA(rs.getString("DIDATTICA"));
			enrollment.setREQUIRES_MANAGER_APPROVAL(rs.getInt("REQUIRESMANAGERAPPROVAL"));
			enrollment.setAREA_TEMATICA_CODE(rs.getString("AREA_TEMATICA_CODE"));
			enrollment.setRealDays(rs.getDouble("realDays"));
			
			CustomField cf = new CustomField();
			cf.setCost(rs.getString("COST_FOR_USER"));
			cf.setSupplier(rs.getString("FORNITORE"));
			cf.setTipologia(rs.getString("TIPOLOGIA"));
			cf.setDuration(rs.getString("DURATA"));
			cf.setAreaTematica(rs.getString("AREA_TEMATICA_DESC"));
			cf.setSource(rs.getString("IntEst"));
			enrollment.setCustomField(cf);
			
			enrollments.add(enrollment);
		}
		
	}
	catch (SQLException sex) 
	{
		log.debug("SQLException: " + sex.getMessage());
		throw(sex);
	} 
	catch (Exception ex) 
	{
		log.debug("Exception: " + ex.getMessage());
		throw(ex);
	} 	  
	finally{
		if (rs != null)
			rs.close();
		if (ps != null)
			ps.close();
	}  
	return enrollments;
}


protected static ArrayList<Enrollment> getWrongEnrollmentsFromTRASP002(
		Connection conn)  
		throws SQLException, Exception
	{
		
		ArrayList<Enrollment> enrollments = null;
		
		String query = 
		"select "
		+ "	  enrollment.oid, "
		+ "	    user_objective.normalized_score score, "
		+ "	    decode(user_objective.is_satisfied ,0,'F',1,'P') STATO, "
		+ " 	usr.USER_ID, "	
		+ " 	ROWNUM "
		+ "		"
		+ "		from "
		+ "		    "+ DBSchemaLMS+"USR,  "
		+ "		    "+ DBSchemaLMS+"METADATA_TREE,     "
		+ "		    "+ DBSchemaLMS+"METADATA,    "
		+ "		    "+ DBSchemaLMS+"ENROLLMENT,    "
		+ "		    "+ DBSchemaLMS+"CATALOGENTRY,"
		+ "		    "+ DBSchemaLMS+"progress,"
		+ "		    "+ DBSchemaLMS+"user_objective,"
		+ "		    "+ DBSchemaLMS+"objective"
		+ "		where     "
		+ "		"
		+ "		ENROLLMENT.USER_OID = USR.oid  "
		+ "		and   ENROLLMENT.CATALOGENTRY_OID = CATALOGENTRY.OID "
		+ "		and   ENROLLMENT.oid = PROGRESS.ENROLLMENT_OID   "
		+ "		and   PROGRESS.METADATA_TREE_OID = METADATA_TREE.oid "
		+ "		and   METADATA_TREE.ref_oid = METADATA.oid      "
		+ "		and   ENROLLMENT.oid = PROGRESS.enrollment_oid "
		+ "		and   user_objective.ENROLLMENT_OID=enrollment.oid "
		+ "		and   metadata_tree.oid=objective.METADATA_TREE_OID "
		+ "		and   objective.oid=user_objective.objective_oid "
		+ "		and   enrollment.state IN (110) "
		+ "		and   catalogentry.code='TRASP002' "
		+ "		and CATALOGENTRY.OID = '"+trasp002_oid_to_fix+"' "
		+ "		and METADATA.item_title ='Test'"
		//+ "		and user_objective.is_satisfied  is not null"
		+ "		and (ENROLLMENT.oid,user_objective.normalized_score) not in ("
		+ "        SELECT "
		+ "        enrollment.oid oid ,"
		+ "        user_objective.normalized_score score "
		+ "    FROM"
		+ "        "+ DBSchemaLMS+"USR,"
		+ "        "+ DBSchemaLMS+"METADATA_TREE,"
		+ "        "+ DBSchemaLMS+"METADATA,"
		+ "        "+ DBSchemaLMS+"ENROLLMENT,"
		+ "        "+ DBSchemaLMS+"CATALOGENTRY,"
		+ "        "+ DBSchemaLMS+"progress,"
		+ "        "+ DBSchemaLMS+"user_objective,"
		+ "        "+ DBSchemaLMS+"objective "
		+ "    WHERE"
		+ "        ENROLLMENT.USER_OID = USR.oid "
		+ "        AND ENROLLMENT.CATALOGENTRY_OID = CATALOGENTRY.OID "
		+ "        AND ENROLLMENT.oid = PROGRESS.ENROLLMENT_OID "
		+ "        AND PROGRESS.METADATA_TREE_OID = METADATA_TREE.oid "
		+ "        AND METADATA_TREE.ref_oid = METADATA.oid "
		+ "        AND ENROLLMENT.oid = PROGRESS.enrollment_oid "
		+ "        AND user_objective.ENROLLMENT_OID=enrollment.oid "
		+ "        AND metadata_tree.oid=objective.METADATA_TREE_OID "
		+ "        AND objective.oid=user_objective.objective_oid "
		+ "        AND enrollment.STATE IN (110) "
		+ "        AND catalogentry.code='TRASP002' "
		+ "		   AND CATALOGENTRY.OID = '"+trasp002_oid_to_fix+"' "
		+ "        AND METADATA.item_title ='La trasparenza tra banca e cliente' "
		//+ "        AND user_objective.is_satisfied IS NOT NULL"
		+ "    ) "
		+ " order by ROWNUM ";
		//+ " and RNUM <=3 ";
		
		log.debug("QUERY:" + query);
		PreparedStatement ps = null;
		ResultSet rs = null;	
		try 
		{
			
			ps = conn.prepareStatement(query);
			
			
			rs = ps.executeQuery();
			enrollments = new ArrayList<Enrollment>();
			while (rs.next()) 
			{			
				Enrollment enrollment = new Enrollment();
				enrollment.setROWNUM(rs.getInt("ROWNUM"));
				enrollment.setOID(rs.getString("oid"));
				enrollment.setScore(rs.getString("score"));
				enrollment.setSTATUS(rs.getString("STATO"));
				enrollment.setUSER_ID(rs.getString("USER_ID"));
				
				enrollments.add(enrollment);
			}
			
		}
		catch (SQLException sex) 
		{
			log.debug("SQLException: " + sex.getMessage());
			throw(sex);
		} 
		catch (Exception ex) 
		{
			log.debug("Exception: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}  
		return enrollments;
	}

protected static boolean TRASP002hasBeenFixed(String enrollmentOid,
		Connection conn)  
		throws SQLException, Exception
	{
		
		boolean result = false;
		
		String query = 
		  "        SELECT "
		+ "        enrollment.oid oid ,"
		+ "        user_objective.normalized_score score "
		+ "    FROM"
		+ "        "+ DBSchemaLMS+"USR,"
		+ "        "+ DBSchemaLMS+"METADATA_TREE,"
		+ "        "+ DBSchemaLMS+"METADATA,"
		+ "        "+ DBSchemaLMS+"ENROLLMENT,"
		+ "        "+ DBSchemaLMS+"CATALOGENTRY,"
		+ "        "+ DBSchemaLMS+"progress,"
		+ "        "+ DBSchemaLMS+"user_objective,"
		+ "        "+ DBSchemaLMS+"objective "
		+ "    WHERE"
		+ "        ENROLLMENT.USER_OID = USR.oid "
		+ "        AND ENROLLMENT.CATALOGENTRY_OID = CATALOGENTRY.OID "
		+ "        AND ENROLLMENT.oid = PROGRESS.ENROLLMENT_OID "
		+ "        AND PROGRESS.METADATA_TREE_OID = METADATA_TREE.oid "
		+ "        AND METADATA_TREE.ref_oid = METADATA.oid "
		+ "        AND ENROLLMENT.oid = PROGRESS.enrollment_oid "
		+ "        AND user_objective.ENROLLMENT_OID=enrollment.oid "
		+ "        AND metadata_tree.oid=objective.METADATA_TREE_OID "
		+ "        AND objective.oid=user_objective.objective_oid "
		+ "        AND enrollment.STATE IN (110) "
		+ "        AND catalogentry.code='TRASP002' "
		+ "		   AND CATALOGENTRY.OID = '"+trasp002_oid_to_fix+"' "
		+ "        AND METADATA.item_title ='La trasparenza tra banca e cliente' "
		+ "        AND user_objective.is_satisfied IS NOT NULL";
		
		log.debug("QUERY:" + query);
		PreparedStatement ps = null;
		ResultSet rs = null;	
		try 
		{
			
			ps = conn.prepareStatement(query);
			
			
			rs = ps.executeQuery();
	
			if (rs.next()) 
			{			
				
				result=true;
			}
			
		}
		catch (SQLException sex) 
		{
			log.debug("SQLException: " + sex.getMessage());
			throw(sex);
		} 
		catch (Exception ex) 
		{
			log.debug("Exception: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}  
		return result;
	}

protected static boolean insertIntoConsuntivazione(String cf,
		Connection conn)  
		throws SQLException, Exception
	{
		
		boolean result = false;
		
		String query = 
		  "insert into "+DBSchemaUCL+"consuntivazione_manuale values ('CF',:1, 'I',sysdate, 'UV00022')";
		
		
		PreparedStatement ps = null;
		try 
		{
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(query);
			ps.setString(1, cf);
			
			log.debug("QUERY:" + query.replaceAll(":1", "'"+cf+"'"));
			
			//rs = ps.executeQuery();
			
			if (ps.executeUpdate() == 1) {
				conn.commit();
				result = true;
			} else {
				conn.rollback();
			}
			
			
			
		}
		catch (SQLException sex) 
		{
			conn.rollback();
			log.debug("SQLException: " + sex.getMessage());
			throw(sex);
		} 
		catch (Exception ex) 
		{
			conn.rollback();
			log.debug("Exception: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{
			
			if (ps != null)
				ps.close();
			
			conn.setAutoCommit(true);
			
		}  
		return result;
	}

/**
 * 
 * @return getStepEnrolments   
 * @throws SQLException
 */
public static List<Enrollment> getStepEnrolments(
	Connection conn,
	String startDate)  
	throws SQLException, Exception
{
	
	List<Enrollment> enrolments = null;
	
	String query = "select "
					+"u.USER_ID, "
				    +"c.CODE, "
				    +"e.ENROLLDATE, " 
				    +"o.STARTDATE " 

					+"from "
	                +"APPROVED_ENROLL_REQUEST a "
	                    
	                +"JOIN "+DBSchemaLMS+"ENROLLMENT e "
	                +"ON a.ENROLLMENT_OID = e.OID "

	                +"JOIN "+DBSchemaLMS+"OFFERING o "
	                +"ON o.OID = e.OFFERING_OID "

	                +"JOIN "+DBSchemaLMS+"CATALOGENTRY c "
	                +"ON c.OID = o.CATALOGENTRY_OID "
	                    
	                +"JOIN "+DBSchemaLMS+"usr u "
	                +"ON u.OID = e.USER_OID "
 	                +"AND u.ACTIVE = 1 "

					+"where "
					+"upper(a.APPLICATION_CODE) = 'STEP' ";
					
		if(startDate!=null){
			query+= "and o.STARTDATE > to_date('"+startDate+"','DD.MM.YYYY')  ";
		}
	        query+= "GROUP BY u.USER_ID,c.CODE,e.ENROLLDATE, o.STARTDATE ";
		
		
	log.debug(" query:" + query);
	PreparedStatement ps = null;
	ResultSet rs = null;	
	try 
	{
		
		ps = conn.prepareStatement(query);
		log.debug("startDate"+startDate);
		
		
		
		rs = ps.executeQuery();
		enrolments = new ArrayList<Enrollment>();
		while (rs.next()) 
		{			
			Enrollment enrollmnet = new Enrollment();
			
			enrollmnet.setUSER_ID(rs.getString("USER_ID").trim());
			enrollmnet.setCATALOG_CODE(rs.getString("CODE").trim());
			enrollmnet.setENROLLDATE(rs.getDate("ENROLLDATE"));
			enrollmnet.setSTART_DATE(rs.getDate("STARTDATE"));
			enrolments.add(enrollmnet);
		}
		
	}
	catch (SQLException sex) 
	{
		log.debug(" SQLException: " + sex.getMessage());
		throw(sex);
	} 
	catch (Exception ex) 
	{
		log.debug(" Exception: " + ex.getMessage());
		throw(ex);
	} 	  
	finally{
		if (rs != null)
			rs.close();
		if (ps != null)
			ps.close();
	}
	return enrolments;
}


/**
 * 
 * @return getCommonName   
 * @throws SQLException
 */
public static String getCommonName(
	Connection conn,
	String userId)  
	throws SQLException, Exception
{
	
	String result = null;
	
	String query = "select initcap(COMMON_NAME) as COMMON_NAME from "+DBSchemaLMS+"usr where upper(USER_ID)=upper(:1) and ACTIVE=1 ";
		
		
	PreparedStatement ps = null;
	ResultSet rs = null;	
	try 
	{
		
		ps = conn.prepareStatement(query);
		ps.setString(1, userId);
		
		rs = ps.executeQuery();
		
		if (rs.next()) 
		{			
			result = rs.getString("COMMON_NAME");
		}
		
	}
	catch (SQLException sex) 
	{
		log.debug(" SQLException: " + sex.getMessage());
		throw(sex);
	} 
	catch (Exception ex) 
	{
		log.debug(" Exception: " + ex.getMessage());
		throw(ex);
	} 	  
	finally{
		if (rs != null)
			rs.close();
		if (ps != null)
			ps.close();
	}
	return result;
}

public static List<Enrollment> getEnrollmentsFromLTP(Connection conn,String startPos,String endPos) 
	throws SQLException, Exception
	{
		List<Enrollment> enrollments = null;
		
		String query = 
			"select eia.REQUEST_DATE as REQUEST_DATE " +
            ",eia.COUNTRY_ID as COUNTRY " +
            ", (case when eia.REQUEST_PRIORITY = 1 then 'HIGH' when eia.REQUEST_PRIORITY = 2 then 'MEDIUM' when eia.REQUEST_PRIORITY = 3 then 'LOW' else '' end) as PRIORITY " +
			",eic.SOURCE_ID as DIDATTICA " +
			",eic.METHOD_ID as METHOD " +
	        ",eic.SUBJECT_AREA_1ST_LEVEL as SUBJECT_AREA_1ST_LEVEL " +
	        ",eic.SUBJECT_AREA_2ND_LEVEL as SUBJECT_AREA_2ND_LEVEL " +
	        ",eic.SUBJECT_AREA_3RD_LEVEL as SUBJECT_AREA_3RD_LEVEL " +
			", eia.USER_ID as USER_ID " +
			", eia.USER_ID as NUM_ID " +
			",eic.ITEM_ID as CATALOG_CODE " +
			",'Internal catalogue' as TRAINING_SOURCE " +
			",eic.ITEM_TITLE_EN as COURSE_DESCRIPTION " +			
			",eic.PROVIDER as PROVIDER " +
			",eic.VENDOR_TRAINING_CODE as PROVIDER_TRAIN_CODE " +
            ",eic.DURATION as DAYS " +
            ",eic.COST as COST " +
            ",eia.REQUEST_ID as REQUEST_ID " +
            ",eia.REQUESTER_ID as REQUESTER_ID " +
            ",' ' as NOTES " +
            ",null as ITEM_START_DATE " +
            ",eia.CATEGORY_L1 as CATEGORY " +
            ",eia.CATEGORY_L2 as GRUPA " +
            ",eia.CATEGORY_L3 as SKILL " +
            ",(case when eia.REQUEST_STATUS = 'YES' then 'Approved' when eia.REQUEST_STATUS = 'NO' then 'Rejected' when eia.REQUEST_STATUS is null then 'Under Authorization' else ' ' end) as STATUS " +
            ",eia.APP_REJ_DATE " +
            "from ELT_INTERNAL_ASSIGNMENT eia " +
            "left join ELT_INTERNAL_COURSE eic " +
			"on eia.ITEM_ID=eic.ITEM_ID and eia.COUNTRY_ID like eic.COUNTRY_ID " +
            "where eia.REQUEST_ID >= "+startPos+" and eia.REQUEST_ID < "+endPos+" " +
			"UNION ALL " +
			"select exa.REQUEST_DATE as REQUEST_DATE " +
            ",exa.COUNTRY_ID as COUNTRY " +
            ", (case when exa.REQUEST_PRIORITY = 1 then 'HIGH' when exa.REQUEST_PRIORITY = 2 then 'MEDIUM' when exa.REQUEST_PRIORITY = 3 then 'LOW' else '' end) as PRIORITY " +
            ",'EXTERNAL' as DIDATTICA " +
            ",'CLASS' as METHOD " +
            ",exa.SUBJECT_AREA_1ST_LEVEL as SUBJECT_AREA_1ST_LEVEL " +
            ",exa.SUBJECT_AREA_2ND_LEVEL as SUBJECT_AREA_2ND_LEVEL " +
            ",exa.SUBJECT_AREA_3RD_LEVEL as SUBJECT_AREA_3RD_LEVEL " +
            ",exa.USER_ID as USER_ID " +
            ",exa.USER_ID as NUM_ID " +
            ",nvl(exa.ITEM_ID_UNICREDIT ,' ') as CATALOG_CODE " +
            ",(case when exa.REQUEST_FORM_TYPE like 'fix%' then 'Fixed vendor form' else (case when REQUEST_FORM_TYPE like 'open%' then 'Open vendor form' else ' ' end) end) as TRAINING_SOURCE" +
            ",exa.ITEM_TITLE as COURSE_DESCRIPTION " +
            ",exa.PROVIDER as PROVIDER " +
            ",exa.ITEM_ID as PROVIDER_TRAIN_CODE " +
            ",exa.DURATION as DAYS " +
            ",exa.COST as COST " +
            ",exa.REQUEST_ID " +
            ",exa.REQUESTER_ID as REQUESTER_ID " +
            ",exa.NOTES " +
            ",exa.ITEM_START_DATE as ITEM_START_DATE " +
            ",exa.CATEGORY_L1 as CATEGORY " +
            ",exa.CATEGORY_L2 as GRUPA " +
            ",exa.CATEGORY_L3 as SKILL " +
            ",(case when exa.REQUEST_STATUS = 'YES' then 'Approved' when exa.REQUEST_STATUS = 'NO' then 'Rejected' when exa.REQUEST_STATUS is null then 'Under Authorization' else ' ' end) as STATUS " +
            ",exa.APP_REJ_DATE " +
            "from ELT_EXTERNAL_ASSIGNMENT exa " + 
            "where exa.REQUEST_ID >= "+startPos+" and exa.REQUEST_ID < "+endPos+" " +
			"order by REQUEST_DATE desc, USER_ID, CATALOG_CODE ";
		
		log.debug("QUERY:" + query);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try 
		{
			log.debug("(getEnrollmentsFromLTP) - Please Wait...");	
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			enrollments = new ArrayList<Enrollment>();
			while (rs.next()) 
			{			
				Enrollment enrollment = new Enrollment();
				
				enrollment.setUSER_ID(rs.getString("USER_ID").trim());
				enrollment.setNUM_ID(rs.getString("NUM_ID").trim().toUpperCase());
				enrollment.setREQUEST_DATE(rs.getDate("REQUEST_DATE"));
				enrollment.setDIDATTICA(rs.getString("DIDATTICA"));
				enrollment.setTYPE(rs.getString("METHOD"));
				enrollment.setCATALOG_CODE(rs.getString("CATALOG_CODE"));
				enrollment.setCOURSE_DESCRIPTION(rs.getString("COURSE_DESCRIPTION"));
				enrollment.setCATALOGENTRY_OID(rs.getString("TRAINING_SOURCE"));
				enrollment.setDAYS(rs.getDouble("DAYS"));
				enrollment.setENROLLMENT_OID(rs.getString("REQUEST_ID"));
				enrollment.setRequester(rs.getString("REQUESTER_ID"));
				enrollment.setNote(rs.getString("NOTES"));
				enrollment.setSTART_DATE(rs.getDate("ITEM_START_DATE"));
				enrollment.setRequestPriority(rs.getString("PRIORITY"));
				enrollment.setCATEGORY(rs.getString("CATEGORY"));
				enrollment.setGROUP(rs.getString("GRUPA"));
				enrollment.setSKILL(rs.getString("SKILL"));
				enrollment.setSTATUS(rs.getString("STATUS"));
				enrollment.setApprRejDate(rs.getDate("APP_REJ_DATE"));

				CustomField cf = new CustomField();
				cf.setCountry(rs.getString("COUNTRY"));
				cf.setSubjArea1(rs.getString("SUBJECT_AREA_1ST_LEVEL"));
				cf.setSubjArea2(rs.getString("SUBJECT_AREA_2ND_LEVEL"));
				cf.setSubjArea3(rs.getString("SUBJECT_AREA_3RD_LEVEL"));
				cf.setSupplier(rs.getString("PROVIDER"));
				cf.setVendorTrainingCode(rs.getString("PROVIDER_TRAIN_CODE"));
				cf.setCost(rs.getString("COST"));
				enrollment.setCustomField(cf);
				
				enrollments.add(enrollment);
			}
			
		}
		catch (SQLException sex) 
		{
			log.debug("SQLException: " + sex.getMessage());
			throw(sex);
		} 
		catch (Exception ex) 
		{
			log.debug("Exception: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		} 
		
		return enrollments;
}

public static int getEnrollmentsSize(Connection conn, String offset) 
	throws SQLException, Exception
	{
	String query = 
		" select sum(A1) as SUM from ( " +
		" select COUNT(*) as A1 from ELT_EXTERNAL_ASSIGNMENT where REQUEST_ID > " +offset+
		" union all " +
		" select COUNT(*) as A2 from ELT_INTERNAL_ASSIGNMENT where REQUEST_ID > " +offset+
		" ) ";
	
		//log.debug("QUERY:" + query);
		int enrSize=0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try 
		{
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
	
			while (rs.next()) 
			{			
				enrSize=rs.getInt("SUM");
			}
			log.debug("***   Enrollments Size: " + enrSize );
		}
		catch (SQLException sex) { log.debug("SQLException: " + sex.getMessage());
			throw(sex);	} 
		catch (Exception ex) { log.debug("Exception: " + ex.getMessage());
			throw(ex);
		} 	  
		finally{ if (rs != null)rs.close(); if (ps != null)ps.close();
		} 
		
		return enrSize;
		}			
		
}