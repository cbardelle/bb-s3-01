/*
 * Created on Mar 2, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch;

import java.util.List;

import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.core.Command;
import pl.ugis.batch.dao.BatchService;

public class FixTRASP002Batch implements Command {

	static Logger log = Logger.getLogger(FixTRASP002Batch.class);
	
	
	/* (non-Javadoc)
	 * @see pl.ugis.batch.utility.Command#execute()
	 */
	public void execute() throws Exception, IllegalStateException {
		
		try{
		
			List<Enrollment> enrolments  = BatchService.getWrongEnrollmentsFromTRASP002();
			log.info("Number of enrollment to digest: "+enrolments.size());
			
			BatchService.setResultByWebServiceAndInsertIntoConsuntivazione(enrolments);
			
			log.debug("skonczone");
		}catch (Exception e) {
			log.error(e.getMessage(),e);
			throw e;
		}
		
	}



	
}
