package pl.ugis.batch.bo;

public class User {

	private String userId;
	private String commonName;
	private String uscountry;
	private String usmanager;
	private String mail;
	
	public String getCommonName() {
		return commonName;
	}
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUscountry() {
		return uscountry;
	}
	public void setUscountry(String uscountry) {
		this.uscountry = uscountry;
	}
	public String getUsmanager() {
		return usmanager;
	}
	public void setUsmanager(String usmanager) {
		this.usmanager = usmanager;
	}
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public User(String userId) {
		this.userId = userId;
	}
	
	public User(String userId, String commonName) {
		super();
		this.userId = userId;
		this.commonName = commonName;
	}
}
