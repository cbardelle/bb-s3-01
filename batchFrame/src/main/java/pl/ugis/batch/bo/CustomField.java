package pl.ugis.batch.bo;

public class CustomField {

private String supplier = "";
private String cost = "";
private String totalCost = "";

private String source = "";
private String duration = "";
private String state = "";
private String societa = "";
private String tipologia = "";
private String areaTematica = "";
private String country = "";
private String vendorTrainingCode = "";
private String subjArea1 = "";
private String subjArea2 = "";
private String subjArea3 = "";

public String getSocieta() {
	return societa;
}
public void setSocieta(String societa) {
	this.societa = societa;
}
public String getState() {
	return state;
}
public String getTotalCost() {
	return totalCost;
}
public void setTotalCost(String totalCost) {
	this.totalCost = totalCost;
}
public void setState(String state) {
	this.state = state;
}
public String getCost() {
	return cost;
}
public void setCost(String cost) {
	this.cost = cost;
}
public String getDuration() {
	return duration;
}
public void setDuration(String duration) {
	this.duration = duration;
}
public String getSource() {
	return source;
}
public void setSource(String source) {
	this.source = source;
}
public String getSupplier() {
	return supplier;
}
public void setSupplier(String supplier) {
	this.supplier = supplier;
}
public String getTipologia() {
	return tipologia;
}
public void setTipologia(String tipologia) {
	this.tipologia = tipologia;
}
public String getAreaTematica() {
	return areaTematica;
}
public void setAreaTematica(String areaTematica) {
	this.areaTematica = areaTematica;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getVendorTrainingCode() {
	return vendorTrainingCode;
}
public void setVendorTrainingCode(String vendorTrainingCode) {
	this.vendorTrainingCode = vendorTrainingCode;
}
public String getSubjArea1() {
	return subjArea1;
}
public void setSubjArea1(String subjArea1) {
	this.subjArea1 = subjArea1;
}
public String getSubjArea2() {
	return subjArea2;
}
public void setSubjArea2(String subjArea2) {
	this.subjArea2 = subjArea2;
}
public String getSubjArea3() {
	return subjArea3;
}
public void setSubjArea3(String subjArea3) {
	this.subjArea3 = subjArea3;
}

}