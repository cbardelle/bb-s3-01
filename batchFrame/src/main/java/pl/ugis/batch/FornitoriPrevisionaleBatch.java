/*
 * Created on Mar 2, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch;

import it.webegg.util.EncryptGoodbuy;

import java.io.File;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Supplier;
import pl.ugis.batch.core.Command;
import pl.ugis.batch.dao.BatchService;
import pl.ugis.batch.utility.CreateExcel;
import pl.ugis.batch.utility.FtpClient;
import pl.ugis.batch.utility.MngmUtil;
import pl.ugis.batch.utility.PropertyFileReader;

/**
 * @author UV00022
 *
 * Fornitori_previsionale file
 * will contain the sum of the cost for each supplier of approved enrolments.
 * These enrollments are in the editions that have start dates starting from current 
 * month till the end of the year. This file will be generated the first day of each month.
 * 
 * 
 */
public class FornitoriPrevisionaleBatch implements Command {

	static private PropertyFileReader pr = PropertyFileReader.getInstance();
	static Logger log = Logger.getLogger(FornitoriPrevisionaleBatch.class);
	
	
	/* (non-Javadoc)
	 * @see pl.ugis.batch.utility.Command#execute()
	 */
	
	public void execute() throws Exception, IllegalStateException {
		
		try{
		
		ArrayList<Supplier> suppliers  = BatchService.getSumOfCostForSuppliersFromCurrentMonth();
		
		String folder = pr.getProperty("folder_"+BatchMain.FORNITORI_PREVISIONALE);
		
		
		File file = new File(pr.getProperty("resources-path")+folder+
							 pr.getProperty("fileName_"+BatchMain.FORNITORI_PREVISIONALE)+"-"+MngmUtil.today()+".xls");
		
		
		CreateExcel.createSupplierWihCostExcel(file,suppliers);
		
		String hostName = pr.getProperty("ftpHostnameEL");
		String userId = pr.getProperty("ftpUseridEL");
		String pw = pr.getProperty("ftpPasswordEL");
		if(pw!=null)
			pw = EncryptGoodbuy.dencrypt(pw);
		
		String numMilliSeconds = pr.getProperty("ftpMilliWaitEL");
		String ftpFolder = pr.getProperty("ftpFolderEL");
		
		if("true".equalsIgnoreCase(pr.getProperty("sendFileViaFtp"))){
			FtpClient ftpc = new FtpClient(hostName,userId,pw,numMilliSeconds,ftpFolder+folder);
			ftpc.sendFile(file);
		}
		
		}catch (IllegalStateException ie) {
			log.error(ie.getMessage(),ie);
			throw ie;
		}
		catch (Exception e) {
			log.error(e.getMessage(),e);
			throw e;
		}
		
	}



	
}
