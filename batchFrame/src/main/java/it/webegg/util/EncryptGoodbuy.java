
package it.webegg.util;

/*--------------------------------------------------------------------*/
/*------------------------- EncryptGoodbuy ---------------------------*/
/*--------------------------------------------------------------------*/

public class 
EncryptGoodbuy 
   {

   /*-----------------------------------------------------------------*/
   /*----------------------- encrypt ---------------------------------*/
   /*-----------------------------------------------------------------*/

   public static String
   encrypt(String toEnc)
      {
      // constant for odd and par
      int iodd;
      int ipar;

      // temp variables
      String dum = toEnc;
      String dum3 = "";

      // array containing the cripted string
      char dum1[] = new char[toEnc.length()];

      // determine the constants
      while((iodd = (int)(Math.random()*100)) < 10)
         ;
      while(((ipar = (int)(Math.random()*100)) >= 10))
          ;

      // encript the string
      for(int i = 0; i < dum.length(); i++)
         {
         if(i != (Math.round(i / 2.0) * 2))
            dum1[i] = (char)(dum.charAt(i) + iodd);
         else
            dum1[i] = (char)(dum.charAt(i) - ipar);

         // set for each cghar 3 digits
         int c = dum1[i];
         if(c < 100 && c > 9)
            dum3 += "0" + c;
         else
         if(c < 10)
            dum3 += "00" + c;
         else
            dum3 += ""+ c;
         }

      // put everything together and return
      return "" + iodd + ipar + dum3;
      } // encrypt

   /*-----------------------------------------------------------------*/
   /*------------------------------ dencrypt -------------------------*/
   /*-----------------------------------------------------------------*/

   public static String
   dencrypt(String toDecNum)
      {
      int toDecInt[] = new int[(int)((toDecNum.length() - 3) / 3)];

      // take back the 2 constants
      int iodd = Integer.parseInt(toDecNum.substring(0, 2));
      int ipar = Integer.parseInt(toDecNum.substring(2, 3));

      // define the toDec
      toDecNum = toDecNum.substring(3, toDecNum.length());
      for(int i = 0; i < toDecInt.length; i++)
         toDecInt[i] = Integer.parseInt(toDecNum.substring(i * 3, (i + 
         1) * 3));

      // take back the string
      char cstr[] = new char[toDecInt.length];
      for(int i = 0; i < toDecInt.length; i++)
         {
         if(!(i == (Math.round(i / 2.0) * 2)))
            cstr[i] = (char)(toDecInt[i] - iodd);
         else
            cstr[i] = (char)(toDecInt[i] + ipar);
         }

      // return the string
      return new String(cstr);
      } // dencrypt

   /*---------------------- UNIT TEST --------------------------------*/
   public static void 
   main(String[] args) 
      {
      for(int i = 0; i < 1; i++)
         {
         String start = "beccahardware";
         String dec = EncryptGoodbuy.encrypt(start);
         System.out.println(dec);
         dec = EncryptGoodbuy.dencrypt(dec);
         System.out.println(dec);
         if(!start.equals(dec))
            System.out.println("Errore di decrypt !");
         }
      }
   } // class EncryptGoodbuy
