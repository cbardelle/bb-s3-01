echo off
echo Fornitori_consuntivo file 
echo will contain the sum of the cost for each supplier of all enrolments sent to Metamorfosi 
echo from the beginning of the year. This file will be generated the first day of each month. 

set Path=%JAVA_1_6%\bin\
java -jar ${pom.build.finalName}.jar  pl.ugis.batch.FornitoriConsuntivoBatch
