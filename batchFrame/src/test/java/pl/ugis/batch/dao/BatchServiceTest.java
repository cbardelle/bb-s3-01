package pl.ugis.batch.dao;

import java.sql.Connection;
import java.util.HashMap;

import junit.framework.TestCase;
import pl.ugis.batch.bo.Department;
import pl.ugis.batch.utility.DBManager;

public class BatchServiceTest extends TestCase {
	
	Connection conn=null;
	@Override
	protected void setUp() throws Exception {
		conn = new DBManager().getConnection();
	}
	
	public void testGetDepartments() throws Exception{
	
		HashMap<String, Department> departments = BatchService.getDepartments(conn);
		
		assertNotNull(departments);
		
	}
	
	@Override
	protected void tearDown() throws Exception {
		if (conn != null)
				conn.close();
	
	}
}
