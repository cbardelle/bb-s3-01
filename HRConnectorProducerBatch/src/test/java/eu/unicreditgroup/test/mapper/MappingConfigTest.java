package eu.unicreditgroup.test.mapper;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import eu.unicreditgroup.config.ResultColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mapper.TableMapper;
import eu.unicreditgroup.test.BaseSpringTest;
import eu.unicreditgroup.validator.IValidator;


public class MappingConfigTest extends BaseSpringTest{
	
	//@Resource(name = "reportDataBean")  
	//private ReportDataBean reportDataBean;  
	
	@Resource(name = "tableMapper")
	private TableMapper tableMapper;
	
	@Resource(name = "sourceColumns")
	private List<SourceColumn> sourceColumns;
	
	
	@Resource(name = "sourceFileConf")
	private SourceFileConf sourceFileConf;
	

	
	private String toId(String s){
		if (s == null) return s;
		
		if (StringUtils.equals(s,StringUtils.upperCase(s))){
			return s;
		}
		
		StringBuilder sb = new StringBuilder();
		
		int upperCounter = 0;
		
		for(int i = 0; i < s.length() ; i++){			
			char ch= s.charAt(i);
			if (i !=0 && upperCounter == 0 && (Character.isUpperCase(ch) || Character.isDigit(ch) ) ){
			
				sb.append("_");				
				upperCounter+=1;
			} else {
				upperCounter=0;	
			}
			sb.append(ch);
		}
		return sb.toString().toUpperCase();		
	}
	
	
	
	private String intToExcelColId(int id){
		if ( id < 0 || id > 677 ){	
			return "";			
		}
		
		// number of letters A-Z
		int n = 26;
		
		int aSignCode = 65; // ASCII code of 'A'
		int zSignCode = 90; // ASCII code of 'Z'
		
		//System.out.println(i + " " + j);
		
		if (id <= n){
						
			return String.valueOf((char)(aSignCode + id - 1));			
			
			
		} else {
			int i = id / n;
			
			int j = id % n;
			
			if (j != 0)	{
				return String.valueOf((char)(aSignCode + i - 1)) + String.valueOf((char)(aSignCode + j - 1));
			} else {
				return String.valueOf((char)(aSignCode + i - 2)) + String.valueOf((char)(zSignCode));				
			}			
		}		
	}
	
	
	//@Test 
	public void outputConfigExcel(){
		
		//TODO create xls spread sheet with actual configuration
		
		//private List<SourceColumn> sourceColumns;
		for (SourceColumn sc : sourceColumns){
			
			System.out.println(					
				(sc.getSrcColIndex() +1) + "\t" +
				intToExcelColId(sc.getSrcColIndex() +1) + "\t" +
				sc.getSrcColDesc() + "\t" +
				sc.getLength()									
				);		
		}
	}
	
	
	//@Test 
	public void generateSQLfor_HR_CONN_RES_ATTR_DICT(){
		List<ResultColumn> resultColumns = tableMapper.getResultColumns();
		
		try {
			FileWriter fw = new FileWriter("sql/HR_CONN_RES_ATTR_DICT.sql");
			
			
			
			fw.write("INSERT ALL\r\n");
			for (ResultColumn rc : resultColumns){
				
				if ( rc.isIncludeInResultFile()){
				
					fw.write("INTO HR_CONN_RES_ATTR_DICT (RES_ATTR_IDX, RES_ATTR_NAME, RES_ATTR_DESC, PLT_ATTR_ID) VALUES ( " + rc.getResColIndex() + ",'" + rc.getResColName() + "','" + rc.getResColDesc() + "','" + rc.getLmsAttrName() +"' )\r\n" );
				}
				
			}			
			fw.write("SELECT * FROM dual;\r\n");
			fw.close();
		} catch (IOException e) {
			
			System.out.println("File HR_CONN_RES_ATTR_DICT.sql was not created: " + e.getMessage()); 
			return;
		} 
		
		
/*		
		System.out.println("INSERT ALL");
		for (ResultColumn rc : resultColumns){			
			System.out.println("INTO HR_CONN_RES_ATTR_DICT (RES_ATTR_IDX, RES_ATTR_NAME, RES_ATTR_DESC, PLT_ATTR_ID) VALUES ( " + rc.getResColIndex() + ",'" + rc.getResColName() + "','" + rc.getResColDesc() + "','" + rc.getLmsAttrName() +"' )" );
			
		}
		System.out.println("SELECT * FROM dual;");
*/	
	}
	

	//@Test 
	public void systemoutResultFileConfig(){
		List<ResultColumn> resultColumns = tableMapper.getResultColumns();
		for (ResultColumn rc : resultColumns){
			int scrColsCnt = 0;
			if ( rc.getSrcCol() != null ){
				
				scrColsCnt = rc.getSrcCol().size();
			}
			//if ( rc.isIncludeInResultFile()){
			System.out.println( rc.getResColName() + "\t" + rc.getResColDesc() + "\t" + rc.getLmsAttrName() + "\t" + rc.isIncludeInResultFile() );
			
			//}
		}			
	}
	

	/**
	 * 
	 * This is the data for the update of HRConnectorProducerBatch(...).doc
	 * 
	 */
	//@Test 
	public void outputResultFileConfig(){
		List<ResultColumn> resultColumns = tableMapper.getResultColumns();
		for (ResultColumn rc : resultColumns){
			int scrColsCnt = 0;
			if ( rc.getSrcCol() != null ){
				
				scrColsCnt = rc.getSrcCol().size();
			}
			
			String sourceCols = "";
			String validators = ""; 
			
			
			if ( rc.getSrcCol() != null ){
				boolean isFirst = true;
				for (SourceColumn sc : rc.getSrcCol()){				
					if ( isFirst ) {
						isFirst = false;						
					} else {
						sourceCols += "-";
					}				
					sourceCols = sourceCols + sc.getSrcColName();				
				}
			}
			
			if ( rc.getValidators() != null ){
				boolean isFirst = true;
				for (IValidator v : rc.getValidators()){				
					if ( isFirst ) {
						isFirst = false;						
					} else {
						validators += "-";
					}				
					validators = validators + v.getClass().getSimpleName();				
				}
			}

			
			//if ( rc.isIncludeInResultFile()){
			System.out.println( 
					rc.getResColDesc() + "\t" +
					rc.getResColName() + "\t" +
					sourceCols + "\t" +
					"Data type \t" +
					"Can be null \t" +					
					rc.getDefaultValue() + "\t" +					
					rc.getMapper().getClass().getSimpleName() + "\t" +
					validators  + "\t" +					
					rc.isIncludeInResultFile() 
			);
			
			//}
		}			
	}
	
	
	
	
}
