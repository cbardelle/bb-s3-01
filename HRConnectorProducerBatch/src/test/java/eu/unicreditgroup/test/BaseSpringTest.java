package eu.unicreditgroup.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={ "file:src/test/resources/testSpring/spring.xml" })
public class BaseSpringTest {
	
	@Test 
	public void dummy(){
		assertTrue(true);		
	}
	
}
