package eu.unicreditgroup.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.junit.Test;

import eu.unicreditgroup.utils.reader.ExternalUsersFileReader;

import net.sf.flatpack.DataSet;
import net.sf.flatpack.DefaultParserFactory;
import net.sf.flatpack.Parser;


public class FlatFileReaderTest extends BaseSpringTest{
	
	
	
	@Resource
	ExternalUsersFileReader externalUsersFileReader;
	
	
	
	/*
	@Test	
	public void testCheckPrereq() throws Exception
	{
		int extUsersRemovalDay = 3;		

		if ( extUsersRemovalDay <= 0 ){
			System.out.println("Not configured!");
			return;
		}
				
		DateTime now = new DateTime();
		
		now = now.plusHours(12);
		
		System.out.println(now);
		//DateTime night = new DateTime();
		
		System.out.println(now.getDayOfWeek());
		
		if (now.getDayOfWeek() == extUsersRemovalDay){ 
			// check it's between 14:00 and 00:00
			
			if ( now.getHourOfDay() >= 14 ){
				System.out.println("FIRE1!!!");
				//return true;
			} else {
				System.out.println("Nth to do1");
			}
		} else {  
			// if it's a following day after user removal day			
			if( now.minusDays(1).getDayOfWeek() ==  extUsersRemovalDay){
				// check it's between 00:00 and 4:00AM
				if ( now.getHourOfDay() >= 0 && now.getHourOfDay() < 4  ){
					System.out.println("FIRE2!!!");
					//return true;
				} else {
					System.out.println("Nth to do2");
				}				
			}
		}
		//return false;
		// next Sunday:
		//System.out.println(">>>>>>>"  + dt.dayOfWeek().getAsText( Locale.ENGLISH) );
		//System.out.println(">>>>>>>"  + dt.getDayOfWeek());
	}
	*/
	
	
	
	//@Test	
	public void testExternalUsersFileReader() throws Exception
	{
		String flatFilePath = "c:\\tmp\\flatpack\\source_aaext.csv";		
		String mappingPath = "c:\\tmp\\flatpack\\external_user_pzmap.xml";

		externalUsersFileReader.setMappingPath(mappingPath);
		
		externalUsersFileReader.initReader(flatFilePath);
		
		while (externalUsersFileReader.readNextRow()){
			
			System.out.println(Arrays.toString(externalUsersFileReader.getColumnsValues()));
		}
	}
	
	
	
	
}
