REM Copy all spring configuration except for spring-db.xml

copy .\..\..\..\main\resources\META-INF\spring\spring-beans.xml	spring-beans.xml
copy .\..\..\..\main\resources\META-INF\spring\spring-conf-beans.xml	spring-conf-beans.xml
rem copy .\..\..\..\main\resources\META-INF\spring\spring-dao-beans.xml	spring-dao-beans.xml
copy .\..\..\..\main\resources\META-INF\spring\spring-mappings-result.xml	spring-mappings-result.xml
copy .\..\..\..\main\resources\META-INF\spring\spring-mappings-source.xml	spring-mappings-source.xml
copy .\..\..\..\main\resources\META-INF\spring\spring-mappings-ext-source.xml	spring-mappings-ext-source.xml
copy .\..\..\..\main\resources\META-INF\spring\spring-util-beans.xml	spring-util-beans.xml
copy .\..\..\..\main\resources\META-INF\spring\spring.xml	spring.xml