<html>

<head>
	
	<style>
		body {
			font-family: verdana, sans-serif;
			font-size: 10pt;
			padding: 6px;
		}
		
		.headInfoDoneWithoutErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #3db00b;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoDoneWithErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #FF8C00;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoFailedFast {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #d8001d;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		
		
		
		.resultTable {
			width: 100%;
			border: 1px #6a8546 solid;
			text-align: center;
		}
		
		.resultTableName {
			background-color: #6a8546;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
			padding: 3px;
			width: 280px;
			margin-top: 10px;
		}
		
		.resultName {
			font-weight: bolder;
			width: 300px;
			text-align: left;
			vertical-align: top;
			font-size: 10pt;
		}
		
		.resultValue {
			text-align: left;
			padding-left: 5px;
			font-size: 10pt;
		}
		
		.errorValue {
			color: #d8001d;
			font-weight: bolder;
		}

		.warningValue {
			color: #ed872c;
			font-weight: bolder;
		}

	</style>

</head>

<body>
	
	<#-- General exit code  -->
	
		
	<div class="headInfoFailedFast"> 
		&radic; NEW ORGANIZATIONS ALERT!
	</div>

	
	
	
	<div class="resultTableName">New organization(s) is/are present! Please take the necessary actions to update the data in LMS!</div>
	<table class="resultTable">
		<tr>
			<td class="resultName">List of new organization(s)</td>
			<td class="resultValue">${rb.newOrganizationList}</td>
		</tr>		
	</table>

	
	
</body>

</html>
