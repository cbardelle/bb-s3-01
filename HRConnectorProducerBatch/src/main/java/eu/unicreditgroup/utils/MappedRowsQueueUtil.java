package eu.unicreditgroup.utils;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedRow;



public class MappedRowsQueueUtil {
	
	public static final String SHUTDOWN_STRING = "###SHUTDOWN_ROW###";
	
	private static MappedRow shutdownRow = new MappedRow();
	static	{
		
		String[] shutDn = {SHUTDOWN_STRING}; 
		shutdownRow.setData(shutDn);
	}
		
	public static MappedRow generateShutdownRow(){
		return shutdownRow;
	}
	
	public static boolean isShutdownRow(MappedRow row){
		
		if (row == null){
			return false;
		} else {
			return (
					row.getData().length == 1 &&
					SHUTDOWN_STRING.equals(row.getData()[0]) == true
				    );	  
					 
		}
	}
}
