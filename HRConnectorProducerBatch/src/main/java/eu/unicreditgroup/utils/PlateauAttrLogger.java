package eu.unicreditgroup.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.unicreditgroup.beans.PlateauAttrLogItem;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.dao.PlateauAttrLogDao;

public class PlateauAttrLogger {

	private PlateauAttrLogDao plateauAttrLogDao;
	
	public void logInformationAboutChangedAttr( List<TrackedAttr> changedAttrs , Date logDate ) throws Exception{
		
		List<PlateauAttrLogItem> logItems = new ArrayList<PlateauAttrLogItem>();
				
		for ( TrackedAttr ta : changedAttrs )
		{
			for (String ap: ta.getAssgnProfiles()){
				PlateauAttrLogItem log = new PlateauAttrLogItem();
				log.setApId(ap);
				log.setAttrSapCode(ta.getAttrCode());
				log.setCreatedDate(logDate);
				log.setNewVal(ta.getMappedValue());
				log.setOldVal(ta.getDictValue());
				log.setResAttrIdx(ta.getAttrIdx());
				logItems.add(log);
			}
		}		
		plateauAttrLogDao.writePlateauAttrLog(logItems);		
	}

	public PlateauAttrLogDao getPlateauAttrLogDao() {
		return plateauAttrLogDao;
	}

	public void setPlateauAttrLogDao(PlateauAttrLogDao plateauAttrLogDao) {
		this.plateauAttrLogDao = plateauAttrLogDao;
	}	
}
