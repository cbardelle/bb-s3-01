package eu.unicreditgroup.utils.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.BatchWorker;
import eu.unicreditgroup.config.ExternalSourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.external.ISapRowMapper;

import net.sf.flatpack.DataSet;
import net.sf.flatpack.DefaultParserFactory;
import net.sf.flatpack.Parser;

public class ExternalUsersFileReader implements FlatFileReader {
	
	Logger logger = Logger.getLogger(ExternalUsersFileReader.class);
	
	private Parser pzparser; 
		 
	private DataSet dataSet;
	
	private String[] colNames;
	
	private InputStream mapping;
	
	/**
	 * Source flat file with external users 
	 */
	private InputStream flatFile;
	
	private String mappingPath = "/META-INF/flatpack/external_user_pzmap.xml";
	
	private List<ExternalSourceColumn> externalSourceColumns;
	
	private List<ISapRowMapper>	sapRowMappers;
	
	private SourceFileConf sourceFileConf;
		
	private ReportDataBean reportDataBean; 
	
	
	/**
	 * The maximum count of valid rows in the external file
	 * If exceeded users are not imported 
	 * 
	 */
	private final long EXTERNAL_USERS_MAX_COUNT = 3000;
	

	@Override
	public FlatFileReader initReader(String flatFilePath) throws Exception {
		
		mapping = this.getClass().getResourceAsStream(mappingPath);
		
		if (mapping == null){
			
			logger.error("FAILED TO LOAD MAPPING : " + mappingPath);
			
		} else {

			logger.debug("MAPPING LOADED : " + mappingPath);
		}
		
		// test only!!!
		//mapping = new FileInputStream(mappingPath);
		
		flatFile = new FileInputStream(flatFilePath);
		
		pzparser = DefaultParserFactory.getInstance().newFixedLengthParser(new InputStreamReader(mapping), new InputStreamReader(flatFile));
				
		// the file is read
		dataSet = pzparser.parse();
		
		reportDataBean.setExternalUsersValidRowsCount(dataSet.getRowCount());
		
		// log info about 
		if (dataSet.getErrorCount() > 0){
	
			logger.debug("Failed to read " + dataSet.getErrorCount() + " lines from the file with external users " + flatFilePath +", errors: \r\n" + dataSet.getErrors());
			//System.out.println("Failed to read " + dataSet.getErrorCount() + " lines from the file with external users " + flatFilePath +", errors: \r\n" + dataSet.getErrors());
			reportDataBean.setExternalUsersInvalidRowsCount(dataSet.getErrorCount());
			
		}
		
	    colNames = dataSet.getColumns();
				
		return this;		
		
	}

	@Override
	public boolean readNextRow() throws Exception {		

		
		/**
		 * File with too many rows will be rejected
		 * 
		 */
		if ( dataSet.getRowCount() > EXTERNAL_USERS_MAX_COUNT ){			
			logger.info("The number of valid rows of the external users file has exceeded the maximum allowable count of: " + EXTERNAL_USERS_MAX_COUNT + ", all rows will be rejected!");
		
			reportDataBean.setExternalUsersMaxCountExceeded(true);
			return false;
		}
		
		
		
		return dataSet.next();		
	}

	@Override
	public String[] getColumnsValues() throws Exception {
		
		String extDataRow[] = new String[externalSourceColumns.size()];
		
		String sapDataRow[] = new String[sourceFileConf.getSourceColumns().size()];
				
		// get row data for an external user
		for (ExternalSourceColumn c : externalSourceColumns){
			String colValue = StringUtils.trim(StringUtils.strip(dataSet.getString(c.getSrcColName()) , ","));
			extDataRow[c.getSrcColIndex()] = colValue;
			
		}	
		// map external columns to the corresponding SAP data columns
		for (ISapRowMapper mapper : sapRowMappers){
			mapper.map(extDataRow, sapDataRow);
		}
		
		//logger.debug("MAPPED SAP ROW" + Arrays.toString( sapDataRow));
		
		return sapDataRow;
	}

	@Override
	public void close() {
		try {
			mapping.close();
			flatFile.close();
		} catch (IOException e) {			 
			logger.error("Exception when closing files: " + e);			
		}
		
		
	}

	public String getMappingPath() {
		return mappingPath;
	}

	public void setMappingPath(String mappingPath) {
		this.mappingPath = mappingPath;
	}

	public List<ExternalSourceColumn> getExternalSourceColumns() {
		return externalSourceColumns;
	}

	public void setExternalSourceColumns(
			List<ExternalSourceColumn> externalSourceColumns) {
		this.externalSourceColumns = externalSourceColumns;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	public List<ISapRowMapper> getSapRowMappers() {
		return sapRowMappers;
	}

	public void setSapRowMappers(List<ISapRowMapper> sapRowMappers) {
		this.sapRowMappers = sapRowMappers;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
	
}
