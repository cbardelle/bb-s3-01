package eu.unicreditgroup.utils.reader;

import java.util.ArrayList;
import java.util.List;

public interface SrcFileNameProvider {
	
	public ArrayList<String> getSourceFilesList();

}
