package eu.unicreditgroup.utils.reader;

public interface FlatFileReader {
	
	
	public FlatFileReader initReader( String flatFilePath ) throws Exception;
	
	public boolean readNextRow() throws Exception;
	
	
	public String[] getColumnsValues() throws Exception;
	
	
	public void close();	

}
