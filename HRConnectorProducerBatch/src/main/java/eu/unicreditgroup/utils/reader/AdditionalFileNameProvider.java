package eu.unicreditgroup.utils.reader;

import java.util.ArrayList;
import java.util.List;

import eu.unicreditgroup.config.ConfigParams;

/**
 * 
 * Provider of the source file name for the file with additional users
 * (used mainly for testing purposes) 
 * @author UV00074
 *
 */

public class AdditionalFileNameProvider implements SrcFileNameProvider {
	
	private ConfigParams configParams;

	@Override
	public ArrayList<String> getSourceFilesList() {
		
		ArrayList<String> sourceFilesList = new ArrayList<String>(); 
		
		sourceFilesList.add(configParams.getStrAdditionalSourceFileName());
		
		return sourceFilesList;
	}
	
	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

}
