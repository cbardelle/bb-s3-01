package eu.unicreditgroup.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.beans.AssgnProfile;
import eu.unicreditgroup.beans.ResultUserAttr;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.beans.UserAttrExcp;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.dao.DictionaryDao;

public class TrackedAttrDict  implements InitializingBean{
	
	Logger logger = Logger.getLogger(TrackedAttrDict.class);
	
	//private final String LOCAL_JOB_POS = "LOCAL_JOB_POS";
	private final Integer LOCAL_JOB_POS_IDX = 50;
	
	
	/**
	 * Map key - index of the column of the mapped attribute in the result file
	 * Map value - description of the mapped attribuite
	 * 
	 *  HashMap<res_attr_idx,Description>
	 */
	private HashMap<Integer,String> resultAttrDescMap;
	

	/**
	 * This map will be used to get assignment profile ids 
	 * for a given attribute index and value
	 * 
	 * 	 
	 * HashMap<res_attr_id, HashMap<ap_attr_value, List<ap_id>>>	  
	 * 
	 */
	private HashMap<Integer, HashMap<String, List<String>>> assgnProfileMap;
	
	
	private DictionaryDao dictionaryDao;
	
	private Dictionary dictionary;
	
	
	private SourceFileConf sourceFileConf;
	
		
	/**
	 * List of exceptions for tracked user attributes
	 * (for some attributes some values like description 
	 * are duplicated in the source file and has to be 
	 * tread like exceptions).
	 */
	private HashMap<Integer, List<String>> trackedAttrExcpMap;
		
	/**
	 * Dictionary of users attributes selected for tracking of changes
	 * HashMap<res_attr_idx, HashMap<res_attr_code,attr_value>>
	 * 
	 */
	private HashMap<Integer, HashMap<String,String>> attrTrackDict;
	
	
	
	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	private void initAttrTrackDict(){
		
		attrTrackDict = new HashMap<Integer, HashMap<String,String>>();  
		
		List<Integer> attrIdxs = dictionaryDao.getAttrToTrackIdxs();
		
		for(Integer attrIdx : attrIdxs ){
			
			HashMap<String,String> mappedValues = dictionaryDao.getAttrToTrackMap(attrIdx);
			attrTrackDict.put(attrIdx, mappedValues);			
		}				
	}	
	
	private void initResultAttrDescMap(){
		resultAttrDescMap = new HashMap<Integer, String>();
		List<ResultUserAttr> attributes = dictionaryDao.getAllResultAttr();

		for(ResultUserAttr attr : attributes){			
			resultAttrDescMap.put(attr.getResAttrIdx(), attr.getResAttrDesc());
		}				
	}
	
	/**
	 * Initializes the assgnProfileMap map
	 *  
	 */
	private void initAssgnProfileMap(){
		assgnProfileMap = new HashMap<Integer, HashMap<String, List<String>>>();
		List<AssgnProfile> assgnProfiles =  dictionaryDao.getAssgnProfiles();
		
		//HashMap<res_attr_id, HashMap<ap_attr_value, List<ap_id>>>
		for(AssgnProfile ap : assgnProfiles){
			if (assgnProfileMap.containsKey(ap.getResAttrIdx()) == true){
				
				HashMap<String, List<String>> values = assgnProfileMap.get(ap.getResAttrIdx());
				if (values.containsKey(ap.getApAttrValue()) == true){
					List<String> apIds = values.get(ap.getApAttrValue());
					if (apIds.contains(ap.getApId()) == false){
						apIds.add(ap.getApId());						
					} // else apId already on the list 				
				} else {
					List<String> apIds = new ArrayList<String>();
					apIds.add(ap.getApId());
					values.put(ap.getApAttrValue(), apIds);					
				}				
			} else {
				List<String> apIds = new ArrayList<String>();
				apIds.add(ap.getApId());
				HashMap<String, List<String>> values = new HashMap<String, List<String>>();
				values.put(ap.getApAttrValue(), apIds);
				assgnProfileMap.put(ap.getResAttrIdx(), values);								
			}
		}	
	}
		
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
		initAttrTrackDict();		
		initTrackedAttrExcpMap();
		initResultAttrDescMap();
		initAssgnProfileMap();
		int t =1;
	}	

	private void initTrackedAttrExcpMap() {
		trackedAttrExcpMap = new HashMap<Integer, List<String>>();
		
		List<UserAttrExcp> exceptions = dictionaryDao.getTrackedAttrExceptions();
		for( UserAttrExcp attrEx : exceptions )		
		{	
			if (trackedAttrExcpMap.containsKey(attrEx.getResAttrIdx())){				
				trackedAttrExcpMap.get(attrEx.getResAttrIdx()).add(attrEx.getSrcAttrCode());
			} else {
				List<String> codes = new ArrayList<String>();
				codes.add(attrEx.getSrcAttrCode());
				trackedAttrExcpMap.put(attrEx.getResAttrIdx(), codes);				
			}
		}			
	}
	
	/**
	 * Check if the mapped value exists in the dictionary table
	 * @param attrName
	 * @param sapCode
	 * @param attrMappedVal
	 * @return
	 */
	public String getTrackedAttrVal(Integer attrIdx, String sapCode) {
		
		if (attrTrackDict.containsKey(attrIdx)){
			if ( (attrTrackDict.get(attrIdx)).containsKey(sapCode) ){
				return (attrTrackDict.get(attrIdx)).get(sapCode);
			} else {
				return null;
			}		
		} else {
			logger.debug("NO TRACKED ATTRIBUTES FOR RES ATTR IDX: " + attrIdx );
			return null;			
		}
	}

	public boolean isTrackedAttrDuplicateException(Integer attrIdx,String srcCode) {
		if (trackedAttrExcpMap.containsKey(attrIdx)){trackedAttrExcpMap.size();
			return trackedAttrExcpMap.get(attrIdx).contains(srcCode);
		}
		//if ("JOB_LEVEL".equals(attrName) && "002".equals(srcCode)) return true;
		//if ("JOB_LEVEL".equals(attrName) && "412".equals(srcCode)) return true;			
		return false;
	}
	
	

	public DictionaryDao getDictionaryDao() {
		return dictionaryDao;
	}

	public void setDictionaryDao(DictionaryDao dictionaryDao) {
		this.dictionaryDao = dictionaryDao;
	}



	public HashMap<Integer, List<String>> getTrackedAttrExcpMap() {
		return trackedAttrExcpMap;
	}

	public void setTrackedAttrExcpMap(
			HashMap<Integer, List<String>> trackedAttrExcpMap) {
		this.trackedAttrExcpMap = trackedAttrExcpMap;
	}

	public HashMap<Integer, HashMap<String, String>> getAttrTrackDict() {
		return attrTrackDict;
	}

	public void setAttrTrackDict(
			HashMap<Integer, HashMap<String, String>> attrTrackDict) {
		this.attrTrackDict = attrTrackDict;
	}

	public String checkForTrackedAttrException(Integer attrIdx, String attrValue) {
		
		if (attrValue != null && attrIdx == LOCAL_JOB_POS_IDX ){			
			String[] res = StringUtils.splitPreserveAllTokens(attrValue, "-", 3);			
			if ( res.length != 3)
			{
				// this should not happend in general
				logger.error("Unable to  parse attribute value: " + attrValue + ", for attr Local Job Position should consit of 3 values: code, abbr, desc");
				return attrValue;
			}
			return res[0] + "-" + res[1];
		} else {					
			return attrValue;
		}
	}	
	
	
	
	
	public String getResultAttrDesc(Integer resAttrIdx){
		if (resultAttrDescMap.containsKey(resAttrIdx))
		{
			return resultAttrDescMap.get(resAttrIdx); 
		}
		// this should not happend 
		return "";
		
	}
	
	private int getStringLength(String s){
		if (s != null){
			return s.length();
		}
		return 0;
	}

	/**
	 * Get list of AP Ids that use value of the given attribute
	 * @param ta result attribute to check
	 * @return list of assignment profile Ids
	 */
	public List<String> getAssgnProfiles(TrackedAttr ta) {
		 
		String dictValue = ta.getDictValue();
		
		String newValue = ta.getMappedValue();

		List<String> result = new ArrayList<String>();
		
		int dictValueLength = getStringLength(dictValue);
		if (assgnProfileMap.containsKey(ta.getAttrIdx()) == true){
			
			HashMap<String, List<String>> assgnProfileValuesMap = assgnProfileMap.get(ta.getAttrIdx());
			for ( String apValue : assgnProfileValuesMap.keySet()){
				int apValueLength = getStringLength(apValue);
				
				if ( dictValueLength == apValueLength){
					if (StringUtils.equals(apValue, dictValue)){
						result.addAll(assgnProfileValuesMap.get(apValue));						
					}					
				} else {
					
					// if the assignem profile uses the like operator
					// and values in format for ex code-abbr
					// chekc if the new value can affect the assignment profile
					if ( StringUtils.startsWith(dictValue, apValue)){
						
						int hyphenIdx = StringUtils.indexOf(apValue, '-');
						
						// is inner '-' present in the asignment profile value?
						if (hyphenIdx >  0 && hyphenIdx < apValueLength - 1 ){
							
							int diff = StringUtils.indexOfDifference(apValue, newValue);
							
							// if the difference between new value and apValues is after hyphen
							// the change probably will affect the assignment profile 
							if ( diff > hyphenIdx && diff < apValueLength  ){
								result.addAll(assgnProfileValuesMap.get(apValue));
							}			
						}			
					}					
				}
			}
		}
		return result;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public boolean isActiveUserData(SourceRow sourceRow) {
		
		String status = dictionary.findEmpStatus(sourceRow.getValue( sourceFileConf.getSrcUserStatusID().getSrcColIndex() ));
		
		if ( "N".equalsIgnoreCase( status ) ){			
			return true;			
		} else {				
			return false;
		}
		
	}

	
	
}
