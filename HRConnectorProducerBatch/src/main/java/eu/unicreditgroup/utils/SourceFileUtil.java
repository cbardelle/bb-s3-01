package eu.unicreditgroup.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;

/**
 * This is a helper class for grouping the functionality related to all
 * operations on source files like: download, upload, backup etc
 * 
 * @author UV00074
 * 
 */

public class SourceFileUtil {

	private ReportDataBean reportDataBean;

	private ConfigParams configParams;

	private FTPUploader sourceDownloader;

	private FTPUploader resultUploader;

	Logger logger = Logger.getLogger(SourceFileUtil.class);
	

	public void downloadSapFile(String sapFileName) throws Exception {

		String sourceFileName = "";
		
		backupSourceFile(sapFileName);
		
		logger.info("Starting download of SAP file: " + sapFileName);
		
		sourceDownloader.downloadFile(sapFileName, configParams.getStrSourceLocalDir() + "\\" + sapFileName);
		
		logger.info("Result file : " + sapFileName + " downloaded.");
		
	}
	

	 public void backupAllSourceFiles() throws Exception 
	 {
		 String localSrcPath = configParams.getStrSourceLocalDir();
		 
		 String sourceFileName= "";
		 
		 for (String  srcFileName: configParams.getSourceFilesList() ){	
			 try {
				 sourceFileName = srcFileName; 
				 backupFile(localSrcPath + "\\" + srcFileName);
				 
			 } catch (Exception e){
				 if (StringUtils.equalsIgnoreCase(sourceFileName, configParams.getStrPrimarySourceFileName())){
					 throw e;
				 } else {
					 logger.info("Failed to backup non-primary source file: " + sourceFileName);
				 }
			 }
		 }
	 }

	

	public void backupSourceFile(String sapFileName) throws Exception {

		String localSrcPath = configParams.getStrSourceLocalDir();
		
		backupFile(localSrcPath + "\\" + sapFileName);			
		
	}
	
	

	public void moveProcessedSapFile(String sapFileName) {
		if (reportDataBean.getFailedFastException() == null) {

			String processedSapFilePath = "";
			try {
				logger.info("Moving SAP file : " + sapFileName);

				SimpleDateFormat sdf = new SimpleDateFormat(
						"'_'dd-MM-yyyy__HH_mm_ss'.csv'");
				processedSapFilePath = configParams
						.getStrProcessedSapFileFtpDir()
						+ "/"
						+ configParams.getFileNameWithoutExt(sapFileName)
						+ sdf.format(new Date());

				String sapFileFtpPath = configParams.getStrSourceRemoteDir()
						+ "/" + sapFileName;

				resultUploader.moveFile(sapFileFtpPath, processedSapFilePath);

				logger.info("SAP file moved.");
			} catch (Exception e) {
				logger.error(
						"Failed to move SAP file: " + processedSapFilePath, e);
			}
		}
	}
	
	
	public void moveProcessedExtrnalUsersFile(){
		
		String processedFileFtpPath = "";
		
		try {
			logger.info("Starting moving of external user source file to processed dir on ftp ...");
			
			String extUserFileName = configParams.getStrExternalUserSourceFileName();
				
			SimpleDateFormat sdf =	new SimpleDateFormat("'_'dd-MM-yyyy__HH_mm_ss'.csv'");	
			
			processedFileFtpPath = configParams.getStrProcessedExternalUserFileFtpDir() + "/"					
			+ configParams.getFileNameWithoutExt(extUserFileName)									
			+ sdf.format(new Date());
			
			String extUserFileFtpPath = configParams.getStrFtpExternalUserDirectory() + "/" + extUserFileName; 
			
			try {					
				resultUploader.moveFile(extUserFileFtpPath, processedFileFtpPath);					
			} catch (Exception e){

					 logger.error("Failed to move external user source file : " + extUserFileName);
			}
			
			logger.info("External user source moved.");
		} catch (Exception e) {
			logger.error("Failed to move external user source file  to: " + processedFileFtpPath ,e);					
		}
	}
	
	
	public void moveProcessedSapFiles(){
		if (reportDataBean.getFailedFastException() == null ){
			
			String processedSapFilePath = "";
			
			String sourceFileName = "";
			
			try {
				logger.info("Starting moving of source files to processed dir ...");
				
				for(String srcFileName : configParams.getSourceFilesListToMove()){
					
					
					SimpleDateFormat sdf =	new SimpleDateFormat("'_'dd-MM-yyyy__HH_mm_ss'.csv'");					
					processedSapFilePath = configParams.getStrProcessedSapFileFtpDir() + "/" 
					+ configParams.getFileNameWithoutExt(srcFileName)									
					+ sdf.format(new Date());
					
					String sapFilePath = configParams.getStrSourceRemoteDir() + "/" + srcFileName; 
					sourceFileName = srcFileName;
					try {					
						resultUploader.moveFile(sapFilePath, processedSapFilePath);					
					} catch (Exception e){
						if (StringUtils.equalsIgnoreCase(sourceFileName, configParams.getStrPrimarySourceFileName())){
							 throw e;
						} else {
							 logger.info("Failed to move non-primary source file: " + sourceFileName);
						}
					}
				}
				logger.info("Sources file moved.");
			} catch (Exception e) {
				logger.info("Failed to move primary sap source file to: " + processedSapFilePath ,e);					
			}
		}
	}
	
	
	

	public void backupFile(String strSourceFilePath) throws Exception {

		byte[] buf = new byte[1024];

		File file = new File(strSourceFilePath);

		if (!file.exists()) {
			return;
		}

		FileInputStream in = new FileInputStream(file);

		String fileName = stripFileExtension(file.getName());

		String dir = file.getParent();

		String zipPattern = "'" + fileName + "_'dd-MM-yyyy__HH_mm_ss'.zip'";

		SimpleDateFormat sdf = new SimpleDateFormat(zipPattern);

		String zipFileName = sdf.format(new Date());

		// Create the ZIP file

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(dir
				+ "\\" + zipFileName));

		// Compress the files
		// Add ZIP entry to output stream.
		out.putNextEntry(new ZipEntry(file.getName()));

		// Transfer bytes from the file to the ZIP file
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}

		// Complete the entry
		out.closeEntry();
		in.close();

		// Complete the ZIP file
		out.close();

		// delete the origin
		file.delete();
	}

	private String stripFileExtension(String fileName) {
		int dotInd = fileName.lastIndexOf('.');
		return (dotInd > 0) ? fileName.substring(0, dotInd) : fileName;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public FTPUploader getSourceDownloader() {
		return sourceDownloader;
	}

	public void setSourceDownloader(FTPUploader sourceDownloader) {
		this.sourceDownloader = sourceDownloader;
	}

	public FTPUploader getResultUploader() {
		return resultUploader;
	}

	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}

}
