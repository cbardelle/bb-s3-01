package eu.unicreditgroup.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

public class ParametersListParser {

	
	private static final String DELIMITERS = " ,;";

	/**
	 * Parses given string and returns
	 * 
	 * @param paramsString
	 * @return
	 */
	
	public List<String> parseAsList(String paramsString) {
		 return  Arrays.asList(parse(paramsString));		
		
	}
	
	public String[] parse(String paramsString) {
		
		if (paramsString == null){
			paramsString = "";
		}
		
		return StringUtils.tokenizeToStringArray(paramsString, DELIMITERS, true, true);
		
		
	}

}
