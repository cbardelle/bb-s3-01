package eu.unicreditgroup.utils;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.beans.ContractType;
import eu.unicreditgroup.beans.EmpStatus;
import eu.unicreditgroup.beans.ExternalUserJob;
import eu.unicreditgroup.beans.FullDepCode;
import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.Locale;
import eu.unicreditgroup.beans.Province;
import eu.unicreditgroup.beans.TimeZone;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.beans.UserCustValue;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.DictionaryDao;

public class Dictionary implements InitializingBean{
	
	private static final Logger logger = Logger.getLogger(Dictionary.class);
	
	private DictionaryDao dictionaryDao;
	
	private HashMap<String,String> timeZoneMap;

	private HashMap<String,String> localeMap;
	
	private HashMap<String,String> empStatusMap;
	
	private List<String> currencyList;
	
	private HashMap<String,ContractType> contractTypesMap;
	
	private HashMap<String,String> provinceMap;
	
	private HashMap<String,String> plateauRoleExecption;
	
	private HashMap<String,String> fullDepartmentCodes;
	
	private HashMap<String,String> lineManagerExecption;
	
	private HashMap<String,HRBPUser> hrbpUsersMap;
	
	private List<String> italianOrganizations;
		
	private List<String> usersGermanManagers;
	
	/*
	 * HashMap<userId,HashMap<resColIdx,resColValue>>
	 */
	private HashMap<String,HashMap<Integer,String>> customValueForResultColumn;
	
	
	// 2014-04-04
	// this is the additional functionality to reset LM for users in table HR_CONN_LM_EXCP 
	// (requested by A.Sacchi)
	public final String LINE_MANAGER_EXCP_NULL = "-=@@@LINE_MANAGER_EXCP_NULL@@@=-";	
	
	
	/**
	 * Job codes and job descriptions for the external users (from the file source_aaext.csv)
	 * 
	 */
	private HashMap<String,String> externalUsersJobs;
		
	
	/* Table for providing user's custom email (read from HR_CONN_CUST_EMAIL) */
	private HashMap<String,String> customEmails;
	
	
	public String findTimeZone(String countryId){
		
		if (! timeZoneMap.containsKey(countryId) ){
			return null;
		} else {
			return timeZoneMap.get(countryId);
		}		
	}
	
	
	
	private void initTimeZoneMap(){
		timeZoneMap = new HashMap<String,String>();		
		List<TimeZone> timeZones = dictionaryDao.getAllTimeZones();
		for( TimeZone tz : timeZones )		
		{			
			timeZoneMap.put(tz.getCountryID(), tz.getPlateauTimeZoneID());
		}		
	}
	
	
	private void initCustomValueForResultColumn(){
		
		//HashMap<userId,HashMap<resColIdx,resColValue>>
		customValueForResultColumn = new HashMap<String,HashMap<Integer,String>>();
		
		List<UserCustValue> values = dictionaryDao.getAllcustomValueForResultColumn();
		
		for ( UserCustValue ucv : values ){
			if (customValueForResultColumn.containsKey( ucv.getUserId() )){
				
				HashMap<Integer,String> custValues = customValueForResultColumn.get(ucv.getUserId()); 
				if (custValues.containsKey( ucv.getResAttrIdx() )){
					//this is not going to happen because resAttrIdx is part of the primary key					
				} else {
					custValues.put(ucv.getResAttrIdx(), ucv.getCustVal());					
				}
				
			} else {
				HashMap<Integer,String> custValues = new HashMap<Integer,String>();
				custValues.put(ucv.getResAttrIdx(), ucv.getCustVal());
				customValueForResultColumn.put(ucv.getUserId(), custValues);
			}			
		}				
				
	}
	
	
	private void initCustomEmails(){
		customEmails = new HashMap<String,String>();		
		List<User> users = dictionaryDao.getCustomEmails();
		for( User usr : users )		
		{			
			customEmails.put(usr.getUserId(), usr.getEmail());
		}		
	}
	
	private void initLocaleMap(){
		localeMap = new HashMap<String,String>();		
		List<Locale> locales = dictionaryDao.getAllLocales();
		for( Locale locale : locales )		
		{			
			localeMap.put(locale.getSourceNationId(), locale.getPlateauLocaleId());
		}		
	}
	
	public String findLocale(String nationdId){
		return localeMap.get(nationdId);		
	}
	
	private void initEmpStatusMap(){
		empStatusMap = new HashMap<String,String>();		
		List<EmpStatus> empStatuses = dictionaryDao.getAllEmpStatus();
		for( EmpStatus empStatus : empStatuses )		
		{			
			empStatusMap.put(empStatus.getSourceUserStatus(), empStatus.getPlateauNotActive());
		}		
	}
	
	public boolean isCurrencyAvaliable(String currCode){
		if (currCode == null)
		{
			return false;
		}		
		return currencyList.contains(currCode.toUpperCase());				
	}
	
	
	private void initCurrencyList(){
		currencyList = dictionaryDao.getAllCurrency();		
	}
	
	private void initItalianOrganizations(){
		italianOrganizations = dictionaryDao.getItalianOrganizations();		
	}
	
	private void initContractTypesMap(){
		contractTypesMap = new HashMap<String,ContractType>();
		List<ContractType> contractTypes = dictionaryDao.getAllContractTypes();
		for( ContractType contracType : contractTypes)
		{
			contractTypesMap.put(contracType.getSourceEmpTypeId(), contracType);
		}
	}
	
	public ContractType findContractType(String contractTypeCode){		
		return contractTypesMap.get(contractTypeCode);
	}
		
	public String findItalianRegion(String province){
		
		if (! provinceMap.containsKey(province) ){
			return null;
		} else {
			return provinceMap.get(province);
		}
	}
	
	public String findCutomsEmail(String userId){
		
		if (! customEmails.containsKey(userId) ){
			return null;
		} else {
			return customEmails.get(userId);
		}
	}
	
	private void initProvinceMap(){
		provinceMap = new HashMap<String,String>();
		List<Province> provinces = dictionaryDao.getAllProvinces();
		for(Province province : provinces){			
			provinceMap.put(province.getProvinceCode(), province.getRegionCode());
		}		
	}
	
	private void initPlateauRoleExecption(){
		
		plateauRoleExecption = new HashMap<String,String>();
		
		List<User> excp = dictionaryDao.getPlateauRoleExecptions();
		for(User user: excp){			
			plateauRoleExecption.put(user.getUserId(), user.getPlateauRole());
		}	
		
	}
	
	private void initLineManagerExecption(){
		
		lineManagerExecption = new HashMap<String,String>();
		
		List<User> excp = dictionaryDao.getLineManagerExecptions();
		for(User user: excp){			
			lineManagerExecption.put(user.getUserId(), user.getLineManager());			
		}	
		
	}
	
	
	public void initHrbpUsersMap(){
		
		hrbpUsersMap = new HashMap<String,HRBPUser>();
		List<HRBPUser> hrbpUsers =  dictionaryDao.getAllHrbpUsers();
		for (HRBPUser hrbpUser : hrbpUsers){
			hrbpUsersMap.put(hrbpUser.getUserId(), hrbpUser);			
		}
	}
	
	public void initExternalUsersJobs(){
		
		externalUsersJobs =  new HashMap<String,String>();
		
		List<ExternalUserJob> jobs = dictionaryDao.getExternalUserJobs();
		for(ExternalUserJob job : jobs){			
			externalUsersJobs.put(job.getJobCode(), job.getJobDescr());			
		}		
	}
	
	public void initFullDepartmentCodes(){
		
		fullDepartmentCodes =  new HashMap<String,String>();
		
		List<FullDepCode> codes = dictionaryDao.getFullDepartmentsCodes();
		
		
		for(FullDepCode fdp : codes){			
			fullDepartmentCodes.put(fdp.getOfficeSapCode(), fdp.getFullDepCode());			
		}		
	}
	
	
	public String findExternalUsersJobsDesc( String jobCode ){
		
		if ( externalUsersJobs.containsKey(jobCode) ){
			
			return externalUsersJobs.get(jobCode);
			
		} else {
			
			return null;
			
		}		
	}
	
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		initTimeZoneMap();		
		initLocaleMap();
		initEmpStatusMap();
		initCurrencyList();
		initContractTypesMap();
		initProvinceMap();
		initCustomEmails();
		initItalianOrganizations();
		initPlateauRoleExecption();
		initLineManagerExecption();
		initExternalUsersJobs();
		initFullDepartmentCodes();
		initCustomValueForResultColumn();
		
		
	}	

	public DictionaryDao getDictionaryDao() {
		return dictionaryDao;
	}

	public void setDictionaryDao(DictionaryDao dictionaryDao) {
		this.dictionaryDao = dictionaryDao;
	}

	public String findEmpStatus(String sourceUserStatus){		
		
		if (! empStatusMap.containsKey(sourceUserStatus) ){
			return null;
		} else {
			return empStatusMap.get(sourceUserStatus);
		}		
	}
	
	public HRBPUser findHRBPUser(String userId){
		
		if (hrbpUsersMap == null)
		{
			return null;
		}
		
		return hrbpUsersMap.get(userId);		
	}
	
	public boolean isItalianOrg(String orgCode){
		
		if (italianOrganizations == null)
		{
			return false;
		}		
		return italianOrganizations.contains(orgCode.toUpperCase());
	}
	
	public HashMap<String, String> getTimeZoneMap() {
		return timeZoneMap;
	}

	public HashMap<String, String> getLocaleMap() {
		return localeMap;
	}

	public HashMap<String, String> getEmpStatusMap() {
		return empStatusMap;
	}

	public List<String> getCurrencyList() {
		return currencyList;
	}

	public HashMap<String, ContractType> getContractTypesMap() {
		return contractTypesMap;
	}

	public HashMap<String, String> getProvinceMap() {
		return provinceMap;
	}

	public HashMap<String, HRBPUser> getHrbpUsersMap() {
		return hrbpUsersMap;
	}

	public String checkForPlateauRoleExcp(String userId) {
		if (plateauRoleExecption.containsKey(userId)){
			return plateauRoleExecption.get(userId);
		} else {
			return null;
		}			
	}
	
	
	public String checkForLineManagerExcp(String userId) {
		if (lineManagerExecption.containsKey(userId)){
			String lme = lineManagerExecption.get(userId);
			
			if ( StringUtils.isEmpty( lme ) ){
				
				// 2014-04-04
				// this is the additional functionality to reset LM for users in table HR_CONN_LM_EXCP 
				// we sent this constant to indicate that the user should be assign that null line manager
				
				return LINE_MANAGER_EXCP_NULL;
				
			} else {
				
				return lme;
			}
			
		} else {
			return null;
		}			
	}

	public String getFullDepartmentCode(String userOfficeCode) {
		if (fullDepartmentCodes.containsKey(userOfficeCode)){
			return fullDepartmentCodes.get(userOfficeCode);
		} else {
			return null;
		}			
	}
	
	/**
	 * Check if a user is a German managers
	 * All german managers are present for the moment in the 
	 * table HR_CONN_SAP_EXCLD, and this is the only way to distinguish them
	 * 
	 *  
	 * 
	 * @param userId
	 * @return
	 */
	public boolean checkForGermanManager(String userId){
		
		// lazy init
		if ( usersGermanManagers == null ){			
			usersGermanManagers = dictionaryDao.getGermanManagers();			
		}
		
		if ( userId == null ) return false;
		
		if ( usersGermanManagers == null ) return false;
		
		if ( usersGermanManagers.contains(userId) ) {
			return true;
		} else {
			return false;
		}
		
	
	}

	public String getCustomValueForResultColumn(String userID, int resColIndex) {

		if (customValueForResultColumn.containsKey( userID )){
			
			Integer idx = Integer.valueOf(resColIndex);
			
			if (customValueForResultColumn.get(userID).containsKey( idx) ){				
				return customValueForResultColumn.get(userID).get( idx );
			}			
		}		
		return null;
	}



	public HashMap<String, String> getFullDepartmentCodes() {
		return fullDepartmentCodes;
	}
	
	
	
	
	
}
