package eu.unicreditgroup.mailer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.ExpatUser;
import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.MappingException;
import eu.unicreditgroup.beans.NewTrackedAttr;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mapper.exception.InvalidColumnException;
import eu.unicreditgroup.mapper.exception.InvalidRowException;

public class ReportDataBean {
	
	private final int ROW_EXCEPTIONS_LIMIT = 200;
	
	private final int COL_EXCEPTIONS_LIMIT = 500;
	
	private Date batchStartDate;
	
	private Date batchEndDate;
	
	/**
	 * Number of rows in source file
	 */	
	private int sourceFileRowsCount;//* included in report *//
	
	/**
	 * Number of invalid rows in source file
	 */	
	private int sourceFileInvalidRowsCount;//* included in report *//
	
	/**
	 * Number of invalid columns in source file
	 */	
	private int sourceFileInvalidColumnsCount;//* included in report *//
	
	/**
	 * Number of identified HRBP users in source file
	 */	
	private int sourceFileHRBPUsersCount; //* included in report *//
	
	/**
	 * Number of excluded outbound expats
	 */
	private int excludedOutboundExpatCount;	
	
	
	/**
	 * Number of excluded Austrian users
	 */
	private int excludedAustrianUsersCount;
	
	
	/**
	 * Number of excluded German users
	 */
	private int excludedGermanUsersCount;
		
		
	/**
	 * Number of valid HRBP users in source file (active and present)
	 */
	private int validHRBPUsersCount; //* included in report *//	
	
	/**
	 * Number of rows inserted into the temp source table
	 */
	private int tempSourceTableRowsInserted; //* NOT included in report *//

	/**
	 * number of rows inserted into the main source table
	 */
	private int mainSourceTableRowsInserted; //* included in report *//
	
	
	/**
	 * number of rows inserted into HR_CONN_SAP_EXCLD
	 */
	private int excludedTableRowsInserted = 0;
	
		
	/**
	 * number of rows with duplicated User Id column
	 */
	private int sourceFileDuplicatedUserIdCount; //* included in report *//
	
	/**
	 * Number of successfully mapped source rows 
	 */
	private int mappedRowsCount; //* included in report *//
	
	//private int resultFileRowsCount;		
	/**
	 * List of invalid HRBP users e.g. not present or not active
	 */
	private  BlockingQueue<String> invalidHRBPQueue;
	
	/**
	 * HRBP users present in the HR_CONN_HRBP_TMP table
	 * and present in the HR_CONN_HRBP table
	 */
	private List<HRBPUser> newHRBPUsers; 
	
	/**
	 * HRBP users present in the HR_CONN_HRBP table
	 * and present in the HR_CONN_HRBP_TMP table
	 */
	private List<HRBPUser> removedHRBPUsers;	
	
	/**
	 * Map of duplicated User Id 
	 * key 		- User Id
	 * value 	- duplicates count 
	 */
	private ConcurrentHashMap<String,Integer> duplicatedUserId;
	
	/**
	 * New HRBP users with empty domain (with no user) 
	 */
	private List<HRBPUser> newHRBPUsersWithEmptyDomain;

	/**
	 * Users that had previously empty domains, but now
	 * they have been filled
	 */
	private List<HRBPUser> usersWithFilledDomain;	
	
	/**
	 * Users with empty resume column in the source file
	 */
	private List<User> usersWithEmptyResume;
	
	/**
	 * Users whose userId has been changed
	 */
	private List<User> usersWithChangedId;
		
	private Date stepLoadSourceTableStartDate;
	
	private Date stepLoadSourceTableEndDate;
	
	private Date stepGenerateResultFileStartDate;
	
	private Date stepGenerateResultFileEndDate;
	
	private Date stepLoadOrgLstTableStartDate;
	
	private Date stepLoadOrgLstTableEndDate;
		
	private BlockingQueue<Exception> exceptionsQueue;
	
	/**
	 * Queue for new values of tracked attributes
	 */
	private BlockingQueue<NewTrackedAttr> newTrackedAttrQueue;
	
	/**
	 * Queue for changed values of tracked attributes
	 */
	private BlockingQueue<TrackedAttr> changedTrackedAttrQueue;	
	
	
	/**
	 * List of external users that will be deactivated
	 * (users not present any more in the file with external users)
	 */
	private String externalUsersToDeactivateList; 
	
	
	/**
	 * The exception which will prevent the batch from proceeding
	 * mostly like IOException reading source/writing result
	 */
	private Throwable failedFastException;
		
	private ConfigParams configParams;

	/**
	 * new or reappeared expat user 
	 * 
	 */
	private List<ExpatUser> outboundExpatsUsers;
	
	
	/**
	 * Number of organizations imported from the source file: source_org_lst.csv
	 * 
	 */
	private int processedOrgCount;
	
	
	/**
	 * Number of user in the source file with empty email
	 * 
	 */
	private int usersWithEmptyEmailCount;
	
	/**
	 * List of sucessfully processed source files
	 */
	private List<String> processedSourceFiles = new ArrayList<String>();
	
	/**
	 * count of valid rows in file with external users (Anagrafe Alleati)
	 * 
	 */
	private int externalUsersValidRowsCount;

	/**
	 * count of invalid rows in file with external users (Anagrafe Alleati)
	 * 
	 */
	private int externalUsersInvalidRowsCount;
	
	/**
	 * The flag indicating that the maximum count of  rows in the file 
	 * with external users (Anagrafe Alleati)  has been exceeded 
	 * 
	 */
	private boolean externalUsersMaxCountExceeded = false;
	
	/**
	 * List of reactivated external users 
	 * (present in HR_CONN_SAP_SRC_HST with REMOVED_DATE and in HR_CONN_SAP_SRC)    
	 */
	private String listOfReactivatedExternalUsers;
	
		
	synchronized public void incSourceFileRowsCount(){
		sourceFileRowsCount++;
	}
	
	synchronized public void incSourceFileInvalidRowsCount(){
		sourceFileInvalidRowsCount++;
	}
	
	synchronized public void addSourceFileInvalidRowsCount(int count){
		sourceFileInvalidRowsCount += count;
	}
	
	synchronized public void incSourceFileInvalidColumnsCount(){
		sourceFileInvalidColumnsCount++;
	}
	
	synchronized public void addSourceFileInvalidColumnsCount(int count){
		sourceFileInvalidColumnsCount += count;
	}
	
	synchronized public void incSourceFileHRBPUsersCount(){
		sourceFileHRBPUsersCount++;
	}
	
	synchronized public void incExcludedOutboundExpatCount(){
		excludedOutboundExpatCount++;
	}
	
	
	synchronized public void incExcludedAustrianUsersCount(){
		excludedAustrianUsersCount++;
	}
	
	
	synchronized public void incExcludedGermanUsersCount(){
		excludedGermanUsersCount++;
	}
	
	
	synchronized public void incMappedRowsCount(){		
		mappedRowsCount++;		
	}
	
	synchronized public void addTempSourceTableRowsInserted(int count) {
		tempSourceTableRowsInserted += count;		
	}

	synchronized public void removeTempSourceTableRowsInserted(int count) {
		tempSourceTableRowsInserted -= count;		
	}
	
	synchronized public void incTempSourceTableRowsInserted() {
		tempSourceTableRowsInserted++;		
	}
	
	private final String ANAGRAFE_ALLEATI_FILENAME = "source_aaext.csv"; 
				
	private boolean presentAnagrafeAlleatiSourceFile;
	
	
	/*
	synchronized public void  incResultFileRowsCount(){
		resultFileRowsCount++; 
	}*/
	
	
	Logger logger = Logger.getLogger(ReportDataBean.class);
	
	private String listToString( List<HRBPUser> list){
		if (list == null) {
			//return null;
			
			return "";
		}
		boolean firsItem = true;
		StringBuilder sb = new StringBuilder();
		for (HRBPUser user : list){
			if (firsItem == true){
				firsItem = false;
			} else {
				sb.append(", ");
			}
			sb.append(user.getUserId());			
		}
		return sb.toString();		
	}
	
	
	
		
	public String getUsersWithChangedIdList(){
		
		boolean firsItem = true;
		StringBuilder sb = new StringBuilder();
		
		if (usersWithChangedId == null || usersWithChangedId.size() == 0){
			return "";
		}
		
		for (User user : usersWithChangedId){
			if (firsItem == true){
				firsItem = false;
			} else {
				sb.append(", ");
			}
			sb.append(user.getOldUserId()).append("->").append(user.getUserId());			
		}
		return sb.toString();
	}
	
	public String getUsersWithEmptyResumeList(){
		
		boolean firsItem = true;
		StringBuilder sb = new StringBuilder();
		
		if (usersWithEmptyResume == null || usersWithEmptyResume.size() == 0){
			return "";
		}
		
		for (User user : usersWithEmptyResume){
			if (firsItem == true){
				firsItem = false;
			} else {
				sb.append(", ");
			}
			sb.append(user.getUserId());			
		}
		String result = sb.toString().trim();
		if (result == null) return "";
		return  result;
		
	}
	
	public String getNewHRBPUsersList()	
	{
		/*
		boolean firsItem = true;
		StringBuilder sb = new StringBuilder();
		for (HRBPUser user : newHRBPUsers){
			if (firsItem == true){
				firsItem = false;
			} else {
				sb.append(", ");
			}
			sb.append(user.getUserId());			
		}
		return sb.toString();
		*/
		return listToString(newHRBPUsers);
	}
	

	public String getRemovedHRBPUsersList()	
	{
		/*
		boolean firsItem = true;
		StringBuilder sb = new StringBuilder();
		for (HRBPUser user : removedHRBPUsers){
			if (firsItem == true){
				firsItem = false;
			} else {
				sb.append(", ");
			}
			sb.append(user.getUserId());			
		}
		return sb.toString();
		*/
		return listToString(removedHRBPUsers); 
	}
	
	
	
	public String getNewHRBPUsersWithEmptyDomainList()
	{
		return listToString(newHRBPUsersWithEmptyDomain);		
	}
	
	public String getUsersWithFilledDomainList(){
		return listToString(usersWithFilledDomain);
	}
	

	public String getInvalidHRBPUsersList()	
	{
		List<String> invalidHRBPList = new ArrayList<String> (invalidHRBPQueue);
		
		StringBuilder sb = new StringBuilder();
		boolean firsItem = true;
		
				
		HashSet<String> invalidHRBPSet = new HashSet<String>();
		
		// remove duplicates if any
		for ( String userId : invalidHRBPList ){
			
			userId = StringUtils.trim(userId);
			logger.debug("Invalid HRBP user on the list: " + userId);
			
			if (!invalidHRBPSet.contains( userId ))
			{
				logger.debug("Adding distinct invalid HRBP user to the report: " + userId);
				invalidHRBPSet.add(userId);
			}
		}
		
		// duplicates removed
		for ( String usrId : invalidHRBPSet ){
			if (firsItem == true){
				firsItem = false;
			} else {
				sb.append(", ");
			}
			sb.append(usrId);					
		}
		
		return sb.toString();	
	}
	
	
	public String getRejectedRowsList()
	{
		List<Throwable> exceptions = new ArrayList<Throwable> (exceptionsQueue);
		
		StringBuilder sb = new StringBuilder();
		int invalidRows = 0;
		for ( Throwable exception : exceptions ){
			if (exception instanceof InvalidRowException)
			{
				invalidRows++;				
				sb.append(exception.getMessage()).append("<BR>");				
			}			
		}
		if (sourceFileInvalidRowsCount > invalidRows ){
			String diff = String.valueOf(sourceFileInvalidRowsCount - invalidRows);
			sb.append("(and ").append(diff).append(" error(s) more...)");
		}		
		return sb.toString();		
	}
		
	
	public int getNewTrackedAttrCount(){
		return newTrackedAttrQueue.size();		
	}
	
	/*
	public String getNewTrackedAttrList()
	{		
		List<NewTrackedAttr> newAttr = new ArrayList<NewTrackedAttr>(newTrackedAttrQueue);
		StringBuilder sb = new StringBuilder();
		
		Collections.sort(newAttr);
		
		for ( NewTrackedAttr ta : newAttr ){							
				sb.append( ta.getAttrDesc() ).append(", ").append(ta.getAttrCode()).append(", ").append(ta.getMappedValue()).append("<BR>");
		}		
		return sb.toString();
	}*/
	
	
	public List<TrackedAttr> getChangedTrackedAttrWithAsgnPrfList()
	{		
		List<TrackedAttr> changedAttr = new ArrayList<TrackedAttr>(changedTrackedAttrQueue);
		List<TrackedAttr> result = new ArrayList<TrackedAttr>();
		
		Collections.sort(changedAttr);
		
		for ( TrackedAttr ta : changedAttr ){			
						
			if (ta.getAssgnProfiles() != null && ta.getAssgnProfiles().isEmpty() == false ){
				result.add(ta);
			}				
		}		
		return result;
	}

	public List<TrackedAttr> getChangedTrackedAttrNoAsgnPrfList()
	{		
		List<TrackedAttr> changedAttr = new ArrayList<TrackedAttr>(changedTrackedAttrQueue);
		List<TrackedAttr> result = new ArrayList<TrackedAttr>();
		
		Collections.sort(changedAttr);
		
		for ( TrackedAttr ta : changedAttr ){			
						
			if (ta.getAssgnProfiles() == null || ta.getAssgnProfiles().isEmpty() == true ){
				result.add(ta);
			}				
		}		
		return result;
	}

	
	
	public String getInvalidColumnsList()
	{
		List<Throwable> exceptions = new ArrayList<Throwable> (exceptionsQueue);
		
		StringBuilder sb = new StringBuilder();		
		int invalidCols = 0;
		for ( Throwable exception : exceptions ){
			if (exception instanceof InvalidColumnException)
			{
				invalidCols++;			
				sb.append(exception.getMessage()).append("<BR>");				
			}			
		}
		if (sourceFileInvalidColumnsCount > invalidCols ){
			String diff = String.valueOf(sourceFileInvalidColumnsCount - invalidCols);
			sb.append("(and ").append(diff).append(" error(s) more...)");
		}		
		return sb.toString();
	}
	
	 public void addNewTrackedAttr(NewTrackedAttr ta){
			 
		 
		 if (newTrackedAttrQueue.contains(ta) == false){
			 newTrackedAttrQueue.add(ta);
		 } else {
			 // check for possible duplicates
			 Iterator<NewTrackedAttr> it = newTrackedAttrQueue.iterator();
			 NewTrackedAttr check;
			 
			 while(it.hasNext()){ 
				 check = it.next();
				 if (check.equals(ta) && check.getMappedValue().equals(ta.getMappedValue()) == false ){
					 logger.debug("DUPLICATED NEW VALUES! user attr: " + ta.getAttrDesc() + ", value: " + ta.getMappedValue());		 
					 
				 }				 	
			 }			 
		 }
	 }
	
	 public void addChangedTrackedAttr (TrackedAttr ta){
		 if (changedTrackedAttrQueue.contains(ta) == false){
			 changedTrackedAttrQueue.add(ta);
		 } else {
			 // check for possible duplicates
			 Iterator<TrackedAttr> it = changedTrackedAttrQueue.iterator();
			 TrackedAttr check;
			 
			 while(it.hasNext()){ 
				 check = it.next();
				 if (check.equals(ta) && check.getMappedValue().equals(ta.getMappedValue()) == false ){
					 logger.debug("DUPLICATED CHANGED VALUES! user attr: " + ta.getAttrDesc() + ", value: " + ta.getMappedValue());				 
					 
				 }				 	
			 }			 
		 }
	 }	
		
	public Date getBatchStartDate() {
		return batchStartDate;
	}

	public void setBatchStartDate(Date batchStartDate) {
		this.batchStartDate = batchStartDate;
	}

	public Date getBatchEndDate() {
		return batchEndDate;
	}

	public void setBatchEndDate(Date batchEndDate) {
		this.batchEndDate = batchEndDate;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public Date getStepLoadSourceTableStartDate() {
		return stepLoadSourceTableStartDate;
	}

	public void setStepLoadSourceTableStartDate(Date stepLoadSourceTableStartDate) {
		this.stepLoadSourceTableStartDate = stepLoadSourceTableStartDate;
	}

	public Date getStepLoadSourceTableEndDate() {
		return stepLoadSourceTableEndDate;
	}

	public void setStepLoadSourceTableEndDate(Date stepLoadSourceTableEndDate) {
		this.stepLoadSourceTableEndDate = stepLoadSourceTableEndDate;
	}

	public Date getStepGenerateResultFileStartDate() {
		return stepGenerateResultFileStartDate;
	}

	public void setStepGenerateResultFileStartDate(
			Date stepGenerateResultFileStartDate) {
		this.stepGenerateResultFileStartDate = stepGenerateResultFileStartDate;
	}

	public Date getStepGenerateResultFileEndDate() {
		return stepGenerateResultFileEndDate;
	}

	public void setStepGenerateResultFileEndDate(Date stepGenerateResultFileEndDate) {
		this.stepGenerateResultFileEndDate = stepGenerateResultFileEndDate;
	}

	public int getSourceFileRowsCount() {
		return sourceFileRowsCount;
	}

	public int getSourceFileRowsInvalidCount() {
		return sourceFileInvalidRowsCount;
	}

	public int getMappedRowsCount() {
		return mappedRowsCount;
	}
	
	public synchronized void addDuplicatedUserId(String userId) {				
		if ( duplicatedUserId.containsKey(userId) )
		{
			int count = duplicatedUserId.get(userId) + 1; 			
			duplicatedUserId.replace(userId, count);
			sourceFileDuplicatedUserIdCount++;
		}
		else 
		{
			// two rows are actually duplicated
			duplicatedUserId.put(userId, 2);
			sourceFileDuplicatedUserIdCount += 2;
		}		
		//sourceFileDuplicatedUserIdCount++;
		
	}
	

	public ConcurrentHashMap<String, Integer> getDuplicatedUserId() {
		return duplicatedUserId;
	}

	public void setDuplicatedUserId(
			ConcurrentHashMap<String, Integer> duplicatedUserId) {
		this.duplicatedUserId = duplicatedUserId;
	}
	
	public int getSourceFileDuplicatedUserIdCount() {
		return sourceFileDuplicatedUserIdCount;
			
		
		
	}

	public List<HRBPUser> getNewHRBPUsers() {
		return newHRBPUsers;
	}

	public void setNewHRBPUsers(List<HRBPUser> newHRBPUsers) {
		this.newHRBPUsers = newHRBPUsers;
	}

	public List<HRBPUser> getRemovedHRBPUsers() {
		return removedHRBPUsers;
	}
	
	public List<HRBPUser> getNewHRBPUsersWithEmptyDomain() {
		return newHRBPUsersWithEmptyDomain;
	}

	public List<HRBPUser> getUsersWithFilledDomain() {
		return usersWithFilledDomain;
	}

	public void setRemovedHRBPUsers(List<HRBPUser> removedHRBPUsers) {
		this.removedHRBPUsers = removedHRBPUsers;
	}

	public int getSourceFileHRBPUsersCount() {
		return sourceFileHRBPUsersCount;
	}

	public int getMainSourceTableRowsInserted() {
		return mainSourceTableRowsInserted;
	}

	public void setMainSourceTableRowsInserted(int mainSourceTableRowsInserted) {
		this.mainSourceTableRowsInserted = mainSourceTableRowsInserted;
	}

	public int getValidHRBPUsersCount() {
		return validHRBPUsersCount;
	}

	public void setValidHRBPUsersCount(int validHRBPUsersCount) {
		this.validHRBPUsersCount = validHRBPUsersCount;
	}

	public BlockingQueue<String> getInvalidHRBPQueue() {
		return invalidHRBPQueue;
	}

	public void setInvalidHRBPQueue(BlockingQueue<String> invalidHRBPQueue) {
		this.invalidHRBPQueue = invalidHRBPQueue;
	}

	public int getSourceFileInvalidRowsCount() {
		return sourceFileInvalidRowsCount;
	}

	public int getSourceFileInvalidColumnsCount() {
		return sourceFileInvalidColumnsCount;
	}

	public int getTempSourceTableRowsInserted() {
		return tempSourceTableRowsInserted;
	}

	// TODO check why the count doen't match 
	//public int getResultFileRowsCount() {
	//	return resultFileRowsCount;
	//}


	
	public void setFailedFastException(Exception failedFastException) {
		this.failedFastException = failedFastException;
	}

	public String getFailedFastExceptionMsg(){
		if (failedFastException == null){
			return "";			
		}
		return failedFastException.getMessage();
	}
	
	public Throwable getFailedFastException() {
		return failedFastException;
	}

	public void setFailedFastException(Throwable failedFastException) {
		this.failedFastException = failedFastException;
	}

	public void addExceptionWithLimit(Exception ex) {
		
		// control the number of invalid row and column exceptions that are put to 
		// the exception queue
		if (ex instanceof InvalidRowException || ex instanceof MappingException ){
			if (sourceFileInvalidRowsCount <= ROW_EXCEPTIONS_LIMIT){
				exceptionsQueue.add(ex);
			}
		}
			
		if (ex instanceof InvalidColumnException){
			if (sourceFileInvalidRowsCount <= COL_EXCEPTIONS_LIMIT){
				exceptionsQueue.add(ex);
			}
		}
	}
	

	public void setNewHRBPUsersWithEmptyDomain(
			List<HRBPUser> newHRBPUsersWithEmptyDomain) {		
		this.newHRBPUsersWithEmptyDomain = newHRBPUsersWithEmptyDomain; 
	}

	public void setUsersWithFilledDomain(List<HRBPUser> usersWithFilledDomain) {
		this.usersWithFilledDomain = usersWithFilledDomain;	
	}

	public void setUsersWithEmptyResume(List<User> emptyResumeUsers) {		
		this.usersWithEmptyResume = emptyResumeUsers;		
	}

	public void setUsersWithChangedId(List<User> usersWithChangedId) {
		this.usersWithChangedId = usersWithChangedId;
		
	}

	public BlockingQueue<NewTrackedAttr> getNewTrackedAttrQueue() {
		return newTrackedAttrQueue;
	}

	public void setNewTrackedAttrQueue(
			BlockingQueue<NewTrackedAttr> newTrackedAttrQueue) {
		this.newTrackedAttrQueue = newTrackedAttrQueue;
	}

	public BlockingQueue<TrackedAttr> getChangedTrackedAttrQueue() {
		return changedTrackedAttrQueue;
	}

	public void setChangedTrackedAttrQueue(
			BlockingQueue<TrackedAttr> changedTrackedAttrQueue) {
		this.changedTrackedAttrQueue = changedTrackedAttrQueue;
	}

	public void addProcessedSourceFiles(String fileName) {		
		if ( processedSourceFiles.contains(fileName) == false ){
			processedSourceFiles.add(fileName);
		}
	}
	
	public String getProcessedSourceFilesList()	
	{
		return StringUtils.join(processedSourceFiles, ",");	 
	}

	public int getExcludedOutboundExpatCount() {
		return excludedOutboundExpatCount;
	}
	
	public List<ExpatUser> getOutboundExpatsUsers() {
		if (outboundExpatsUsers == null){
			outboundExpatsUsers = new ArrayList<ExpatUser>();
		}
		
		return outboundExpatsUsers;
	}

	public void setOutboundExpatsUsers(List<ExpatUser> outboundExpatsUsers) {
		this.outboundExpatsUsers = outboundExpatsUsers;
	}

	public void addOutboundExpatUsers(List<ExpatUser> expatUsers) {
		
		if (outboundExpatsUsers == null)
		{
			outboundExpatsUsers = new ArrayList<ExpatUser>();
		}
		
		outboundExpatsUsers.addAll(expatUsers);
	}

	public Date getStepLoadOrgLstTableStartDate() {
		return stepLoadOrgLstTableStartDate;
	}

	public void setStepLoadOrgLstTableStartDate(Date stepLoadOrgLstTableStartDate) {
		this.stepLoadOrgLstTableStartDate = stepLoadOrgLstTableStartDate;
	}

	public Date getStepLoadOrgLstTableEndDate() {
		return stepLoadOrgLstTableEndDate;
	}

	public void setStepLoadOrgLstTableEndDate(Date stepLoadOrgLstTableEndDate) {
		this.stepLoadOrgLstTableEndDate = stepLoadOrgLstTableEndDate;
	}

	public int getProcessedOrgCount() {
		return processedOrgCount;
	}

	public void setProcessedOrgCount(int processedOrgCount) {
		this.processedOrgCount = processedOrgCount;
	}

	public int getUsersWithEmptyEmailCount() {
		return usersWithEmptyEmailCount;
	}

	public void setUsersWithEmptyEmailCount(int usersWithEmptyEmailCount) {
		this.usersWithEmptyEmailCount = usersWithEmptyEmailCount;
	}

	public int getExcludedTableRowsInserted() {
		return excludedTableRowsInserted;
	}

	synchronized public void addExcludedTableRowsInsertedOrInvalid(int count) {
		this.excludedTableRowsInserted += count;
	}

	public int getExternalUsersValidRowsCount() {
		return externalUsersValidRowsCount;
	}

	public void setExternalUsersValidRowsCount(int externalUsersValidRowsCount) {
		this.externalUsersValidRowsCount = externalUsersValidRowsCount;
	}

	public int getExternalUsersInvalidRowsCount() {
		return externalUsersInvalidRowsCount;
	}

	public void setExternalUsersInvalidRowsCount(int externalUsersInvalidRowsCount) {
		this.externalUsersInvalidRowsCount = externalUsersInvalidRowsCount;
	}
	
	
	public int getExcludedAustrianUsersCount() {
		return excludedAustrianUsersCount;
	}

	
	public void setExcludedAustrianUsersCount(int excludedAustrianUsersCount) {
		this.excludedAustrianUsersCount = excludedAustrianUsersCount;
	}

	public String getExternalUsersToDeactivateList() {
		if (externalUsersToDeactivateList == null){
			externalUsersToDeactivateList = " ";
		}
		
		return externalUsersToDeactivateList;
	}

	public void setExternalUsersToDeactivateList(
			String externalUsersToDeactivateList) {
		this.externalUsersToDeactivateList = externalUsersToDeactivateList;
	}

	public boolean isExternalUsersMaxCountExceeded() {
		return externalUsersMaxCountExceeded;
	}

	public void setExternalUsersMaxCountExceeded(
			boolean externalUsersMaxCountExceeded) {
		this.externalUsersMaxCountExceeded = externalUsersMaxCountExceeded;
	}

	public String getListOfReactivatedExternalUsers() {
		return listOfReactivatedExternalUsers;
	}

	public void setListOfReactivatedExternalUsers(
			String listOfReactivatedExternalUsers) {
		this.listOfReactivatedExternalUsers = listOfReactivatedExternalUsers;
	}

	public boolean isPresentAnagrafeAlleatiSourceFile() {
		
		presentAnagrafeAlleatiSourceFile = false;

		for (String f : processedSourceFiles)
		{
			if ( ANAGRAFE_ALLEATI_FILENAME.equalsIgnoreCase(f) ){
				presentAnagrafeAlleatiSourceFile = true;
				return presentAnagrafeAlleatiSourceFile;
			}
		}
		
		return presentAnagrafeAlleatiSourceFile;
	}

	
	public String getNewOrganizationList() {
		
		String newOrgList =  getNewOrgList();
		
		if (StringUtils.isEmpty(getNewOrgList())) {
			
			return " ";
		} else {
			return newOrgList;
		}
		
	}
	
	
	
	/**
	 * Get list of new host legal entities
	 * 
	 * @return
	 */
	public String getNewOrgList() {
 		List<NewTrackedAttr> newAttr = new ArrayList<NewTrackedAttr>(newTrackedAttrQueue);
			
		if ( newAttr.isEmpty() ){
			return null;	
		}
		
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		
		for ( NewTrackedAttr ta : newAttr ){
						
			if (ta.isHostLegalEntAttr() == true){
				if (first == false){
					sb.append(", ");
				} else {
					first = false;
				}
				sb.append(ta.getAttrCode());
				
			} 
		}		
		return sb.toString();
		
	}

	public int getExcludedGermanUsersCount() {
		return excludedGermanUsersCount;
	}

	public void setExcludedGermanUsersCount(int excludedGermanUsersCount) {
		this.excludedGermanUsersCount = excludedGermanUsersCount;
	}
	
	
	
}
