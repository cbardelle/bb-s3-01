package eu.unicreditgroup.mailer;

import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class FtlReportGenerator extends AbstractReportGenerator {

	private static final Logger logger = Logger.getLogger(FtlReportGenerator.class);

	public FtlReportGenerator(String pathToLogFile, String logFileName) {
		this.logFileName = logFileName;
		this.pathToLogFile = pathToLogFile;
	}

	/* (non-Javadoc)
	 * @see it.ucilearning.elvis.util.ReportGenerator#generateAndSendReport(it.ucilearning.elvis.bean.ReportDataBean)
	 */
	public void generateAndSendReport(ReportDataBean rb){
		logger.info("Starting email generation...");
		
		List<Throwable> exceptionsList = new ArrayList<Throwable> (rb.getExceptionsQueue());
		
		try {
			//build and send mail
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd__HH_mm_ss");
			String strDate = sdf.format(rb.getBatchEndDate());
		
			EmailSender emailSender = new EmailSender(EMAIL_SENDER_CONFIG);
			emailSender.addZippedAttachmentFromFile(pathToLogFile + logFileName, logFileName);
			emailSender.setSubject("HR Conncetor Producer report for " + strDate);
			
			if(exceptionsList.size() > 0){
				String exceptions = generateTextExceptionsReport(exceptionsList);
				System.out.println(exceptions);
				
				emailSender.addAttachmentFromString(exceptions, "exceptions_"+strDate+".txt");
 			} 
			emailSender.setMessage( prepateFtlMessage(rb, "report.ftl") );
			sendEmail(emailSender);

		} catch (Exception e) {
			logger.error("Exception during email sending.", e);
		}
	}
	
	
	/**
	 * Sends simplified version of  the report without exceptions 
	 * and only with info about changed attributes
	 * @param rb
	 */
	public void generateAndSendTrackedAttrReport(ReportDataBean rb){
		logger.info("Starting tracked attribute email generation...");		
				
		try {
			//build and send mail
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd__HH_mm_ss");
			String strDate = sdf.format(rb.getBatchEndDate());
		
			EmailSender emailSender = new EmailSender(TRACK_ATTR_EMAIL_SENDER_CONFIG);
			
			emailSender.setSubject("HR Conncetor Producer report for " + strDate);			
			 
			emailSender.setMessage( prepateFtlMessage(rb, "reportTrackAttr.ftl") );
			sendEmail(emailSender);

		} catch (Exception e) {
			logger.error("Exception during tracked attribute email sending.", e);
		}
	}
	
	public void generateAndSendNewOrgAlert(ReportDataBean rb) {
		// TODO Auto-generated method stub
		
		String newOrgList = rb.getNewOrgList();
		
		
		if ( StringUtils.isNotEmpty(newOrgList)  ){
					
			try{
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd__HH_mm_ss");
				String strDate = sdf.format(rb.getBatchEndDate());
			
				EmailSender emailSender = new EmailSender(NEW_ORG_ALERT_SENDER_CONFIG);
				
				emailSender.setSubject("[ALERT] NEW ORGANIZATION(S) PRESENT: " + newOrgList);			
				 
				emailSender.setMessage( prepateFtlMessage(rb, "newOrgAlert.ftl") );
				sendEmail(emailSender);
				
				
			} catch (Exception e) {
				logger.error("Exception during sending email with new organizations alert!", e);
			}
			
		} 
		
		
		
	}
	
	
	
		
	
	private String prepateFtlMessage(ReportDataBean rb, String templeteName ) throws IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(getClass(), "/META-INF/ftl/");
		cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
		
		Map<String, Object> params = new HashMap<String, Object>();
    	params.put("rb", rb);
    	
    	 // Get the template object
        Template t = cfg.getTemplate(templeteName);
        StringWriter sw = new StringWriter();
        
        // Merge the data-model and the template
        try {
            t.process(params,sw);
        } catch (TemplateException e) {
        	logger.error("Problem during mail generation", e);
        	sw.append(e.toString());
        } 
        
        return sw.toString();
	}

	
	
}
