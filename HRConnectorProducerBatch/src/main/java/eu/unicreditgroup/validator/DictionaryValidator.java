package eu.unicreditgroup.validator;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.BatchWorker;
import eu.unicreditgroup.beans.NewTrackedAttr;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.Dictionary;
import eu.unicreditgroup.utils.TrackedAttrDict;

public class DictionaryValidator implements IValidator {

	
	private TrackedAttrDict trackedAttrDict;
	
	private ReportDataBean reportDataBean;
	
	boolean shouldRejectRow = false;	
		
	@Override
	public boolean isRowRejectable() {		
		return isShouldRejectRow();
	}	
	
	Logger logger = Logger.getLogger(DictionaryValidator.class);
	
	@Override
	public boolean validate(String mappedValue, String[] srcValues, Integer resColIndex, SourceRow sourceRow) {
		
		String attrDesc = trackedAttrDict.getResultAttrDesc(resColIndex);
		
		// check if the tracked value is duplicated and should not be validated
		if (trackedAttrDict.isTrackedAttrDuplicateException(resColIndex, srcValues[0]))
		{
			return true;
		}
		
		if (StringUtils.isEmpty(mappedValue)){
			// do nothing for empty strings
			return true;
		}
		
		
		// Changed on 2015-01-15
		// Do not track changes for inactive users because their data is not up-to-date
		// and may contain some obsolate data
		if ( trackedAttrDict.isActiveUserData( sourceRow ) == false ){

			
			return true;
		}
		
		
		
		// for some attributes this value can be truncated
		String dictValue = trackedAttrDict.getTrackedAttrVal( resColIndex, srcValues[0] /* attr code */ );
		
		// tracking
		if (StringUtils.isEmpty(dictValue)){
			//the value is not present in the dictionary tab
			// do not log in the production for performance
			// logger.debug("Attribute " + attrName + " value is not present in the dictionary " + attrName + ", attr code: " + srcValues[0] + ", mapped value: " + mappedValueToLog);
			
			NewTrackedAttr ta = new NewTrackedAttr();
			ta.setAttrIdx(resColIndex);		
			//Attribute description will be used in the final report
			ta.setAttrDesc(attrDesc);
			ta.setAttrCode(srcValues[0]);
			ta.setMappedValue(mappedValue);//new value not present in a dictionary
			reportDataBean.addNewTrackedAttr(ta);			
			// for the moment only tracking!!!
			if (isRowRejectable() == true){
				return false;
			}			
		} else {
			if( !StringUtils.equals(mappedValue, dictValue) ){			
				
				// either abbr or desc of the attribute has changed include it in the report				
				// do not log in the production for performance
				//logger.debug("Attribute " + attrName + " , attr code: " + srcValues[0] 
				// +  " , has changed its value! Old value: " + dictValueToLog + ", new value: " + mappedValueToLog);
				
				TrackedAttr ta = new TrackedAttr();
				ta.setAttrIdx(resColIndex);		
				//Attribute description will be used in the final report
				ta.setAttrDesc(attrDesc);
				ta.setAttrCode(srcValues[0]);
				ta.setMappedValue(mappedValue);//value has changed
				ta.setDictValue(dictValue);		
				
				// get list of assignment profiles which use the   
				List<String> assgnProfiles = trackedAttrDict.getAssgnProfiles(ta);
												
				ta.setAssgnProfiles(assgnProfiles);
				
				// add only attributes that are used only in any assignment profile
				if ( assgnProfiles.isEmpty() == false ){
					
					logger.debug("CHANGED ATTR USED IN AP, old val:" + ta.getDictValue() + " new val: " + ta.getMappedValue() + " AP: " + assgnProfiles );
				}	
				reportDataBean.addChangedTrackedAttr(ta);
				
				
				// for the moment only tracking!!!
				if (isRowRejectable() == true){
					return false;
				}
			}	
		}		 
		
		// reject rows  
		if (isRowRejectable() == true){
			
			if (StringUtils.isEmpty(dictValue)){
				// if not present reject the row
				logger.debug("Attribute " + attrDesc + " , attr code: " + srcValues[0] + ", attr value: " + mappedValue + " is not present in the dictionary ");
				
				return false;			
			} else {
				
				String mappedValueToCompare = trackedAttrDict.checkForTrackedAttrException(resColIndex, mappedValue);
				
				String dictValueToCompare = trackedAttrDict.checkForTrackedAttrException(resColIndex, dictValue); 
					
				if( !StringUtils.equals(mappedValueToCompare, dictValueToCompare) ){			
					
					// either abbr or desc of the attribute has changed include it in the report				
					// TODO do not log in production when this will be used
					logger.debug("Attribute " + attrDesc + " , attr code: " + srcValues[0] +  " , has changed its value! Old value: " + dictValue + ", new value: " + mappedValue);					
					return false;
				}				
			}			
		}		
		
		return true;
	}


	public TrackedAttrDict getTrackedAttrDict() {
		return trackedAttrDict;
	}

	public void setTrackedAttrDict(TrackedAttrDict trackedAttrDict) {
		this.trackedAttrDict = trackedAttrDict;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public boolean isShouldRejectRow() {
		return shouldRejectRow;
	}

	public void setShouldRejectRow(boolean shouldRejectRow) {
		this.shouldRejectRow = shouldRejectRow;
	}
	

}
