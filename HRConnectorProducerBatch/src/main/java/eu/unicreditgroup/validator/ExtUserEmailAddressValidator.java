package eu.unicreditgroup.validator;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;

public class ExtUserEmailAddressValidator implements IValidator {
	
	SourceFileConf sourceFileConf; 

	@Override
	public boolean validate(String value, String[] srcValues, Integer resColIndex, SourceRow sourceRow) {
		
		
		// for all external users the email is  mandatory
		if ( sourceFileConf.isExternalUser(sourceRow) ){
				
			
			// rows with empty email will be rejected 
			if (StringUtils.isEmpty(value)){
				return false;
			}
			
			// rows with invalid email will be rejected 
			int position = value.indexOf("@"); 
			
			if ( position > 0 && position < value.length()  ){
				return true;
			} else {
				return false;
			}
		
		} else {
			return true; // do nth for non external users
		}
		
	}

	@Override
	public boolean isRowRejectable() {
		return true;
	}

	
	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}
	

}
