package eu.unicreditgroup.validator;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.SourceRow;

public class NotNullValidator implements IValidator {

	@Override
	public boolean validate(String value, String[] srcValues, Integer resColIndex, SourceRow sourceRow) {
	
		return (StringUtils.isNotEmpty(value));
	}

	@Override
	public boolean isRowRejectable() {
		return false;
	}


}
