package eu.unicreditgroup.validator;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;

public class ExtUserLocalJobPositionValidator implements IValidator {
	
	Dictionary dictionary;
	
	SourceFileConf sourceFileConf; 

	@Override
	public boolean validate(String value, String[] srcValues,
			Integer resColIndex, SourceRow sourceRow) {
		
		
		// for all external users job code has to be validated
		if ( sourceFileConf.isExternalUser(sourceRow) ){
			
			String jobCode = srcValues[0];
			
			// check the jobCode is in the dictionary
			String jobDesc = dictionary.findExternalUsersJobsDesc(jobCode);
			
			if (StringUtils.isNotEmpty(jobDesc )){
				return true;
			} else {
				return false; //external user without proper jobCode will be rejected 
			}
			
		} else {
			return true; // do nth for non external users
		}
		
	}

	@Override
	public boolean isRowRejectable() {
		return true;
	}

	
	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	

}
