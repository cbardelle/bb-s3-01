package eu.unicreditgroup.config;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.utils.ParametersListParser;


public class ConfigParams implements InitializingBean{
	
	private static final Logger logger = Logger.getLogger(ConfigParams.class);

	// required percenatge of valid rows in the source file
	private static final float VALID_SOURCE_ROWS_PERCENTAGE = 50;
	
	// required percenatge of valid rows inserted into the source table
	private static final float VALID_INSERTED_ROWS_PERCENTAGE = 90;
	
	private ParametersListParser parametersListParser;
	
	private String strStepLoadSourceTable;
	
	private String strStepGenerateResultFile;
	
	private String strStepLoadOrgLstTable;
	
	private String strSourceRowProcessThreadsNumber;
		
	//private String strSourceFileName;
	
	private String strPrimarySourceFileName;
	
	private String strOrgLstFileName;
	
	private String strSourceFilesList;
	
	private List<String> sourceFilesList;
	
	
	
	/**
	 * 
	 * List of source files which are mandatory for the batch execution
	 * (loading of the source table and generation of the result file)   
	 */
	private String strMandatorySourceFilesList;	
	private List<String> mandatorySourceFilesList;
	
	
	private String strSourceFilesListToMove;
	
	private List<String> sourceFilesListToMove;
	
	private String strAdditionalSourceFileName;
	
	private String strExternalUserSourceFileName;
	
	private String strSourceLocalDir;
		
	/**
	 * remote directory of source file
	 */
	private String strSourceRemoteDir;
		
	private String strSourceBufferSize;
	
	private String strProcessHRBPUsers;

	private String strProcessExternalUsersFile;
	
	private String strProcessExpatUsers;
	
	private String strGenearteCsvFile;
		
	private String strCsvFileDir;
	
	private String strPlateauFileDir;
	
	private boolean genearteCsvFile;
		
	private boolean stepLoadSourceTableEnabled;
	
	private boolean stepGenerateResultFileEnabled;
	
	private boolean stepLoadOrgLstTableEnabled;
			
	private int sourceRowProcessThreadsNumber;	
	
	private int sourceBufferSize;
		
	private boolean processHRBPUsers;
	
	private boolean processExpatUsers;
	
	private boolean downloadSourceFileEnabled;
	
	private boolean processExternalUsersFileEnabled;
	
	private boolean downloadExternalUsersFileEnabled;
	
	private String strDownloadSourceFile; 
	
	private String strDownloadExternalUsersFile;
		
	private String strResultFileName; 
	
	private boolean uploadGeneratedFileEnabled;
	
	private String strUploadGeneratedFile;
	
	private boolean moveProcessedSapFileEnabled;
	
	private boolean moveProcessedExternalUsersFileEnabled;
	
	private String strMoveProcessedSapFile;
	
	private String strMoveProcessedExternalUsersFile;
	
	private String strProcessedSapFileFtpDir;
	
	/**
	 * The remote dir on ftp for processed external user file
	 */
	private String strProcessedExternalUserFileFtpDir;
	
	/**
	 * The remote dir on ftp for external user file
	 */
	private String strFtpExternalUserDirectory;
	
	private String strStepCheckUsersWithEmptyResume;
	
	private String strStepCheckUsersWithChangedId;

	private boolean stepCheckUsersWithEmptyResume;
	
	private boolean stepCheckUsersWithChangedId;
	
	private String strDownloadOrgLstFile;
	
	private String strMoveOrgLstFile;
	
	private boolean downloadOrgLstFile;
	
	private boolean moveOrgLstFile;
	
	private int externalUserRemovalDay;
	
	private boolean processManualSourceTable;
	
	private String strProcessManualSourceTable;
	
	/**
	 * The day to deactivate external users
	 * (which are present in the history table but are not present in 
	 * the current file with external users)
	 * 
	 */
	private String strExternalUserRemovalDay;
	
	@Override
	public void afterPropertiesSet() throws Exception {		
		
		ParametersListParser listParser = new ParametersListParser();
		
		sourceRowProcessThreadsNumber = NumberUtils.toInt(strSourceRowProcessThreadsNumber, -1);
		if (sourceRowProcessThreadsNumber < 1){
			throw new IllegalStateException(String.format("Number of threads must be positive number but has value: '%s'.", strSourceRowProcessThreadsNumber));
		}
		
		sourceBufferSize = NumberUtils.toInt(strSourceBufferSize, -1);
		if (sourceBufferSize < 1){
			throw new IllegalStateException(String.format("Source buffer size must be positive number but has value: '%s'.", strSourceBufferSize));
		}
	
		stepLoadSourceTableEnabled = processBooleanParameter(strStepLoadSourceTable);
		
		stepGenerateResultFileEnabled = processBooleanParameter(strStepGenerateResultFile);
		
		stepLoadOrgLstTableEnabled = processBooleanParameter(strStepLoadOrgLstTable);
		
		processHRBPUsers = processBooleanParameter(strProcessHRBPUsers);
		
		processExpatUsers = processBooleanParameter(strProcessExpatUsers);
		
		genearteCsvFile = processBooleanParameter(strGenearteCsvFile);
		
		downloadSourceFileEnabled = processBooleanParameter(strDownloadSourceFile);
				
		downloadExternalUsersFileEnabled = processBooleanParameter(strDownloadExternalUsersFile);
		
		uploadGeneratedFileEnabled = processBooleanParameter(strUploadGeneratedFile);
		
		moveProcessedSapFileEnabled = processBooleanParameter(strMoveProcessedSapFile);
		
		moveProcessedExternalUsersFileEnabled = processBooleanParameter(strMoveProcessedExternalUsersFile);
		
		processExternalUsersFileEnabled = processBooleanParameter(strProcessExternalUsersFile);
		
		processManualSourceTable = processBooleanParameter(strProcessManualSourceTable);
		
		stepCheckUsersWithEmptyResume = processBooleanParameter(strStepCheckUsersWithEmptyResume);
		
		stepCheckUsersWithChangedId = processBooleanParameter(strStepCheckUsersWithChangedId);
		
		downloadOrgLstFile = processBooleanParameter(strDownloadOrgLstFile);
		
		moveOrgLstFile = processBooleanParameter(strMoveOrgLstFile);
		
		sourceFilesList = listParser.parseAsList(strSourceFilesList);
		
		sourceFilesListToMove = listParser.parseAsList(strSourceFilesListToMove);
		
		externalUserRemovalDay = dayOfWeekToInt(strExternalUserRemovalDay);
				
		mandatorySourceFilesList = listParser.parseAsList(strMandatorySourceFilesList);		
		
		logger.debug("ConfigParams object: " + toString());
		
		
	}
	

	private int dayOfWeekToInt(String strExtUsersRemovalDay) {
		
		
		if("Monday".equalsIgnoreCase(strExtUsersRemovalDay))  return 1;
		if("Tuesday".equalsIgnoreCase(strExtUsersRemovalDay))  return 2;
		if("Wednesday".equalsIgnoreCase(strExtUsersRemovalDay))  return 3;
		if("Thursday".equalsIgnoreCase(strExtUsersRemovalDay))  return 4;
		if("Friday".equalsIgnoreCase(strExtUsersRemovalDay))  return 5;
		if("Saturday".equalsIgnoreCase(strExtUsersRemovalDay))  return 6;
		if("Sunday".equalsIgnoreCase(strExtUsersRemovalDay))  return 7;
		
		return 0;
	}


	public boolean processBooleanParameter(String parameter){
		if (StringUtils.equalsIgnoreCase(parameter, "y")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "1")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "tak")){
			parameter = "true";
		}
		
		if (StringUtils.equalsIgnoreCase(parameter, "si")){
			parameter = "true";
		}
		
		return BooleanUtils.toBoolean(parameter);
	}
	
	public ParametersListParser getParametersListParser() {
		return parametersListParser;
	}

	public void setParametersListParser(ParametersListParser parametersListParser) {
		this.parametersListParser = parametersListParser;
	}

	public String getStrStepLoadSourceTable() {
		return strStepLoadSourceTable;
	}

	public void setStrStepLoadSourceTable(String strStepLoadSourceTable) {
		this.strStepLoadSourceTable = strStepLoadSourceTable;
	}
	
	public boolean isStepLoadSourceTableEnabled() {
		return stepLoadSourceTableEnabled;
	}

	public void setStepLoadSourceTableEnabled(boolean stepLoadSourceTableEnabled) {
		this.stepLoadSourceTableEnabled = stepLoadSourceTableEnabled;
	}
	
	
	/**
	 * Returns the full path to local SAP source file 
	 * (which was downloaded from a ftp server) 
	 * @return
	 */
	/*
	public String getStrSourceLocalFilePath() {		
		return strSourceLocalDir + "\\" + strSourceFileName;		
	}*/
	
	/**
	 * Returns the full path to an additional source file 
	 * (which has to be present in the source local dir) 
	 * @return
	 */
	
	public String getStrAdditionalSourceLocalFilePath() {		
		return strSourceLocalDir + "\\" + strAdditionalSourceFileName;		
	}		
	
	public String getStrExternalUserSourceFilePath() {		
		return strSourceLocalDir + "\\" + strExternalUserSourceFileName;

	}
	
	public String getStrStepGenerateResultFile() {
		return strStepGenerateResultFile;
	}

	public void setStrStepGenerateResultFile(String strStepGenerateResultFile) {
		this.strStepGenerateResultFile = strStepGenerateResultFile;
	}

	public boolean isStepGenerateResultFileEnabled() {
		return stepGenerateResultFileEnabled;
	}

	public void setStepGenerateResultFileEnabled(
			boolean stepGenerateResultFileEnabled) {
		this.stepGenerateResultFileEnabled = stepGenerateResultFileEnabled;
	}
	
	public String getStrSourceRowProcessThreadsNumber() {
		return strSourceRowProcessThreadsNumber;
	}

	public void setStrSourceRowProcessThreadsNumber(
			String strSourceRowProcessThreadsNumber) {
		this.strSourceRowProcessThreadsNumber = strSourceRowProcessThreadsNumber;
	}

	public String getStrSourceBufferSize() {
		return strSourceBufferSize;
	}

	public void setStrSourceBufferSize(String strSourceBufferSize) {
		this.strSourceBufferSize = strSourceBufferSize;
	}

	public String getStrProcessHRBPUsers() {
		return strProcessHRBPUsers;
	}

	public void setStrProcessHRBPUsers(String strProcessHRBPUsers) {
		this.strProcessHRBPUsers = strProcessHRBPUsers;
	}
	
	public String getStrProcessExpatUsers() {
		return strProcessExpatUsers;
	}


	public void setStrProcessExpatUsers(String strProcessExpatUsers) {
		this.strProcessExpatUsers = strProcessExpatUsers;
	}


	public int getSourceRowProcessThreadsNumber() {
		return sourceRowProcessThreadsNumber;
	}

	public void setSourceRowProcessThreadsNumber(int sourceRowProcessThreadsNumber) {
		this.sourceRowProcessThreadsNumber = sourceRowProcessThreadsNumber;
	}

	public int getSourceBufferSize() {
		return sourceBufferSize;
	}

	public void setSourceBufferSize(int sourceBufferSize) {
		this.sourceBufferSize = sourceBufferSize;
	}

	public boolean isProcessHRBPUsers() {
		return processHRBPUsers;
	}
	
	public void setProcessHRBPUsers(boolean processHRBPUsers) {
		this.processHRBPUsers = processHRBPUsers;
	}
	
	public boolean isProcessExpatUsers() {
		return processExpatUsers;
	}

	public void setProcessExpatUsers(boolean processExpatUsers) {
		this.processExpatUsers = processExpatUsers;
	}

	public String getStrGenearteCsvFile() {
		return strGenearteCsvFile;
	}

	public void setStrGenearteCsvFile(String strGenearteCsvFile) {
		this.strGenearteCsvFile = strGenearteCsvFile;
	}
	
	public String getStrSourceLocalDir() {
		return strSourceLocalDir;
	}

	public void setStrSourceLocalDir(String strSourceLocalDir) {
		this.strSourceLocalDir = strSourceLocalDir;
	}

	public String getStrCsvFileDir() {
		return strCsvFileDir;
	}

	public void setStrCsvFileDir(String strCsvFileDir) {
		this.strCsvFileDir = strCsvFileDir;
	}

	public String getStrPlateauFileDir() {
		return strPlateauFileDir;
	}

	public void setStrPlateauFileDir(String strPlateauFileDir) {
		this.strPlateauFileDir = strPlateauFileDir;
	}

	public boolean isGenearteCsvFile() {
		return genearteCsvFile;
	}

	public void setGenearteCsvFile(boolean genearteCsvFile) {
		this.genearteCsvFile = genearteCsvFile;
	}
	
	public String getStrStepCheckUsersWithEmptyResume() {
		return strStepCheckUsersWithEmptyResume;
	}

	public void setStrStepCheckUsersWithEmptyResume(
			String strStepCheckUsersWithEmptyResume) {
		this.strStepCheckUsersWithEmptyResume = strStepCheckUsersWithEmptyResume;
	}

	public String getStrStepCheckUsersWithChangedId() {
		return strStepCheckUsersWithChangedId;
	}

	public void setStrStepCheckUsersWithChangedId(
			String strStepCheckUsersWithChangedId) {
		this.strStepCheckUsersWithChangedId = strStepCheckUsersWithChangedId;
	}

	public boolean isStepCheckUsersWithEmptyResume() {
		return stepCheckUsersWithEmptyResume;
	}

	public void setStepCheckUsersWithEmptyResume(
			boolean stepCheckUsersWithEmptyResume) {
		this.stepCheckUsersWithEmptyResume = stepCheckUsersWithEmptyResume;
	}

	public boolean isStepCheckUsersWithChangedId() {
		return stepCheckUsersWithChangedId;
	}

	public void setStepCheckUsersWithChangedId(boolean stepCheckUsersWithChangedId) {
		this.stepCheckUsersWithChangedId = stepCheckUsersWithChangedId;
	}


	@Override
	public String toString() {
		
		
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public float getValidSourceRowsPercentage() {
		
		return VALID_SOURCE_ROWS_PERCENTAGE;
		
	}
	
	public float getValidInsertedRowsPercentage() {
		
		return VALID_INSERTED_ROWS_PERCENTAGE;
		
	}
	

	/*
	public String getStrSourceFileName() {
		return strSourceFileName;
	}

	public void setStrSourceFileName(String strSourceFileName) {
		this.strSourceFileName = strSourceFileName;
	}*/
	
	public boolean isDownloadSourceFileEnabled() {
		return downloadSourceFileEnabled;
	}

	public String getStrPrimarySourceFileName() {
		return strPrimarySourceFileName;
	}

	public void setStrPrimarySourceFileName(String strPrimarySourceFileName) {
		this.strPrimarySourceFileName = strPrimarySourceFileName;
	}


	public void setDownloadSourceFileEnabled(boolean downloadSourceFileEnabled) {
		this.downloadSourceFileEnabled = downloadSourceFileEnabled;
	}

	public String getStrDownloadSourceFile() {
		return strDownloadSourceFile;
	}

	public void setStrDownloadSourceFile(String strDownloadSourceFile) {
		this.strDownloadSourceFile = strDownloadSourceFile;
	}


	public String getStrResultFileName() {
		return strResultFileName;
	}

	public void setStrResultFileName(String strResultFileName) {
		this.strResultFileName = strResultFileName;
	}

	public boolean isUploadGeneratedFileEnabled() {
		return uploadGeneratedFileEnabled;
	}

	public void setUploadGeneratedFileEnabled(boolean uploadGeneratedFileEnabled) {
		this.uploadGeneratedFileEnabled = uploadGeneratedFileEnabled;
	}

	public String getStrUploadGeneratedFile() {
		return strUploadGeneratedFile;
	}

	public void setStrUploadGeneratedFile(String strUploadGeneratedFile) {
		this.strUploadGeneratedFile = strUploadGeneratedFile;
	}

	public boolean isMoveProcessedSapFileEnabled() {
		return moveProcessedSapFileEnabled;
	}

	public void setMoveProcessedSapFileEnabled(boolean moveProcessedSapFileEnabled) {
		this.moveProcessedSapFileEnabled = moveProcessedSapFileEnabled;
	}

	public String getStrMoveProcessedSapFile() {
		return strMoveProcessedSapFile;
	}

	public void setStrMoveProcessedSapFile(String strMoveProcessedSapFile) {
		this.strMoveProcessedSapFile = strMoveProcessedSapFile;
	}

	public String getStrProcessedSapFileFtpDir() {
		return strProcessedSapFileFtpDir;
	}

	public void setStrProcessedSapFileFtpDir(String strProcessedSapFileFtpDir) {
		this.strProcessedSapFileFtpDir = strProcessedSapFileFtpDir;
	}

	public String getStrSourceRemoteDir() {
		return strSourceRemoteDir;
	}

	public void setStrSourceRemoteDir(String strSourceRemoteDir) {
		this.strSourceRemoteDir = strSourceRemoteDir;
	}

	public String getPlateauFullFilePath() {		
		return strPlateauFileDir + "\\" + strResultFileName;
	}
	
	public String getExternalUserSourceFullFilePath() {
		
		return strSourceLocalDir + "\\" + strExternalUserSourceFileName;
	}

	
	public String getCsvFullFilePath() {
		
		return strCsvFileDir + "\\" + getFileNameWithoutExt(strResultFileName) + ".csv";
	}	
	
	public String getFileNameWithoutExt(String fileName){
		if (fileName == null) return null;
		int dotPos = fileName.lastIndexOf('.');	
		return dotPos < 0 ? fileName : fileName.substring(0, dotPos);		
	}


	public String getStrAdditionalSourceFileName() {
		return strAdditionalSourceFileName;
	}


	public void setStrAdditionalSourceFileName(String strAdditionalSourceFileName) {
		this.strAdditionalSourceFileName = strAdditionalSourceFileName;
	}
	
	public String getStrExternalUserSourceFileName() {
		return strExternalUserSourceFileName;
	}


	public void setStrExternalUserSourceFileName(
			String strExternalUserSourceFileName) {
		this.strExternalUserSourceFileName = strExternalUserSourceFileName;
	}


	public String getStrSourceFilesList() {
		return strSourceFilesList;
	}


	public void setStrSourceFilesList(String strSourceFilesList) {
		this.strSourceFilesList = strSourceFilesList;
	}


	public List<String> getSourceFilesList() {
		return sourceFilesList;
	}


	public String getStrSourceFilesListToMove() {
		return strSourceFilesListToMove;
	}


	public void setStrSourceFilesListToMove(String strSourceFilesListToMove) {
		this.strSourceFilesListToMove = strSourceFilesListToMove;
	}


	public List<String> getSourceFilesListToMove() {
		return sourceFilesListToMove;
	}


	public String getStrStepLoadOrgLstTable() {
		return strStepLoadOrgLstTable;
	}


	public void setStrStepLoadOrgLstTable(String strStepLoadOrgLstTable) {
		this.strStepLoadOrgLstTable = strStepLoadOrgLstTable;
	}


	public boolean isStepLoadOrgLstTableEnabled() {
		return stepLoadOrgLstTableEnabled;
	}


	public String getStrOrgLstFileName() {
		return strOrgLstFileName;
	}

	public void setStrOrgLstFileName(String strOrgLstFileName) {
		this.strOrgLstFileName = strOrgLstFileName;
	}


	public String getStrDownloadOrgLstFile() {
		return strDownloadOrgLstFile;
	}


	public void setStrDownloadOrgLstFile(String strDownloadOrgLstFile) {
		this.strDownloadOrgLstFile = strDownloadOrgLstFile;
	}


	public String getStrMoveOrgLstFile() {
		return strMoveOrgLstFile;
	}


	public void setStrMoveOrgLstFile(String strMoveOrgLstFile) {
		this.strMoveOrgLstFile = strMoveOrgLstFile;
	}


	public boolean isDownloadOrgLstFile() {
		return downloadOrgLstFile;
	}


	public boolean isMoveOrgLstFile() {
		return moveOrgLstFile;
	}


	public boolean isDownloadExternalUsersFileEnabled() {
		return downloadExternalUsersFileEnabled;
	}


	public void setDownloadExternalUsersFileEnabled(
			boolean downloadExternalUsersFileEnabled) {
		this.downloadExternalUsersFileEnabled = downloadExternalUsersFileEnabled;
	}


	public String getStrDownloadExternalUsersFile() {
		return strDownloadExternalUsersFile;
	}


	public void setStrDownloadExternalUsersFile(String strDownloadExternalUsersFile) {
		this.strDownloadExternalUsersFile = strDownloadExternalUsersFile;
	}


	public String getStrProcessExternalUsersFile() {
		return strProcessExternalUsersFile;
	}


	public void setStrProcessExternalUsersFile(String strProcessExternalUsersFile) {
		this.strProcessExternalUsersFile = strProcessExternalUsersFile;
	}


	public boolean isProcessExternalUsersFileEnabled() {
		return processExternalUsersFileEnabled;
	}


	public void setProcessExternalUsersFileEnabled(
			boolean processExternalUsersFileEnabled) {
		this.processExternalUsersFileEnabled = processExternalUsersFileEnabled;
	}


	public boolean isMoveProcessedExternalUsersFileEnabled() {
		return moveProcessedExternalUsersFileEnabled;
	}


	public void setMoveProcessedExternalUsersFileEnabled(
			boolean moveProcessedExternalUsersFileEnabled) {
		this.moveProcessedExternalUsersFileEnabled = moveProcessedExternalUsersFileEnabled;
	}


	public String getStrMoveProcessedExternalUsersFile() {
		return strMoveProcessedExternalUsersFile;
	}


	public void setStrMoveProcessedExternalUsersFile(
			String strMoveProcessedExternalUsersFile) {
		this.strMoveProcessedExternalUsersFile = strMoveProcessedExternalUsersFile;
	}




	public String getStrExternalUserRemovalDay() {
		return strExternalUserRemovalDay;
	}


	public void setStrExternalUserRemovalDay(String strExternalUserRemovalDay) {
		this.strExternalUserRemovalDay = strExternalUserRemovalDay;
	}


	public int getExternalUserRemovalDay() {
		return externalUserRemovalDay;
	}


	public String getStrProcessedExternalUserFileFtpDir() {
		return strProcessedExternalUserFileFtpDir;
	}


	public void setStrProcessedExternalUserFileFtpDir(
			String strProcessedExternalUserFileFtpDir) {
		this.strProcessedExternalUserFileFtpDir = strProcessedExternalUserFileFtpDir;
	}


	public String getStrFtpExternalUserDirectory() {
		return strFtpExternalUserDirectory;
	}


	public void setStrFtpExternalUserDirectory(String strFtpExternalUserDirectory) {
		this.strFtpExternalUserDirectory = strFtpExternalUserDirectory;
	}


	public String getStrMandatorySourceFilesList() {
		return strMandatorySourceFilesList;
	}


	public void setStrMandatorySourceFilesList(String strMandatorySourceFilesList) {
		this.strMandatorySourceFilesList = strMandatorySourceFilesList;
	}


	public List<String> getMandatorySourceFilesList() {
		return mandatorySourceFilesList;
	}


	public boolean isProcessManualSourceTable() {
		return processManualSourceTable;
	}


	public void setProcessManualSourceTable(boolean processManualSourceTable) {
		this.processManualSourceTable = processManualSourceTable;
	}


	public String getStrProcessManualSourceTable() {
		return strProcessManualSourceTable;
	}


	public void setStrProcessManualSourceTable(String strProcessManualSourceTable) {
		this.strProcessManualSourceTable = strProcessManualSourceTable;
	}
	

	
	
}
