package eu.unicreditgroup.mapper;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;

public class FixedEmailMapper implements IMapper {

	private String defaultEmail; 
	
	public FixedEmailMapper(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		return new MappedColumn(defaultEmail);
	}

}
