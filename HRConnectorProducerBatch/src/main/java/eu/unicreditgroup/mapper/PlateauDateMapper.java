package eu.unicreditgroup.mapper;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class PlateauDateMapper implements IMapper, InitializingBean {

	private String dateFormat = "dd.MM.yyyy";

	private final String time = " 12:00:00"; // this is Plateau requirement

	private SimpleDateFormat srcFormat;

	// the date format agreed with SAP
	// private SimpleDateFormat srcFormat = new SimpleDateFormat("dd.MM.yyyy",
	// java.util.Locale.UK);

	private SimpleDateFormat resFormat = new SimpleDateFormat("MMM-dd-yyyy",
			java.util.Locale.UK);

	DateTimeFormatter parser = DateTimeFormat.forPattern("dd.MM.yyyy");
	DateTime minDate = parser.parseDateTime("01.01.1900");

	private Dictionary dictionary;

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal,
			SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn();

		// The default value for all German managers
		if (dictionary.checkForGermanManager(sourceRow.getUserId())) {
			mappedCol.setValue(null);
			return mappedCol;
		}

		String strDate = srcVal[0];
		if (StringUtils.isEmpty(strDate) || strDate.length() < 8) {
			mappedCol.setValue(null);
			return mappedCol;
		}

		Date d = null;
		try {
			d = srcFormat.parse(strDate);
		} catch (Exception e) {
			// mappedCol.setErrorMsg(e.getMessage());

			mappedCol.createMappingException(strDate, e.getMessage(), this
					.getClass().getSimpleName(), sourceRow.getUserId());

			return mappedCol;
		}

		// additional check when the date is unparsable
		// like 00.00.0000
		if (minDate.isAfter(d.getTime())) {
			mappedCol.setValue(null);
			
			//mappedCol.setErrorMsg("The date " + d + " is before the date of " + minDate.toString());
			
			mappedCol.createMappingException(
					strDate,
					"The date " + d + " is before the date of " + minDate.toString(),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);
			
			return mappedCol;
		}

		String mapped = resFormat.format(d);

		mapped = mapped.toUpperCase() + time;

		mappedCol.setValue(mapped);

		return mappedCol;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		srcFormat = new SimpleDateFormat(dateFormat, java.util.Locale.UK);

	}

}
