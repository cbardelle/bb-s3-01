package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.ContractType;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class EmployeeStatusDescMapper implements IMapper {

	Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn(); 
		
		String contractTypeCode = srcVal[0];
		
		String statusDesc = srcVal[1];
		
		// The default for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
		
		if (StringUtils.isEmpty(contractTypeCode)){
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		}
		
		ContractType contractType = dictionary.findContractType(contractTypeCode);
		
		if ( contractType == null ){			
			//mappedCol.setErrorMsg( String.format("Failed to map employee status desc for contract type code: %s", contractTypeCode) );
			
			mappedCol.createMappingException(
					statusDesc,
					String.format("Failed to map employee status desc for contract type code: %s", contractTypeCode),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);
			
			
		} else {
			if ("N".equalsIgnoreCase(contractType.getGlobalType())){
				
				// to reduce log size 
				//mappedCol.setErrorMsg(	String.format("Failed to map employee status description for contract type code: %s. Local values are not in use any more.", contractTypeCode) );
				
			} else {
				mappedCol.setValue( contractType.getPlateauEmpStDesc());
			}		
		}
		
		/*
		else {		
			if ("Y".equalsIgnoreCase(contractType.getGlobalType())){
				// for global values the value is taken from the table				
				mappedCol.setValue( contractType.getPlateauEmpStDesc());
			} else {
				// for local values use the source value
				mappedCol.setValue( statusDesc );
			}
		}*/
		
		return mappedCol;
		
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

}
