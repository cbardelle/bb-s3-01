package eu.unicreditgroup.mapper;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.utils.Dictionary;

/**
 * This is mapper for custom column 280.
 * Enables to assign custom values for CC 280 for selected users.
 * Custom values are stored in a table HR_CONN_CUST_RES_VALUE.
 * If a custom value for given user and result column index is present in above table 
 * it's assigned to the user in the result file. Otherwise CC 280 has its default value (NULL).
 * 
 * @author UV00074
 *
 */
public class CustCol280ValueMapper implements IMapper {

	
	private Dictionary dictionary;
	
	private final int resColIndex = 75;
	
	private static final Logger logger = Logger.getLogger(CustCol280ValueMapper.class);
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		String userID = srcVal[0];
		
		String result = dictionary.getCustomValueForResultColumn( userID , resColIndex);
		
		MappedColumn mappedCol = new MappedColumn();
		
		mappedCol.setValue( result );
		
		return mappedCol;		
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	
	
}
