package eu.unicreditgroup.mapper;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

/**
 * Mapping to a fixed value
 * 
 * @author UV00074
 *
 */
public class FixedValuePlusGeMngrMapper implements IMapper {
	
	private Dictionary dictionary;


	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	
		
	@Override
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			
			return new MappedColumn(null);
			
		} else {
			
			return new MappedColumn(defaultVal);
			
		}
				
	}

}
