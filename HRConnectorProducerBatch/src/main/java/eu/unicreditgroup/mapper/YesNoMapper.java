package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;

public class YesNoMapper implements IMapper {

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn();

		String booleanVal = srcVal[0];
		if (StringUtils.isEmpty(booleanVal)) {
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}

		if (booleanVal.equalsIgnoreCase("y")
				|| booleanVal.equalsIgnoreCase("yes")) {
			mappedCol.setValue("Yes");
		} else {
			if (booleanVal.equalsIgnoreCase("n")
					|| booleanVal.equalsIgnoreCase("no")) {
				mappedCol.setValue("No");
			} else {				
				
				mappedCol.createMappingException(
						booleanVal,
						String.format("Failed to map boolean value: %s", booleanVal),
						this.getClass().getSimpleName(),
						sourceRow.getUserId()				
					);
				
				
				
			}			
		}
		return mappedCol;
	}
}
