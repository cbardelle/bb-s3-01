package eu.unicreditgroup.mapper;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class RoleSeniorityMapper implements IMapper , InitializingBean{
		
	private String dateFormat =  "dd.MM.yyyy";
	
	private DateTimeFormatter parser;
	
	private Dictionary dictionary;

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		
		String strJobRoleStartDate = srcVal[0];
		
		MappedColumn mappedCol = new MappedColumn();
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
		
		if (strJobRoleStartDate == null){
			return mappedCol;
		}		
		
				
		DateTime jobRoleStartDate = null;
		try {
			jobRoleStartDate = parser.parseDateTime(strJobRoleStartDate);
		} catch (Exception e){
			//mappedCol.setErrorMsg(e.getMessage());
			
			mappedCol.createMappingException(
					strJobRoleStartDate,
					e.getMessage(),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);			
			
			return mappedCol; 
		}
				
		//calculate role seniority value	
		DateTime now = new DateTime();
		
		int monthsCount = Months.monthsBetween(jobRoleStartDate, now).getMonths();
		int roleSeniority;
		
		if (monthsCount < 5 ){
			
			roleSeniority = 0;
			
		} else if( monthsCount == 5 ){
			
			roleSeniority = 1;
			
		} else {
			// the first five months is treated as one year of exp
			// this is kind of odd but it's the way it is
			monthsCount = monthsCount - 5;
			roleSeniority = 1;
			
			// for the rest of months calculate whole years of exp
			roleSeniority += monthsCount / 12;			
		}
				
		mappedCol.setValue(String.valueOf(roleSeniority));
		
		return mappedCol;	
		
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		parser = DateTimeFormat.forPattern(dateFormat);
		
	}
	
	

}
