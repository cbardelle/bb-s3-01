package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;




//TODO check if direct mapper cannot be used 
// (check after merging trunk back to trunk)
public class LocalJobPositionMapper implements IMapper {
	
	private Dictionary dictionary;

	SourceFileConf sourceFileConf;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
	
		
		
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {		
						
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				mappedCol.setValue(defaultVal);
				return mappedCol;
			}
						
			// srcVal[0] contain attribute code (which will be used for validation) 
			// srcVal[1] and srcVal[2] contain attribute abbr and desc
			
			// for the external users there's no job role abbreviation available 
			// (so only code and desc will be used) 
			if (sourceFileConf.isExternalUser(sourceRow) ){
				sb.append(srcVal[0]);
				sb.append("-");
				sb.append(srcVal[2]);
			} else {
				sb.append(srcVal[0]);
				sb.append("-");
				sb.append(srcVal[1]);			
				sb.append("-");				
				sb.append(srcVal[2]);
			}
			
			mappedCol.setValue(sb.toString());
			
			return mappedCol;
		}		
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	
		
}
