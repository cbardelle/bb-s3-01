package eu.unicreditgroup.mapper;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class SupervisorIdMapper implements IMapper {

	Dictionary dictionary;
	
	Logger logger = Logger.getLogger(SupervisorIdMapper.class);
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn(); 
		
		String lineManagerId = srcVal[0];
		
		String userId = sourceRow.getUserId();
		
		String lineManagerExcp = dictionary.checkForLineManagerExcp(userId);		
		
		if ( lineManagerExcp == null ){			
			mappedCol.setValue(lineManagerId);
		} else {
			
			// 2014-04-04
			// this is the additional functionality to reset LM for users in table HR_CONN_LM_EXCP 
			// (requested by A.Sacchi)
			if (dictionary.LINE_MANAGER_EXCP_NULL.equals( lineManagerExcp )){
				mappedCol.setValue(null);
			} else {
				mappedCol.setValue(lineManagerExcp);
				logger.debug("User: " + userId + " will be assigned with line manager: " +  lineManagerExcp + ", instead of the line manager: " +lineManagerId);
			}
		}
		
		return mappedCol;
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

}
