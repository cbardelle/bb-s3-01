package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class OrgUnitLocTypeMapper implements IMapper {

	
	private Dictionary dictionary;


	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
		
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		String orgUnitLocType = srcVal[0];
		
		MappedColumn mappedCol = new MappedColumn();
		
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}		

		// set upper case
		mappedCol.setValue(StringUtils.upperCase(orgUnitLocType));
		
		return mappedCol;		
	}
	
}