package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

//TODO not in use any more remove this class
public class JobLocationMapper implements IMapper {

	//TODO use direct mapper instead of this one
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
		
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {			
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				mappedCol.setValue(defaultVal);
				return mappedCol;
			}
						
			// srcVal[0] contain attribute code (which will be used for validation) 
			// srcVal[1] contain attribute abbr 
			sb.append(srcVal[0]);
			sb.append("-");
			sb.append(srcVal[1]);
						
			mappedCol.setValue(sb.toString());			
			return mappedCol;
		}		
	}
	
	
}
