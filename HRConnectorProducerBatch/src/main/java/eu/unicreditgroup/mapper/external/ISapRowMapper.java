package eu.unicreditgroup.mapper.external;

public interface ISapRowMapper {

	
	public void map(String[] externalRow, String[] sapRowData);
	
}
