package eu.unicreditgroup.mapper.external;

import eu.unicreditgroup.config.ExternalSourceColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;



/**
 * Mapping of an external column to a SAP column
 * 
 * @author UV00074
 *
 */
public class DirectMapper implements ISapRowMapper {

	SourceColumn resultSapColumn;
	
	ExternalSourceColumn externalColumn;
		
	/**
	 * Performs direct mapping of an external column
	 * to  the column specified in resultSourceColumn 
	 *  
	 */
	@Override
	public void map(String[] externalRow, String[] sapRowData) {
		sapRowData[resultSapColumn.getSrcColIndex()] = externalRow[externalColumn.getSrcColIndex()];
	}

	public SourceColumn getResultSapColumn() {
		return resultSapColumn;
	}

	public void setResultSapColumn(SourceColumn resultSapColumn) {
		this.resultSapColumn = resultSapColumn;
	}

	public ExternalSourceColumn getExternalColumn() {
		return externalColumn;
	}

	public void setExternalColumn(ExternalSourceColumn externalColumn) {
		this.externalColumn = externalColumn;
	}
	
}
