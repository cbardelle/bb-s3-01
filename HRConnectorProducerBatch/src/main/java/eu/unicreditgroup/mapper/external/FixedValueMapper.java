package eu.unicreditgroup.mapper.external;

import eu.unicreditgroup.config.ExternalSourceColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;



/**
 * Mapping of an external column to a SAP column
 * 
 * @author UV00074
 *
 */
public class FixedValueMapper implements ISapRowMapper {

	SourceColumn resultSapColumn;
	
	String defaultValue;
		
	/**
	 * Performs direct mapping to a fixed value 
	 *  
	 */
	@Override
	public void map(String[] externalRow, String[] sapRowData) {
		sapRowData[resultSapColumn.getSrcColIndex()] = defaultValue;
	}

	public SourceColumn getResultSapColumn() {
		return resultSapColumn;
	}

	public void setResultSapColumn(SourceColumn resultSapColumn) {
		this.resultSapColumn = resultSapColumn;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
}
