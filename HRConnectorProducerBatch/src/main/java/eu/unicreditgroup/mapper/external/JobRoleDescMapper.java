package eu.unicreditgroup.mapper.external;

import eu.unicreditgroup.config.ExternalSourceColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;



/**
 * Mapping of an external column to a SAP column
 * 
 * @author UV00074
 *
 */
public class JobRoleDescMapper implements ISapRowMapper {

	private SourceColumn resultSapColumn;
	
	private ExternalSourceColumn externalColumn;
	
	private Dictionary dictionary;
		
	/**
	 * Performs direct mapping of an external column
	 * to  the column specified in resultSourceColumn 
	 *  
	 */
	@Override
	public void map(String[] externalRow, String[] sapRowData) {
		
		String jobDesc = dictionary.findExternalUsersJobsDesc(externalRow[externalColumn.getSrcColIndex()]);
		
		sapRowData[resultSapColumn.getSrcColIndex()] = jobDesc;
		
	}

	public SourceColumn getResultSapColumn() {
		return resultSapColumn;
	}

	public void setResultSapColumn(SourceColumn resultSapColumn) {
		this.resultSapColumn = resultSapColumn;
	}

	public ExternalSourceColumn getExternalColumn() {
		return externalColumn;
	}

	public void setExternalColumn(ExternalSourceColumn externalColumn) {
		this.externalColumn = externalColumn;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
