package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class LocaleIDMapper implements IMapper {
	
	

	Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();
		
		//String localLang = srcVal[0];
		
		// refactoring on 2013.08.19 - nation_id col will be used for mapping user's locale instead of local_lang
		String nationdId = srcVal[0];
		
		
		if ( StringUtils.isEmpty(nationdId) ){
			mappedCol.setValue( defaultVal );
			return mappedCol;	
		}
		String plateauLocaleId = dictionary.findLocale(nationdId);		
		if ( StringUtils.isEmpty(plateauLocaleId)){	
			
		
			//mappedCol.setErrorMsg( String.format("Failed to map locale for local language code: %s", localLang) );
		
			// if the locale cannot be found for the given nation use the default value
			mappedCol.setValue( defaultVal );
			
		} else {
			
			// simplify the checking - locale is mapped only if given nation is present in the dictionary table
			// otherwise the default value is used
			
			//if( ("IT").equalsIgnoreCase(localLang) || ("EN").equalsIgnoreCase(localLang) || ("DE").equalsIgnoreCase(localLang) ){
			
			mappedCol.setValue( plateauLocaleId );				
			
			//} else {
			//	mappedCol.setValue(defaultVal);
			//}	
			
		}		
		return mappedCol;		
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
