package eu.unicreditgroup.mapper;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;

/**
 * Mapping to a fixed value
 * 
 * @author UV00074
 *
 */
public class FixedValueMapper implements IMapper {
		
	@Override
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		return new MappedColumn(defaultVal);		
	}

}
