package eu.unicreditgroup.mapper;

import java.util.concurrent.BlockingQueue;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.dao.HRBPUserDao;
import eu.unicreditgroup.mailer.ReportDataBean;

public class HRBPUserProcessor {
	
	/*
	private SourceColumn hrbpColumn;
	
	private SourceColumn userIdColumn;
	
	private SourceColumn firstNameColumn;
	
	private SourceColumn lastNameColumn;
	*/
	
	private SourceFileConf sourceFileConf;	
		
	HRBPUserDao hrbpUserDao;
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	public void process(SourceRow sourceRow) throws Exception{
		
		if ( isHBRPUser(sourceRow) ){
			
			String userId = sourceRow.getValue(sourceFileConf.getSrcUserID().getSrcColIndex());	
			
			String firstName = sourceRow.getValue(sourceFileConf.getSrcFirstName().getSrcColIndex());
			
			String lastName = sourceRow.getValue(sourceFileConf.getSrcLastName().getSrcColIndex());
			
			hrbpUserDao.queueHRBPUser(userId, firstName, lastName);
			
		}
	}
		
	public boolean isHBRPUser(SourceRow sourceRow){				
		String strHRBP =  sourceRow.getValue(sourceFileConf.getSrcHRBP().getSrcColIndex());		
		return ("Y".equalsIgnoreCase(strHRBP));				
	}
	
	
	public HRBPUserDao getHrbpUserDao() {
		return hrbpUserDao;
	}

	public void setHrbpUserDao(HRBPUserDao hrbpUserDao) {
		this.hrbpUserDao = hrbpUserDao;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}
	
}
