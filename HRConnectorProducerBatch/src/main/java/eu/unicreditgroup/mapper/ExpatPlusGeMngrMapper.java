package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

/**
 * 
 * Mapper for the Expatriate custom column 180  
 * 
 * 
 * 
 * @author UV00074
 *
 */
public class ExpatPlusGeMngrMapper implements IMapper {

	private Dictionary dictionary;
	
	private final String EXPAT_Y = "Y";
	private final String EXPAT_YES = "YES";
	private final String EXPAT_N = "N";
	private final String EXPAT_NO = "NO";
	private final String EXPAT_TYPE_INBOUND = "I";
	private final String EXPAT_TYPE_OUTBOUND = "O";
	
	
	private final String MAPPED_VAL_YES = "Yes";
	private final String MAPPED_VAL_NO = "No";
	private final String MAPPED_VAL_INBOUND = "I";
	private final String MAPPED_VAL_OUTBOUND = "O";
		
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn();
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}

		String expatriateYesNo = srcVal[0];
		String expatType = srcVal[1];
		
		if (StringUtils.isEmpty(expatriateYesNo)) {
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}
		
		if (EXPAT_Y.equalsIgnoreCase( expatriateYesNo ) || EXPAT_YES.equalsIgnoreCase( expatriateYesNo )){
			
			if ( StringUtils.isEmpty( expatType ) ){				
				mappedCol.setValue(MAPPED_VAL_YES);				
			} else if ( EXPAT_TYPE_INBOUND.equalsIgnoreCase( expatType )) {				
				mappedCol.setValue(MAPPED_VAL_INBOUND);				
			} else if ( EXPAT_TYPE_OUTBOUND.equalsIgnoreCase( expatType )) {
				mappedCol.setValue(MAPPED_VAL_OUTBOUND);
			} else {
				//mappedCol.setErrorMsg(String.format("Failed to map Expat Type value: %s", expatriateYesNo));
								
				mappedCol.createMappingException(
						expatType,
						String.format("Failed to map Expat Type value: %s", expatriateYesNo),
						this.getClass().getSimpleName(),
						sourceRow.getUserId()				
					);
			}
			
		} else {			
			if (EXPAT_N.equalsIgnoreCase( expatriateYesNo ) || EXPAT_NO.equalsIgnoreCase( expatriateYesNo )){				
				mappedCol.setValue(MAPPED_VAL_NO);
			} else { // unknown source value
				
				
				//mappedCol.setErrorMsg(String.format("Failed to map boolean value: %s", expatriateYesNo));
				mappedCol.createMappingException(
						expatriateYesNo,
						String.format("Failed to map boolean value: %s", expatriateYesNo),
						this.getClass().getSimpleName(),
						sourceRow.getUserId()				
					);
				
				
			}			
		}
		
		return mappedCol;
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	
}
