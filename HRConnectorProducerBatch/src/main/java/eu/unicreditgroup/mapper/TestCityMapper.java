package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;

/**
 * Test mapper for sending spaces to the HR Connector
 * 
 * @author UV00074
 *
 */
public class TestCityMapper implements IMapper {

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		MappedColumn mappedCol = new MappedColumn(); 
		
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {			
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				mappedCol.setValue(defaultVal);
				return mappedCol;
			}
			
			
			for (int i = 0 ; i < srcVal.length ; i++ ){			
				sb.append(srcVal[i]);				
				if (i < srcVal.length - 1){				
					sb.append("-");				
				}
			}
						
			// test sending spaces to HR Connector
			String mappedVal = sb.toString();			
			if ("TestCity".equals(mappedVal)){
				mappedVal = " ";
			}
			
			mappedCol.setValue(mappedVal);
			return mappedCol;
		}
	}

}
