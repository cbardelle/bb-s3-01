package eu.unicreditgroup.mapper;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class CurrencyIDMapper implements IMapper {

	private Dictionary dictionary;
	
	@Override
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String currCode = srcVal[0];
		MappedColumn mappedCol = new MappedColumn();
			
		if (currCode == null){
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}				
			
		if ( dictionary.isCurrencyAvaliable(currCode) == true ){			
			mappedCol.setValue(currCode);
		} else {			 
			//mappedCol.setErrorMsg(String.format("The currency : '%s' is not avaliable.", currCode));
			
			mappedCol.createMappingException(
				currCode,
				String.format("The currency : '%s' is not avaliable.", currCode),
				this.getClass().getSimpleName(),
				sourceRow.getUserId()				
			);
			
			
		}		
		return mappedCol;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
