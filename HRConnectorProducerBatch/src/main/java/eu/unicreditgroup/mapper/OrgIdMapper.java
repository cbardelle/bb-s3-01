package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;


/**
 * This purpose of this class is to assign Audit Acadaemy and Anagrafe Alleati users 
 * with a default Organization Unit (which is required for the proper user view conf in LMS)   
 *  
 * @author UV00074
 *
 */
public class OrgIdMapper implements IMapper {
	
	private final static String HOST_LE_CODE_ANAGRAFE = "AA";
	
	private final static String HOST_LE_CODE_AUDIT = "AA2";
	
	private final static String AUDIT_ACADEMY_OU = "AUDIT_ACADEMY_OU";
	
	private final static String ANAGRAFE_ALLEATI_OU = "ANAGRAFE_ALLEATI_OU";
	
	
	/**
	 * 	The  mapper will return passed values separated by '-'
	 *  or the default value when source table is null or empty 
	 * 
	 */	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();		
				
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {
			
			String hostLe = srcVal[1];
			
			// assign Audit and Anagrafe to predefined OUs 
			if (HOST_LE_CODE_ANAGRAFE.equals( hostLe ) ){
				mappedCol.setValue(ANAGRAFE_ALLEATI_OU);
				return mappedCol;				
			} else if (HOST_LE_CODE_AUDIT.equals( hostLe ) ) {
				mappedCol.setValue(AUDIT_ACADEMY_OU);
				return mappedCol;				
			} else { 
				
				String userOfficeCode = srcVal[0];				
				if ( StringUtils.isEmpty(userOfficeCode) ){
					mappedCol.setValue(defaultVal);
					return mappedCol;
				} else {
					mappedCol.setValue(userOfficeCode);
					return mappedCol;
				}
						
			}			
		}
	}
}
