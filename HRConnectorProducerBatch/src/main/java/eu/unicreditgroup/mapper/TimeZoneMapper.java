package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class TimeZoneMapper implements IMapper {

	private Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {			
		String countryId = srcVal[0];		
		String result = dictionary.findTimeZone(countryId);
		
		MappedColumn mappedCol = new MappedColumn();
		if (StringUtils.isEmpty(countryId)){
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}
		
				
		if (result == null){	
			// limit logging
			if (  "AA".equals(countryId ) == false ){
				//mappedCol.setErrorMsg( String.format("Failed to map 'Time Zone' field for Country ID: '%s'", countryId) );
				
				mappedCol.createMappingException(
				countryId,
				String.format("Failed to map 'Time Zone' field for Country ID: '%s'", countryId),
				this.getClass().getSimpleName(),
				sourceRow.getUserId()				
		);
				
			}
		} else {
			mappedCol.setValue(result);
		}
				
		return mappedCol;		
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
