package eu.unicreditgroup.mapper;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;

public class RegionIDMapper implements IMapper {

	SourceFileConf sourceFileConf;
	
	private Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		String countryID = srcVal[0];
		String province = srcVal[1];
		String result;
		
		MappedColumn mappedCol = new MappedColumn();

		// no region for external users!
		
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){
			
			mappedCol.setValue(null);
			return mappedCol;
			
		} else if (sourceFileConf.isExternalUser(sourceRow) ){
			mappedCol.setValue(null);
		} else {
			if ( "IT".equalsIgnoreCase(countryID) ){			
				result = dictionary.findItalianRegion(province);			
				if (result == null){
					
					//mappedCol.setErrorMsg(String.format("Failed to map 'Region ID' field for province code: '%s'", province));
					mappedCol.createMappingException(
							province,
							String.format("Failed to map 'Region ID' field for province code: '%s'", province),
							this.getClass().getSimpleName(),
							sourceRow.getUserId()				
						);
					
				}		
				mappedCol.setValue(result);
			} else {			
				mappedCol.setValue(countryID);
			}
		
		}
		
		return mappedCol;		
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}
	
	
	
}
