package eu.unicreditgroup.mapper.exception;


public class InvalidRowException extends Exception {
	
	public InvalidRowException(String errMsg){
		super(errMsg);
	}


}
