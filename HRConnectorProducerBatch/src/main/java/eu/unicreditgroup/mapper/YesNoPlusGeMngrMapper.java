package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class YesNoPlusGeMngrMapper implements IMapper {

	private Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn();
		

		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
		

		String booleanVal = srcVal[0];
		if (StringUtils.isEmpty(booleanVal)) {
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}

		if (booleanVal.equalsIgnoreCase("y")
				|| booleanVal.equalsIgnoreCase("yes")) {
			mappedCol.setValue("Yes");
		} else {
			if (booleanVal.equalsIgnoreCase("n")
					|| booleanVal.equalsIgnoreCase("no")) {
				mappedCol.setValue("No");
			} else {
				
				//String srcValue, String message, IMapper mapper, String userId
				mappedCol.createMappingException(
					booleanVal,
					String.format("Failed to map boolean value: %s", booleanVal),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);
				
				
				
				//mappedCol.setErrorMsg(String.format(Failed to map boolean value: %s", booleanVal));
			}			
		}
		return mappedCol;
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	
}
