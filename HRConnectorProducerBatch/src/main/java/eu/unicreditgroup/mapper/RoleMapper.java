package eu.unicreditgroup.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;

public class RoleMapper implements IMapper {
	
	
	
	
	// the custom role for users involved in involved in raccolta formativa 2015-2016
	private final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 
	
	
	// Static data required for assignment of custom role 
	private static class StaticDataDict{
		
		// Verification of data in the DB
		/*	
		SELECT USER_ID, FIRST_NAME, LAST_NAME ,LINE_MANAGER, HOST_LEGAL_ENT_CODE, COMPETENCE_LINE_ABBR,DIVISION_ABBR, ORG_1ST_LEVEL_CODE, ORG_2ND_LEVEL_CODE  FROM HR_CONN_SAP_SRC 
		WHERE LINE_MANAGER = 'Y'
		AND HOST_LEGAL_ENT_CODE IN ('FI01','UC12','E140','E170')
		OR (
		LINE_MANAGER = 'Y' AND
		HOST_LEGAL_ENT_CODE = 'UC01' AND 
		    (
		        COMPETENCE_LINE_ABBR IN ('HR','PFA','GIC','COM','ORG','LEG','HOF','ICT','BO','GOT','SEC', 'AUD')
		        OR (
		        COMPETENCE_LINE_ABBR = 'RM' AND DIVISION_ABBR = 'CC_GOV'
		        )
		        OR (
		        COMPETENCE_LINE_ABBR = 'MKG' AND DIVISION_ABBR IN ('CC_GOV_GM','CORP','RTL') AND (ORG_1ST_LEVEL_CODE <> '11541035' or ORG_1ST_LEVEL_CODE is null) 
		        )
		        OR (
		        COMPETENCE_LINE_ABBR = 'BOT' AND DIVISION_ABBR IN ('CC_GOV_GM','CC_OT','CORP','RTL') AND 
		            (ORG_2ND_LEVEL_CODE <> '50009880'   
		            AND ORG_2ND_LEVEL_CODE <> '50009881'
		            AND ORG_2ND_LEVEL_CODE <> '50009882'
		            AND ORG_2ND_LEVEL_CODE <> '50009883'
		            AND ORG_2ND_LEVEL_CODE <> '50009884'
		            AND ORG_2ND_LEVEL_CODE <> '50009885'
		            AND ORG_2ND_LEVEL_CODE <> '50009886'
		            or ORG_2ND_LEVEL_CODE is null
		            )            
		        )
		    )
		) 

	*/

		
		
		private List<String> legalEntityCodeList = new ArrayList<String>();
		private List<String> competenceLineCodeList = new ArrayList<String>();
		
		private List<String> divisionCode4MKG_Marketing_List = new ArrayList<String>(); // MKG-Marketing
		private List<String> divisionCode4BOT_Other_Business_List = new ArrayList<String>(); // BOT-Other Business		
		
		private List<String> org2ndLevelCode4BOT_Other_Business_List = new ArrayList<String>();
		
		
		
		public boolean matches(String hostLegalEntityCode, String competenceLineCode, String divisionCode, String org1stLvlCode, String org2ndLvlCode ){			
			
			if ( legalEntityCodeList.contains(hostLegalEntityCode) ) {
				if ( "UC01".equals(hostLegalEntityCode) ){
					if ( competenceLineCodeList.contains( competenceLineCode ) ) {						
						
						if ( "10000007".equals(competenceLineCode) ) {	//RM	Risk Management							
							if ( divisionCode.equals("10000046") ){	//CC_GOV	CORPORATE CENTER GOVERNANCE"
								return true; 
							} else {
								return false;
							}							
						} else if ( competenceLineCode.equals("10000034") ) {	//MKG	Marketing
							
							if ( divisionCode4MKG_Marketing_List.contains( divisionCode ) ){								
								if ( "11541035".equals( org1stLvlCode ) == false ) { 
									return true; // DOES NOT MATCH 11541035-UC01CALLCEN-UNICREDIT DIRECT
								} else {
									return false; // exclude 11541035-UC01CALLCEN-UNICREDIT DIRECT
								}
							} else {
								return false;
							}														
						} else if ( competenceLineCode.equals( "10000035") ) {//BOT	Other Business							
							if ( divisionCode4BOT_Other_Business_List.contains( divisionCode ) ){
								
								if ( org2ndLevelCode4BOT_Other_Business_List.contains( org2ndLvlCode ) == false){ // exclude this 2nd lvl orgs
									return true;							
								} else {
									return false;
								}
								
							} else {
								return false;
							}
						} else {
							return true;
						}
					} else {
						return false;
					}	
				} else {					
					return true;					
				}				
			} else {
				return false;			
			}
			
		}
		
		
		
		private StaticDataDict(){
			
			legalEntityCodeList.add("UC01");
			legalEntityCodeList.add("FI01");
			legalEntityCodeList.add("E140");
			legalEntityCodeList.add("UC12");
			legalEntityCodeList.add("E170");
												
			competenceLineCodeList.add("10000019");	//BO	GBS - Operations
			competenceLineCodeList.add("10000035");	//BOT	Other Business
			competenceLineCodeList.add("10000038");	//COM	Compliance
			competenceLineCodeList.add("10000011");	//GIC	Identity & Communications
			competenceLineCodeList.add("10000024");	//GOT	GBS - Others
			competenceLineCodeList.add("10000016");	//HOF	Head Office Functions
			competenceLineCodeList.add("10000003");	//HR	Human Resources
			competenceLineCodeList.add("10000025");	//ICT	GBS - ICT
			competenceLineCodeList.add("10000014");	//LEG	Legal
			competenceLineCodeList.add("10000034");	//MKG	Marketing
			competenceLineCodeList.add("10000026");	//ORG	Organization
			competenceLineCodeList.add("10000006");	//PFA	Planning, Finance & Administration
			competenceLineCodeList.add("10000007");	//RM	Risk Management
			competenceLineCodeList.add("10000032");	//SEC	GBS - Security		
			competenceLineCodeList.add("10000005");	//AUD	Internal Audit
						
			divisionCode4MKG_Marketing_List.add("10000047"); //CC_GOV_GM	CORPORATE CENTER GOVERNANCE_GENERAL MGT
			divisionCode4MKG_Marketing_List.add("10000051"); //CORP	CORPORATE
			divisionCode4MKG_Marketing_List.add("10000050"); //RTL	RETAIL

			divisionCode4BOT_Other_Business_List.add("10000047"); //CC_GOV_GM	CORPORATE CENTER GOVERNANCE_GENERAL MGT
			divisionCode4BOT_Other_Business_List.add("10000051"); //CORP	CORPORATE
			divisionCode4BOT_Other_Business_List.add("10000050"); //RTL	RETAIL
			divisionCode4BOT_Other_Business_List.add("10000048");	//CC_OT	OTHER CORPORATE CENTER
			
			org2ndLevelCode4BOT_Other_Business_List.add("50009880");	//UC0142012	REGION NORD OVEST
			org2ndLevelCode4BOT_Other_Business_List.add("50009886");	//UC0142018	REGION SICILIA
			org2ndLevelCode4BOT_Other_Business_List.add("50009882");	//UC0142014	REGION NORD EST
			org2ndLevelCode4BOT_Other_Business_List.add("50009885");	//UC0142017	REGION SUD
			org2ndLevelCode4BOT_Other_Business_List.add("50009884");	//UC0142016	REGION CENTRO
			org2ndLevelCode4BOT_Other_Business_List.add("50009883");	//UC0142015	REGION CENTRO NORD
			org2ndLevelCode4BOT_Other_Business_List.add("50009881");	//UC0142013	REGION LOMBARDIA
			
		}
		
	}
	
	
	private static StaticDataDict staticDataDict = new StaticDataDict(); 
	

	Dictionary dictionary;
	
	Logger logger = Logger.getLogger(RoleMapper.class);
	
	private SourceFileConf sourceFileConf;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn(); 
		
		String userId = srcVal[0];
		
		String roleExcp;
		
		// The default role for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {
			
			// table with role exceptions for users 
			roleExcp = dictionary.checkForPlateauRoleExcp(userId);				
		
			if ( roleExcp != null ){
				mappedCol.setValue(roleExcp);
				logger.debug("User: " + userId + " will be assigned with role1: " +  roleExcp + ", instead of the default role: " +defaultVal);
				
			} else {

				// check for static exceptions for selected LE
				roleExcp = checkForPlateauRoleStaticExcp(sourceRow);
				
				if ( roleExcp != null){
					mappedCol.setValue(roleExcp);
					logger.debug("User: " + userId + " will be assigned with role2: " +  roleExcp + ", instead of the default role: " +defaultVal);					
					
				} else { // no exceptins, set the dafault value								
					mappedCol.setValue(defaultVal);
				}
			}
			return mappedCol;
		}
	}
	

	/**
	 * This function checks for static exceptions  
	 * (request from 2015-05-21)
	 *  
	 */
	private String checkForPlateauRoleStaticExcp(SourceRow sourceRow) {
		
		if ( sourceFileConf.isLineManager( sourceRow ) ){
			
			String hostLegalEntityCode = sourceFileConf.getSrcHostLegalEntCode(sourceRow);
			String competenceLineCode = sourceFileConf.getSrcCompetenceLineCode(sourceRow);
			String divisionCode = sourceFileConf.getSrcDivisionCode(sourceRow);
			String org1stLvlCode = sourceFileConf.getSrcOrg1stLevelCode(sourceRow);
			String org2ndLvlCode = sourceFileConf.getSrcOrg2ndLevelCode(sourceRow);
			
			if (staticDataDict.matches(hostLegalEntityCode, competenceLineCode, divisionCode, org1stLvlCode, org2ndLvlCode)){
				return USER_ROLE_TRAIN_COLLECT;
			}
		}
		return null;
	}


	
	

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}


	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}


	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

}
