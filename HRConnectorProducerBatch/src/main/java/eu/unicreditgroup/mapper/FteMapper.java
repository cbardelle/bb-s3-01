package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;


/**
 * Email mapping 
 *  
 * @author UV00074
 *
 */
public class FteMapper implements IMapper {
	
	private Dictionary dictionary;


	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	/**
	 * 	The  mapper will return passed values separated by '-'
	 *  or the default value when source table is null or empty 
	 * 
	 */	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();
		
		// defaults to 0 for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue("0");
			return mappedCol; 
		}
		
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {			
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				mappedCol.setValue(defaultVal);
				return mappedCol;
			}
			
			
			for (int i = 0 ; i < srcVal.length ; i++ ){			
				sb.append(srcVal[i]);				
				if (i < srcVal.length - 1){				
					sb.append("-");				
				}
			}			
			mappedCol.setValue(sb.toString());
			return mappedCol;
		}
	}
}
