package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.ContractType;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class EmployeeStatusIDMapper implements IMapper {

	Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String contractTypeCode = srcVal[0]; 

		MappedColumn mappedCol = new MappedColumn();
		
		// The default for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
		
		if (StringUtils.isEmpty(contractTypeCode)){
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}
		
		ContractType contractType = dictionary.findContractType(contractTypeCode);
		
		if ( contractType == null ){			
			//mappedCol.setValue(	String.format("Failed to map employee status id for contract type: %s", contractType) );
			//mappedCol.setErrorMsg(	String.format("Failed to map employee status id for contract type code: %s", contractTypeCode) );
			
			mappedCol.createMappingException(
					contractTypeCode,
					String.format("Failed to map employee status id for contract type code: %s", contractTypeCode),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);
			
			
			
			return mappedCol;
		} else {
			// check if not the old local italian value
			if ("N".equalsIgnoreCase(contractType.getGlobalType())){
				
				// to reduce log size
				//mappedCol.setErrorMsg(	String.format("Failed to map employee status id for contract type code: %s. Local values are not in use any more.", contractTypeCode) );
				
				return mappedCol;
			}
		}

		// all values are now global, and values are taken from the dictionary table
		mappedCol.setValue(contractType.getPlateauEmpStId());
		
		/*
		if ("Y".equalsIgnoreCase(contractType.getGlobalType())){
			// for global values the value is taken from the table			
			mappedCol.setValue(contractType.getPlateauEmpStId());
			
		} else {
			// for local values it is determined using status description
			String statusDesc = srcVal[1];
			if ("FULL TIME".equalsIgnoreCase(statusDesc)){
				mappedCol.setValue("FT");
			} else if ("PART TIME".equalsIgnoreCase(statusDesc)){
				mappedCol.setValue("PT");
			} else {
				mappedCol.setErrorMsg(String.format("Failed to map employee status id for contract type: %s becasue of unknown status description: %s" , contractType, statusDesc));
			}			
		}
		*/
		
		return mappedCol;
	}
	

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
