package eu.unicreditgroup.mapper;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class NotActiveMapper implements IMapper {
	
	Dictionary dictionary;

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
				
		String userStatusCode = srcVal[0];		
		MappedColumn mappedCol = new MappedColumn();		
		String result = dictionary.findEmpStatus(userStatusCode);
		
		if (result == null){			
			//mappedCol.setErrorMsg( String.format("Failed to map 'Not Active' field for User Status Code: '%s'", userStatusCode) );	
			
			mappedCol.createMappingException(
					userStatusCode,
					String.format("Failed to map 'Not Active' field for User Status Code: '%s'", userStatusCode),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);
			
		}else {
			mappedCol.setValue(result);
		}		
		return mappedCol;		
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	

}
