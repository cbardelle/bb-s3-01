package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;

public class DomainDescMapper implements IMapper {

	//private final String HRBP_NOT_FOUND_DESC = "Not found";
	
	// changed due to Stefano Prest request
	private final String DOMAIN_DESC_PREFIX = "Learners Portfolio of ";
	
	private final String DOMAIN_DESC_NO_HRBPID = "No Learners Portfolio available";
	
	private final String DEFAULT_DOMAIN_DESC_4_EXTERNAL_USERS = "Learners Portfolio of Anagrafe Alleati";
	
	private final String DEFAULT_DOMAIN_DESC_4_AUDIT_ACADEMY = "Learners Portfolio of Audit";
	
	private final String AUDIT_ACADEMY_DOMAIN = "AA2";
	
	private final String DEFAULT_DOMAIN_DESC_4_GE_MNGR = "Domain for German managers";
	
	private SourceFileConf sourceFileConf;
		
	private Dictionary dictionary;
		
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String hrbpUserId = srcVal[0];
		
		String hostLe = srcVal[1];
			
		MappedColumn mappedCol = new MappedColumn();
		
		
		if ( AUDIT_ACADEMY_DOMAIN.equals(hostLe) ) {
			
			mappedCol.setValue(DEFAULT_DOMAIN_DESC_4_AUDIT_ACADEMY);			
			return mappedCol;
			
		} else if (sourceFileConf.isExternalUser(sourceRow) ){
			
			mappedCol.setValue(DEFAULT_DOMAIN_DESC_4_EXTERNAL_USERS);			
			return mappedCol;
		} else if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){
			
			mappedCol.setValue(DEFAULT_DOMAIN_DESC_4_GE_MNGR);			
			return mappedCol;
			
		} else {
		
			StringBuilder sb = new StringBuilder();
			
			if (StringUtils.isEmpty(hrbpUserId)){
				// 2010-11-23 request : users without HRBPID columns won't be rejected
				// and fixed domain description will be used			
				sb.append(DOMAIN_DESC_NO_HRBPID);
				
			} else {
				
							
				HRBPUser hrbpUser = dictionary.findHRBPUser(hrbpUserId);
				
				if (hrbpUser == null){			
					sb.append(DOMAIN_DESC_PREFIX).append(hrbpUserId);			
				} else {
					sb.append(DOMAIN_DESC_PREFIX).append(hrbpUser.getFirstName()).append(" ").append(hrbpUser.getLastName());
				}
			}
			
			mappedCol.setValue(sb.toString());
			return  mappedCol;
		
		}
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}
	
	
	
}
