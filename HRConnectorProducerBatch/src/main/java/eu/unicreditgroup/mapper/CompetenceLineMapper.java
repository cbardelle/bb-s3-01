package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class CompetenceLineMapper implements IMapper {
	
	private Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
		 
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		} else if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {			
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				mappedCol.setValue(defaultVal);
				return mappedCol;
			}
						
			// srcVal[0] contain competence line id (which will be used for validation) 
			// srcVal[1] and srcVal[2] contain competence line abbr and desc
			sb.append(srcVal[1]);				
			sb.append("-");				
			sb.append(srcVal[2]);						
			mappedCol.setValue(sb.toString());
			
			return mappedCol;
		}
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
