package eu.unicreditgroup.mapper;

import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class AgeMapper implements IMapper, InitializingBean {
	
	private String dateFormat =  "dd.MM.yyyy";
	
	private DateTimeFormatter parser;
	
	private Dictionary dictionary;

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal , SourceRow sourceRow) {
		
		String strDateOfBirth = srcVal[0];
		
		MappedColumn mappedCol = new MappedColumn();
		
		// The default value for all German managers
		if (dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
		
		if (strDateOfBirth == null){
			mappedCol.setValue(defaultVal);
			return mappedCol;
		}		
	
		DateTime dateOfBirth = null;
		try {
			dateOfBirth = parser.parseDateTime(strDateOfBirth);
		} catch (Exception e){
			//mappedCol.setErrorMsg("Failed to parse the date of birth due to: " + e.getMessage());
			
			mappedCol.createMappingException(
					strDateOfBirth,
					String.format( "Failed to parse the date of birth due to: " + e.getMessage() ),
					this.getClass().getSimpleName(),
					sourceRow.getUserId()				
				);
			
			
			return mappedCol; 
		}
				
		DateTime now = new DateTime();
		Years age = Years.yearsBetween(dateOfBirth, now);		
		
		mappedCol.setValue(String.valueOf(age.getYears()));
		
		return mappedCol;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		parser =  DateTimeFormat.forPattern(dateFormat);		
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
