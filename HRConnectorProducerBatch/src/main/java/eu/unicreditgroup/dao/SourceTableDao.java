package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;


/**
 * @author UV00074
 *
 */
public class SourceTableDao {

	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;
	
	private SourceFileConf sourceFileConf;		
		
	private static int[] columnTypes;
	
	private String SQL_INSERT_INTO_TEMP_TABLE = "";	
		
	private String SQL_TRUNCATE_TEMP_TABLE = "TRUNCATE TABLE hr_conn_sap_src_tmp";
	
	private String SQL_TRUNCATE_MAIN_TABLE = "TRUNCATE TABLE hr_conn_sap_src";
	
	private String SQL_INSERT_NEW_SOURCE_ROWS = 
		"  INSERT INTO hr_conn_sap_src                                                                        " + 
		"   SELECT *                                                                                          " +
		"     FROM hr_conn_sap_src_tmp                                                                        " ;
	
	
	private final String SQL_GET_USERS_WITH_EMPTY_RESUME = "SELECT user_id, first_name, last_name FROM hr_conn_sap_src_tmp WHERE resume IS NULL";
	
	private final String SQL_REMOVED_EXTERNAL_USERS = " SELECT * FROM HR_CONN_SAP_SRC_HST " + 
	" WHERE OFFICE_COUNTRY = 'AA' AND HOME_LEGAL_ENT_CODE= 'AA' AND HOST_LEGAL_ENT_CODE= 'AA' " +
	" AND (REMOVED_DATE IS NULL OR REMOVED_DATE >= SYSDATE - 30) " + 
	" AND USER_ID NOT IN " +
	" ( SELECT USER_ID " +
	" FROM HR_CONN_SAP_SRC " +
	" WHERE OFFICE_COUNTRY = 'AA' AND HOME_LEGAL_ENT_CODE= 'AA' AND HOST_LEGAL_ENT_CODE= 'AA' )";
	
	
	private final String SQL_REACTIVATED_EXTERNAL_USERS = " SELECT H.USER_ID FROM HR_CONN_SAP_SRC_HST H, HR_CONN_SAP_SRC S " +
			" WHERE H.OFFICE_COUNTRY = 'AA' AND H.HOME_LEGAL_ENT_CODE= 'AA' AND H.HOST_LEGAL_ENT_CODE= 'AA' " +
			" AND H.REMOVED_DATE IS NOT NULL " +
			" AND H.USER_ID = S.USER_ID ";
	
	
	private class HistorySourceRowMapper implements RowMapper<SourceRow> {
		@Override
		public SourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			SourceRow srcRow = new SourceRow();
			
			int sourceColsCount = sourceFileConf.getSourceColumns().size(); 
					
			String[] row = new String[sourceColsCount];					
			
			for ( int i = 0; i < sourceColsCount ; i++ ){
				row[i] = rs.getString(i+1);
			}
			
			// Set user's status as witdrown
			row[sourceFileConf.getSrcUserStatusID().getSrcColIndex()] = sourceFileConf.USER_STATUS_ID_WITHDRAWN;
			row[sourceFileConf.getSrcUserStatusDesc().getSrcColIndex()] = sourceFileConf.USER_STATUS_ID_WITHDRAWN_DESC;
					
			srcRow.setData(row);			
			return srcRow;
		}
	}
	
	private String SQL_GET_USERS_WITH_CHANGED_ID = 
		" SELECT t.user_id as new_user_id,        " +
		"  s.user_id as old_user_id,              " +
		"  t.first_name,                       " +
		"  t.last_name ,                       " +
		"  t.fiscal_code                       " +
		"  FROM hr_conn_sap_src_tmp t          " +
		"  JOIN hr_conn_sap_src s              " +
		"  ON t.fiscal_code = s.fiscal_code    " +
		"  WHERE t.user_id <> s.user_id        " +
		"  AND t.fiscal_code IS NOT NULL 	   ";	
	
	
	private String getInsertIntoTempTableQuery(){
		
		if (StringUtils.isNotEmpty(SQL_INSERT_INTO_TEMP_TABLE)){
			return SQL_INSERT_INTO_TEMP_TABLE;
		}
		
		String sqlTemplate = " INSERT INTO HR_CONN_SAP_SRC_TMP  VALUES ( %s )";
		
		int sourceColumnsCount = sourceFileConf.getSourceColumns().size();
		
		columnTypes = new int[sourceColumnsCount];
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < sourceColumnsCount ; i++){
			columnTypes[i] = java.sql.Types.VARCHAR;
			// build the inserting sql (?, ?, ?, ... )
			if (i > 0){
				sb.append(",");
			}
			sb.append("?");			
		}		
		SQL_INSERT_INTO_TEMP_TABLE = String.format(sqlTemplate, sb.toString() );
		return SQL_INSERT_INTO_TEMP_TABLE; 
		
	}
	
	/*
	static{
		
		int sourceColumnsCount = sourceColumns.size();
		
		columnTypes = new int[sourceColumnsCount];
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < sourceColumnsCount ; i++){
			columnTypes[i] = java.sql.Types.VARCHAR;
			// build the inserting sql (?, ?, ?, ... )
			if (i > 0){
				sb.append(",");
			}
			sb.append("?");			
		}		  
		SQL_ADD_TO_TEMP_TABLE = String.format(SQL_ADD_TO_TEMP_TABLE, sb.toString() );		
	}
	*/
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        //this.insertResult = new SimpleJdbcInsert(dataSource).withTableName("");
        this.dataSource = dataSource;
    }	
	
	
   /**
     * Adds batch of records to that table mandatory_courses_result_temp.
     * When DuplicateKeyException occurs all rows are rolled back
     * 
     * @param results
     */		
	@Transactional(value="ltp",readOnly=false, rollbackFor=org.springframework.dao.DuplicateKeyException.class)
    public boolean addRowsToTempSourceTable(List<Object[]> rows) throws Exception{    	
    	//try {    		
    	simpleJdbcTemplate.batchUpdate( getInsertIntoTempTableQuery(), rows, columnTypes);
    	return true;    		
    	//} catch (Exception e){
    	//	throw e;
    	//}
    }
	
    public boolean addRowToTempSourceTable(Object[] rowData) throws Exception{    	
    	    		
    	List<Object[]> rows = new ArrayList<Object[]>();
    	rows.add( rowData );    	
    	simpleJdbcTemplate.batchUpdate( getInsertIntoTempTableQuery(), rows, columnTypes);
    	return true;   		    	
    }
	

    public void cleanTempTable(){
    	simpleJdbcTemplate.update(SQL_TRUNCATE_TEMP_TABLE);
    }
    
    /**
     *  Deletes all rows in table hr_conn_sap_src.
     */
    private void cleanMainTable(){
    	simpleJdbcTemplate.update(SQL_TRUNCATE_MAIN_TABLE);
    }
    
    /**
     * Inserts new ldap departments into table mandatory_courses_ldap_dep using data taken from mandatory_courses_ldap_dep_tmp.
     * 
     * @return Number of inserted rows. 
     */
    public int insertSourceRows(){
        int updatedRows = simpleJdbcTemplate.update(SQL_INSERT_NEW_SOURCE_ROWS);
        return updatedRows;
    }
    
    @Transactional("ltp")
    public int refreshMainTable(){
    	cleanMainTable();
    	return insertSourceRows();    	
    }  

	public int removeDuplicatedUserId(Set<String> userIds) {
		
		// delete duplicates from the temporary table! 
		String SQL_DELETE_DUPLICATES = "DELETE FROM hr_conn_sap_src_tmp WHERE user_id IN ( %s )";		
		
		StringBuilder sb = new StringBuilder();
		boolean firstItem = true;
		for (String userId : userIds){
			if ( firstItem == true )
			{
				firstItem = false;
			} else {
				sb.append(",");
			}						
			sb.append("'").append(userId).append("'");			
		}
		
		SQL_DELETE_DUPLICATES = String.format(SQL_DELETE_DUPLICATES, sb.toString());		
		int updatedRows = simpleJdbcTemplate.update(SQL_DELETE_DUPLICATES);				
		return updatedRows;
	}

	/**
	 * List of users with empty resume column
	 * 
	 */
	public List<User> getUsersWithEmptyResume() {
		return simpleJdbcTemplate.query(SQL_GET_USERS_WITH_EMPTY_RESUME, new RowMapper<User>(){
		@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setUserId( rs.getString("user_id") );
				user.setFirstName( rs.getString("first_name") );
				user.setLastName( rs.getString("last_name") );				
				return user;
			}				
		});		
	}

	public List<User> getUsersWithChangedId() {
		return simpleJdbcTemplate.query(SQL_GET_USERS_WITH_CHANGED_ID, new RowMapper<User>(){
		@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setUserId( rs.getString("new_user_id") );
				user.setOldUserId( rs.getString("old_user_id") );
				user.setFirstName( rs.getString("first_name") );
				user.setLastName( rs.getString("last_name") );
				user.setFiscalCode(rs.getString("fiscal_code"));
				return user;
			}				
		});
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	public int getUsersWithEmptyEmailCount() {
	
		return simpleJdbcTemplate.queryForInt( "select count(*) from hr_conn_sap_src where office_email is null" );

	}
	
	/**
	 * Get list of external users not present in the HR_CONN_SAP_SRC 
	 * but present in the HR_CONN_SAP_SRC_HST
	 * 
	 */
	public List<SourceRow> getRemovedExternalUsers(){
		return simpleJdbcTemplate.query( SQL_REMOVED_EXTERNAL_USERS, new HistorySourceRowMapper());
	}
	

	
	/**
	 * Get list of reactivated external users (present in the HR_CONN_SAP_SRC_HST as removed 
	 * and present (again) in the HR_CONN_SAP_SRC)
	 * 
	 */
	public List<String> getReactivatedExternalUsers(){
				
		return simpleJdbcTemplate.query(SQL_REACTIVATED_EXTERNAL_USERS, new RowMapper<String>() {

			@Override
			public String mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				
				return rs.getString("user_id");
			}
		});
		
	}

	/**
	 * create backup of the main source table
	 * this table will contain backup of the data of the table HR_CONN_SAP_SRC
	 * from the last 7 days. After 7 days the data will be truncated 
	 * @return
	 */
	@Transactional("ltp")
	public long bakupMainSourceTable() {
		
		String copySql = " INSERT INTO HR_CONN_SAP_SRC_IMPRTD SELECT S.*, SYSDATE FROM HR_CONN_SAP_SRC S ";
		//String cleanSql = " TRUNCATE TABLE HR_CONN_SAP_SRC_IMPRTD ";

		String cleanSql = " DELETE FROM HR_CONN_SAP_SRC_IMPRTD WHERE TMSTP < SYSDATE - 7";
		
		// delete old data
		simpleJdbcTemplate.update(cleanSql);
			
		
		return  simpleJdbcTemplate.update(copySql);
		
	}
	

	
	

}	
