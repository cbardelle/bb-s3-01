//GIT01
package eu.unicreditgroup.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.mapper.TableMapper;



public class BufferedResultTableWriter  {
	
	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;
	
	/**
	 * Default buffer size
	 */
	private final int DEF_BUFF_SIZE = 1024;
	
	private static final Logger logger = Logger.getLogger(BufferedResultTableWriter.class);
	
	List<MappedRow> buffer = new ArrayList<MappedRow>(DEF_BUFF_SIZE);
	
	
	List<Exception> exceptions = new ArrayList<Exception>();
	
	private TableMapper tableMapper;
	
	private String insertIntoTmpTableSql = "";
	
	private static int[] columnTypes;
	
	
	private String getInsertIntoTmpTableSql(){
		
		if (StringUtils.isNotEmpty(insertIntoTmpTableSql)){
			return insertIntoTmpTableSql;
		}
				
		String sqlTemplate = " INSERT INTO HR_CONN_RES_FILE_TMP  VALUES ( %s )";
		
		int resultColumnsCount = tableMapper.getResultColumns().size();
		
		columnTypes = new int[resultColumnsCount];
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < resultColumnsCount ; i++){
			columnTypes[i] = java.sql.Types.VARCHAR;
			// build the inserting sql (?, ?, ?, ... )
			if (i > 0){
				sb.append(",");
			}
			sb.append("?");			
		}		
		insertIntoTmpTableSql = String.format(sqlTemplate, sb.toString() );
		return insertIntoTmpTableSql; 
	}
	
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        //this.insertResult = new SimpleJdbcInsert(dataSource).withTableName("");
        this.dataSource = dataSource;
    }	
	
	
    @Transactional(value="ltp",readOnly=false)
	public void flush() {
		
		if (buffer.size() == 0){
			return;
		}
				
		ArrayList<Object[]> rows = new ArrayList<Object[]>(buffer.size());
		
		for (int i = 0 ; i < buffer.size() ; i++){			
			rows.add(buffer.get(i).getData());					
		}
		
		try {						
			simpleJdbcTemplate.batchUpdate( getInsertIntoTmpTableSql(), rows, columnTypes);
		} catch (Exception ex) {
			exceptions.add(ex);
			logger.error("Exception when writing result file table: " + ex);			
		}
		
		// clear the buffer anyway to prevent infinite loop
		buffer.clear();
	}
	
	
	
	
	

	/**
	 * Puts row to a buffer or writes it down when the buffer is full
	 * 
	 * @param mappedRow
	 */
	public void writeResultRow(MappedRow mappedRow) {
		
		buffer.add(mappedRow);
		if (buffer.size() < DEF_BUFF_SIZE){
			//the buffer is not full
			return;			
		}
		
		// looks like buffer is full, write down buffered data
		flush();
		
	}
		
	@Transactional(value="ltp",readOnly=false)
	public void replaceMainTable(){
		if (exceptions.size() > 0){
			logger.error("The table HR_CONN_RES_FILE will not be replaced due to errors!");
			return;
		}
		
		try{
			// truncate main table
			simpleJdbcTemplate.update("TRUNCATE TABLE HR_CONN_RES_FILE");
			logger.info("Table HR_CONN_RES_FILE truncated.");
		 		
			int inserted = simpleJdbcTemplate.update("INSERT INTO HR_CONN_RES_FILE SELECT * FROM HR_CONN_RES_FILE_TMP");
			logger.info("Table HR_CONN_RES_FILE replaced with the temporary table. The number of inserted row is: " + inserted);
			 
		} catch (Exception ex){
			exceptions.add(ex);
			logger.error("Exception when replace main table: HR_CONN_RES_FILE: ", ex);
		}		
	}	
	
	 public void clearTempTable(){
		 try{
			 simpleJdbcTemplate.update("TRUNCATE TABLE HR_CONN_RES_FILE_TMP");
			 
		 } catch (Exception ex){
			 exceptions.add(ex);
			 logger.debug("Exception when truncating table: HR_CONN_RES_FILE_TMP", ex);
		 }
	 }
	    
	   

	public TableMapper getTableMapper() {
		return tableMapper;
	}

	public void setTableMapper(TableMapper tableMapper) {
		this.tableMapper = tableMapper;
	}
	
}
