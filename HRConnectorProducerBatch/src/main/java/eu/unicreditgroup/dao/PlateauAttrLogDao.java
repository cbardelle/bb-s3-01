package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.HRBPUserLog;
import eu.unicreditgroup.beans.PlateauAttrLogItem;
import eu.unicreditgroup.beans.TimeZone;

/**
 * Dao for table PLT_ATTR_LOG
 * This table is used for keeping track of changes of attributes 
 * used in assignment profiles
 * 
 * @author UV00074
 *
 */
public class PlateauAttrLogDao  {
	
	private static final Logger logger = Logger.getLogger(PlateauAttrLogDao.class);
	 	
	private SimpleJdbcTemplate simpleJdbcTemplate;
		
	private DataSource dataSource;
	
	
	private final String SQL_INSERT_HRBP_LOG = 
		"INSERT INTO hr_conn_hrbp_log (user_id, first_name, last_name, log_date, action) " +
		" VALUES (:userId, :firstName, :lastName, :logDate, :action)"; 

	
	private final String SQL_INSERT_PLT_ATTR_LOG = 
		"INSERT INTO hr_conn_plt_attr_log (res_attr_idx, attr_sap_code, new_val, old_val, ap_id, created_date) " +
		" VALUES (:resAttrIdx, :attrSapCode, :newVal, :oldVal, :apId, :createdDate)";
	
	
	/**
	 * Write informations about changed attributes and assignments profiles 
	 * that may be affected by such change
	 *  
	 * @param logItems list of changes
	 */
	public void writePlateauAttrLog(List<PlateauAttrLogItem> logItems) throws Exception {
		
		if ( logItems == null || logItems.isEmpty() ){			
			return;
		}
				
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(logItems.toArray());		
        simpleJdbcTemplate.batchUpdate(SQL_INSERT_PLT_ATTR_LOG, batch);		
	}
	
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }

	
	
	
}
