//GIT01
package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.HRBPUserLog;
import eu.unicreditgroup.beans.TimeZone;


public class HRBPUserDao  {
	
	private static final Logger logger = Logger.getLogger(HRBPUserDao.class);
	
	private final ConcurrentHashMap<String, HRBPUser> hrbpUserMap = new ConcurrentHashMap<String, HRBPUser>(); 
	
	private SimpleJdbcTemplate simpleJdbcTemplate;
		
	private DataSource dataSource;
	
	private final String SQL_DELETE_MAIN_TABLE = "TRUNCATE TABLE hr_conn_hrbp";
	
	private final String SQL_DELETE_TMP_TABLE= "TRUNCATE TABLE hr_conn_hrbp_tmp";
	
	private final String SQL_ADD_NEW_HRBP_USERS = "INSERT INTO hr_conn_hrbp SELECT * FROM hr_conn_hrbp_tmp";
	
	private final String SQL_DELETE_EMPTY_TO_FILLED_USERS = "DELETE FROM hr_conn_hrbp_log WHERE action = 'NEW_WITH_EMPTY_DOMAIN' AND user_id  = ? ";
	
	
	//select +NEW+ HRBP users
	private final String SQL_GET_NEW_HRBP_USERS = 
		" SELECT user_id, first_name, last_name  " +
		" FROM hr_conn_hrbp_tmp WHERE user_id NOT IN ( SELECT user_id FROM hr_conn_hrbp )";

	//select +REMOVED+ HRBP users
	private final String SQL_GET_REMOVED_HRBP_USERS =  
		" SELECT user_id, first_name, last_name  " +
		" FROM hr_conn_hrbp WHERE user_id NOT IN ( SELECT user_id FROM hr_conn_hrbp_tmp )";


	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }
	
	private final String SQL_INSERT_HRBP_USERS_TMP = 
		"INSERT INTO hr_conn_hrbp_tmp (user_id, first_name, last_name) " +
		" VALUES (:userId, :firstName, :lastName)"; 
		
	private final String SQL_INSERT_HRBP_LOG = 
		"INSERT INTO hr_conn_hrbp_log (user_id, first_name, last_name, log_date, action) " +
		" VALUES (:userId, :firstName, :lastName, :logDate, :action)"; 
	
	
	private final String SQL_GET_DOMAIN_COUNT =
		"SELECT count(*) FROM hr_conn_sap_src_tmp WHERE hrbp_id = ?";
	
	private final String SQL_GET_HRBP_WITH_EMPTY_DOMAIN_FROM_LOG =
	" SELECT l.user_id, l.first_name, l.last_name, MAX(log_date)	" +
	" FROM hr_conn_hrbp_log l	" +
	" JOIN hr_conn_hrbp_tmp h	ON h.user_id   = l.user_id	" +
	" WHERE l.action = 'NEW_WITH_EMPTY_DOMAIN' GROUP BY l.user_id,l.first_name,l.last_name";
	
	
	public void queueHRBPUser(String userId, String firstName, String lastName) {		
		if ( hrbpUserMap.containsKey(userId) == false) {				
			HRBPUser hrbpUser = new HRBPUser(userId, firstName, lastName);
			hrbpUserMap.put(userId, hrbpUser);
		}
	}
	
	/**
	 * Return the list of active HRBP
	 * warn: this query might return users that has not set HRBP = 'Y' 
	 * @return
	 */
	private List<HRBPUser> getActiveHRBPUsers(){
		
		
		// TODO join with HR_CONN_EMP_ST
		String SQL_GET_ACTIVE_HRBP_USERS = 
			"select user_id, first_name, last_name from hr_conn_sap_src " +
			" where user_id in ( select distinct hrbp_id from hr_conn_sap_src where hrbp_id is not null and user_status_id = 3 )" +
			" and user_status_id = 3";
		
		return simpleJdbcTemplate.query(SQL_GET_ACTIVE_HRBP_USERS, new RowMapper<HRBPUser>(){
			@Override
			public HRBPUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				HRBPUser hrbpUser = new HRBPUser();
				hrbpUser.setUserId( rs.getString("user_id") );
				hrbpUser.setFirstName( rs.getString("first_name") );
				hrbpUser.setLastName( rs.getString("last_name") );				
				return hrbpUser;
			}				
		});
	}
	
	/*
	private List<HRBPUser> getActiveHRBPUsersDetails( Set userIds){
		
		String SQL_GET_ACTIVE_HRBP_USERS = 
		"	SELECT user_id, first_name, last_name                                        " +         
		"	 FROM hr_conn_sap_src src                                                    " +						 
		"	 INNER JOIN hr_conn_emp_st st ON src.user_status_id = st.source_user_status  " +
		"	 WHERE st.PLATEAU_NOT_ACTIVE = 'N' AND user_id IN ( %s )                     ";

		
		StringBuilder sb = new StringBuilder();
		boolean firstItem = true;
		for ( String userId : hrbpUserMap.keySet()) {
			
			if ( firstItem == true )
			{
				firstItem = false;
			} else {
				sb.append(",");
			}						
			sb.append("'").append(userId).append("'");
		}
		
		String filter = sb.toString();
		logger.debug("Getting details for HRBP users : " + filter);
		
		SQL_GET_ACTIVE_HRBP_USERS = String.format(SQL_GET_ACTIVE_HRBP_USERS, filter);
		
		return simpleJdbcTemplate.query(SQL_GET_ACTIVE_HRBP_USERS, new RowMapper<HRBPUser>(){
			@Override
			public HRBPUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				HRBPUser hrbpUser = new HRBPUser();
				hrbpUser.setUserId( rs.getString("user_id") );
				hrbpUser.setFirstName( rs.getString("first_name") );
				hrbpUser.setLastName( rs.getString("last_name") );				
				return hrbpUser;
			}				
		});
	}
	*/
	
	public List<HRBPUser> getNewHRBPUsers() {
		
		return simpleJdbcTemplate.query(SQL_GET_NEW_HRBP_USERS, new RowMapper<HRBPUser>(){
			@Override
			public HRBPUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				HRBPUser hrbpUser = new HRBPUser();
				hrbpUser.setUserId( rs.getString("user_id") );
				hrbpUser.setFirstName( rs.getString("first_name") );
				hrbpUser.setLastName( rs.getString("last_name") );				
				return hrbpUser;
			}				
		});
	}

	public List<HRBPUser> getRemovedHRBPUsers() {
	
		return simpleJdbcTemplate.query(SQL_GET_REMOVED_HRBP_USERS, new RowMapper<HRBPUser>(){
			@Override
			public HRBPUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				HRBPUser hrbpUser = new HRBPUser();
				hrbpUser.setUserId( rs.getString("user_id") );
				hrbpUser.setFirstName( rs.getString("first_name") );
				hrbpUser.setLastName( rs.getString("last_name") );				
				return hrbpUser;
			}				
		});
	}
		
	
	@Transactional("ltp")
	public int writeHRBPUsersTempTable(){
			
		
		/*
		if ( hrbpUserMap.isEmpty() ){
			logger.debug("No HRBP users where found!");
			return 0;
		}
				
		cleanTempTable();						
		// check user is active and was not duplicated
		List<HRBPUser> hrbpUsers = getActiveHRBPUsersDetails(hrbpUserMap.keySet());
		
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(hrbpUsers.toArray());		
        simpleJdbcTemplate.batchUpdate(SQL_INSERT_HRBP_USERS, batch);				
		return hrbpUsers.size();
		*/		
						
		cleanTempTable();						
		
		// get the list of active users with filled domains
		List<HRBPUser> hrbpUsers = getActiveHRBPUsers();
		
		if ( hrbpUsers.isEmpty() ){
			// should not happen 
			logger.debug("No HRBP users where found!");
			return 0;
		}		
		
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(hrbpUsers.toArray());
		
        simpleJdbcTemplate.batchUpdate(SQL_INSERT_HRBP_USERS_TMP, batch);
				
		return hrbpUsers.size();				
	}	
	
	@Transactional("ltp")
	public void refreshMainTable(){
		cleanMainTable();
		copyFromTempTable();
	}
		
	public void cleanMainTable() {
		simpleJdbcTemplate.update(SQL_DELETE_MAIN_TABLE);
	}
	
	public void cleanTempTable() {
		simpleJdbcTemplate.update(SQL_DELETE_TMP_TABLE);
	}
	
	private void copyFromTempTable(){
		simpleJdbcTemplate.update(SQL_ADD_NEW_HRBP_USERS);
		
	}

	public void writeUsersToLogTable(List<HRBPUser> users, String action) {
		
		if ( users == null || users.isEmpty() ){
			logger.debug("No " + action +  " HRBP users to log where found!");
			return;
		}

		List<HRBPUserLog> hrbpUserLog = new ArrayList<HRBPUserLog>();		
		Date now = new Date();		
		for (HRBPUser u : users){
			HRBPUserLog logInfo = new HRBPUserLog();
			logInfo.setUserId(u.getUserId());
			logInfo.setFirstName(u.getFirstName());
			logInfo.setLastName(u.getLastName());
			logInfo.setLogDate(now);
			logInfo.setAction(action);
			hrbpUserLog.add(logInfo);
		}
		
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(hrbpUserLog.toArray());
		
        simpleJdbcTemplate.batchUpdate(SQL_INSERT_HRBP_LOG, batch);
		
	}
	
	/*
	 * returns number of users related to the given hrbpId 
	 * Note that the *temporary* table is being checked: hr_conn_sap_src_tmp 
	 */
	private int getDomianCount(String hrbpId){
		
		return simpleJdbcTemplate.queryForInt(SQL_GET_DOMAIN_COUNT, hrbpId);
		
	}
	
	
	public List<HRBPUser> checkForEmptyDomains(List<HRBPUser> newHRBPUsers) {
		
		List<HRBPUser> hrbpWithEmptyDomain = new ArrayList<HRBPUser>();
		
		if (newHRBPUsers == null || newHRBPUsers.isEmpty()){
			return hrbpWithEmptyDomain;
		}
		
		for(HRBPUser user : newHRBPUsers){
			if( getDomianCount(user.getUserId()) == 0 ){
				hrbpWithEmptyDomain.add(user);
			}
		}		
		return hrbpWithEmptyDomain;
	}
	
	
	/**
	 * Return hrbp users with empty domain from the hr_conn_hrbp_log table
	 * that *are present* in hr_conn_hrbp_tmp  (do not process removed or non-exisiting users)
 	 * 
	 */
	public List<HRBPUser> getHrbpWithEmptyDomian() {
		
		return simpleJdbcTemplate.query(SQL_GET_HRBP_WITH_EMPTY_DOMAIN_FROM_LOG, new RowMapper<HRBPUser>(){
			@Override
			public HRBPUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				HRBPUser hrbpUser = new HRBPUser();
				hrbpUser.setUserId( rs.getString("user_id") );
				hrbpUser.setFirstName( rs.getString("first_name") );
				hrbpUser.setLastName( rs.getString("last_name") );				
				return hrbpUser;
			}				
		});
	}
	
	/**
	 *	Get list of all user from the log table
	 *	that are present in the 	
	 *   
	 * @return
	 */
	public List<HRBPUser> getHrbpWithFilledDomain() {
		
		// get actual list of hrbp users with empty domain
		List<HRBPUser> emptyDomainUsers = getHrbpWithEmptyDomian();
		List<HRBPUser> filledDomainUsers = new ArrayList<HRBPUser>();
		
		//check if hrbp's domain has been filled in the meantime
		for(HRBPUser user : emptyDomainUsers){
			if( getDomianCount(user.getUserId()) > 0 ){
				filledDomainUsers.add(user);
			}
		}		
		return filledDomainUsers;
		
	}
	
	/**
	 * Delete log info about hrbp users with empty domains 
	 * for who this information is no longer valid 
	 * - becasue the domain has been filled
	 */
	public void deleteLogInfoForFilledDomain(List<HRBPUser> usersWithFilledDomain) {
		
		if (usersWithFilledDomain == null || usersWithFilledDomain.isEmpty()){
			return;
		}
				
		for (HRBPUser user : usersWithFilledDomain){
			simpleJdbcTemplate.update(SQL_DELETE_EMPTY_TO_FILLED_USERS, user.getUserId());	
		}
		
	}

}
