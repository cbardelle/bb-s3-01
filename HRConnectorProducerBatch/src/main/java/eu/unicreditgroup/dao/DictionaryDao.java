//GIT01
package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import eu.unicreditgroup.beans.AssgnProfile;
import eu.unicreditgroup.beans.ContractType;
import eu.unicreditgroup.beans.EmpStatus;
import eu.unicreditgroup.beans.ExternalUserJob;
import eu.unicreditgroup.beans.FullDepCode;
import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.Locale;
import eu.unicreditgroup.beans.NewTrackedAttr;
import eu.unicreditgroup.beans.Province;
import eu.unicreditgroup.beans.ResultUserAttr;
import eu.unicreditgroup.beans.TimeZone;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.beans.UserAttr;
import eu.unicreditgroup.beans.UserAttrExcp;
import eu.unicreditgroup.beans.UserCustValue;
import eu.unicreditgroup.config.ConfigParams;

public class DictionaryDao extends SimpleJdbcDaoSupport {

	
	private String sqlAllCustomValueForResultColumn =" SELECT USER_ID , LISTAGG(CUST_VAL, ';') WITHIN GROUP (ORDER BY CUST_VAL) AS CUST_VAL, RES_ATTR_IDX FROM HR_CONN_CUST_RES_VALUE GROUP BY USER_ID, RES_ATTR_IDX "; 
	
	
	private static final String USR_DICT_TABLE = "hr_conn_usr_attr_dict";
	
	
	public List<TimeZone> getAllTimeZones() {

		String sql = "select source_country_id, plateau_timezone_id, plateau_timezone_desc from hr_conn_time_zones";

		return getJdbcTemplate().query(sql, new RowMapper<TimeZone>() {

			@Override
			public TimeZone mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TimeZone timeZone = new TimeZone();
				timeZone.setCountryID(rs.getString("source_country_id"));
				timeZone.setPlateauTimeZoneID(rs
						.getString("plateau_timezone_id"));
				timeZone.setPlateauTimeZoneDesc(rs
						.getString("plateau_timezone_desc"));
				return timeZone;
			}
		});
	}

	public List<Locale> getAllLocales() {
		String sql = " SELECT source_nation_id , plateau_locale_id   "
				+ "   FROM hr_conn_locale           		        ";

		return getJdbcTemplate().query(sql, new RowMapper<Locale>() {

			@Override
			public Locale mapRow(ResultSet rs, int rowNum) throws SQLException {
				Locale locale = new Locale();
				locale.setSourceNationId(rs.getString("source_nation_id"));
				locale.setPlateauLocaleId(rs.getString("plateau_locale_id"));
				return locale;
			}
		});
	}

	public List<EmpStatus> getAllEmpStatus() {

		String sql = " SELECT source_user_status , plateau_not_active   "
				+ "   FROM hr_conn_emp_st           		           ";

		return getJdbcTemplate().query(sql, new RowMapper<EmpStatus>() {

			@Override
			public EmpStatus mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				EmpStatus empStatus = new EmpStatus();
				empStatus.setSourceUserStatus(rs
						.getString("source_user_status"));
				empStatus.setPlateauNotActive(rs
						.getString("plateau_not_active"));
				return empStatus;
			}
		});
	}
	
	
	public List<User> getCustomEmails() {

		String sql = " SELECT user_id , user_email from HR_CONN_CUST_EMAIL ";

		return getJdbcTemplate().query(sql, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				User user = new User();
				user.setUserId(rs.getString("user_id"));
				user.setEmail(rs.getString("user_email"));
				return user;
			}
		});
	}


	public List<ContractType> getAllContractTypes() {
		String sql = " SELECT source_emp_type_id, plateau_emp_st_desc,  "
				+ "  plateau_emp_st_id, is_global_type, plateau_full_time               "
				+ "  FROM hr_conn_contract_types     		           ";

		return getJdbcTemplate().query(sql, new RowMapper<ContractType>() {
			@Override
			public ContractType mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ContractType contractType = new ContractType();

				contractType.setSourceEmpTypeId(rs
						.getString("source_emp_type_id"));
				contractType.setGlobalType(rs.getString("is_global_type"));
				contractType.setPlateauEmpStDesc(rs
						.getString("plateau_emp_st_desc"));
				contractType.setPlateauEmpStId(rs
						.getString("plateau_emp_st_id"));				
				contractType.setPlateauFullTime(rs
						.getString("plateau_full_time"));

				// empStatus.setPlateauNotActive(rs.getString("plateau_not_active"));
				return contractType;
			}
		});
	}
	
	/**
	 * 
	 * Get ids of German managers
	 * WARN: the file is loaded by the load source table step 
	 * 
	 * 
	 * @return
	 */
	public List<String> getGermanManagers() {

		String sql = " SELECT USER_ID FROM HR_CONN_SAP_EXCLD ";

		return getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("USER_ID");
			}
		});
	}
	
	
	
	public List<String> getAllCurrency() {

		String sql = " SELECT plateau_curr_id FROM hr_conn_curr ";

		return getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("plateau_curr_id");
			}
		});
	}

	/**
	 * Get indexes of attributes selected for tracking
	 * 
	 * @return
	 */
	public List<Integer> getAttrToTrackIdxs() {

		
		String sql = " select distinct res_attr_idx from " + USR_DICT_TABLE;

		return getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("res_attr_idx");
			}
		});
	}

	public List<Province> getAllProvinces() {

		String sql = " SELECT province_code, region_code FROM hr_conn_it_province ";

		return getJdbcTemplate().query(sql, new RowMapper<Province>() {
			@Override
			public Province mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Province province = new Province();

				province.setProvinceCode(rs.getString("province_code"));
				province.setRegionCode(rs.getString("region_code"));
				return province;
			}
		});
	}

	public List<HRBPUser> getAllHrbpUsers() {

		String sql = " SELECT * FROM HR_CONN_HRBP";

		return getJdbcTemplate().query(sql, new RowMapper<HRBPUser>() {
			@Override
			public HRBPUser mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				HRBPUser hrbpUser = new HRBPUser();
				hrbpUser.setUserId(rs.getString("user_id"));
				hrbpUser.setFirstName(rs.getString("first_name"));
				hrbpUser.setLastName(rs.getString("last_name"));
				return hrbpUser;
			}
		});
	}

	private List<UserAttr> getAttrToTrackList(Integer attrIdx) {
		String sql = " select src_attr_code, res_attr_val  from " + USR_DICT_TABLE + " where res_attr_idx = " + String.valueOf(attrIdx);

		return getJdbcTemplate().query(sql, new RowMapper<UserAttr>() {
			@Override
			public UserAttr mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				UserAttr userAttr = new UserAttr();
				userAttr.setAttrCode(rs.getString("src_attr_code"));
				userAttr.setAttrVal(rs.getString("res_attr_val"));
				return userAttr;
			}
		});
	}

	/**
	 * Return hash map with values for given attribute
	 * 
	 * @param attr
	 * @return
	 */
	public HashMap<String, String> getAttrToTrackMap(Integer attrIdx) {

		List<UserAttr> attrsValues = getAttrToTrackList(attrIdx);

		HashMap<String, String> mappedValues = new HashMap<String, String>();

		for (UserAttr ua : attrsValues) {
			mappedValues.put(ua.getAttrCode(), ua.getAttrVal());
		}
		return mappedValues;
		
	}

	public void updateUserAttrDict(List<NewTrackedAttr> newAttrs, List<TrackedAttr> changedAttrs) {
		//logger.debug("NEW VALUES : " + newAttrs );
		
		//logger.debug("CHANGED VALUES : " + changedAttrs );
		
		/*
		for (NewTrackedAttr ta:  newAttrs){			
			logger.debug("$$$\t " + ta.getAttrIdx() + " >" + ta.getAttrCode() + "<" );			
		}*/
				
		//add new attributes
		List<Object[]> batch = new ArrayList<Object[]>();
		for (NewTrackedAttr attr : newAttrs) {
			Object[] values = new Object[] { 
					attr.getAttrCode(), /* src_attr_code */
					attr.getAttrIdx(), 	/* res_attr_idx */				
					attr.getMappedValue(), /* res_attr_val */
					toSqlTimeStamp(new java.util.Date()) /* added date */
						
					};
			batch.add(values);
		}
		//try{
		getSimpleJdbcTemplate()
				.batchUpdate(
						"insert into " + USR_DICT_TABLE + " (src_attr_code, res_attr_idx, res_attr_val, added_date ) values(?, ?, ?, ?) ",
						batch);
		//} catch (Exception e){
		//	System.out.println(e);;
		//}
			
		
		 
		//update changed attributes		
		String updateSql = "update " + USR_DICT_TABLE  + " set res_attr_val = ?, last_updated_date = sysdate " +
		" where src_attr_code = ? and res_attr_idx = ?";
				
		for (TrackedAttr attr : changedAttrs){			
			//logger.debug("UPDATING USER ATTRIBUTE :" + attr.toString() );
			
			this.getSimpleJdbcTemplate().update(updateSql, new Object[] {attr.getMappedValue(),attr.getAttrCode(),attr.getAttrIdx()});
			
			
		}
	}
	
	private java.sql.Timestamp toSqlTimeStamp(java.util.Date javaDate){
		
		return new java.sql.Timestamp(javaDate.getTime());
		
	}

	
	public List<UserAttrExcp> getTrackedAttrExceptions() {
		String sql = " SELECT src_attr_code, res_attr_idx "
			+ "   FROM HR_CONN_USR_ATTR_EXCP ";
		return getJdbcTemplate().query(sql, new RowMapper<UserAttrExcp>() {	
			@Override
			public UserAttrExcp mapRow(ResultSet rs, int rowNum) throws SQLException {
				UserAttrExcp ex = new UserAttrExcp();
				ex.setResAttrIdx(rs.getInt("res_attr_idx"));
				ex.setSrcAttrCode(rs.getString("src_attr_code"));			
				return ex;
			}
		});	
	}
	
	

	public List<ResultUserAttr> getAllResultAttr() {

		String sql = " SELECT res_attr_idx, res_attr_name, res_attr_desc, plt_attr_id FROM hr_conn_res_attr_dict ";

		return getJdbcTemplate().query(sql, new RowMapper<ResultUserAttr>() {
			@Override
			public ResultUserAttr mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ResultUserAttr attr = new ResultUserAttr();
				
				attr.setResAttrIdx(rs.getInt("res_attr_idx"));
				attr.setResAttrDesc(rs.getString("res_attr_desc"));
				attr.setResAttrName(rs.getString("res_attr_name"));
				attr.setPlateauAttrId(rs.getString("plt_attr_id"));
				
				return attr;
			}
		});
	}

	public List<AssgnProfile> getAssgnProfiles() {
		String sql = " SELECT ap_id , res_attr_idx, ap_val FROM hr_conn_plt_asgn_prf ";

		return getJdbcTemplate().query(sql, new RowMapper<AssgnProfile>() {
			@Override
			public AssgnProfile mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				AssgnProfile prf = new AssgnProfile();
				
				prf.setApAttrValue(rs.getString("ap_val"));
				prf.setApId(rs.getString("ap_id"));
				prf.setResAttrIdx(rs.getInt("res_attr_idx"));
				
				return prf;
			}
		});
	}
	
	public List<String> getItalianOrganizations() {
		String sql = " select upper(org_id) as org_id from hr_conn_org_lst where upper(country_id) = 'IT'  ";
		return getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("org_id");
			}
		});
	}

	
	public List<User> getPlateauRoleExecptions() {
		String sql = " SELECT USER_ID, PLT_ROLE FROM HR_CONN_PLT_RL_EXCP";

		return getJdbcTemplate().query(sql, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				User user = new User();
				
				user.setUserId(rs.getString("USER_ID"));
				user.setPlateauRole(rs.getString("PLT_ROLE"));
				
				return user;
			}
		});
	}
	
	public List<User> getLineManagerExecptions() {
		String sql = " SELECT USER_ID, LINE_MANAGER_ID FROM HR_CONN_LM_EXCP";

		return getJdbcTemplate().query(sql, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				User user = new User();
				
				user.setUserId(rs.getString("USER_ID"));
				user.setLineManager(rs.getString("LINE_MANAGER_ID"));
				
				return user;
			}
		});
	}

	public List<ExternalUserJob> getExternalUserJobs() {
		
		String sql = " SELECT JOB_CODE, JOB_DESC FROM HR_CONN_EXT_USER_JOBS";
		return getJdbcTemplate().query(sql, new RowMapper<ExternalUserJob>() {
			@Override
			public ExternalUserJob mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ExternalUserJob job = new ExternalUserJob();
				
				job.setJobCode(rs.getString("JOB_CODE"));
				job.setJobDescr(rs.getString("JOB_DESC"));
				
				return job;
			}
		});
	}

	public List<FullDepCode> getFullDepartmentsCodes() {

		String sql = " SELECT DISTINCT OU_SAPCODE, FULL_DEP_CODE FROM HR_ORG_STRUCTURE ";
		return getJdbcTemplate().query(sql, new RowMapper<FullDepCode>() {
			@Override
			public FullDepCode mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				FullDepCode fdp = new FullDepCode();
								
				fdp.setFullDepCode(rs.getString("FULL_DEP_CODE"));
				fdp.setOfficeSapCode(rs.getString("OU_SAPCODE"));
								
				return fdp;
			}
		});	
	}

	public List<UserCustValue> getAllcustomValueForResultColumn() {
		
		// 2014-04-23
		// New feature: it's possible to store several values for the same users and custom column in the table HR_CONN_CUST_RES_VALUE
		// If more then one value for a given user and column id is present, values will be concatenated with semicolon
		
		
		String sql = sqlAllCustomValueForResultColumn;
		
		//String sql = "  ";
		
		return getJdbcTemplate().query(sql, new RowMapper<UserCustValue>() {
			@Override
			public UserCustValue mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				UserCustValue ucv = new UserCustValue();
								
				ucv.setCustVal(rs.getString("CUST_VAL"));
				ucv.setResAttrIdx( Integer.valueOf( rs.getInt("RES_ATTR_IDX") ) );
				ucv.setUserId(rs.getString("USER_ID"));
								
				return ucv;
			}
		});	

	}

	
	
	
	// this is for JUnit (HSQL cannot parse listagg Oracle function)
	public void setSqlAllCustomValueForResultColumn(
			String sqlAllCustomValueForResultColumn) {
		this.sqlAllCustomValueForResultColumn = sqlAllCustomValueForResultColumn;
	}

	public long checkDistinctFullDepartmentCodes() {
		
		String sql = " SELECT COUNT(DISTINCT(FULL_DEP_CODE)) FROM HR_ORG_STRUCTURE ";	
		return getJdbcTemplate().queryForLong(sql);
		
	}

	
	
	
}
