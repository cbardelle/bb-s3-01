//GIT01
package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.ExpatUser;
import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.HRBPUserLog;


public class ExpatUserDao  {
	
	private static final Logger logger = Logger.getLogger(ExpatUserDao.class);
	
	public final String SQL_DATE_FORMAT = "YYYY-MM-DD HH24:MI:SS";
	
	private SimpleJdbcTemplate simpleJdbcTemplate;
		
	private DataSource dataSource;
	
	private final String EXPAT_TYPE_OUTBOUND = "O";
	
	private final String SQL_NEW_EXPATS = "select user_id, first_name, last_name, date_of_birth from hr_conn_sap_src where user_id not in (select expat_user_id from hr_conn_expat_users) and upper(expat_type) = 'O'";
	
	private final String SQL_INSERT_NEW_EXPATS = "insert into hr_conn_expat_users (expat_user_id, first_name, last_name, date_of_birth, expat_type, create_date, remove_date) " +
			"values (:expatUserId, :firstName, :lastName, :dateOfBirth, :expatType, :createDate, :removeDate)";
	
	private final String SQL_REAPPEARED_EXPATS = 
						"select e.expat_user_id, e.first_name, e.last_name, e.date_of_birth, e.remove_date, e.expat_type " +
						" from hr_conn_expat_users e " +
						" where e.remove_date is not null " +
						" and upper(e.expat_type) = 'O' " +
						" and e.expat_user_id in (select s.user_id from hr_conn_sap_src s where  upper(s.expat_type) = 'O')";
	
	private final String SQL_FIND_RELATED_INBOUND_EXPATS =
			" SELECT  U.USER_ID " + 
			" FROM " +
			" (SELECT " + // inbounds from the source table
			" USER_ID, FIRST_NAME, LAST_NAME, DATE_OF_BIRTH, EXPAT_TYPE " + 
			" FROM HR_CONN_SAP_SRC " +
			" WHERE UPPER(EXPAT_TYPE) = 'I' " +
			" UNION ALL " +
			" SELECT " + // inbounds from the history table not present in source table
			" USER_ID,FIRST_NAME, LAST_NAME, DATE_OF_BIRTH, EXPAT_TYPE " + 
			" FROM HR_CONN_SAP_SRC_HST " + 
			" WHERE USER_ID NOT IN " +
			" (SELECT USER_ID FROM HR_CONN_SAP_SRC WHERE UPPER(EXPAT_TYPE) = 'I') " +
			" AND UPPER(EXPAT_TYPE) = 'I' " +
			" ) U " +
			" WHERE " +
			" UPPER(U.USER_ID) <> UPPER(?) " +
			" AND UPPER(U.FIRST_NAME) = UPPER(?) " + 
			" AND UPPER(U.LAST_NAME) = UPPER(?) " + 
			" AND U.DATE_OF_BIRTH = ? ";
	
	private final String SQL_FIND_REMOVED_OUTBOUND_EXPATS = 
			" select expat_user_id, first_name, last_name, date_of_birth " +
			" from hr_conn_expat_users " +
			" where expat_user_id not in (select user_id from hr_conn_sap_src where upper(expat_type) = 'O')";
	
	private final String  SQL_UPDATE_RM_DATE_EXPAT = "update hr_conn_expat_users set (remove_date) = sysdate where expat_user_id = '%s'";
	
	
	public List<ExpatUser> getNewOutboundExpatUsers() {
				
		return simpleJdbcTemplate.query(SQL_NEW_EXPATS, new RowMapper<ExpatUser>(){
			@Override
			public ExpatUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				ExpatUser expatUser = new ExpatUser();				
				expatUser.setExpatUserId(rs.getString("user_id"));
				expatUser.setFirstName(rs.getString("first_name"));
				expatUser.setLastName(rs.getString("last_name"));
				expatUser.setDateOfBirth(rs.getString("date_of_birth"));
				
				expatUser.setExpatType("O");
				//expatUser.setCreateDate(new Date());
				//expatUser.setRemoveDate(null);				
				return expatUser;
			}				
		});
	}

	public void addNewOutboundExpatUsers(List<ExpatUser> newExpats) {
		if ( newExpats == null || newExpats.isEmpty() ){
			return;
		}
				
		Date now = new Date();		
		for (ExpatUser ex : newExpats){
			ex.setExpatType(EXPAT_TYPE_OUTBOUND);
			ex.setCreateDate(now);
		}
		
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(newExpats.toArray());
		
        simpleJdbcTemplate.batchUpdate(SQL_INSERT_NEW_EXPATS, batch);
		
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }

	// get expat users that were present in the source file and were deleted at some point
	// but have reappeared  
	public List<ExpatUser> getReappearedOutboundExpatUsers() {
		return simpleJdbcTemplate.query(SQL_REAPPEARED_EXPATS, new RowMapper<ExpatUser>(){
			@Override
			public ExpatUser mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				ExpatUser expatUser = new ExpatUser();				
				expatUser.setExpatUserId(rs.getString("expat_user_id"));
				//expatUser.setRemoveDate(rs.getString("remove_date"));
				expatUser.setFirstName(rs.getString("first_name"));
				expatUser.setLastName(rs.getString("last_name"));
				expatUser.setDateOfBirth(rs.getString("date_of_birth"));

				expatUser.setExpatType(rs.getString("expat_type"));
				
				return expatUser;
			}				
		});
	}

	// reset column remove_date 
	public void resetRemoveDateForExpat(List<ExpatUser> reappearedOutboundExpats) {
		for (ExpatUser user : reappearedOutboundExpats){
			logger.debug("Reseting remove date for expat: " + user.getExpatUserId());
			
			logger.debug("Reseting remove_date for expat user: " + user.getExpatUserId());
			simpleJdbcTemplate.update("update hr_conn_expat_users set remove_date = null where expat_user_id = ?", new Object[] {user.getExpatUserId()});
			
		}		
	}

	
	/**
	 * Finds using  the view VIEW_ALL_SAP_USERS users 
	 * having the same the same name in upper case, surname in upper case, birth date and ExpatType = I
	 * 
	 * @param outboundExpatsUsers
	 */
	public void findAndSetRelatedInboundsExpats(
			List<ExpatUser> outboundExpatsUsers) {
		
				
		for (ExpatUser e : outboundExpatsUsers) {
			//String surname = (String) this.jdbcTemplate.queryForObject("select surname from t_actor where id = ?", new Object[]{new Long(1212)}, String.class);
			
			logger.debug("Looking for expat: " + e.getExpatUserId() + " " + e.getFirstName() + " " + e.getLastName() + " " + e.getDateOfBirth());
			
			List<String> users =  simpleJdbcTemplate.query(SQL_FIND_RELATED_INBOUND_EXPATS, new RowMapper<String>(){
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {									
					return rs.getString("user_id");
				}				
			}, new Object[]{e.getExpatUserId(),e.getFirstName(), e.getLastName(), e.getDateOfBirth()} );			
			
			if(users != null && users.size() == 1){
				e.setRelatedInboudExpatUserId(users.get(0));
				logger.debug("Found related inbound expat : " + users.get(0) + " for " + e.getExpatUserId());
			}
		}		
	}

	/**
	 * Find outbound expats removed from current version of the source file  
	 * 
	 * @return
	 */
	public List<ExpatUser> findRemovedOutboundExpats() {
		
		return simpleJdbcTemplate.query(SQL_FIND_REMOVED_OUTBOUND_EXPATS, new RowMapper<ExpatUser>(){
			@Override
			public ExpatUser mapRow(ResultSet rs, int rowNum) throws SQLException {				
				ExpatUser expatUser = new ExpatUser();				
				expatUser.setExpatUserId(rs.getString("expat_user_id"));
				expatUser.setFirstName(rs.getString("first_name"));
				expatUser.setLastName(rs.getString("last_name"));
				expatUser.setDateOfBirth(rs.getString("date_of_birth"));				
				expatUser.setExpatType("O");
				return expatUser;
			}				
		});
		
	}
	

	public void setRemoveDate(List<ExpatUser> removedOutboundExpats, Date date) {
		
		if (removedOutboundExpats == null || removedOutboundExpats.isEmpty()){
			return;
		}
				
		for (ExpatUser user : removedOutboundExpats){			
			String sql = String.format(SQL_UPDATE_RM_DATE_EXPAT, user.getExpatUserId());			
			simpleJdbcTemplate.update(sql);			
			logger.debug("Remove date set for outboud expat: " + user.getExpatUserId());						
		}		
	}
	
}
