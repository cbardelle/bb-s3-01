//GIT01
package eu.unicreditgroup.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mailer.ReportDataBean;


/**
 * 
 * This class is responsible for loading users data to a table 
 * specified in excldTableName.
 * It's may be used for loading of users which should't be inserted 
 * into the table HR_CONN_SAP_SRC (like German users for the time being etc.)
 * 
 * @author UV00074
 *
 */
public class ExcludedDataTableDao {
	
	private static final Logger logger = Logger
			.getLogger(ExcludedDataTableDao.class);
	
		
	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;

	private static int[] columnTypes;
	
	private SourceFileConf sourceFileConf;
	
	private String SQL_INSERT_INTO_EXCLD_TABLE = "";
	
	
	private ReportDataBean reportDataBean;
	
	private List<String> inserted = new ArrayList<String>();
	
	private List<Object[]> rowBuffer = new ArrayList<Object[]>();
	
	private String excldTableName;  // HR_CONN_SAP_EXCLD
	
	
	
		
	private String getInsertIntoTempTableQuery(){
		
		if (StringUtils.isNotEmpty(SQL_INSERT_INTO_EXCLD_TABLE)){
			return SQL_INSERT_INTO_EXCLD_TABLE;
		}
		
		String sqlTemplate = " INSERT INTO " + excldTableName + "  VALUES ( %s )";
		
		int sourceColumnsCount = sourceFileConf.getSourceColumns().size();
		
		columnTypes = new int[sourceColumnsCount];
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < sourceColumnsCount ; i++){
			columnTypes[i] = java.sql.Types.VARCHAR;
			// build the inserting sql (?, ?, ?, ... )
			if (i > 0){
				sb.append(",");
			}
			sb.append("?");			
		}		
		SQL_INSERT_INTO_EXCLD_TABLE = String.format(sqlTemplate, sb.toString() );
		return SQL_INSERT_INTO_EXCLD_TABLE; 		
	}
	
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        //this.insertResult = new SimpleJdbcInsert(dataSource).withTableName("");
        this.dataSource = dataSource;
    }	
	
	
   /**
     * 
     * @param results
     */		
	@Transactional(value="ltp")
    private void addRowsToExcldTable(List<Object[]> rows){    	
    	try {    		
    		
    		reportDataBean.addExcludedTableRowsInsertedOrInvalid(rows.size());
    		   		
    		cleanExcldTable();
		
    		simpleJdbcTemplate.batchUpdate( getInsertIntoTempTableQuery(), rows, columnTypes);
    		
    		
    		
    		logger.debug("Users inserted into " + excldTableName + " : " + inserted);
    		
    	} catch (Exception e){
    		logger.error("Failed to upadte the table " + excldTableName, e);
    	}
    }
	

	/**
	 * Add to a buffer
	 * 
	 * @param sourceRow
	 */
	synchronized public void bufferedAddRow(SourceRow sourceRow) {
		
		try {		
			if ( inserted.contains(sourceRow.getUserId()) == false ){
				logger.debug("Adding user: " +sourceRow.getUserId() + ", source file name: " + sourceRow.getSourceFileName() );
				rowBuffer.add(sourceRow.getData());
				inserted.add(sourceRow.getUserId());	
				
			} else {
				reportDataBean.addExcludedTableRowsInsertedOrInvalid(1);
				
				
				logger.error("User already insetred : " + sourceRow.getUserId() + ", source file name: " + sourceRow.getSourceFileName() );			
			}	
		} catch (Exception ex) {
			logger.error("Exception when adding row to a buffer: ", ex);			
		}
	}

	/**
	 * Write the buffer
	 */
	synchronized public void flushData() {
		
		if (rowBuffer.size() == 0 ){
			logger.debug("Row buffer is empty. Nothing to do.");
			return;
		}		
		
		addRowsToExcldTable(rowBuffer);
		
		rowBuffer.clear();
	}

    private void cleanExcldTable(){
    	simpleJdbcTemplate.update("TRUNCATE TABLE " + excldTableName);
    }

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}


	public String getExcldTableName() {
		return excldTableName;
	}


	public void setExcldTableName(String excldTableName) {
		this.excldTableName = excldTableName;
	}

	
	
	
	

}	
