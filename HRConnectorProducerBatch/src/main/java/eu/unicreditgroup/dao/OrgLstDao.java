package eu.unicreditgroup.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

public class OrgLstDao {

	private static final Logger logger = Logger
			.getLogger(OrgLstDao.class);

	private final String SQL_INSERT_ORG_LST_TMP = " insert into hr_conn_org_lst_tmp values ( ?, ?, ? )";

	private String SQL_TRUNCATE_TEMP_TABLE = "delete from hr_conn_org_lst_tmp";
	
	private String SQL_TRUNCATE_MAIN_TABLE = "delete from hr_conn_org_lst";
	
	private String SQL_INSERT_NEW_SOURCE_ROWS = 
			"insert into hr_conn_org_lst select * from hr_conn_org_lst_tmp";

	private final int columnTypes[] = { java.sql.Types.VARCHAR,
			java.sql.Types.VARCHAR, java.sql.Types.VARCHAR };

	private SimpleJdbcTemplate simpleJdbcTemplate;

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
		// this.insertResult = new
		// SimpleJdbcInsert(dataSource).withTableName("");
		this.dataSource = dataSource;
	}

	private void cleanTempTable() {
		simpleJdbcTemplate.update(SQL_TRUNCATE_TEMP_TABLE);
	}
	
	private void cleanMainTable(){
	   	simpleJdbcTemplate.update(SQL_TRUNCATE_MAIN_TABLE);
	}

	private int insertSourceRows(){
        int updatedRows = simpleJdbcTemplate.update(SQL_INSERT_NEW_SOURCE_ROWS);
        return updatedRows;
    }
	
	private boolean addRowsToTempSourceTable(List<Object[]> buff)
			throws Exception {
		simpleJdbcTemplate.batchUpdate(SQL_INSERT_ORG_LST_TMP, buff,
				columnTypes);
		return true;
	}

	/**
	 *
	 * 
	 * @param buffer
	 * @return
	 * @throws Exception 
	 */
	@Transactional(value = "ltp")	
	public int addRows(List<Object[]> rows) throws Exception {
		
		cleanTempTable();

		addRowsToTempSourceTable(rows);
		
    	cleanMainTable();
    	
    	return insertSourceRows();
		
	}

}
