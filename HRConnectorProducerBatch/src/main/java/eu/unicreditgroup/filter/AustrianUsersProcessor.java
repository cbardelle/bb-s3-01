package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.dao.ExcludedDataTableDao;
import eu.unicreditgroup.mailer.ReportDataBean;

public class AustrianUsersProcessor implements AdditionalProcessor {

	ExcludedDataTableDao excludedDataTableDao;
	
	private final static String AT_USERS_SRC_FILE_1 = "source_at_ubis_data.csv";
	
	private ReportDataBean reportDataBean;
	
	
	public static boolean isAustrianUsersSourceFile(String fileName){
		if (AT_USERS_SRC_FILE_1.equals(fileName) ){			
			return true;
		}				
		return false;
		
		
	}
	
	
	/**
	 * Do additional processing of Austrian users
	 * For the time being Austrian users are not imported into Plateau
	 * Instead they will be put into table HR_CONN_SAP_EXCLD_AT
	 * 
	 * 
	 */
	@Override
	public void process(SourceRow sourceRow) {		
		// all german managers will be excluded for the time being
		if ( AT_USERS_SRC_FILE_1.equals(sourceRow.getSourceFileName()) ){
			
			excludedDataTableDao.bufferedAddRow(sourceRow);			
			
			reportDataBean.incExcludedAustrianUsersCount();
		}
	}

	@Override
	public void finishProcessing() {		
		// check is German manages
		excludedDataTableDao.flushData();
	}

	public ExcludedDataTableDao getExcludedDataTableDao() {
		return excludedDataTableDao;
	}

	public void setExcludedDataTableDao(ExcludedDataTableDao excludedDataTableDao) {
		this.excludedDataTableDao = excludedDataTableDao;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
	
}
