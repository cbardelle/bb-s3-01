package eu.unicreditgroup.filter;

import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.SourceFileConsumer;
import eu.unicreditgroup.beans.SourceRow;

public class ExcludeGermanManagersFilter implements Filter {

	private final static String GER_MANAGERS_SRC_FILE_1 ="source_noit_ubis_mgrde.csv";
	
	private final static String GER_MANAGERS_SRC_FILE_2 ="source_noit_ubis_mgrde_wr.csv";
	
	@Override
	public boolean accept(SourceRow sourceRow) {
		
		if (GER_MANAGERS_SRC_FILE_1.equals(sourceRow.getSourceFileName()) || GER_MANAGERS_SRC_FILE_2.equals(sourceRow.getSourceFileName())){			
			return false;
		}				
		return true;
	}
	
	public static boolean isGermanManagerSourceFile1(String fileName){
		if (GER_MANAGERS_SRC_FILE_1.equals(fileName) || GER_MANAGERS_SRC_FILE_2.equals(fileName)){			
			return true;
		}				
		return false;
		
		
	}
	

}
