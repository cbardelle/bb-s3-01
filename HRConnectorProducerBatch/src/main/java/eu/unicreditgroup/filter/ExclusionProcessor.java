package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;

public interface ExclusionProcessor {
	
	/**
	 * Do additional processing for excluded data
	 * @param sourceRow
	 * @return
	 */
	public void process(SourceRow sourceRow);
	
	
	/**
	 * Flush data buffers etc
	 * 
	 */
	public void finishProcessing();
		
	

}
