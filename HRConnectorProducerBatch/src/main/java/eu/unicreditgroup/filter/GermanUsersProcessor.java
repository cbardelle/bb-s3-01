package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.dao.ExcludedDataTableDao;
import eu.unicreditgroup.mailer.ReportDataBean;

/**
 * This is additional processing for German users
 * For the time being German users are not imported into Plateau
 * Instead they will be put into table HR_CONN_SAP_EXCLD_DE table  
 *  
 * 
 * @author UV00074
 *
 */
public class GermanUsersProcessor implements AdditionalProcessor {

	ExcludedDataTableDao excludedDataTableDao;
	
	private final static String DE_USERS_SRC_FILE_1 = "source_de_ubis_data.csv";
	
	private ReportDataBean reportDataBean;
	
	
	public static boolean isGermanUsersSourceFile(String fileName){
		if (DE_USERS_SRC_FILE_1.equals(fileName) ){			
			return true;
		}				
		return false;
		
		
	}
	
	
	@Override
	public void process(SourceRow sourceRow) {		
		// all german managers will be excluded for the time being
		if ( DE_USERS_SRC_FILE_1.equals(sourceRow.getSourceFileName()) ){
			
			excludedDataTableDao.bufferedAddRow(sourceRow);			
			
			reportDataBean.incExcludedGermanUsersCount();
		}
	}

	@Override
	public void finishProcessing() {		
		// check is German manages
		excludedDataTableDao.flushData();
	}

	public ExcludedDataTableDao getExcludedDataTableDao() {
		return excludedDataTableDao;
	}

	public void setExcludedDataTableDao(ExcludedDataTableDao excludedDataTableDao) {
		this.excludedDataTableDao = excludedDataTableDao;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
	
}
