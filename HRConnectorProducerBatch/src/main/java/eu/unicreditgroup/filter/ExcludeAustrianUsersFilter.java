package eu.unicreditgroup.filter;

import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.SourceFileConsumer;
import eu.unicreditgroup.beans.SourceRow;

public class ExcludeAustrianUsersFilter implements Filter {

	
	
	
	
	private final static String AT_USERS_SRC_FILE_1 = "source_at_ubis_data.csv";
	
	@Override
	public boolean accept(SourceRow sourceRow) {
		
		if ( AT_USERS_SRC_FILE_1.equals(sourceRow.getSourceFileName()) ){			
			return false;
		}				
		return true;
	}
	
	
	public static boolean isAustianUsersSourceFile(String fileName){
		if ( AT_USERS_SRC_FILE_1.equals(fileName) ){			
			return true;
		}				
		return false;
				
	}
	
	
	

}
