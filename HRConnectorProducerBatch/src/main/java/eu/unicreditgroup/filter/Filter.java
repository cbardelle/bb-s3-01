package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;

public interface Filter {
	
	public boolean accept(SourceRow sourceRow);

}
