package eu.unicreditgroup.filter;

import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.SourceFileConsumer;
import eu.unicreditgroup.beans.SourceRow;

public class ExcludeGermanUsersFilter implements Filter {

	
	
	
	
	private final static String DE_USERS_SRC_FILE_1 = "source_de_ubis_data.csv";
	
	@Override
	public boolean accept(SourceRow sourceRow) {
		
		if ( DE_USERS_SRC_FILE_1.equals(sourceRow.getSourceFileName()) ){			
			return false;
		}				
		return true;
	}
	
	
	
	

}
