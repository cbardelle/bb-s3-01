package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.dao.ExcludedDataTableDao;

public class GermanManagersProcessor implements AdditionalProcessor {

	ExcludedDataTableDao excludedDataTableDao;
	
	private final String GER_MANAGERS_SRC_FILE_1 ="source_noit_ubis_mgrde.csv";
	
	private final String GER_MANAGERS_SRC_FILE_2 ="source_noit_ubis_mgrde_wr.csv";
				
	
	/**
	 * Do additional processing of UBIS German managers
	 * For the time being German managers won't be processed 
	 * and imported into Plateau
	 * Instead they will be put into table HR_CONN_SAP_EXCLD
	 * 
	 * 
	 */
	@Override
	public void process(SourceRow sourceRow) {		
		// all german managers will be excluded for the time being
		if (GER_MANAGERS_SRC_FILE_1.equals(sourceRow.getSourceFileName()) || GER_MANAGERS_SRC_FILE_2.equals(sourceRow.getSourceFileName())){
			
			excludedDataTableDao.bufferedAddRow(sourceRow);
		}
	}

	@Override
	public void finishProcessing() {		
		// check is German manages
		excludedDataTableDao.flushData();
	}

	public ExcludedDataTableDao getExcludedDataTableDao() {
		return excludedDataTableDao;
	}

	public void setExcludedDataTableDao(ExcludedDataTableDao excludedDataTableDao) {
		this.excludedDataTableDao = excludedDataTableDao;
	}
	
	
}
