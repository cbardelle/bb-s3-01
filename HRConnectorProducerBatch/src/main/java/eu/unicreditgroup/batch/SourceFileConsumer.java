package eu.unicreditgroup.batch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.dao.SourceTableDao;
import eu.unicreditgroup.filter.AdditionalProcessor;
import eu.unicreditgroup.filter.ExclusionProcessor;
import eu.unicreditgroup.filter.Filter;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.exception.InvalidRowException;
import eu.unicreditgroup.utils.SourceRowQueueUtil;

public class SourceFileConsumer implements Runnable {

	private static final Logger logger = Logger.getLogger(SourceFileConsumer.class);
	
	private ConfigParams configParams;
	
	private int bufferSize=100;
	
	private int consumerNumber;
	
	private ReportDataBean reportDataBean;
		
	private BlockingQueue<SourceRow> sourceRowsQueue;
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private SourceTableDao sourceTableDao;
		
	//private SourceColumn srcUserId;
	
	private SourceFileConf sourceFileConf;
	 
	private List<Filter> userFilters;
	
	//private ExclusionProcessor exclusionProcessor; 
	private AdditionalProcessor additionalProcessor;
	
	
	private AdditionalProcessor additionalProcessor2;
	
	
	private AdditionalProcessor additionalProcessor3;

	
	private boolean checkNotFiltered(SourceRow sourceRow)
	{
		
		if (userFilters == null){
			return true;
		}
		
		for(Filter f : userFilters){
			if (!f.accept(sourceRow) ){
				logger.debug("User row: " + sourceRow.getIdOrUserName() + " is exculded by the filter: " + f.getClass().getName() );
				
				//reportDataBean.incExcludedOutboundExpatCount();
				
				return false;				
			}
		}
		return true;
	}
	
	@Override
	public void run() {		
		SourceRow sourceRow = null;
		
		boolean shutdownRowFound = false;
		
		ArrayList<Object[]> sourceRowsBuffer = new ArrayList<Object[]>(bufferSize);
		
		try {
			while (true) {
				Thread.yield();
				
				sourceRowsBuffer.clear();
				for (int i = 0 ; i < bufferSize ; i++){
					sourceRow = sourceRowsQueue.take();
					if (SourceRowQueueUtil.isShutdownRow(sourceRow)){
						shutdownRowFound = true;												
						sourceRowsQueue.put(sourceRow);
						
						break;	
					}					
					
					// the source row may be filtered out by a filter					
					if (checkNotFiltered(sourceRow) == true){
						sourceRowsBuffer.add(sourceRow.getData());
						
					}
					
					
					// do some additional processing for some users like for example German manages
					if (additionalProcessor != null)
					{
						additionalProcessor.process(sourceRow);							
					}


					if (additionalProcessor2 != null)
					{
						additionalProcessor2.process(sourceRow);							
					}
					
					
					if (additionalProcessor3 != null)
					{
						additionalProcessor3.process(sourceRow);							
					}


					
					/*else {
						// do some processing for excluded row if any processors are present
						if (exclusionProcessor != null)
						{
							exclusionProcessor.process(sourceRow);							
						}		
					}*/
					
					
					
				}
				
				// write the buffer to the source table
				if (sourceRowsBuffer.isEmpty() == false) {
					
					
					try {
						sourceTableDao.addRowsToTempSourceTable(sourceRowsBuffer);
												
						reportDataBean.addTempSourceTableRowsInserted(sourceRowsBuffer.size());
					} catch (Exception e){
						
						logger.error("Failed to insert rows due to: ", e);						
						// handle duplicated USerId 
						if (e instanceof org.springframework.dao.DuplicateKeyException ){
														
							logger.debug("Duplicated keys found will try to insert row by row");
							// try to insert row by row
							for(Object[] rowData : sourceRowsBuffer ){
								
								try {
									sourceTableDao.addRowToTempSourceTable(rowData);									
									reportDataBean.incTempSourceTableRowsInserted();									
								} catch (Exception ex){
									exceptionsQueue.add(e);
									
									String userId = (String)rowData[ sourceFileConf.getSrcUserID().getSrcColIndex()];
									
									if (ex instanceof  org.springframework.dao.DuplicateKeyException){
										//process duplicated keys
										
										String errMsg = String.format("Failed to insert user %s due to user ID duplication: ", userId);
										logger.error(errMsg, e);
										reportDataBean.addDuplicatedUserId(userId);
										InvalidRowException ire = new InvalidRowException(errMsg);
										reportDataBean.addExceptionWithLimit(ire);
										
									} else {
										logger.error(String.format("Failed to insert user %s due to: ", userId), e);										
									}
								}
							}							
						}
					}
				}				
				
				if (shutdownRowFound == true){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					
					
					// flush the data in buffers
					if (additionalProcessor != null) { additionalProcessor.finishProcessing(); }
					if (additionalProcessor2 != null) { additionalProcessor2.finishProcessing(); }
					if (additionalProcessor3 != null) { additionalProcessor3.finishProcessing(); }
					
					return;					
				}
			}
		} catch (Exception ex) {
			logger.debug(String.format("[Cons %d] Exception during processing soure row '%s'.", consumerNumber, sourceRow), ex);			
			exceptionsQueue.add(ex);
		} 
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public int getConsumerNumber() {
		return consumerNumber;
	}

	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}

	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public SourceTableDao getSourceTableDao() {
		return sourceTableDao;
	}
	
	public void setSourceTableDao(SourceTableDao sourceTableDao) {
		this.sourceTableDao = sourceTableDao;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	public List<Filter> getUserFilters() {
		return userFilters;
	}

	public void setUserFilters(List<Filter> userFilters) {
		this.userFilters = userFilters;
	}

	public AdditionalProcessor getAdditionalProcessor() {
		return additionalProcessor;
	}

	public void setAdditionalProcessor(AdditionalProcessor additionalProcessor) {
		this.additionalProcessor = additionalProcessor;
	}

	public AdditionalProcessor getAdditionalProcessor2() {
		return additionalProcessor2;
	}

	public void setAdditionalProcessor2(AdditionalProcessor additionalProcessor2) {
		this.additionalProcessor2 = additionalProcessor2;
	}

	public AdditionalProcessor getAdditionalProcessor3() {
		return additionalProcessor3;
	}

	public void setAdditionalProcessor3(AdditionalProcessor additionalProcessor3) {
		this.additionalProcessor3 = additionalProcessor3;
	}


	
	
	
	/*
	public ExclusionProcessor getExclusionProcessor() {
		return exclusionProcessor;
	}

	public void setExclusionProcessor(ExclusionProcessor exclusionProcessor) {
		this.exclusionProcessor = exclusionProcessor;
	}*/
	
	
}
