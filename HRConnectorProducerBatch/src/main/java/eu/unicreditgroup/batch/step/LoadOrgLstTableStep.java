package eu.unicreditgroup.batch.step;

import java.util.Date;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.processor.OrgLstFileProcessor;

public class LoadOrgLstTableStep implements BatchStep {

	private OrgLstFileProcessor orgLstFileProcessor;
	
	private ConfigParams configParams; 
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	
	Logger logger = Logger.getLogger(LoadOrgLstTableStep.class);	
	
	@Override
	public void execute() {

		// Step load organizations list source file into table		
		if (configParams.isStepLoadOrgLstTableEnabled()){
			
			try {				

				reportDataBean.setStepLoadOrgLstTableStartDate(new Date());
				
				orgLstFileProcessor.processOrgLstFile();
				
				reportDataBean.setStepLoadOrgLstTableEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step load organization dictionary table", e);
				exceptionsQueue.add(e);
			}
		} else {
			logger.info("Step load organization dictionary table is disabled. Nothing to do here.");
		}

	}

	public OrgLstFileProcessor getOrgLstFileProcessor() {
		return orgLstFileProcessor;
	}

	public void setOrgLstFileProcessor(OrgLstFileProcessor orgLstFileProcessor) {
		this.orgLstFileProcessor = orgLstFileProcessor;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}


	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	

}
