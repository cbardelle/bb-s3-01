package eu.unicreditgroup.batch;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.TableMapper;
import eu.unicreditgroup.utils.SourceRowQueueUtil;

public class SourceTableConsumer implements Runnable {

	private static final Logger logger = Logger.getLogger(SourceTableConsumer.class);
	
	private ConfigParams configParams;
			
	private int consumerNumber;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<SourceRow> sourceRowsQueue;
		
	private BlockingQueue<MappedRow> mappedRowsQueue;
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private TableMapper tableMapper;
	
	//private SourceColumn userIdCol;
	
	private SourceFileConf sourceFileConf;
		 
	
	@Override
	public void run() {
		
		SourceRow sourceRow = new SourceRow();
		try {	
			
			while(true){
				Thread.yield();
				
				sourceRow = sourceRowsQueue.take();
				
				if (SourceRowQueueUtil.isShutdownRow(sourceRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					sourceRowsQueue.put(sourceRow);
					return;
				}
				
				MappedRow mappedRow = tableMapper.processRow(sourceRow, sourceRow.getValue(sourceFileConf.getSrcUserID().getSrcColIndex()));
				
				
				if ( mappedRow.getValid() == true )	{					
					reportDataBean.incMappedRowsCount();					
					
					mappedRowsQueue.put(mappedRow);
					
					
					
				} else {					
					reportDataBean.incSourceFileInvalidRowsCount();					
					String errMsg = "Mapped row for userID: " + sourceRow.getValue(sourceFileConf.getSrcUserID().getSrcColIndex()) + " is invalid and won't be written to the result file.";					
					exceptionsQueue.add( new Exception(errMsg) );					
					logger.error( errMsg );					
				}
							
			}
			
		} catch (Exception ex) {
			logger.debug(String.format("[Cons %d] Exception during processing soure row for userID '%s'.", consumerNumber, sourceRow.getValue(sourceFileConf.getSrcUserID().getSrcColIndex())), ex);			
			exceptionsQueue.add(ex);
		} 
		
		/*
		this is problematic in multithreading... 
		
		finally {
				try{
					
					// put end of the queue marker
					mappedRowsQueue.put(MappedRowsQueueUtil.generateShutdownRow());
					
				} catch(Exception e){
					logger.error("Exception occured : " + e);
				}			
		}*/
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}


	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}


	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	
	
	public BlockingQueue<MappedRow> getMappedRowsQueue() {
		return mappedRowsQueue;
	}


	public void setMappedRowsQueue(BlockingQueue<MappedRow> mappedRowsQueue) {
		this.mappedRowsQueue = mappedRowsQueue;
	}


	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}

	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}


	public TableMapper getTableMapper() {
		return tableMapper;
	}


	public void setTableMapper(TableMapper tableMapper) {
		this.tableMapper = tableMapper;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	
}
