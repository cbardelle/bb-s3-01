package eu.unicreditgroup.batch;



public interface SourceDataProducer extends Runnable {

	public abstract String getSourceFilesAsString();

}