package eu.unicreditgroup.batch;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.BufferedResultTableWriter;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.TableMapper;
import eu.unicreditgroup.utils.MappedRowsQueueUtil;
import eu.unicreditgroup.utils.ResultFileWriter;

public class MappedRowConsumer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(MappedRowConsumer.class);
	
	private ConfigParams configParams;
			
	private int consumerNumber;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<MappedRow> mappedRowsQueue;
	
	private BlockingQueue<Exception> exceptionsQueue;
		 
	private TableMapper tableMapper;
		
	private final String CSV_COLUMN_DELIMITER = ";";	
	
	private final String CSV_LINE_END = "\r\n";
			
	private final String PLATEAU_COLUMN_DELIMITER = "|";	
	
	private final String PLATEAU_LINE_END = "!##!\r\n";
	
	private ResultFileWriter plateauFileWriter = null;
		
	private BufferedResultTableWriter bufferedResultTableWriter;
	
	public void closeResultFile(){
		if ( plateauFileWriter!= null ){
			try {				
				plateauFileWriter.close();
			} catch (Exception e) {
				logger.error("Failed to close result file", e);
			}
		}		
	}
	
	@Override
	public void run() {
		
		ResultFileWriter csvFileWriter = null;
		
		try {
			
			plateauFileWriter = new ResultFileWriter(PLATEAU_COLUMN_DELIMITER, PLATEAU_LINE_END, configParams.getPlateauFullFilePath());
			
			
			if (configParams.isGenearteCsvFile() == true){
				csvFileWriter = new ResultFileWriter(CSV_COLUMN_DELIMITER, CSV_LINE_END, configParams.getCsvFullFilePath());
			}
						
			plateauFileWriter.writeHeader(tableMapper.getResultColumns());
			
			bufferedResultTableWriter.clearTempTable();
			
			if (configParams.isGenearteCsvFile())
			{
				csvFileWriter.writeHeader(tableMapper.getResultColumns());
			}
			
						
			while(true){
				Thread.yield();
				
				MappedRow mappedRow = mappedRowsQueue.take();
				
				if (MappedRowsQueueUtil.isShutdownRow(mappedRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					mappedRowsQueue.put(mappedRow);
					
					bufferedResultTableWriter.flush();					
					bufferedResultTableWriter.replaceMainTable();
					return;					
				}
								
				//reportDataBean.incResultFileRowsCount();			

				plateauFileWriter.writeResultRow( mappedRow.getData(),tableMapper.getResultColumns() );
				
				bufferedResultTableWriter.writeResultRow( mappedRow );
				
				if (configParams.isGenearteCsvFile())
				{
					csvFileWriter.writeResultRow( mappedRow.getData(),tableMapper.getResultColumns() );
				}
								
			}
			
		} catch (Exception ex) {
			logger.error(String.format("Exception during processing mapped rows" ), ex);			
			exceptionsQueue.add(ex);
			
			// when exception occurs during writing the result the file is not sent
			reportDataBean.setFailedFastException(ex);
		} finally {
			
			try {
				plateauFileWriter.close();
				if (configParams.isGenearteCsvFile())
				{
					csvFileWriter.close();
				}
				bufferedResultTableWriter.flush();
				
			} catch (Exception e) {				
				logger.error(e);
				logger.debug("Exception during closing result file writer", e);				
			}			
		}
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}


	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}


	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}


	public BlockingQueue<MappedRow> getMappedRowsQueue() {
		return mappedRowsQueue;
	}

	public void setMappedRowsQueue(BlockingQueue<MappedRow> mappedRowsQueue) {
		this.mappedRowsQueue = mappedRowsQueue;
	}

	public TableMapper getTableMapper() {
		return tableMapper;
	}

	public void setTableMapper(TableMapper tableMapper) {
		this.tableMapper = tableMapper;
	}

	public BufferedResultTableWriter getBufferedResultTableWriter() {
		return bufferedResultTableWriter;
	}

	public void setBufferedResultTableWriter(
			BufferedResultTableWriter bufferedResultTableWriter) {
		this.bufferedResultTableWriter = bufferedResultTableWriter;
	}

	
	
	

}
