package eu.unicreditgroup.batch;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.dao.ManualSourceTableDao;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.SAPDataProcessor;
import eu.unicreditgroup.utils.SourceRowQueueUtil;

/**
 * This class is responsible for reading data rows from the table HR_CONN_MNL_SRC.
 * This table enables manual insertion of users for the purpose of Audit Academy.
 *   
 * Each row is parsed and put into the sourceUsersQueue. When all records are read 
 * a false user is put to the queue to stop processing.
 * 
 * 
 * @author UV00074
 * 
 */
public class SourceManualDataProducer implements SourceDataProducer {

	private static final Logger logger = Logger
			.getLogger(SourceManualDataProducer.class);

	

	private SAPDataProcessor sapDataProcessor;

	private BlockingQueue<SourceRow> sourceRowsQueue;

	private BlockingQueue<Exception> exceptionsQueue;

	private ReportDataBean reportDataBean;

	private final String sourceName = "HR_CONN_MNL_SRC";
	
	private ManualSourceTableDao manualSourceTableDao;
	
	//private HRBPUserProcessor hrbpUserProcessor;

	//private final char CSV_COL_DELIMITER = ';';

	//private SourceColumn srcResume;
	
	private SourceFileConf sourceFileConf;	

	// private String additionalSourceFileName;

	//private boolean primarySourceFileProducer = false;
	
	//private FlatFileReader flatFileReader;
	
	
	//private SrcFileNameProvider  srcFileNameProvider;

	
	@Override
	public void run() {		
		
		List<SourceRow> sourceRows = manualSourceTableDao.getUsersToSend();

		try {
			String[] sourceDataRow = null;
			for ( SourceRow sourceRow : sourceRows ) {
				
				sourceDataRow = sourceRow.getData();
				reportDataBean.incSourceFileRowsCount();
					
				SourceRow processedSourceRow = sapDataProcessor.processSourceData(sourceDataRow);
				
				// set source file name for the purpose of 
				// filtering out data from selected files 
				processedSourceRow.setSourceFileName(sourceName);

				if (processedSourceRow.getValid() == false) {
					
					logger.error("Source row nr : "
						+ reportDataBean.getSourceFileRowsCount()
						+ " is invalid, employee id : "
						+ processedSourceRow.getValue(sourceFileConf.getSrcResume().getSrcColIndex()));						
					reportDataBean.incSourceFileInvalidRowsCount();							
					
				} else {
					sourceRowsQueue.put(processedSourceRow);					
				}
				
			}			
					
			// if the file has been successfully red add to the list					
			reportDataBean.addProcessedSourceFiles(sourceName);

			
		} catch (Exception e) {

		
			logger.error("Exception when reading source file", e);
		

			exceptionsQueue.add(e);

			// exception at this point will be fatal exception and result file
			// will be not generated

			// if (isPrimarySourceFile() &&
			// StringUtils.equalsIgnoreCase(srcFileName,
			// configParams.getStrPrimarySourceFileName())){

			// }

		} finally {
			try {
				
				/*
				if (reader != null) {
					reader.close();
				}*/
				// put end of the queue marker
				sourceRowsQueue.put(SourceRowQueueUtil.generateShutdownRow());

			} catch (Exception e) {
				// logger.error("Failed to close file : " +
				// configParams.getStrSourceLocalFilePath());
				logger.error("Failed to put end of the data marker : " + sourceName);

			}
		}
	}

	
	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}

	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}



	public SAPDataProcessor getSapDataProcessor() {
		return sapDataProcessor;
	}

	public void setSapDataProcessor(SAPDataProcessor sapDataProcessor) {
		this.sapDataProcessor = sapDataProcessor;
	}


	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}


	public ManualSourceTableDao getManualSourceTableDao() {
		return manualSourceTableDao;
	}


	public void setManualSourceTableDao(ManualSourceTableDao manualSourceTableDao) {
		this.manualSourceTableDao = manualSourceTableDao;
	}


	@Override
	public String getSourceFilesAsString() {
		return sourceName;
	}

	
	
	
}
