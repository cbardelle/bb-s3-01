package eu.unicreditgroup.batch;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.HRBPUserDao;
import eu.unicreditgroup.dao.SourceTableReader;
import eu.unicreditgroup.filter.Filter;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.HRBPUserProcessor;
import eu.unicreditgroup.processor.ExternalUserProcessor;
import eu.unicreditgroup.utils.SourceRowQueueUtil;

/**
 * This class is responsible for reading rows from the source table.
 * Each row is put into source rows queue, which acts like a read buffer.
 * When all rows from source table are processed termination object is put into table.
 *   
 * 
 * @author UV00074
 *
 */
public class SourceTableProducer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(SourceTableProducer.class);
	
	private ConfigParams configParams;
			
	private BlockingQueue<SourceRow> sourceRowsQueue;
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	private SourceTableReader sourceTableReader;
		
	private int consumerNumber;
	
	private List<Filter> userFilters;
	
	private ExternalUserProcessor externalUserProcessor; 
	
	private boolean checkNotFiltered(SourceRow sourceRow)
	{				
		if ( userFilters != null){
			for(Filter f : userFilters){
				if (!f.accept(sourceRow) ){		
					logger.debug("User row: " + sourceRow.getIdOrUserName() + " is exculded by the filter: " + f.getClass().getName() );
					reportDataBean.incExcludedOutboundExpatCount();
					return false;				
				}
			}
		}
		return true;		
	}
	
	
	@Override
	public void run() {

		try {			
			
			while( true ){
				
				Thread.yield();				
				
				//if (sourceTableReader.isEmpty() == true){
					
				if ( SourceRowQueueUtil.isShutdownRow( sourceRowsQueue.peek()) ){
					//sourceTableReader.setEmpty(true);
					logger.debug(String.format("[Source table procuder %d] will stop its work ", consumerNumber));
					 
					return;
				}
				
				List<SourceRow> buffer = sourceTableReader.getNextBunch();			
				
				if (buffer == null){
					
					// all rows from the source table have been read 
					// check for some additional rows to send like for example external users to deactivate					
					externalUserProcessor.putExternalsUsersToDeactivate(sourceRowsQueue);
					
					return;
				}
			
				for (SourceRow srcRow : buffer){
					
					// rows which do not meet filter criteria won't be processed and put to the result file					
					if (checkNotFiltered(srcRow) == true){					
						sourceRowsQueue.put(srcRow);
					}										
				}
			}
			
		} catch (Exception e) {
			
			logger.error("Exception in source tabale producer.", e);
			exceptionsQueue.add(e);
			
		} finally {
			
			try{				
				// put end of the queue marker
				sourceRowsQueue.put(SourceRowQueueUtil.generateShutdownRow());				
			} catch(Exception e){
				logger.error("Exception occured : " + e);
			}			
		}
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}

	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}
	
	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public SourceTableReader getSourceTableReader() {
		return sourceTableReader;
	}

	public void setSourceTableReader(SourceTableReader sourceTableReader) {
		this.sourceTableReader = sourceTableReader;
	}

	public void setConsumerNumber(int i) {
		consumerNumber = i;
		
	}

	public List<Filter> getUserFilters() {
		return userFilters;
	}

	public void setUserFilters(List<Filter> userFilters) {
		this.userFilters = userFilters;
	}

	public ExternalUserProcessor getExternalUserProcessor() {
		return externalUserProcessor;
	}

	public void setExternalUserProcessor(ExternalUserProcessor externalUserProcessor) {
		this.externalUserProcessor = externalUserProcessor;
	}
	

}
