package eu.unicreditgroup.beans;

public class FullDepCode {
	
	private String officeSapCode;
	
	private String fullDepCode;

	public String getOfficeSapCode() {
		return officeSapCode;
	}

	public void setOfficeSapCode(String officeSapCode) {
		this.officeSapCode = officeSapCode;
	}

	public String getFullDepCode() {
		return fullDepCode;
	}

	public void setFullDepCode(String fullDepCode) {
		this.fullDepCode = fullDepCode;
	}
	
	
	
	

}
