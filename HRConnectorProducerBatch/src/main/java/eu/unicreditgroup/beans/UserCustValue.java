package eu.unicreditgroup.beans;

public class UserCustValue {
	
	
	/**
	 * User Id
	 * 
	 */
	private String userId;
	
	
	/**
	 * Result column custom value
	 * 
	 */
	private String custVal;
	
	
	/**
	 * Index of the result column
	 */
	private Integer resAttrIdx;


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getCustVal() {
		return custVal;
	}


	public void setCustVal(String custVal) {
		this.custVal = custVal;
	}


	public Integer getResAttrIdx() {
		return resAttrIdx;
	}


	public void setResAttrIdx(Integer resAttrIdx) {
		this.resAttrIdx = resAttrIdx;
	}
	
	

	

}
