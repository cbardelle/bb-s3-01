package eu.unicreditgroup.beans;

public class ContractType {
	
	private String sourceEmpTypeId;
		
	private String plateauEmpStDesc;
	
	private String plateauEmpStId;
	
	private String globalType;
	
	private String plateauFullTime;

	@Override
	public String toString(){
		return sourceEmpTypeId + " " + plateauEmpStDesc + " " + plateauEmpStId + " " + globalType + " " + plateauFullTime;
	}
	
	
	public String getSourceEmpTypeId() {
		return sourceEmpTypeId;
	}

	public void setSourceEmpTypeId(String sourceEmpTypeId) {
		this.sourceEmpTypeId = sourceEmpTypeId;
	}

	public String getPlateauEmpStDesc() {
		return plateauEmpStDesc;
	}

	public void setPlateauEmpStDesc(String plateauEmpStDesc) {
		this.plateauEmpStDesc = plateauEmpStDesc;
	}

	public String getPlateauEmpStId() {
		return plateauEmpStId;
	}

	public void setPlateauEmpStId(String plateauEmpStId) {
		this.plateauEmpStId = plateauEmpStId;
	}

	public String getGlobalType() {
		return globalType;
	}

	public void setGlobalType(String globalType) {
		this.globalType = globalType;
	}


	public String getPlateauFullTime() {
		return plateauFullTime;
	}

	public void setPlateauFullTime(String plateauFullTime) {
		this.plateauFullTime = plateauFullTime;
	}
	

}
