package eu.unicreditgroup.beans;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.SourceFileConsumer;

public class SourceRow {
	
	String sourceFileName;

	String[] data;
	
	Boolean valid;
	
	//TODO should be revised when the order of source columns is changed
		
	//TODO REFACTOR this to user sourceTableConf.get[ColName]Value(sourceRow)
	
	final int USER_ID = 2;
	
	final int FIRST_NAME = 3;
	
	final int LAST_NAME = 4;
	
	
	//private final static String GER_MANAGERS_SRC_FILE_1 ="source_noit_ubis_mgrde.csv";
	
	//private final static String GER_MANAGERS_SRC_FILE_2 ="source_noit_ubis_mgrde_wr.csv";
	
	
	private static final Logger logger = Logger.getLogger(SourceFileConsumer.class);
	
	
	
	/*
	public boolean isGermanManager() {
		
		logger.debug(" Row: " + this.getIdOrUserName() + " file " + sourceFileName);
		
		if (GER_MANAGERS_SRC_FILE_1.equals(this.getSourceFileName()) || GER_MANAGERS_SRC_FILE_2.equals(this.getSourceFileName())){		
			
			logger.debug(" GERMAN MANAGER ROW: " + this.getIdOrUserName() + " file " + sourceFileName);
			
			return true;
		}				
		return false;
	}*/
	
	
	public String getUserId()
	{
		if (data == null){
			return "";
		}		
		return data[USER_ID];		
	}
	
	
	public String getIdOrUserName()
	{
		if (data == null){
			return "";
		}
		
		if (StringUtils.isNotEmpty(data[USER_ID])){
			return data[USER_ID];
		} else {
			// if there's no user Id return fname + last name
			return data[FIRST_NAME] + " " + data[LAST_NAME];	
		}	
		
	}
	
	public String getValue(int index){	
		
		if (data == null){
			return null;
		}
		if (index >= data.length){
			return null;
		}
		
		return data[index];
	}
	
	public void setValue(int index, String val){		
		data[index] = val;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

}
