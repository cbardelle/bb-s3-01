package eu.unicreditgroup.beans;

import java.util.Date;

/**
 * Stores information about changed attribute and related assignment profile
 * 
 * @author UV00074
 *
 */
public class PlateauAttrLogItem {
	
	/**
	 * index of the result attribute
	 */
	private int resAttrIdx;
	
	/**
	 * SAP code of the attribute
	 */
	private String attrSapCode;
	
	/**
	 * New value that comes from the new source file
	 */
	private String newVal;
	
	/**
	 * Old value of the attribute (from a dictionary table)
	 */
	private String oldVal;
	
	/**
	 * Id id of an assignment profile that could be affected by the change
	 */
	private String apId; 
	
	/**
	 * Date of the change
	 */
	private Date createdDate;
	
	public int getResAttrIdx() {
		return resAttrIdx;
	}
	
	public void setResAttrIdx(int resAttrIdx) {
		this.resAttrIdx = resAttrIdx;
	}
	
	public String getAttrSapCode() {
		return attrSapCode;
	}
	
	public void setAttrSapCode(String attrSapCode) {
		this.attrSapCode = attrSapCode;
	}
	
	public String getNewVal() {
		return newVal;
	}
	
	public void setNewVal(String newVal) {
		this.newVal = newVal;
	}
	
	public String getOldVal() {
		return oldVal;
	}
	
	public void setOldVal(String oldVal) {
		this.oldVal = oldVal;
	}
	
	public String getApId() {
		return apId;
	}
	
	public void setApId(String apId) {
		this.apId = apId;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
}
