package eu.unicreditgroup.beans;

public class HRBPUser {

	private String userId;
	
	private String firstName;
	
	private  String lastName;
	
	public HRBPUser()
	{
		
	}
	
	public HRBPUser( String _hrbpId, String _firstName, String _lastName ){
		this.userId = _hrbpId;
		this.firstName = _firstName;  
		this.lastName = _lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
}
