package eu.unicreditgroup.beans;

public class UserAttr {
	
	private String attrCode;
	
	private String attrVal;
	
	public String getAttrCode() {
		return attrCode;
	}

	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}

	public String getAttrVal() {
		return attrVal;
	}

	public void setAttrVal(String attrVal) {
		this.attrVal = attrVal;
	}
	
}
