package eu.unicreditgroup.beans;

public class EmpStatus {
	
	private String sourceUserStatus;
	
	private String  plateauNotActive;

	public String getSourceUserStatus() {
		return sourceUserStatus;
	}

	public void setSourceUserStatus(String sourceUserStatus) {
		this.sourceUserStatus = sourceUserStatus;
	}

	public String getPlateauNotActive() {
		return plateauNotActive;
	}

	public void setPlateauNotActive(String plateauNotActive) {
		this.plateauNotActive = plateauNotActive;
	}

}
