package eu.unicreditgroup.beans;

public class ExternalUserJob {
	
	private String jobCode;
	
	private String jobDescr;

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobDescr() {
		return jobDescr;
	}

	public void setJobDescr(String jobDescr) {
		this.jobDescr = jobDescr;
	}
	

}
