package eu.unicreditgroup.beans;

public class AssgnProfile {
	
	private Integer resAttrIdx;
	
	private String apAttrValue;
	
	private String apId;

	public Integer getResAttrIdx() {
		return resAttrIdx;
	}

	public void setResAttrIdx(Integer resAttrIdx) {
		this.resAttrIdx = resAttrIdx;
	}

	public String getApAttrValue() {
		return apAttrValue;
	}

	public void setApAttrValue(String apAttrValue) {
		this.apAttrValue = apAttrValue;
	}

	public String getApId() {
		return apId;
	}

	public void setApId(String apId) {
		this.apId = apId;
	}
	
}
