package eu.unicreditgroup.beans;

import java.util.Date;

public class ExpatUser {
	
	private String expatUserId;
	
	private String firstName;
	
	private String lastName;
	
	private String dateOfBirth;	
	
	private String expatType ;
	
	private Date createDate;
	
	private Date removeDate;
	
	private String relatedInboudExpatUserId;

	public String getExpatUserId() {
		return expatUserId;
	}

	public void setExpatUserId(String expatUserId) {
		this.expatUserId = expatUserId;
	}

	public String getExpatType() {
		return expatType;
	}

	public void setExpatType(String expatType) {
		this.expatType = expatType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getRemoveDate() {
		return removeDate;
	}

	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}

	public String getRelatedInboudExpatUserId() {
		
		return relatedInboudExpatUserId == null ? "" : relatedInboudExpatUserId;
	}

	public void setRelatedInboudExpatUserId(String relatedInboudExpatUserId) {
		this.relatedInboudExpatUserId = relatedInboudExpatUserId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	
	
}
