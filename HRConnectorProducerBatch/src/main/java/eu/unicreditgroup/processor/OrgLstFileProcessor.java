package eu.unicreditgroup.processor;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.OrgLstDao;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.CsvReader;
import eu.unicreditgroup.utils.SourceFileUtil;

public class OrgLstFileProcessor {

	private OrgLstDao orgLstDao;
	
	private ConfigParams configParams;

	private BlockingQueue<Exception> exceptionsQueue;

	private ReportDataBean reportDataBean;
	
	private SourceFileUtil  sourceFileUtil;

	private List<Object[]> buffer;
	
	private static final Logger logger = Logger
			.getLogger(OrgLstFileProcessor.class);

	private final int COLUMN_LEN[] = { 20, 80, 2 };

	int COLUMN_COUNT = 3;
	
	private final char CSV_COL_DELIMITER = ';';
	
	public void processOrgLstFile() {

		CsvReader reader = null;
		String orgLstFileName = configParams.getStrOrgLstFileName();
		String sourceLocalDir = configParams.getStrSourceLocalDir();

		try {
			
			
			if (configParams.isDownloadOrgLstFile()){			
				sourceFileUtil.backupSourceFile(configParams.getStrOrgLstFileName());			
				sourceFileUtil.downloadSapFile(configParams.getStrOrgLstFileName());
			}

			String orgLstFileLocalPath = sourceLocalDir + "\\" + orgLstFileName;

			File checkFile = new File(orgLstFileLocalPath);

			if (checkFile.exists() == false) {

				logger.error("The organization list file " + orgLstFileName
						+ " doesn't exist.");
				return;
			}

			if (checkFile.length() == 0) {

				logger.error("The organization list file " + orgLstFileName	+ " is of zero length.");

				try {
					checkFile.delete();
				} catch (Exception e) {
					logger.error("Failed to delete empty organization list file " + orgLstFileName, e);
				}
				return;

			}

			reader = new CsvReader(orgLstFileLocalPath);
			logger.info("PROCESSING SOURCE FILE: " + orgLstFileName);

			reader.setDelimiter(CSV_COL_DELIMITER);

			// read the source file
			while (reader.readRecord()) {
				String[] sourceDataRow = null;
				sourceDataRow = reader.getValues();

				// reportDataBean.incSourceFileRowsCount();
				processSourceRow(sourceDataRow);
			}
			
			// write the data to the dict table
			reportDataBean.setProcessedOrgCount(writeSourceData(buffer));

			// if the file has been successfully red and the dictionary table loaded add to the list
			reportDataBean.addProcessedSourceFiles(orgLstFileName);
			
			
			if (configParams.isMoveOrgLstFile()){
				sourceFileUtil.moveProcessedSapFile(configParams.getStrOrgLstFileName());
			}

		} catch (Exception e) {
			logger.error("Exception when reading organization list file", e);
			exceptionsQueue.add(e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
				logger.error("Failed to close file : " + orgLstFileName);
			}
		}
	}
	

	/**
	 * Validate the row and add to a buffer 
	 * 
	 * @param row to validate
	 * @throws UnsupportedEncodingException
	 */
	private void processSourceRow(String[] row) throws UnsupportedEncodingException {

		if (validateRow(row) == true) {
			if (buffer == null) {
				buffer = new ArrayList<Object[]>();
			}
			buffer.add(row);
		}
	}

	/**
	 * Check row number of columns and columns length
	 * 
	 * @param row
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private boolean validateRow(String[] row) throws UnsupportedEncodingException {

		// check number of columns
		if (row.length != COLUMN_COUNT) {
			logger.info("The org list source file is invalid becasue of the wrong number of columns: "
					+ row.length + ", row data: " + row);
			return false;
		}

		// validate columns length
		for(int i =1 ; i< COLUMN_COUNT; i++){
			if (validateUTF8Length(row[i],COLUMN_LEN[i]) == false){
				logger.debug("The column: " + (i + 1) + " is too long: " + row[i] + ", row data: " + row);
			
				return false;
			}			
		}

		return true;
	}

	/**
	 * Check lenght of a UTF-8 encoded string
	 * @param str
	 * @param length
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private boolean validateUTF8Length(String str, int length)
			throws UnsupportedEncodingException {
		if (str == null) {
			return true;
		}
		// check the leght of UTF-8 encoded string
		int colLength = str.getBytes("UTF-8").length;
		return colLength <= length ? true : false;
	}

	
	/**
	 * Write all valid row to the dictionary table
	 * 
	 * @return
	 * @throws Exception
	 */
	private int writeSourceData(List<Object[]> buffer) throws Exception {
		
		if (buffer == null || buffer.isEmpty() || buffer.size() < 1  ) {
			logger.info("Nothing to do. There are no organizations to insert to the table!");
			return 0;	
		}
		return orgLstDao.addRows(buffer);
	}

	public OrgLstDao getOrgLstDao() {
		return orgLstDao;
	}

	public void setOrgLstDao(OrgLstDao orgLstDao) {
		this.orgLstDao = orgLstDao;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public SourceFileUtil getSourceFileUtil() {
		return sourceFileUtil;
	}

	public void setSourceFileUtil(SourceFileUtil sourceFileUtil) {
		this.sourceFileUtil = sourceFileUtil;
	}
	

}
