package eu.unicreditgroup.processor;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.dao.SourceTableDao;
import eu.unicreditgroup.mailer.ReportDataBean;

public class ExternalUserProcessor {
	
	
	private ConfigParams configParams;
	
	private SourceTableDao sourceTableDao;
	
	private SourceFileConf sourceFileConf;
	
	private ReportDataBean reportDataBean;
	
	private static final Logger logger = Logger
			.getLogger(ExternalUserProcessor.class);

	/**
	 *  Put into the queue external users to deactivate 
	 * (users that were present in the past  but not present in current file with external users) 
	 * 
	 * This additional processing is necessary because, the information about deactivation is not sent 
	 * for externals users
	 *  
	 * 
	 * @param sourceRowsQueue
	 */
	public void putExternalsUsersToDeactivate( BlockingQueue<SourceRow> sourceRowsQueue ){
		
		if (checkDeactivationPrerequisites() ){
			
			List<SourceRow> removed = sourceTableDao.getRemovedExternalUsers();
			
			StringBuilder sb = new StringBuilder();
			
			if( removed != null ){				
				for(SourceRow row : removed){
					try {
						sourceRowsQueue.put(row);
						logger.debug("Added external user to deactivate : " + row.getUserId());
						
						if (sb.length() > 0){
							sb.append(", ");
						}
						sb.append(row.getUserId());
						
					} catch (InterruptedException e) {
						
						logger.error("Failed to inqueue external user : " + row.getData()[sourceFileConf.getSrcUserID().getSrcColIndex()]);
					}
				}
				
				reportDataBean.setExternalUsersToDeactivateList(sb.toString());
				
			}
		}
	}
	
	
	/**
	 * The reactivation is performed once a week
	 * The prerequisites are met in two cases:
	 * If the removal day is now and the time is between 14:00 - 00:00
	 * or now is day after the removal date but the time is between 00:00 - 04:00 
	 * 
	 * @return
	 */
	private boolean checkDeactivationPrerequisites() {
		
		if (configParams.isProcessExternalUsersFileEnabled() == false ){			
			// run only processing of external users is enabled!
			return false;			
		}
		
		
		int extUsersRemovalDay = configParams.getExternalUserRemovalDay();		
		
		if ( extUsersRemovalDay <= 0 ){
			logger.error("Removal day of external users not specified in the config file.");			
			
			return false;
		}
				
		DateTime now = new DateTime();
		
				
		// if the removal day is today
		if (now.getDayOfWeek() == extUsersRemovalDay){ 
			// check it's between 14:00 and 00:00
			if ( now.getHourOfDay() >= 14 ){
				//System.out.println("FIRE1!!!");
				return true;
			}
			//} else {
			//	System.out.println("Nth to do1");
			//}
		} else {  
			// if it's a following day after removal day			
			if( now.minusDays(1).getDayOfWeek() ==  extUsersRemovalDay){
				// check it's between 00:00 and 4:00AM
				if ( now.getHourOfDay() >= 0 && now.getHourOfDay() < 4  ){
					//System.out.println("FIRE2!!!");
					return true;
				}
				//} else {
				//	System.out.println("Nth to do2");
				//}				
			}
		}
		return false;
		
	}
	
	/**
	 * Return user_id list of reactivated external users:
	 * External users will be reactivated in case when
	 * is present in the HR_CONN_SAP_SRC_HST table with set REMOVED_DATE value
	 * and is present in the HR_CONN_SAP_SRC table (the current version of the file) 
	 * 
	 * @return
	 */
	public String getListOfReactivatedExternalUsers(){
		
		StringBuilder sb = new StringBuilder();
		
		try{
			List<String> reactivated = sourceTableDao.getReactivatedExternalUsers();
			int i = 0;
			for (String s : reactivated){
				if (i++ > 0 ){
					sb.append(", ");					
				}
				sb.append(s);		
			}
		} catch(Exception e){
			logger.error("Error when trying to obtain list of reactiaved external users!", e);			
		}
		return sb.toString() == null ? " " : sb.toString();		
	}
	

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public SourceTableDao getSourceTableDao() {
		return sourceTableDao;
	}

	public void setSourceTableDao(SourceTableDao sourceTableDao) {
		this.sourceTableDao = sourceTableDao;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
}
