package eu.unicreditgroup.config;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.utils.ParametersListParser;


public class ConfigParams implements InitializingBean{
	
	private static final Logger logger = Logger.getLogger(ConfigParams.class);
	
	private int lmsDataConsumerThreadsNumber;
		
	private String strLmsDataConsumerThreadsNumber;
	
	private String strStepCustColConnector;
	
	private String strStepGenerateFileForBizx;
	
	private String strStepGenerateFileForBizxEp;
		
	private String strStepApprRoleConn;
	
	private String strStepOrgConnector;
	
	
	private boolean stepCustColConnectorEnabled;
		
	private boolean stepOrgConnectorEnabled;
	
	private boolean stepApprRoleConnEnabled;	
	
	private boolean stepGenerateFileForBizxEnabled;
	
	private boolean stepGenerateFileForBizxEpEnabled;
	
		
	public String strRecurReportsDwnldDir;
	
	public String strRecurReportUniOrgList;
	
	
	private ParametersListParser parametersListParser;
	
	
	private int sourceDataConsumerThreadsNumberEP = 5;
	
	
	
	/**
	 * Result file (local) BizX connector
	 * 
	 * 
	 */
	private String resultFileFullPathBizxConn;
	
	
	/**
	 * Result file (local) export file for BizX EP
	 * 
	 */
	private String resultFileFullPathBizxEp; 
	
	/**
	 * Result file (local) Approval Role Connector
	 * 
	 */
	private String resultFileFullPathApprRoleConn;// = "c:\\tmp\\approle_data_unicredit.txt";
	
	
	/**
	 * Result file (local) Custom Column Connector
	 * 
	 */
	private String resultFileFullPathCustColConn;
	
	/**
	 * Result file (local) Organizations Connector
	 * 
	 */
	private String resultFileFullPathOrgConn;
	
	
	/**
	 * Remote file (ftp) BizX connector
	 * 
	 * 
	 */
	private String resultFtpPathPathBizxConn;
	
	
	/**
	 * Remote file (ftp) BizX EP
	 * 
	 * 
	 */
	private String resultFtpPathPathBizxEp;
	
	
	/**
	 * Remote file (ftp) Approval Role Connector
	 * 
	 */
	private String resultFtpPathApprRoleConn;
	
	
	/**
	 * Remote file (ftp) Custom Column Connector
	 * 
	 */
	private String resultFtpPathCustColConn;
	
	
	/**
	 * Remote file (ftp) Organizations Connector
	 * 
	 */
	private String resultFtpPathOrgConn;
	
	
	private final float APPR_ROLE_NEW_MAX_RATIO_PERC = 75.0f;
	
	private final float APPR_ROLE_RM_MAX_RATIO_PERC = 30.0f;


	public int getLmsDataConsumerThreadsNumber() {
		return lmsDataConsumerThreadsNumber;
	}

	public void setLmsDataConsumerThreadsNumber(int lmsDataConsumerThreadsNumber) {
		this.lmsDataConsumerThreadsNumber = lmsDataConsumerThreadsNumber;
	}

	public String getStrStepCustColConnector() {
		return strStepCustColConnector;
	}

	public void setStrStepCustColConnector(String strStepCustColConnector) {
		this.strStepCustColConnector = strStepCustColConnector;
	}

	public String getStrStepGenerateFileForBizx() {
		return strStepGenerateFileForBizx;
	}

	public void setStrStepGenerateFileForBizx(String strStepGenerateFileForBizx) {
		this.strStepGenerateFileForBizx = strStepGenerateFileForBizx;
	}

	public boolean isStepCustColConnectorEnabled() {
		return stepCustColConnectorEnabled;
	}

	public void setStepCustColConnectorEnabled(boolean stepCustColConnectorEnabled) {
		this.stepCustColConnectorEnabled = stepCustColConnectorEnabled;
	}

	public boolean isStepGenerateFileForBizxEnabled() {
		return stepGenerateFileForBizxEnabled;
	}

	public void setStepGenerateFileForBizxEnabled(
			boolean stepGenerateFileForBizxEnabled) {
		this.stepGenerateFileForBizxEnabled = stepGenerateFileForBizxEnabled;
	}
	
	public String getStrLmsDataConsumerThreadsNumber() {
		return strLmsDataConsumerThreadsNumber;
	}

	public void setStrLmsDataConsumerThreadsNumber(
			String strLmsDataConsumerThreadsNumber) {
		this.strLmsDataConsumerThreadsNumber = strLmsDataConsumerThreadsNumber;
	}
	
	
	@Override
	public void afterPropertiesSet() throws Exception {		
		
		ParametersListParser listParser = new ParametersListParser();
		
		lmsDataConsumerThreadsNumber = NumberUtils.toInt(strLmsDataConsumerThreadsNumber, -1);
		if (lmsDataConsumerThreadsNumber < 1){
			throw new IllegalStateException(String.format("Number of threads must be positive number but has value: '%s'.", strLmsDataConsumerThreadsNumber));
		}
		
		stepGenerateFileForBizxEnabled = processBooleanParameter(strStepGenerateFileForBizx);
		
		stepGenerateFileForBizxEpEnabled = processBooleanParameter(strStepGenerateFileForBizxEp);
		
		stepCustColConnectorEnabled = processBooleanParameter(strStepCustColConnector);
		
		stepOrgConnectorEnabled = processBooleanParameter(strStepOrgConnector);
		
		stepApprRoleConnEnabled = processBooleanParameter(strStepApprRoleConn);
		
	}

	public boolean processBooleanParameter(String parameter){
		if (StringUtils.equalsIgnoreCase(parameter, "y")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "1")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "tak")){
			parameter = "true";
		}
		
		if (StringUtils.equalsIgnoreCase(parameter, "si")){
			parameter = "true";
		}
		
		return BooleanUtils.toBoolean(parameter);
	}
	
	public ParametersListParser getParametersListParser() {
		return parametersListParser;
	}

	public void setParametersListParser(ParametersListParser parametersListParser) {
		this.parametersListParser = parametersListParser;
	}
	
	public String getStrStepApprRoleConn() {
		return strStepApprRoleConn;
	}

	public void setStrStepApprRoleConn(String strStepApprRoleConn) {
		this.strStepApprRoleConn = strStepApprRoleConn;
	}

	public boolean isStepApprRoleConnEnabled() {
		return stepApprRoleConnEnabled;
	}

	public void setStepApprRoleConnEnabled(boolean stepApprRoleConnEnabled) {
		this.stepApprRoleConnEnabled = stepApprRoleConnEnabled;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public float getApproRoleConnNewRowsMaxCnt() {
		
		return APPR_ROLE_NEW_MAX_RATIO_PERC;
	}

	public float getApproRoleConnRmRowsMaxCnt() {
		
		return APPR_ROLE_RM_MAX_RATIO_PERC;
	}

	
	public String getResultFileFullPathBizxConn() {
		return resultFileFullPathBizxConn;
	}

	public void setResultFileFullPathBizxConn(String resultFileFullPathBizxConn) {
		this.resultFileFullPathBizxConn = resultFileFullPathBizxConn;
	}

	public String getResultFileFullPathApprRoleConn() {
		return resultFileFullPathApprRoleConn;
	}

	public void setResultFileFullPathApprRoleConn(
			String resultFileFullPathApprRoleConn) {
		this.resultFileFullPathApprRoleConn = resultFileFullPathApprRoleConn;
	}

	public String getResultFileFullPathCustColConn() {
		return resultFileFullPathCustColConn;
	}

	public void setResultFileFullPathCustColConn(
			String resultFileFullPathCustColConn) {
		this.resultFileFullPathCustColConn = resultFileFullPathCustColConn;
	}

	public String getResultFtpPathPathBizxConn() {
		return resultFtpPathPathBizxConn;
	}

	public void setResultFtpPathPathBizxConn(String resultFtpPathPathBizxConn) {
		this.resultFtpPathPathBizxConn = resultFtpPathPathBizxConn;
	}
	
	

	public String getResultFtpPathPathBizxEp() {
		return resultFtpPathPathBizxEp;
	}

	public void setResultFtpPathPathBizxEp(String resultFtpPathPathBizxEp) {
		this.resultFtpPathPathBizxEp = resultFtpPathPathBizxEp;
	}

	public String getResultFtpPathApprRoleConn() {
		return resultFtpPathApprRoleConn;
	}

	public void setResultFtpPathApprRoleConn(String resultFtpPathApprRoleConn) {
		this.resultFtpPathApprRoleConn = resultFtpPathApprRoleConn;
	}

	public String getResultFtpPathCustColConn() {
		return resultFtpPathCustColConn;
	}

	public void setResultFtpPathCustColConn(String resultFtpPathCustColConn) {
		this.resultFtpPathCustColConn = resultFtpPathCustColConn;
	}

	public String getStrStepOrgConnector() {
		return strStepOrgConnector;
	}

	public void setStrStepOrgConnector(String strStepOrgConnector) {
		this.strStepOrgConnector = strStepOrgConnector;
	}

	public boolean isStepOrgConnectorEnabled() {
		return stepOrgConnectorEnabled;
	}

	public void setStepOrgConnectorEnabled(boolean stepOrgConnectorEnabled) {
		this.stepOrgConnectorEnabled = stepOrgConnectorEnabled;
	}

	public String getResultFileFullPathOrgConn() {
		return resultFileFullPathOrgConn;
	}

	public void setResultFileFullPathOrgConn(String resultFileFullPathOrgConn) {
		this.resultFileFullPathOrgConn = resultFileFullPathOrgConn;
	}

	public String getResultFtpPathOrgConn() {
		return resultFtpPathOrgConn;
	}

	public void setResultFtpPathOrgConn(String resultFtpPathOrgConn) {
		this.resultFtpPathOrgConn = resultFtpPathOrgConn;
	}

	public String getStrRecurReportsDwnldDir() {
		return strRecurReportsDwnldDir;
	}

	public void setStrRecurReportsDwnldDir(String strRecurReportsDwnldDir) {
		this.strRecurReportsDwnldDir = strRecurReportsDwnldDir;
	}

	public String getStrRecurReportUniOrgList() {
		return strRecurReportUniOrgList;
	}

	public void setStrRecurReportUniOrgList(String strRecurReportUniOrgList) {
		this.strRecurReportUniOrgList = strRecurReportUniOrgList;
	}

	public int getSourceDataConsumerThreadsNumberEP() {
		return sourceDataConsumerThreadsNumberEP;
	}

	public void setSourceDataConsumerThreadsNumberEP(
			int sourceDataConsumerThreadsNumberEP) {
		this.sourceDataConsumerThreadsNumberEP = sourceDataConsumerThreadsNumberEP;
	}

	public String getResultFileFullPathBizxEp() {
		return resultFileFullPathBizxEp;
	}

	public void setResultFileFullPathBizxEp(String resultFileFullPathBizxEp) {
		this.resultFileFullPathBizxEp = resultFileFullPathBizxEp;
	}

	public String getStrStepGenerateFileForBizxEp() {
		return strStepGenerateFileForBizxEp;
	}

	public void setStrStepGenerateFileForBizxEp(String strStepGenerateFileForBizxEp) {
		this.strStepGenerateFileForBizxEp = strStepGenerateFileForBizxEp;
	}

	public boolean isStepGenerateFileForBizxEpEnabled() {
		return stepGenerateFileForBizxEpEnabled;
	}

	public void setStepGenerateFileForBizxEpEnabled(
			boolean stepGenerateFileForBizxEpEnabled) {
		this.stepGenerateFileForBizxEpEnabled = stepGenerateFileForBizxEpEnabled;
	}

	

	
	
	
}
