package eu.unicreditgroup.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;

/**
 * This is a helper class for grouping the functionality related to all
 * operations on source files like: download, upload, backup etc
 * 
 * @author UV00074
 * 
 */

public class BackupUtil {

	Logger logger = Logger.getLogger(BackupUtil.class);
	


	public static void backupFile(String strSourceFilePath) throws Exception {

		byte[] buf = new byte[1024];

		File file = new File(strSourceFilePath);

		if (!file.exists()) {
			return;
		}

		FileInputStream in = new FileInputStream(file);

		String fileName = stripFileExtension(file.getName());

		String dir = file.getParent();

		String zipPattern = "'" + fileName + "_'dd-MM-yyyy__HH_mm_ss'.zip'";

		SimpleDateFormat sdf = new SimpleDateFormat(zipPattern);

		String zipFileName = sdf.format(new Date());

		// Create the ZIP file

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(dir
				+ "\\" + zipFileName));

		// Compress the files
		// Add ZIP entry to output stream.
		out.putNextEntry(new ZipEntry(file.getName()));

		// Transfer bytes from the file to the ZIP file
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}

		// Complete the entry
		out.closeEntry();
		in.close();

		// Complete the ZIP file
		out.close();

		// delete the origin
		file.delete();
	}

	private static String stripFileExtension(String fileName) {
		int dotInd = fileName.lastIndexOf('.');
		return (dotInd > 0) ? fileName.substring(0, dotInd) : fileName;
	}

	
}
