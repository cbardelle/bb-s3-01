package eu.unicreditgroup.mailer;

 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.springframework.util.StringUtils;

import eu.unicreditgroup.config.ConfigParams;

public class ReportDataBean {
	
		
	
	
	private Date batchStartDate;
	
	private Date batchEndDate;
	
	private Date stepGenerateFileForBizxStartDate;
	
	private Date stepGenerateFileForBizxEndDate;
	
	private Date stepGenerateFileForBizxEpStartDate;
	
	private Date stepGenerateFileForBizxEpEndDate;
		
	private Date stepCustColConnectorStartDate;
	
	private Date stepCustColConnectorEndDate;
		
	private Date stepApprRoleConnStartDate;
	
	private Date stepApprRoleConnEndDate;
	
	private Date stepOrgConnStartDate;
	
	private Date stepOrgConnEndDate;
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
	
	private BlockingQueue<Exception> bizxEpExceptionsQueue;
	
	private BlockingQueue<Exception> apprRoleExceptionsQueue;
		
	private BlockingQueue<Exception> custColExceptionsQueue;
	
	private BlockingQueue<Exception> orgExceptionsQueue;
	
	
	
	


	private Throwable failedFastException;
		
	private ConfigParams configParams;
	
	private static final int EXCEPTIONS_LIMIT = 100;
	
	private int lmsSourceRowTotal;
	
	private int bizxRowTotal;
	
	
	/**
	 * Total count of mapped rows per BizX EP
	 */
	private int bizxEpRowTotal;
	
	
	/**
	 * Non exisiting roles combinations
	 * 
	 */
	private List<Integer> bizxEpRoleExceptions = new ArrayList<Integer>(); 
	
	/**
	 * Roles exceptions for the BizxEP file  
	 * 
	 */
	private String strBizxEpRoleExceptions;
	
	
	private int appRoleConnNewRolesCount;
	
	private int appRoleConnRemovedCount;
	
	
	
	
	/**
	 * 
	 * The number of referenced values with different description
	 * for the same referenced value id
	 * 
	 * (Custom Column Connector)
	 * 
	 */
	private int custColumConnDiffDescr = 0;
	
	
	/**
	 * The total number of rows writtten by 
	 * the custom column connector
	 * 
	 */
	private int custColumConnTotalWrite = 0;

	/**
	 * The count of source users from SAP for Bizx EP 
	 */
	private int bizxEpSapUsersCount;

	/**
	 * The count of source users from SSC for Bizx EP 
	 */
	private int bizxEpSscUsersCount;
	
	
	
	
	/**
	 * Total count of orgs
	 */
	private int orgRowTotal;
	
	
	
	public List<Integer> getBizxEpRoleExceptions() {
		return bizxEpRoleExceptions;
	}

	
	
	
	public void setBizxEpRoleExceptions(List<Integer> bizxEpRoleExceptions) {
		this.bizxEpRoleExceptions = bizxEpRoleExceptions;
	}

	public int getCustColumConnTotalWrite() {
		return custColumConnTotalWrite;
	}

	public void setCustColumConnTotalWrite(int custColumConnTotalWrite) {
		this.custColumConnTotalWrite = custColumConnTotalWrite;
	}

	public void addBizExceptionWithLimit(Exception ex) {
		
		if (bizxExceptionsQueue.size() <= EXCEPTIONS_LIMIT){
			bizxExceptionsQueue.add(ex);
		}		
	}
	
	public void addOrgExceptionWithLimit(Exception ex) {
		
		if (orgExceptionsQueue.size() <= EXCEPTIONS_LIMIT){
			orgExceptionsQueue.add(ex);
		}		
	}
	
	
	
	public void addBizxEpExceptionWithLimit(Exception ex) {
		
		if (bizxEpExceptionsQueue.size() <= EXCEPTIONS_LIMIT){
			bizxEpExceptionsQueue.add(ex);
		}		
	}
	
		
	synchronized public void incCustColumConnDiffDescr(){
		custColumConnDiffDescr++;		
	}
	
	public void incCustColumConnTotalWrite(){
		custColumConnTotalWrite++;
	}
	
		
	public int getCustColumConnDiffDescr() {
		return custColumConnDiffDescr;
	}

	public void setCustColumConnDiffDescr(int custColumConnDiffDescr) {
		this.custColumConnDiffDescr = custColumConnDiffDescr;
	}

	public Date getBatchStartDate() {
		return batchStartDate;
	}

	public void setBatchStartDate(Date batchStartDate) {
		this.batchStartDate = batchStartDate;
	}

	public Date getBatchEndDate() {
		return batchEndDate;
	}

	public void setBatchEndDate(Date batchEndDate) {
		this.batchEndDate = batchEndDate;
	}

	
	public Throwable getFailedFastException() {
		return failedFastException;
	}

	public void setFailedFastException(Throwable failedFastException) {
		this.failedFastException = failedFastException;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public Date getStepCustColConnectorStartDate() {
		return stepCustColConnectorStartDate;
	}

	public void setStepCustColConnectorStartDate(Date stepCustColConnectorStartDate) {
		this.stepCustColConnectorStartDate = stepCustColConnectorStartDate;
	}

	public Date getStepCustColConnectorEndDate() {
		return stepCustColConnectorEndDate;
	}

	public void setStepCustColConnectorEndDate(Date stepCustColConnectorEndDate) {
		this.stepCustColConnectorEndDate = stepCustColConnectorEndDate;
	}

	public Date getStepGenerateFileForBizxStartDate() {
		return stepGenerateFileForBizxStartDate;
	}

	public void setStepGenerateFileForBizxStartDate(
			Date stepGenerateFileForBizxStartDate) {
		this.stepGenerateFileForBizxStartDate = stepGenerateFileForBizxStartDate;
	}

	public Date getStepGenerateFileForBizxEndDate() {
		return stepGenerateFileForBizxEndDate;
	}

	public void setStepGenerateFileForBizxEndDate(
			Date stepGenerateFileForBizxEndDate) {
		this.stepGenerateFileForBizxEndDate = stepGenerateFileForBizxEndDate;
	}


	public int getLmsSourceRowTotal() {
		return lmsSourceRowTotal;
	}


	public void setLmsSourceRowTotal(int lmsSourceRowTotal) {
		this.lmsSourceRowTotal = lmsSourceRowTotal;
	}


	public int getBizxRowTotal() {
		return bizxRowTotal;
	}

	
	public void incBizxRowTotal(){
		this.bizxRowTotal++;
	}
	
	public void incBizxEpRowTotal(){
		this.bizxEpRowTotal++;
	}	

	public void setBizxRowTotal(int bizxRowTotal) {
		this.bizxRowTotal = bizxRowTotal;
	}
	
	public int getTotalErrorsCnt(){		
		return bizxExceptionsQueue.size() + apprRoleExceptionsQueue.size() + custColExceptionsQueue.size();		
	}

	
	public Date getStepApprRoleConnStartDate() {
		return stepApprRoleConnStartDate;
	}


	public void setStepApprRoleConnStartDate(Date stepApprRoleConnStartDate) {
		this.stepApprRoleConnStartDate = stepApprRoleConnStartDate;
	}


	public Date getStepApprRoleConnEndDate() {
		return stepApprRoleConnEndDate;
	}


	public void setStepApprRoleConnEndDate(Date stepApprRoleConnEndDate) {
		this.stepApprRoleConnEndDate = stepApprRoleConnEndDate;
	}

	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}

	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}

	public BlockingQueue<Exception> getApprRoleExceptionsQueue() {
		return apprRoleExceptionsQueue;
	}

	public void setApprRoleExceptionsQueue(
			BlockingQueue<Exception> apprRoleExceptionsQueue) {
		this.apprRoleExceptionsQueue = apprRoleExceptionsQueue;
	}

	public BlockingQueue<Exception> getCustColExceptionsQueue() {
		return custColExceptionsQueue;
	}

	public void setCustColExceptionsQueue(
			BlockingQueue<Exception> custColExceptionsQueue) {
		this.custColExceptionsQueue = custColExceptionsQueue;
	}


	public int getAppRoleConnNewRolesCount() {
		return appRoleConnNewRolesCount;
	}


	public void setAppRoleConnNewRolesCount(int appRoleConnNewRolesCount) {
		this.appRoleConnNewRolesCount = appRoleConnNewRolesCount;
	}


	public int getAppRoleConnRemovedCount() {
		return appRoleConnRemovedCount;
	}


	public void setAppRoleConnRemovedCount(int appRoleConnRemovedCount) {
		this.appRoleConnRemovedCount = appRoleConnRemovedCount;
	}

	public Date getStepOrgConnStartDate() {
		return stepOrgConnStartDate;
	}

	public void setStepOrgConnStartDate(Date stepOrgConnStartDate) {
		this.stepOrgConnStartDate = stepOrgConnStartDate;
	}

	public Date getStepOrgConnEndDate() {
		return stepOrgConnEndDate;
	}

	public void setStepOrgConnEndDate(Date stepOrgConnEndDate) {
		this.stepOrgConnEndDate = stepOrgConnEndDate;
	}

	public BlockingQueue<Exception> getBizxEpExceptionsQueue() {
		return bizxEpExceptionsQueue;
	}

	public void setBizxEpExceptionsQueue(
			BlockingQueue<Exception> bizxEpExceptionsQueue) {
		this.bizxEpExceptionsQueue = bizxEpExceptionsQueue;
	}

	public Date getStepGenerateFileForBizxEpStartDate() {
		return stepGenerateFileForBizxEpStartDate;
	}

	public void setStepGenerateFileForBizxEpStartDate(
			Date stepGenerateFileForBizxEpStartDate) {
		this.stepGenerateFileForBizxEpStartDate = stepGenerateFileForBizxEpStartDate;
	}

	public Date getStepGenerateFileForBizxEpEndDate() {
		return stepGenerateFileForBizxEpEndDate;
	}

	public void setStepGenerateFileForBizxEpEndDate(
			Date stepGenerateFileForBizxEpEndDate) {
		this.stepGenerateFileForBizxEpEndDate = stepGenerateFileForBizxEpEndDate;
	}

	public String getStrBizxEpRoleExceptions() {
		
		return StringUtils.collectionToDelimitedString(bizxEpRoleExceptions  , ",");
		
	}

	public void setStrBizxEpRoleExceptions(String strBizxEpRoleExceptions) {
		this.strBizxEpRoleExceptions = strBizxEpRoleExceptions;
	}

	
	public int getBizxEpRowTotal() {
		return bizxEpRowTotal;
	}

	public void setBizxEpRowTotal(int bizxEpRowTotal) {
		this.bizxEpRowTotal = bizxEpRowTotal;
	}

	public void setBizxEpSapUsersCount(int cnt) {
		this.bizxEpSapUsersCount = cnt;
	}

	public void setBizxEpSscUsersCount(int cnt) {
		this.bizxEpSscUsersCount = cnt;
	}


	public int getBizxEpSapUsersCount() {
		return bizxEpSapUsersCount;
	}

	public int getBizxEpSscUsersCount() {
		return bizxEpSscUsersCount;
	}

	public void addBizxEpRoleExceptions(int roleCode) {		
		if ( !bizxEpRoleExceptions.contains(roleCode) ){
			this.bizxEpRoleExceptions.add(roleCode);
		}		
	}

	public int getOrgRowTotal() {
		return orgRowTotal;
	}

	public void setOrgRowTotal(int orgRowTotal) {
		this.orgRowTotal = orgRowTotal;
	}


	public BlockingQueue<Exception> getOrgExceptionsQueue() {
		return orgExceptionsQueue;
	}


	public void setOrgExceptionsQueue(BlockingQueue<Exception> orgExceptionsQueue) {
		this.orgExceptionsQueue = orgExceptionsQueue;
	}	
	
	
	
	
	
}








