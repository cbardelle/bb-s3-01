package eu.unicreditgroup.mailer;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

public abstract class AbstractReportGenerator {

	private static final Logger logger = Logger.getLogger(AbstractReportGenerator.class);
	
	protected static final String EMAIL_SENDER_CONFIG = "conf/emailSender.ini";
	
	
	protected static final String TRACK_ATTR_EMAIL_SENDER_CONFIG = "conf/emailTrackedAttrSender.ini";
	
	protected String pathToLogFile;
	
	protected String logFileName;
	
	/**
	 * Returns text version of exceptions and their stack traces from
	 * passed list. 
	 * 
	 * @param exceptionsList
	 * @return
	 */
	public String generateTextExceptionsReport(List<Throwable> exceptionsList){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		if (exceptionsList.size() > 0) {
			for (int i=0; i < exceptionsList.size(); i++) {
				Exception elem = (Exception) exceptionsList.get(i);
				pw.println("=== " + (i+1) + " ====================================================================");
				pw.println("Exception class: " + elem.getClass().getName());
				pw.println("Exception toString(): " + elem.toString());
				pw.println("----Stack trace -------------------------------");
				elem.printStackTrace(pw);
				pw.println("==========================================================================");
				pw.println();
				pw.println();
			}
		}
		
		return sw.toString();
	}
	
	
	/**
	 * Wrapper for EmailSender.sendEmail() method - just for debugging purpose.
	 * 
	 * @param emailSender
	 * @throws EmailException
	 */
	protected void sendEmail(EmailSender emailSender) throws EmailException {
		logger.debug("Trying to send email...");
		logger.debug("emailSender object:");
		logger.debug(emailSender.toString());
		emailSender.sendEmail();
		logger.debug("Email successfully sent!");
	}

}
