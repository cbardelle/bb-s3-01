package eu.unicreditgroup.batch.step;

public interface BatchStep {
	public void execute() throws Exception; 
}
