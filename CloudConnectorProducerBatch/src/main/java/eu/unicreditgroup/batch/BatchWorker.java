package eu.unicreditgroup.batch;

import java.util.Date;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.approle.step.StepApprRoleConn;
import eu.unicreditgroup.cn.bizx.step.StepGenerateFileForBizx;
import eu.unicreditgroup.cn.bizxep.step.StepGenerateFileForBizxEp;
import eu.unicreditgroup.cn.custcol.step.StepCustColConnector;
import eu.unicreditgroup.cn.org.step.StepOrgConnector;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;


public class BatchWorker {
	
	Logger logger = Logger.getLogger(BatchWorker.class);
	
	private ConfigParams configParams;
	
	//private BlockingQueue<Exception> exceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	private FTPUploader resultUploader;
	
	private StepGenerateFileForBizx stepGenerateFileForBizx;
	
	
	private StepGenerateFileForBizxEp stepGenerateFileForBizxEp;
	
	private StepCustColConnector stepCustColConnector;
	
	private StepApprRoleConn stepApprRoleConn;
	
	private StepOrgConnector stepOrgConnector;
	
	
	
		
	/**
	 * Main method here. Executes steps as set in config file.
	 * 
	 */
	public void execute(){		
		reportDataBean.setBatchStartDate(new Date());
		
		// generete import file for the BizX		
		if (configParams.isStepGenerateFileForBizxEnabled()){
			try {				
				
				reportDataBean.setStepGenerateFileForBizxStartDate(new Date());	
				
				stepGenerateFileForBizx.execute();				
				
				reportDataBean.setStepGenerateFileForBizxEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step generete import file for BizX", e);
				stepGenerateFileForBizx.addException(e);
				
			}
		} else {
			logger.debug("Step generete import file for BizX is disabled. Nothing to do.");
		}
		
		
		// generete import file for the BizX EP	
		if (configParams.isStepGenerateFileForBizxEpEnabled()){
			try {				
				
				reportDataBean.setStepGenerateFileForBizxEpStartDate(new Date());	
				
				stepGenerateFileForBizxEp.execute();				
				
				reportDataBean.setStepGenerateFileForBizxEpEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step generete import file for BizX EP", e);
				stepGenerateFileForBizxEp.addException(e);
				
			}
		} else {
			logger.debug("Step generete import file for BizX EP is disabled. Nothing to do.");
		}
		
		
		
		//generate the source file for the Custom Column Connector
		if (configParams.isStepCustColConnectorEnabled()){
			try {				
				
				reportDataBean.setStepCustColConnectorStartDate(new Date());		
				
				stepCustColConnector.execute();
								
				reportDataBean.setStepCustColConnectorEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step generete file for Custom Column Connector", e);
				stepCustColConnector.addException(e);
			}
		} else {
			logger.debug("Step 'generete file for Custom Column Connector' is disabled. Nothing to do.");
		}
		
		
		//generate the source file for the Approval Role Assignment Connector
		
		if (configParams.isStepApprRoleConnEnabled()){
			try {				
				
				reportDataBean.setStepApprRoleConnStartDate(new Date());		
				
				stepApprRoleConn.execute();
								
				reportDataBean.setStepApprRoleConnEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step generete file for Approval Role Assignment Connector", e);
				stepApprRoleConn.addException(e);
				
			}
		} else {
			logger.debug("Step 'generete file for Approval Role Assignment Connector' is disabled. Nothing to do.");
		}
		
		
		//generate the source file for the Organizations Connector
		
		if (configParams.isStepOrgConnectorEnabled()){
			try {				
				
				reportDataBean.setStepOrgConnStartDate(new Date());		
				
				stepOrgConnector.execute();
								
				reportDataBean.setStepOrgConnEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step generete file for Organizations Connector", e);
				stepApprRoleConn.addException(e);
				
			}
		} else {
			logger.debug("Step 'generete file for Organizations' is disabled. Nothing to do.");
		}
		
		
		reportDataBean.setBatchEndDate(new Date());	
		
		long diff = reportDataBean.getBatchEndDate().getTime() - reportDataBean.getBatchStartDate().getTime(); 
		
		long diffMinutes = diff / (60 * 1000);
		
			 
		System.out.println("--------------------------------------------------");
		System.out.println("Batch executed in " + diffMinutes);
	}
	
	
	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	

	/*
	public SourceTableReader getSourceTableReader() {
		return sourceTableReader;
	}

	public void setSourceTableReader(SourceTableReader sourceTableReader) {
		this.sourceTableReader = sourceTableReader;
	}*/


	public FTPUploader getResultUploader() {
		return resultUploader;
	}


	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}
	

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}


	public StepGenerateFileForBizx getStepGenerateFileForBizx() {
		return stepGenerateFileForBizx;
	}


	public void setStepGenerateFileForBizx(
			StepGenerateFileForBizx stepGenerateFileForBizx) {
		this.stepGenerateFileForBizx = stepGenerateFileForBizx;
	}


	public StepCustColConnector getStepCustColConnector() {
		return stepCustColConnector;
	}


	public void setStepCustColConnector(StepCustColConnector stepCustColConnector) {
		this.stepCustColConnector = stepCustColConnector;
	}


	public StepApprRoleConn getStepApprRoleConn() {
		return stepApprRoleConn;
	}


	public void setStepApprRoleConn(StepApprRoleConn stepApprRoleConn) {
		this.stepApprRoleConn = stepApprRoleConn;
	}


	public StepOrgConnector getStepOrgConnector() {
		return stepOrgConnector;
	}


	public void setStepOrgConnector(StepOrgConnector stepOrgConnector) {
		this.stepOrgConnector = stepOrgConnector;
	}

	public StepGenerateFileForBizxEp getStepGenerateFileForBizxEp() {
		return stepGenerateFileForBizxEp;
	}

	public void setStepGenerateFileForBizxEp(
			StepGenerateFileForBizxEp stepGenerateFileForBizxEp) {
		this.stepGenerateFileForBizxEp = stepGenerateFileForBizxEp;
	}


	
	
}
