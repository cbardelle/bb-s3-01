package eu.unicreditgroup.cn.bizxep.queue;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;
import eu.unicreditgroup.cn.bizxep.dao.SourceDao;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.cn.bizxep.queue.SourceRowQueueUtil;

/**
 * 
 * 
 * 
 * This class is responsible for reading rows from the HR_CONN_RES_FILE
 * (a table with users data which is imported to LMS) 
 *  * Each row is put into a queue, which acts like a read buffer.
 * When all rows from the table are processed termination object is put into table.
 *   
 * 
 * @author UV00074
 *
 */
public class SourceDataProducer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(SourceDataProducer.class);
	
	private ConfigParams configParams;
			
	private BlockingQueue<SapSourceRow> sapDataQueue;
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	
	
	private int consumerNumber;
	
	 
	private SourceDao sourceDao;
	
	
	
	@Override
	public void run() {
		
		
		List<SapSourceRow> sapUsersList = sourceDao.getSapUsersData();
		
		List<SapSourceRow> sscUsersList = sourceDao.getSscUsersData();
		
		//TODO check for users present in the BIZEX_ROLE_RSA but not in the SAP!
		
		// Users from BIZXEP_ROLE_RSA not present in the SAP table (check for non exisiting users)
		//SELECT R.USER_ID, R.NOTE FROM BIZXEP_ROLE_RSA R WHERE  NOT EXISTS (SELECT 1 FROM HR_CONN_SAP_SRC S WHERE S.USER_ID = R.USER_ID)

		
		reportDataBean.setBizxEpSapUsersCount( sapUsersList.size() );
		
		reportDataBean.setBizxEpSscUsersCount( sscUsersList.size() ); 
		
		
		try {
			for (SapSourceRow r : sapUsersList ){		
				sapDataQueue.put(r);			
			}				
			
			for (SapSourceRow r : sscUsersList ){		
				sapDataQueue.put(r);			
			}				
			
			sapDataQueue.put(SourceRowQueueUtil.generateShutdownRow());
			
		} catch (Exception e) {
			
			logger.error("Exception in source tabale producer.", e);
			bizxExceptionsQueue.add(e);
			
		} finally {
			
			try{				
				// put end of the queue marker
				sapDataQueue.put(SourceRowQueueUtil.generateShutdownRow());		
				
				
				//TODO set the statistics
				//reportDataBean.setLmsSourceRowTotal(lmsTableReader.getTotalRowsCount());
				
			} catch(Exception e){
				logger.error("Exception occured : " + e);
			}			
		}
		
		
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}
	
	public SourceDao getSourceDao() {
		return sourceDao;
	}

	public void setSourceDao(SourceDao sourceDao) {
		this.sourceDao = sourceDao;
	}

	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}

	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public void setConsumerNumber(int i) {
		consumerNumber = i;		
	}

	

	

	public BlockingQueue<SapSourceRow> getSapDataQueue() {
		return sapDataQueue;
	}

	public void setSapDataQueue(BlockingQueue<SapSourceRow> sapDataQueue) {
		this.sapDataQueue = sapDataQueue;
	}

	public int getConsumerNumber() {
		return consumerNumber;
	}

	
	
}
