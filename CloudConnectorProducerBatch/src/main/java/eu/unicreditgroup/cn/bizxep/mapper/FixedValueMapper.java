package eu.unicreditgroup.cn.bizxep.mapper;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;

/**
 * Mapping to a fixed value
 * 
 * @author UV00074
 *
 */
public class FixedValueMapper implements IMapper {
		
	@Override
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		return new MappedColumn(defaultVal);		
	}

	
}
