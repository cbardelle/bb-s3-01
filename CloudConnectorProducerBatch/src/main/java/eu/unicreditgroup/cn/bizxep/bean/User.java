package eu.unicreditgroup.cn.bizxep.bean;

public class User {
	private String userId;
	
	private String oldUserId;
	
	private String firstName;
	
	private String lastName;
	
	private String fiscalCode;
	
	private String email;
	
	private String plateauRole;
	
	private String bizxRole;
	
	private String lineManager;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOldUserId() {
		return oldUserId;
	}
	public void setOldUserId(String oldUserId) {
		this.oldUserId = oldUserId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPlateauRole() {
		return plateauRole;
	}
	public void setPlateauRole(String plateauRole) {
		this.plateauRole = plateauRole;
	}
	public String getLineManager() {
		return lineManager;
	}
	public void setLineManager(String lineManager) {
		this.lineManager = lineManager;
	}
	public String getBizxRole() {
		return bizxRole;
	}
	public void setBizxRole(String bizxRole) {
		this.bizxRole = bizxRole;
	}
	
	
	

}
