package eu.unicreditgroup.cn.bizxep.queue;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


public class SourceRowQueueUtil {
	
	public static final String SHUTDOWN_STRING = "###SHUTDOWN_ROW###";
	
	private static SapSourceRow shutdownRow = new SapSourceRow();
		
	static	{	
		shutdownRow.setShutDownRow(true);		
	}
			
	public static SapSourceRow generateShutdownRow(){
		return shutdownRow;
	}
	
	public static boolean isShutdownRow(SapSourceRow row){		
		if (row == null){
			return false;
		} else {
			return row.isShutDownRow(); 
				 
		}		
	}
	
}
