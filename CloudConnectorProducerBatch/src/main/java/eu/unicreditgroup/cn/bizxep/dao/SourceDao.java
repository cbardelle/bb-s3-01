package eu.unicreditgroup.cn.bizxep.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;
import eu.unicreditgroup.cn.bizxep.config.BizxEpColumn;
import eu.unicreditgroup.cn.bizxep.config.SapDataConf;



public class SourceDao {
		
	private static final Logger logger = Logger.getLogger(SourceDao.class);
	
	
	private final String STATUS_ACTIVE = "active";	
	//private final String DEFAULT_LM = "NO_MANAGER";
	//private final String DEFAULT_HRBP = "NO_HR";
	

	private List<BizxEpColumn> resultColumnsBizxEp;
	
	/**
	 * 
	 * SAP Users with role RecruitingCoordinator, SeniorHR, Admin or Fake HRBP
	 * 
	 */
	private String sqlSapData = 
	" SELECT R.SENIOR_HR, R.RC, R.ADMIN, " + 
	" S.USER_STATUS_DESC, " +	
	" S.USER_ID, " +
	" S.USER_ID, " +
	" S.FIRST_NAME, " +
	" S.LAST_NAME, " +
	" S.GENDER, " +
	" S.OFFICE_EMAIL, " +
	" S.LINE_MANAGER_ID, " +
	" S.HRBP_ID, " +
	" S.HRBP, " +
	" S.LINE_MANAGER, " +
	" S.COMPETENCE_LINE_DESC, " +
	" S.GLOBAL_JOB_DESC, " +
	" S.GLOBAL_JOB_ABBR, " +
	" S.DIVISION_DESC, " +
	" S.OFFICE_COUNTRY_DESC, " +
	" S.HIRE_DATE, " +
	" S.USER_ID, " +
	" S.USER_OFFICE_DESC, " +
	" S.OFFICE_FIX_PHONE, " +
	" S.OFFICE_ADDR, " +
	" S.OFFICE_CITY, " +
	" S.OFFICE_PROVINCE, " +
	" S.OFFICE_POSTAL_CODE, " +
	" S.DATE_OF_BIRTH, " +
	" S.HOST_LEGAL_ENT_DESC, " +
	" S.HOST_LEGAL_ENT_CODE, " +
	" S.USER_SAP_ID, " +
	" S.GLOBAL_JOB_BAND_DESC, " +
	" S.COMPETENCE_LINE_DESC, " +
	" R.FAKE_HRBP " +
	" FROM HR_CONN_SAP_SRC S, BIZXEP_ROLE_RSA R WHERE R.USER_ID = S.USER_ID AND S.USER_STATUS_ID = 3 " +
	" UNION " +
	" SELECT '' AS SENIOR_HR, '' AS RC, '' AS ADMIN, " + 
	" S.USER_STATUS_DESC, " +	
	" S.USER_ID, " +
	" S.USER_ID, " +
	" S.FIRST_NAME, " +
	" S.LAST_NAME, " +
	" S.GENDER, " +
	" S.OFFICE_EMAIL, " +
	" S.LINE_MANAGER_ID, " +
	" S.HRBP_ID, " +
	" S.HRBP, " +
	" S.LINE_MANAGER, " +
	" S.COMPETENCE_LINE_DESC, " +
	" S.GLOBAL_JOB_DESC, " +
	" S.GLOBAL_JOB_ABBR, " +
	" S.DIVISION_DESC, " +
	" S.OFFICE_COUNTRY_DESC, " +
	" S.HIRE_DATE, " +
	" S.USER_ID, " +
	" S.USER_OFFICE_DESC, " +
	" S.OFFICE_FIX_PHONE, " +
	" S.OFFICE_ADDR, " +
	" S.OFFICE_CITY, " +
	" S.OFFICE_PROVINCE, " +
	" S.OFFICE_POSTAL_CODE, " +
	" S.DATE_OF_BIRTH, " +
	" S.HOST_LEGAL_ENT_DESC, " +
	" S.HOST_LEGAL_ENT_CODE, " +
	" S.USER_SAP_ID, " +
	" S.GLOBAL_JOB_BAND_DESC, " +
	" S.COMPETENCE_LINE_DESC, " +	
	" '' AS FAKE_HRBP " +
	" FROM HR_CONN_SAP_SRC S " +
	" WHERE S.HOST_LEGAL_ENT_CODE IN ('C100','E130','E170','LE10','UC01','UC04','UC11','UC12','UC16','UC52','E140','FI01') " +
	" AND ( S.LINE_MANAGER = 'Y' OR S.HRBP = 'Y' ) " +
	" AND NOT EXISTS (SELECT 1 FROM BIZXEP_ROLE_RSA R WHERE R.USER_ID = S.USER_ID) AND S.USER_STATUS_ID = 3 ";
	
			
	String sqlSscData = " SELECT USER_ID, FIRST_NAME, LAST_NAME, GENDER, EMAIL FROM BIZXEP_ROLE_C";
	
	
	private String sqlBizxEpUser = 
	" SELECT " +
			 " STATUS, " +
			 " USERID, " +
			 " USERNAME, " +
			 " FIRSTNAME, " +
			 " LASTNAME, " +
			 " MI, " +
			 " GENDER, " +
			 " EMAIL, " +
			 " MANAGER, " +
			 " HR, " +
			 " DEPARTMENT, " +
			 " JOBCODE, " +
			 " DIVISION, " +
			 " LOCATION, " +
			 " TIMEZONE, " +
			 " HIREDATE, " +
			 " EMPID, " +
			 " TITLE, " +
			 " BIZ_PHONE, " +
			 " FAX, " +
			 " ADDR1, " +
			 " ADDR2, " +
			 " CITY, " +
			 " STATE, " +
			 " ZIP, " +
			 " COUNTRY, " +
			 " REVIEW_FREQ, " +
			 " LAST_REVIEW_DATE, " +
			 " CUSTOM01, " +
			 " CUSTOM02, " +
			 " CUSTOM03, " +
			 " CUSTOM04, " +
			 " CUSTOM05, " +
			 " CUSTOM06, " +
			 " CUSTOM07, " +
			 " CUSTOM08, " +
			 " CUSTOM09, " +
			 " CUSTOM10, " +
			 " CUSTOM11, " +
			 " CUSTOM12, " +
			 " CUSTOM13, " +
			 " CUSTOM14, " +
			 " CUSTOM15, " +
			 " MATRIX_MANAGER, " +
			 " DEFAULT_LOCALE, " +
			 " PROXY, " +
			 " CUSTOM_MANAGER, " +
			 " SECOND_MANAGER, " +
			 " LOGIN_METHOD " +
			 " FROM BIZXEP_USER ORDER BY STATUS, USERID";

			
	
	
	private class SscDataMapper implements RowMapper<SapSourceRow> {
		@Override
		public SapSourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			SapSourceRow srcRow = new SapSourceRow();
						
			
			
			srcRow.setUserId(rs.getString("USER_ID"));			
			srcRow.setFirstName(rs.getString("FIRST_NAME"));
			srcRow.setLastName(rs.getString("LAST_NAME"));
			srcRow.setGender(rs.getString("GENDER"));
			srcRow.setOfficeEmail (rs.getString("EMAIL"));
			// set the default role for all users from SSC
			srcRow.setRoleScc("Y");
			
			
			// Fixed values!!!
			srcRow.setUserStatusDesc(STATUS_ACTIVE);
			//srcRow.setLineManagerId(DEFAULT_LM);
			//srcRow.setHrbpId(DEFAULT_HRBP);
			
						
			return srcRow;
		}
	}

	
	
	private class SapDataMapper implements RowMapper<SapSourceRow> {
		@Override
		public SapSourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			SapSourceRow srcRow = new SapSourceRow();
						
			srcRow.setRoleSeniorHr(rs.getString("SENIOR_HR"));
			srcRow.setRoleRecruitingCoordinator(rs.getString("RC"));
			srcRow.setRoleAdmin(rs.getString("ADMIN"));
			srcRow.setUserStatusDesc(rs.getString("USER_STATUS_DESC"));
			srcRow.setUserId(rs.getString("USER_ID"));			
			srcRow.setFirstName(rs.getString("FIRST_NAME"));
			srcRow.setLastName(rs.getString("LAST_NAME"));
			srcRow.setGender(rs.getString("GENDER"));
			srcRow.setOfficeEmail (rs.getString("OFFICE_EMAIL"));
			srcRow.setLineManagerId(rs.getString("LINE_MANAGER_ID"));
			srcRow.setHrbpId(rs.getString("HRBP_ID"));
			srcRow.setCompetenceLineDesc(rs.getString("COMPETENCE_LINE_DESC"));
			srcRow.setGlobalJobDesc(rs.getString("GLOBAL_JOB_DESC"));
			srcRow.setGlobalJobAbbr(rs.getString("GLOBAL_JOB_ABBR"));
			srcRow.setDivisionDesc(rs.getString("DIVISION_DESC"));
			srcRow.setOfficeCountryDesc(rs.getString("OFFICE_COUNTRY_DESC"));
			srcRow.setHireDate (rs.getString("HIRE_DATE"));
			srcRow.setUserOfficeDesc (rs.getString("USER_OFFICE_DESC"));			
			srcRow.setOfficeFixPhone(rs.getString("OFFICE_FIX_PHONE"));			
			srcRow.setOfficeAddr(rs.getString("OFFICE_ADDR"));			
			srcRow.setOfficeCity(rs.getString("OFFICE_CITY"));
			srcRow.setOfficeProvince(rs.getString("OFFICE_PROVINCE"));			
			srcRow.setOfficePostalCode(rs.getString("OFFICE_POSTAL_CODE"));
			srcRow.setDateOfBirth(rs.getString("DATE_OF_BIRTH"));
			srcRow.setHostLegalEntDesc (rs.getString("HOST_LEGAL_ENT_DESC"));
			srcRow.setHostLegalEntCode(rs.getString("HOST_LEGAL_ENT_CODE"));
			srcRow.setUserSapId(rs.getString("USER_SAP_ID"));
			srcRow.setGlobalJobBandDesc(rs.getString("GLOBAL_JOB_BAND_DESC"));
			srcRow.setCompetenceLineDesc(rs.getString("COMPETENCE_LINE_DESC"));
			srcRow.setHrbp(rs.getString("HRBP"));
			srcRow.setLineManager(rs.getString("LINE_MANAGER"));			
			srcRow.setFakeHrbp(rs.getString("FAKE_HRBP"));
												
			return srcRow;
		}
	}
		
	private SimpleJdbcTemplate simpleJdbcTemplate;
		
	private DataSource dataSource;
	
	
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }	
	
	
	public List<SapSourceRow> getSscUsersData(){		
		return simpleJdbcTemplate.query(sqlSscData, new SscDataMapper());
	}
	
	
	public List<SapSourceRow> getSapUsersData(){		
		return simpleJdbcTemplate.query(sqlSapData, new SapDataMapper());
	}


	public void writeResultTmpTable(List<MappedRow> buffer) throws Exception {
		
		String insertQuery = 
				" INSERT INTO BIZXEP_USER_TMP (" +				
				"STATUS, " + 
				"USERID, " +
				"USERNAME, " +
				"FIRSTNAME, " +
				"LASTNAME, " +
				"MI, " +
				"GENDER, " +
				"EMAIL, " +
				"MANAGER, " +
				"HR, " +
				"DEPARTMENT, " +
				"JOBCODE, " +
				"DIVISION, " +
				"LOCATION, " +
				"TIMEZONE, " +
				"HIREDATE, " +
				"EMPID, " +
				"TITLE, " +
				"BIZ_PHONE, " +
				"FAX, " +
				"ADDR1, " +
				"ADDR2, " +
				"CITY, " +
				"STATE, " +
				"ZIP, " +
				"COUNTRY, " +
				"REVIEW_FREQ, " +
				"LAST_REVIEW_DATE, " +
				"CUSTOM01, " +
				"CUSTOM02, " +
				"CUSTOM03, " +
				"CUSTOM04, " +
				"CUSTOM05, " +
				"CUSTOM06, " +
				"CUSTOM07, " +
				"CUSTOM08, " +
				"CUSTOM09, " +
				"CUSTOM10, " +
				"CUSTOM11, " +
				"CUSTOM12, " +
				"CUSTOM13, " +
				"CUSTOM14, " +
				"CUSTOM15, " +
				"MATRIX_MANAGER, " +
				"DEFAULT_LOCALE, " +
				"PROXY, " +
				"CUSTOM_MANAGER, " +
				"SECOND_MANAGER, " +
				"LOGIN_METHOD " +									
				" ) " + 
				" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
			
				
			List<Object[]> parameters = new ArrayList<Object[]>();
			/*
	        for ( cust : customers) {
	            parameters.add(new Object[] {cust.getCustId(), cust.getName(), cust.getAge()});
	        }
	        */
			
			for (MappedRow row : buffer){
				
				Object[] paramArray = new Object[ resultColumnsBizxEp.size() ];
				
				
				for (int i=0;i < resultColumnsBizxEp.size() ;i++ ){
					paramArray[i] = row.getData()[i];
				}
				
				// now
				//Date d = new Date(new java.util.Date().getTime());				
				//paramArray[resultColumnsBizxEp.size()] = d;
				parameters.add(paramArray);
								
			}
			
			
	        
	        simpleJdbcTemplate.batchUpdate(insertQuery, parameters);
	        
		
	}


	public List<BizxEpColumn> getResultColumnsBizxEp() {
		return resultColumnsBizxEp;
	}


	public void setResultColumnsBizxEp(List<BizxEpColumn> resultColumnsBizxEp) {
		this.resultColumnsBizxEp = resultColumnsBizxEp;
	}


	public void cleanBizxepUserTmp() {
		
		simpleJdbcTemplate.update( "TRUNCATE TABLE BIZXEP_USER_TMP " );
		
		
	}


	public void backupBizxepUser() {
		
		String cleanUp = " DELETE FROM BIZXEP_USER_BKP WHERE BKP_TST < SYSDATE - 5  ";
		
		simpleJdbcTemplate.update(cleanUp);
		
		String insert = " INSERT INTO BIZXEP_USER_BKP SELECT U.*,SYSDATE AS BKP_TST FROM BIZXEP_USER U ";
		
		simpleJdbcTemplate.update(insert); 
		
		
		
	}


	/**
	 * 
	 * Merge the table BIZXEP_USER using BIZXEP_USER_TMP 
	 *  - overwrite all existing users and set LST_UPD_TST to null
	 *  - insert all new users with LST_UPD_TST = null
	 *  
	 *  - set all users not currently present as inactive (where LST_UPD_TST is not null)
	 *  - finally set LST_UPD_TST for all current users (SAP/SSC)
	 * 
	 */
	public void bizxepMergeNewUsers() throws Exception {
		
		
		String sql = 	" MERGE INTO BIZXEP_USER U " +
						" USING BIZXEP_USER_TMP T " +
						" ON (U.USERID = T.USERID) " +
						" WHEN NOT MATCHED THEN " +
						" INSERT ( " +
						" U.STATUS, " +
						" U.USERID, " +
						" U.USERNAME, " +
						" U.FIRSTNAME, " +
						" U.LASTNAME, " +
						" U.MI, " +
						" U.GENDER, " +
						" U.EMAIL, " +
						" U.MANAGER, " +
						" U.HR, " +
						" U.DEPARTMENT, " +
						" U.JOBCODE, " +
						" U.DIVISION, " +
						" U.LOCATION, " +
						" U.TIMEZONE, " +
						" U.HIREDATE, " +
						" U.EMPID, " +
						" U.TITLE, " +
						" U.BIZ_PHONE, " +
						" U.FAX, " +
						" U.ADDR1, " +
						" U.ADDR2, " +
						" U.CITY, " +
						" U.STATE, " +
						" U.ZIP, " +
						" U.COUNTRY, " +
						" U.REVIEW_FREQ, " +
						" U.LAST_REVIEW_DATE, " +
						" U.CUSTOM01, " +
						" U.CUSTOM02, " +
						" U.CUSTOM03, " +
						" U.CUSTOM04, " +
						" U.CUSTOM05, " +
						" U.CUSTOM06, " +
						" U.CUSTOM07, " +
						" U.CUSTOM08, " +
						" U.CUSTOM09, " +
						" U.CUSTOM10, " +
						" U.CUSTOM11, " +
						" U.CUSTOM12, " +
						" U.CUSTOM13, " +
						" U.CUSTOM14, " +
						" U.CUSTOM15, " +
						" U.MATRIX_MANAGER, " +
						" U.DEFAULT_LOCALE, " +
						" U.PROXY, " +
						" U.CUSTOM_MANAGER, " +
						" U.SECOND_MANAGER, " +
						" U.LOGIN_METHOD " +
						" ) " +
						" VALUES ( " +
						" T.STATUS, " +
						" T.USERID, " +
						" T.USERNAME, " +
						" T.FIRSTNAME, " +
						" T.LASTNAME, " +
						" T.MI, " +
						" T.GENDER, " +
						" T.EMAIL, " +
						" T.MANAGER, " +
						" T.HR, " +
						" T.DEPARTMENT, " +
						" T.JOBCODE, " +
						" T.DIVISION, " +
						" T.LOCATION, " +
						" T.TIMEZONE, " +
						" T.HIREDATE, " +
						" T.EMPID, " +
						" T.TITLE, " +
						" T.BIZ_PHONE, " +
						" T.FAX, " +
						" T.ADDR1, " +
						" T.ADDR2, " +
						" T.CITY, " +
						" T.STATE, " +
						" T.ZIP, " +
						" T.COUNTRY, " +
						" T.REVIEW_FREQ, " +
						" T.LAST_REVIEW_DATE, " +
						" T.CUSTOM01, " +
						" T.CUSTOM02, " +
						" T.CUSTOM03, " +
						" T.CUSTOM04, " +
						" T.CUSTOM05, " +
						" T.CUSTOM06, " +
						" T.CUSTOM07, " +
						" T.CUSTOM08, " +
						" T.CUSTOM09, " +
						" T.CUSTOM10, " +
						" T.CUSTOM11, " +
						" T.CUSTOM12, " +
						" T.CUSTOM13, " +
						" T.CUSTOM14, " +
						" T.CUSTOM15, " +
						" T.MATRIX_MANAGER, " +
						" T.DEFAULT_LOCALE, " +
						" T.PROXY, " +
						" T.CUSTOM_MANAGER, " +
						" T.SECOND_MANAGER, " +						
						" T.LOGIN_METHOD " +
						" ) " +
						" WHEN MATCHED THEN " +
						" UPDATE SET " +
						" U.STATUS = T.STATUS, " +						
						" U.USERNAME = T.USERNAME, " +
						" U.FIRSTNAME = T.FIRSTNAME, " +
						" U.LASTNAME = T.LASTNAME, " +
						" U.MI = T.MI, " +
						" U.GENDER = T.GENDER, " +
						" U.EMAIL = T.EMAIL, " +
						" U.MANAGER = T.MANAGER, " +
						" U.HR = T.HR, " +
						" U.DEPARTMENT = T.DEPARTMENT, " +
						" U.JOBCODE = T.JOBCODE, " +
						" U.DIVISION = T.DIVISION, " +
						" U.LOCATION = T.LOCATION, " +
						" U.TIMEZONE = T.TIMEZONE, " +
						" U.HIREDATE = T.HIREDATE, " +
						" U.EMPID = T.EMPID, " +
						" U.TITLE = T.TITLE, " +
						" U.BIZ_PHONE = T.BIZ_PHONE, " +
						" U.FAX = T.FAX, " +
						" U.ADDR1 = T.ADDR1, " +
						" U.ADDR2 = T.ADDR2, " +
						" U.CITY = T.CITY, " +
						" U.STATE = T.STATE, " +
						" U.ZIP = T.ZIP, " +
						" U.COUNTRY = T.COUNTRY, " +
						" U.REVIEW_FREQ = T.REVIEW_FREQ, " +
						" U.LAST_REVIEW_DATE = T.LAST_REVIEW_DATE, " +
						" U.CUSTOM01 = T.CUSTOM01, " +
						" U.CUSTOM02 = T.CUSTOM02, " +
						" U.CUSTOM03 = T.CUSTOM03, " +
						" U.CUSTOM04 = T.CUSTOM04, " +
						" U.CUSTOM05 = T.CUSTOM05, " +
						" U.CUSTOM06 = T.CUSTOM06, " +
						" U.CUSTOM07 = T.CUSTOM07, " +
						" U.CUSTOM08 = T.CUSTOM08, " +
						" U.CUSTOM09 = T.CUSTOM09, " +
						" U.CUSTOM10 = T.CUSTOM10, " +
						" U.CUSTOM11 = T.CUSTOM11, " +
						" U.CUSTOM12 = T.CUSTOM12, " +
						" U.CUSTOM13 = T.CUSTOM13, " +
						" U.CUSTOM14 = T.CUSTOM14, " +
						" U.CUSTOM15 = T.CUSTOM15, " +
						" U.MATRIX_MANAGER = T.MATRIX_MANAGER, " +
						" U.DEFAULT_LOCALE = T.DEFAULT_LOCALE, " +
						" U.PROXY = T.PROXY, " +
						" U.CUSTOM_MANAGER = T.CUSTOM_MANAGER, " +
						" U.SECOND_MANAGER = T.SECOND_MANAGER, " +
						" U.LOGIN_METHOD = T.LOGIN_METHOD, " +
						" U.LST_UPD_TST = NULL " ;
		
		
		simpleJdbcTemplate.update(sql);
		
		
		String sqlInactivate = " UPDATE BIZXEP_USER SET STATUS = 'inactive' WHERE LST_UPD_TST IS NOT NULL ";
				
		simpleJdbcTemplate.update(sqlInactivate);
		
		String sqlUpdateTst = " UPDATE BIZXEP_USER SET LST_UPD_TST = SYSDATE WHERE LST_UPD_TST IS NULL ";
				
		simpleJdbcTemplate.update(sqlUpdateTst);
		
	}

	
	
	
	
	
	private class MappedRowMapper implements RowMapper<MappedRow> {
		@Override
		public MappedRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			MappedRow mappedRow = new MappedRow();
			
			mappedRow.setData( new String[ resultColumnsBizxEp.size() ] );			
			
			mappedRow.setValue(0, rs.getString("STATUS"));
			mappedRow.setValue(1, rs.getString("USERID"));
			mappedRow.setValue(2, rs.getString("USERNAME"));
			mappedRow.setValue(3, rs.getString("FIRSTNAME"));
			mappedRow.setValue(4, rs.getString("LASTNAME"));
			mappedRow.setValue(5, rs.getString("MI"));
			mappedRow.setValue(6, rs.getString("GENDER"));
			mappedRow.setValue(7, rs.getString("EMAIL"));
			mappedRow.setValue(8, rs.getString("MANAGER"));
			mappedRow.setValue(9, rs.getString("HR"));
			mappedRow.setValue(10, rs.getString("DEPARTMENT"));
			mappedRow.setValue(11, rs.getString("JOBCODE"));
			mappedRow.setValue(12, rs.getString("DIVISION"));
			mappedRow.setValue(13, rs.getString("LOCATION"));
			mappedRow.setValue(14, rs.getString("TIMEZONE"));
			mappedRow.setValue(15, rs.getString("HIREDATE"));
			mappedRow.setValue(16, rs.getString("EMPID"));
			mappedRow.setValue(17, rs.getString("TITLE"));
			mappedRow.setValue(18, rs.getString("BIZ_PHONE"));
			mappedRow.setValue(19, rs.getString("FAX"));
			mappedRow.setValue(20, rs.getString("ADDR1"));
			mappedRow.setValue(21, rs.getString("ADDR2"));
			mappedRow.setValue(22, rs.getString("CITY"));
			mappedRow.setValue(23, rs.getString("STATE"));
			mappedRow.setValue(24, rs.getString("ZIP"));
			mappedRow.setValue(25, rs.getString("COUNTRY"));
			mappedRow.setValue(26, rs.getString("REVIEW_FREQ"));
			mappedRow.setValue(27, rs.getString("LAST_REVIEW_DATE"));
			mappedRow.setValue(28, rs.getString("CUSTOM01"));
			mappedRow.setValue(29, rs.getString("CUSTOM02"));
			mappedRow.setValue(30, rs.getString("CUSTOM03"));
			mappedRow.setValue(31, rs.getString("CUSTOM04"));
			mappedRow.setValue(32, rs.getString("CUSTOM05"));
			mappedRow.setValue(33, rs.getString("CUSTOM06"));
			mappedRow.setValue(34, rs.getString("CUSTOM07"));
			mappedRow.setValue(35, rs.getString("CUSTOM08"));
			mappedRow.setValue(36, rs.getString("CUSTOM09"));
			mappedRow.setValue(37, rs.getString("CUSTOM10"));
			mappedRow.setValue(38, rs.getString("CUSTOM11"));
			mappedRow.setValue(39, rs.getString("CUSTOM12"));
			mappedRow.setValue(40, rs.getString("CUSTOM13"));
			mappedRow.setValue(41, rs.getString("CUSTOM14"));
			mappedRow.setValue(42, rs.getString("CUSTOM15"));
			mappedRow.setValue(43, rs.getString("MATRIX_MANAGER"));
			mappedRow.setValue(44, rs.getString("DEFAULT_LOCALE"));
			mappedRow.setValue(45, rs.getString("PROXY"));
			mappedRow.setValue(46, rs.getString("CUSTOM_MANAGER"));
			mappedRow.setValue(47, rs.getString("SECOND_MANAGER"));
			mappedRow.setValue(48, rs.getString("LOGIN_METHOD"));
					
						
			return mappedRow;
		}
	}
	
	
	
	/**
	 * 
	 * Return all rows from BIZEXEP_USER
	 * 
	 * @return
	 */
	public List<MappedRow> getBizxEpUserData() {
				
		return simpleJdbcTemplate.query(sqlBizxEpUser, new MappedRowMapper());
		
	}


	
	
	
		
}
