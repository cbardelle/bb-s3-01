package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class LegalEntityCf01Mapper implements IMapper {

	
	private final String DEFAULT_LE_4_SSC = "SSC";
	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
				
		
		
		
		if (  "Y".equalsIgnoreCase( sourceRow.getRoleScc() ) == true ){
			mappedCol.setValue( DEFAULT_LE_4_SSC );
			
		} else {
			
			StringBuffer sb = new StringBuffer();

			sb.append( sourceRow.getHostLegalEntDesc() );
			if (StringUtils.isNotEmpty( sourceRow.getHostLegalEntCode() )){
				sb.append(" (").append( sourceRow.getHostLegalEntCode() ).append(")");
			}
				
			mappedCol.setValue( sb.toString() );
		
		}		
		return mappedCol;

	}

	
}
