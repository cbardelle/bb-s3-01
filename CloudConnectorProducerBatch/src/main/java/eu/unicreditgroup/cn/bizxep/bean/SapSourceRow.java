package eu.unicreditgroup.cn.bizxep.bean;

import org.apache.commons.lang.StringUtils;


public class SapSourceRow {
	
	
	
	
	
	String sourceFileName;
	
	
	private boolean shutDownRow = false;
	
	
	private boolean valid;

	
	// roles for th BizX EP users (Y/N values)
	
	/**
	 * Line managers (SAP data)  
	 */
	private String roleLineManager;
	
	/**
	 * HRBP users (SAP data)
	 */
	private String roleHrbp;
	
	/**
	 * Role Senior HR (definition in  BIZXEP_ROLE_RSA)
	 */
	private String roleSeniorHr;
	
	/**
	 * Role Recruiting Coordinator (definition in BIZXEP_ROLE_RSA)
	 * 
	 */
	private String roleRecruitingCoordinator;
	
	
	/**
	 * Role Admin (definition in BIZXEP_ROLE_RSA)
	 * 
	 */
	private String roleAdmin;
	
	/**
	 * Role C (all users from table BIZXEP_ROLE_C will have this role)
	 * 
	 */
	private String roleScc;
	
	
	
	private String userStatusDesc;
	private String userId;
	private String firstName;
	private String lastName;
	private String gender;
	private String officeEmail;
	private String lineManagerId;
	private String hrbpId;
	private String globalJobDesc;
	private String globalJobAbbr;
	private String divisionDesc;
	private String officeCountryDesc;
	private String hireDate;
	private String userOfficeDesc;
	private String officeFixPhone;
	private String officeAddr;
	private String officeCity;
	private String officeProvince;
	private String officePostalCode;
	private String dateOfBirth;
	private String hostLegalEntDesc;
	private String hostLegalEntCode;
	private String userSapId;
	private String globalJobBandDesc;
	private String competenceLineDesc;
	private String lineManager;
	private String hrbp;
	private String fakeHrbp;
	
	
	
	
	
	
	
	public String getFakeHrbp() {
		return fakeHrbp;
	}

	public void setFakeHrbp(String fakeHrbp) {
		this.fakeHrbp = fakeHrbp;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

	public String getRoleLineManager() {
		return roleLineManager;
	}

	public void setRoleLineManager(String roleLineManager) {
		this.roleLineManager = roleLineManager;
	}

	public String getRoleHrbp() {
		return roleHrbp;
	}

	public void setRoleHrbp(String roleHrbp) {
		this.roleHrbp = roleHrbp;
	}

	public String getRoleSeniorHr() {
		return roleSeniorHr;
	}


	public void setRoleSeniorHr(String roleSeniorHr) {
		this.roleSeniorHr = roleSeniorHr;
	}


	public String getRoleRecruitingCoordinator() {
		return roleRecruitingCoordinator;
	}


	public void setRoleRecruitingCoordinator(String roleRecruitingCoordinator) {
		this.roleRecruitingCoordinator = roleRecruitingCoordinator;
	}


	public String getRoleScc() {
		return roleScc;
	}


	public void setRoleScc(String roleScc) {
		this.roleScc = roleScc;
	}


	public String getRoleAdmin() {
		return roleAdmin;
	}


	public void setRoleAdmin(String roleAdmin) {
		this.roleAdmin = roleAdmin;
	}

	public String getUserStatusDesc() {
		return userStatusDesc;
	}

	public void setUserStatusDesc(String userStatusDesc) {
		this.userStatusDesc = userStatusDesc;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOfficeEmail() {
		return officeEmail;
	}

	public void setOfficeEmail(String officeEmail) {
		this.officeEmail = officeEmail;
	}

	public String getLineManagerId() {
		return lineManagerId;
	}

	public void setLineManagerId(String lineManagerId) {
		this.lineManagerId = lineManagerId;
	}

	public String getHrbpId() {
		return hrbpId;
	}

	public void setHrbpId(String hrbpId) {
		this.hrbpId = hrbpId;
	}

	public String getGlobalJobDesc() {
		return globalJobDesc;
	}

	public void setGlobalJobDesc(String globalJobDesc) {
		this.globalJobDesc = globalJobDesc;
	}

	public String getGlobalJobAbbr() {
		return globalJobAbbr;
	}

	public void setGlobalJobAbbr(String globalJobAbbr) {
		this.globalJobAbbr = globalJobAbbr;
	}

	public String getDivisionDesc() {
		return divisionDesc;
	}

	public void setDivisionDesc(String divisionDesc) {
		this.divisionDesc = divisionDesc;
	}

	public String getOfficeCountryDesc() {
		return officeCountryDesc;
	}

	public void setOfficeCountryDesc(String officeCountryDesc) {
		this.officeCountryDesc = officeCountryDesc;
	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

	public String getUserOfficeDesc() {
		return userOfficeDesc;
	}

	public void setUserOfficeDesc(String userOfficeDesc) {
		this.userOfficeDesc = userOfficeDesc;
	}

	public String getOfficeFixPhone() {
		return officeFixPhone;
	}

	public void setOfficeFixPhone(String officeFixPhone) {
		this.officeFixPhone = officeFixPhone;
	}

	public String getOfficeAddr() {
		return officeAddr;
	}

	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr;
	}

	public String getOfficeCity() {
		return officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getOfficeProvince() {
		return officeProvince;
	}

	public void setOfficeProvince(String officeProvince) {
		this.officeProvince = officeProvince;
	}

	public String getOfficePostalCode() {
		return officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	
	
	public String getHostLegalEntDesc() {
		return hostLegalEntDesc;
	}

	public void setHostLegalEntDesc(String hostLegalEntDesc) {
		this.hostLegalEntDesc = hostLegalEntDesc;
	}

	public String getHostLegalEntCode() {
		return hostLegalEntCode;
	}

	public void setHostLegalEntCode(String hostLegalEntCode) {
		this.hostLegalEntCode = hostLegalEntCode;
	}

	public String getUserSapId() {
		return userSapId;
	}

	public void setUserSapId(String userSapId) {
		this.userSapId = userSapId;
	}

	public String getGlobalJobBandDesc() {
		return globalJobBandDesc;
	}

	public void setGlobalJobBandDesc(String globalJobBandDesc) {
		this.globalJobBandDesc = globalJobBandDesc;
	}

	public String getCompetenceLineDesc() {
		return competenceLineDesc;
	}

	public void setCompetenceLineDesc(String competenceLineDesc) {
		this.competenceLineDesc = competenceLineDesc;
	}

	public String getLineManager() {
		return lineManager;
	}

	public void setLineManager(String lineManager) {
		this.lineManager = lineManager;
	}

	public String getHrbp() {
		return hrbp;
	}

	public void setHrbp(String hrbp) {
		this.hrbp = hrbp;
	}

	public boolean isShutDownRow() {
		return shutDownRow;
	}

	public void setShutDownRow(boolean shutDownRow) {
		this.shutDownRow = shutDownRow;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	
	
	
	

}
