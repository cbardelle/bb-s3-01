package eu.unicreditgroup.cn.bizxep.util;

import java.util.ArrayList;
import java.util.List;

import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.dao.SourceDao;


public class ResultTableWriter {

	//TODO Spring config
	public SourceDao sourceDao;
		
	private List<MappedRow>  buffer = new ArrayList<MappedRow>();
	
	private final int bufferSize = 1024;
			
	
	public void writeResultRow(MappedRow mappedRow) throws Exception {
		
		buffer.add(mappedRow);
		
		if (  buffer.size() > bufferSize ){	
			
			// write the buffer
			sourceDao.writeResultTmpTable( buffer );
			
			// clean the buffer
			buffer.clear();
		}
		
	}

	public void flushData() throws Exception {
		
		if ( buffer.size() > 0 ){
			sourceDao.writeResultTmpTable( buffer );
			
			buffer.clear();
		}		
		
	}

	public SourceDao getSourceDao() {
		return sourceDao;
	}

	public void setSourceDao(SourceDao sourceDao) {
		this.sourceDao = sourceDao;
	}
	
		
	
	

}
