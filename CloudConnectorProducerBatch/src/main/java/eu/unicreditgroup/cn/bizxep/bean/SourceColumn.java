package eu.unicreditgroup.cn.bizxep.bean;

public class SourceColumn {
	
	private String srcColName;
	
	private String srcColDesc;
	
	private int srcColIndex;
	
	private String type;
	
	private int length;
	
	private boolean mandatory = false;
	

	public String getSrcColName() {
		return srcColName;
	}

	public void setSrcColName(String srcColName) {
		this.srcColName = srcColName;
	}

	public int getSrcColIndex() {
		return srcColIndex;
	}

	public void setSrcColIndex(int srcColIndex) {
		this.srcColIndex = srcColIndex;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getSrcColDesc() {
		return srcColDesc;
	}

	public void setSrcColDesc(String srcColDesc) {
		this.srcColDesc = srcColDesc;
	}
	

}
