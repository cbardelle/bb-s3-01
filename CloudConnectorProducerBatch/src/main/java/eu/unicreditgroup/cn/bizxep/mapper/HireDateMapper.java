package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class HireDateMapper implements IMapper {
	
	private final String DEFAULT_HIRE_DATE_SSC = "01/01/1950";

	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();
		
		
		if (  "Y".equalsIgnoreCase( sourceRow.getRoleScc() ) == true ){ 
			mappedCol.setValue( DEFAULT_HIRE_DATE_SSC );
		} else {
			mappedCol.setValue(  StringUtils.replace(sourceRow.getHireDate(), ".", "/")  );
		}
		
		return mappedCol;

	}

	
}
