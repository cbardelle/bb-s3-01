package eu.unicreditgroup.cn.bizxep.bean;

public class MappedColumn {
	
	private String value;
	
	private String errorMsg = null;
	
	
	
	public String getValue() {
		return value;
	}
	
	public MappedColumn(){}
	
	public MappedColumn(String value){
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	
		
}
