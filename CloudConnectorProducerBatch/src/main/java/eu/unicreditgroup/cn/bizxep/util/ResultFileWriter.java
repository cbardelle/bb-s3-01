package eu.unicreditgroup.cn.bizxep.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.config.BizxEpColumn;
import eu.unicreditgroup.utils.UnicodeUtil;

public class ResultFileWriter {
		
	private final String columnDelimiter;
	
	private final String lineEnd;	
	
	private File resFile = null;
	
	private BufferedWriter resFileWriter = null;
	
	private StringBuilder sb;
	
		
	
			
	public ResultFileWriter(String columnDelimiter, String lineEnd,	String fileName) throws Exception {		
		this.columnDelimiter = columnDelimiter;
		this.lineEnd = lineEnd;					
		resFile = new File(fileName);		
		resFileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resFile), UnicodeUtil.UTF8_FILE_ENCODING));				
	}

	public void writeHeader(List<BizxEpColumn> columns) throws Exception{
		
				
		sb = new StringBuilder();		
		//write header
		boolean once = true;
		for(BizxEpColumn resCol : columns)
		{	
			if (once == true){
				once = false;
			} else {					
				sb.append(columnDelimiter);					
			}
			sb.append("\"").append(resCol.getResColName()).append("\"");
		
		}
		sb.append(lineEnd);
						
		resFileWriter.write( UnicodeUtil.convert( sb.toString().getBytes(), UnicodeUtil.UTF8_FILE_ENCODING) );
		
	}

	public void writeHeaderDescriptions(List<BizxEpColumn> columns) throws Exception{
		
		sb = new StringBuilder();		
		//write header
		boolean once = true;
		for(BizxEpColumn resCol : columns)
		{	
			if (once == true){
				once = false;
			} else {					
				sb.append(columnDelimiter);					
			}
			sb.append("\"").append(resCol.getResColDesc()).append("\"");
		
		}
		sb.append(lineEnd);
		
		//resFileWriter.write( UnicodeUtil.convert( sb.toString().getBytes(), UnicodeUtil.UTF8_FILE_ENCODING) );
		resFileWriter.write(sb.toString());		
	}

	
	
	public void writeResultRow(String[] rowData) throws Exception{
		sb = new StringBuilder();				
		for(int i = 0 ; i< rowData.length ; i++){
			if (i > 0){			
				sb.append(columnDelimiter);									
			}			
			String value = rowData[i];
			if (StringUtils.isEmpty( rowData[i] )){
				value = "";
			} 			
			sb.append("\"").append(value).append("\"");						
		}
		sb.append(lineEnd);				
		resFileWriter.write(sb.toString());
		
		//UTF-8 with BOM required for some reasons 
		//resFileWriter.write( UnicodeUtil.convert( sb.toString().getBytes(), UnicodeUtil.UTF8_FILE_ENCODING) );
		
		
	}
		
	public void close() throws Exception{
		if (resFileWriter != null){
			resFileWriter.close();
		}
	}
	
	
}
