package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class DepartmentMapper implements IMapper {

	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
		
		if (  "Y".equalsIgnoreCase( sourceRow.getRoleScc() ) == false ){
			
			mappedCol.setValue( sourceRow.getCompetenceLineDesc());
				
		}
		
		return mappedCol;

	}

	
}
