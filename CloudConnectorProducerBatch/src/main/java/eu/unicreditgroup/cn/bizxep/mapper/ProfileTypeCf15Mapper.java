package eu.unicreditgroup.cn.bizxep.mapper;

import java.util.HashMap;
import java.util.Map;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;
import eu.unicreditgroup.mailer.ReportDataBean;


/**
 *  
 * @author UV00074
 *
 */
public class ProfileTypeCf15Mapper implements IMapper {
	
	private static Map<Integer, String> roles = new HashMap();
	
	private ReportDataBean reportDataBean; 
	
	static {
		
				
		roles.put(1, "RCM_M");
		roles.put(2, "RCM_H");
		roles.put(3, "RCM_MH");
		roles.put(4, "RCM_S");
		roles.put(5, "RCM_MS");
		roles.put(6, "RCM_HS");
		roles.put(7, "RCM_MHS");
		roles.put(8, "RCM_R");
		roles.put(9, "RCM_MR");
		roles.put(10, "RCM_HR");
		roles.put(11, "RCM_MHR");
		roles.put(12, "RCM_SR");
		roles.put(13, "RCM_MSR");
		roles.put(14, "RCM_HSR");
		roles.put(15, "RCM_MHSR");
		roles.put(16, "RCM_C");
		roles.put(32, "RCM_A");		
		roles.put(33, "RCM_MA");		
		roles.put(34, "RCM_HA");
		roles.put(35, "RCM_MHA");
		roles.put(36, "RCM_SA");
		roles.put(37, "RCM_MSA");
		roles.put(38, "RCM_HSA");
		roles.put(39, "RCM_MHSA");
		roles.put(40, "RCM_RA");
		roles.put(41, "RCM_MRA");
		roles.put(42, "RCM_HRA");
		roles.put(43, "RCM_MHRA");
		roles.put(44, "RCM_SRA");
		roles.put(45, "RCM_MSRA");
		roles.put(46, "RCM_HSRA");
		roles.put(47, "RCM_MHSRA");
	}
	
	
	
	private static int ROLE_LM = 1;
	private static int ROLE_HRBP = 2;
	private static int ROLE_SENIOR_HR = 4;
	private static int ROLE_RECRUTING_COORD = 8;
	private static int ROLE_SSC = 16;
	private static int ROLE_ADMIN = 32; 
	
	
	
	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		int roleCode = 0;
		
		MappedColumn mappedCol = new MappedColumn();
		
		if ( "Y".equalsIgnoreCase(sourceRow.getLineManager())  ){			
			roleCode += ROLE_LM;			
		} 
		if ("Y".equalsIgnoreCase( sourceRow.getHrbp() ) || "Y".equalsIgnoreCase( sourceRow.getFakeHrbp() )) {
			roleCode += ROLE_HRBP;			
		}
		if ("Y".equalsIgnoreCase( sourceRow.getRoleSeniorHr() )) {
			roleCode += ROLE_SENIOR_HR;			
		}
		if ("Y".equalsIgnoreCase( sourceRow.getRoleRecruitingCoordinator() )) {
			roleCode += ROLE_RECRUTING_COORD;
		}
		if ("Y".equalsIgnoreCase( sourceRow.getRoleScc())) {
			roleCode += ROLE_SSC;			
		}
		if ("Y".equalsIgnoreCase( sourceRow.getRoleAdmin() )) {
			roleCode += ROLE_ADMIN;
		} 
				

		if (roles.containsKey( roleCode ) == false){
			mappedCol.setErrorMsg("Not exisiting roles combination: " + roleCode);
			
			reportDataBean.addBizxEpRoleExceptions( roleCode );
			
		} else {
			
			mappedCol.setValue( roles.get(roleCode) );
			
		}
		
		return mappedCol;
		
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
}
