package eu.unicreditgroup.cn.bizxep.validator;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;

public class NotNullValidator implements IValidator {

	@Override
	public boolean validate(String value, String[] srcValues, Integer resColIndex, SapSourceRow sourceRow) {
	
		return (StringUtils.isNotEmpty(value));
	}

	@Override
	public boolean isRowRejectable() {
		return false;
	}


}
