package eu.unicreditgroup.cn.bizxep.step;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import eu.unicreditgroup.batch.step.BatchStep;
import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.queue.BizxEpDataConsumer;
import eu.unicreditgroup.cn.bizxep.queue.BizxRowsQueueUtil;
import eu.unicreditgroup.cn.bizxep.queue.SourceDataConsumer;
import eu.unicreditgroup.cn.bizxep.queue.SourceDataProducer;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.AppContext;
import eu.unicreditgroup.utils.BackupUtil;

public class StepGenerateFileForBizxEp implements BatchStep {
	
	Logger logger = Logger.getLogger(StepGenerateFileForBizxEp.class);	
	
	/**
	 * 
	 * This producer will provide data for users from 
	 * HR_CONN_SAP_SRC and BIZXEP_
	 * 
	 *  
	 */
	private SourceDataProducer sourceDataProducer;
	
	private ConfigParams configParams;
	
	private BizxEpDataConsumer bizxEpDataConsumer;
	
	private BlockingQueue<MappedRow> mappedEpRowsQueue;
	
	private BlockingQueue<Exception> bizxEpExceptionsQueue;
	
	private ReportDataBean reportDataBean; 
	
	private FTPUploader resultUploader;
	

	@Override
	public void execute() throws Exception{
		logger.debug("Start of execution of step generate file for BizX EP");
		
		
		// TODO backup the file
		/*
		try {
			BackupUtil.backupFile(configParams.getResultFileFullPathBizxEP());
		} catch (Exception e2) {
			logger.error("Failed to backup the result file for BizX EP!");
			bizxEpExceptionsQueue.add(e2);
		}
		*/
		
		
		ApplicationContext ctx = AppContext.getApplicationContext();
		
		
		Thread sourceDataProducerThread = new Thread(sourceDataProducer);		
		sourceDataProducerThread.start();
		
		logger.debug("Source data (SAP & SSC) producer thread started.");


		
		logger.debug("Start of source data queue consumer creation.");
		
		//List<Thread> sourceDataConsumerThreads = new ArrayList<Thread>();
		//for (int i = 0; i < configParams.getSourceDataConsumerThreadsNumberEP(); i++) {
			//logger.debug(String.format("Creating source data queue consumer number %d.", i));
			logger.debug(String.format("Creating source data queue consumer number %d.", 1));
			SourceDataConsumer sourceDataconsumer = ctx.getBean(SourceDataConsumer.class);
			//consumer.setConsumerNumber(i);
			sourceDataconsumer.setConsumerNumber(1);			
			Thread sourceDataConsumerThread = new Thread(sourceDataconsumer);
			//sourceDataConsumerThreads.add(consumerThread);
			sourceDataConsumerThread.start();
		//}
		logger.debug("Source data queue consumer created & started.");
		
		
		// backup the data 
		bizxEpDataConsumer.cleanAndBackupData();
		
		
		// get mapped BizX EP data and write to temporary table
		Thread bizxEpDataConsumerThread = new Thread(bizxEpDataConsumer);
		bizxEpDataConsumerThread.start();
		logger.debug("BizX EP rows consumer thread started.");
	
		
		//for (Thread thread : sourceDataConsumerThreads) {
		try {
			//thread.join();
			sourceDataConsumerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for source data consumer threads.");
			bizxEpExceptionsQueue.add(e);
		}
		//}		
		
		try {
			if (!BizxRowsQueueUtil.isShutdownRow(mappedEpRowsQueue.peek())){
				mappedEpRowsQueue.put(BizxRowsQueueUtil.generateShutdownRow());
			}
		} catch (InterruptedException e1) {
			logger.error("Exception occured : " + e1);
		}
	
			
		
		try {
			sourceDataProducerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of source data producer thread.");
			bizxEpExceptionsQueue.add(e);
		}
				
		
		try {
			bizxEpDataConsumerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of BizX EP data consumer thread.");
			bizxEpExceptionsQueue.add(e);
		}
		
		
		bizxEpDataConsumer.bizxResultTableWriterFlushData();
		
		// at this point the table BIZXEP_USER_TMP is loaded with the actual data
		logger.debug("Merging new data into the last data");
		bizxEpDataConsumer.mergeNewUsers();
		
		
		// the data in the table BIZXEP_USER is updated the file can be finally generated
		bizxEpDataConsumer.generateResultFile();

		
		if ( StringUtils.isNotEmpty( configParams.getResultFtpPathPathBizxEp() ) ){			
			
			if (reportDataBean.getFailedFastException() == null ){
				try {
					
					File resultFile = new File(configParams.getResultFileFullPathBizxEp());
					
					if ( !resultFile.exists() || resultFile.length() == 0){
						
						throw new Exception("The result file doesn't exist or is empty!");
					}
					
					logger.debug("Starting upload of result file ...");
					
					resultUploader.setDirectory(configParams.getResultFtpPathPathBizxEp() );
					
					resultUploader.uploadFile(resultFile);
										
					
					logger.debug("Result file uploaded.");
				} catch (Exception e) {
					reportDataBean.setFailedFastException(new Exception("Failed to upload the result file",e));					
					bizxEpExceptionsQueue.add(e);
				}
			}
			
		} else {
			logger.debug("Upload of result file was skipped.");
		}
		logger.debug("End of execution of step generate file for BizX EP");		
	}
	

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public SourceDataProducer getSourceDataProducer() {
		return sourceDataProducer;
	}

	public void setSourceDataProducer(SourceDataProducer sourceDataProducer) {
		this.sourceDataProducer = sourceDataProducer;
	}

	public BizxEpDataConsumer getBizxEpDataConsumer() {
		return bizxEpDataConsumer;
	}

	public void setBizxEpDataConsumer(BizxEpDataConsumer bizxEpDataConsumer) {
		this.bizxEpDataConsumer = bizxEpDataConsumer;
	}

	public BlockingQueue<MappedRow> getMappedEpRowsQueue() {
		return mappedEpRowsQueue;
	}


	public void setMappedEpRowsQueue(BlockingQueue<MappedRow> mappedEpRowsQueue) {
		this.mappedEpRowsQueue = mappedEpRowsQueue;
	}


	public BlockingQueue<Exception> getBizxEpExceptionsQueue() {
		return bizxEpExceptionsQueue;
	}


	public void setBizxEpExceptionsQueue(
			BlockingQueue<Exception> bizxEpExceptionsQueue) {
		this.bizxEpExceptionsQueue = bizxEpExceptionsQueue;
	}



	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}



	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}



	public FTPUploader getResultUploader() {
		return resultUploader;
	}



	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}


	public void addException(Exception e)  {
		try {
			bizxEpExceptionsQueue.put(e);
		} catch (InterruptedException e1) {
			logger.debug(e1);
		}
		
	}

}
