package eu.unicreditgroup.cn.bizxep.queue;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;
import eu.unicreditgroup.cn.bizxep.mapper.BizxEpMapper;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.cn.bizxep.queue.SourceRowQueueUtil;

public class SourceDataConsumer implements Runnable{
	
	private ConfigParams configParams;
	
	private BlockingQueue<SapSourceRow> sapDataQueue;
	
	private BlockingQueue<MappedRow> mappedEpRowsQueue;
	
	private BlockingQueue<Exception> bizxEpExceptionsQueue;	
	
	private ReportDataBean reportDataBean;
	
	private int consumerNumber;
	
	private BizxEpMapper bizxEpMapper;
	
	private static final Logger logger = Logger.getLogger(SourceDataConsumer.class);
	
	
	
	@Override	
	public void run() {
		
		SapSourceRow sourceRow = new SapSourceRow();
		try {	
			
			while(true){
				Thread.yield();
				
				sourceRow = sapDataQueue.take();
				
				if (SourceRowQueueUtil.isShutdownRow(sourceRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					sapDataQueue.put(sourceRow);
					
					return;
				}
				
				MappedRow mappedRow = bizxEpMapper.processRow(sourceRow, sourceRow.getUserId());
			
				if ( mappedRow.getValid() == true )	{					
					//reportDataBean.incBizxEpMappedRowsCount();					
					mappedEpRowsQueue.put(mappedRow);	
					
				} else {					
					//reportDataBean.incSourceFileBizxEpInvalidRowsCount();					
					String errMsg = "Mapped row for userID: " + sourceRow.getUserId() + " is invalid and won't be written to the result file.";					
					bizxEpExceptionsQueue.add( new Exception(errMsg) );			
					
					logger.error( errMsg );					
				}
							
			}
			
		} catch (Exception ex) {
			logger.debug(String.format("[Cons %d] Exception during processing soure row for userID '%s'.", consumerNumber, sourceRow.getUserId()), ex);			
			bizxEpExceptionsQueue.add(ex);
		}
		
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}

	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}




	public BlockingQueue<SapSourceRow> getSapDataQueue() {
		return sapDataQueue;
	}


	public void setSapDataQueue(BlockingQueue<SapSourceRow> sapDataQueue) {
		this.sapDataQueue = sapDataQueue;
	}


	public BlockingQueue<MappedRow> getMappedEpRowsQueue() {
		return mappedEpRowsQueue;
	}


	public void setMappedEpRowsQueue(BlockingQueue<MappedRow> mappedEpRowsQueue) {
		this.mappedEpRowsQueue = mappedEpRowsQueue;
	}


	public BlockingQueue<Exception> getBizxEpExceptionsQueue() {
		return bizxEpExceptionsQueue;
	}


	public void setBizxEpExceptionsQueue(
			BlockingQueue<Exception> bizxEpExceptionsQueue) {
		this.bizxEpExceptionsQueue = bizxEpExceptionsQueue;
	}


	public BizxEpMapper getBizxEpMapper() {
		return bizxEpMapper;
	}


	public void setBizxEpMapper(BizxEpMapper bizxEpMapper) {
		this.bizxEpMapper = bizxEpMapper;
	}
	
	

	

	
}
