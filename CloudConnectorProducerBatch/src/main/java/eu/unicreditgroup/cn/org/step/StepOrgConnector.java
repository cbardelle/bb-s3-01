package eu.unicreditgroup.cn.org.step;

import java.io.File;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.step.BatchStep;
import eu.unicreditgroup.cn.org.bean.OrgData;
import eu.unicreditgroup.cn.org.dao.OrgDao;
import eu.unicreditgroup.cn.org.dao.UniReportUploader;
import eu.unicreditgroup.cn.org.util.OrgConnFileWriter;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.BackupUtil;

public class StepOrgConnector implements BatchStep {
			
	
	
	private ConfigParams configParams;
	
	private FTPUploader resultUploader;
	
	private ReportDataBean reportDataBean;
	
	Logger logger = Logger.getLogger(StepOrgConnector.class);	
	
	private OrgConnFileWriter orgConnFileWriter;
	
	private UniReportUploader uniReportUploader;
	
	private OrgDao orgDao;
	
	
	@Override
	public void execute() throws Exception {
		
		logger.debug("Start of execution of step generate file for Organization connector");
		
		
		// load table ORG_CONN_PA_ORG
		try {
		uniReportUploader.uploadUniOrgListReport();
		} catch (Exception e) {
			logger.error("Failed to upload table ORG_CONN_PA_ORG!");
			reportDataBean.addOrgExceptionWithLimit(e);
			reportDataBean.setFailedFastException(e);
			throw e;
		}
		
		
		try {
			BackupUtil.backupFile(configParams.getResultFileFullPathOrgConn());
		} catch (Exception e2) {
			logger.error("Failed to backup the result file for Organization connector!");
			reportDataBean.addOrgExceptionWithLimit(e2);
		}
		
		List<OrgData> orgs = orgDao.getOrgList();
				
		if (orgs == null || orgs.isEmpty() ){
			
			throw new Exception("No organizations found! The table HR_ORG_STRUCTURE seems to be empty!");
			
		}
		
		reportDataBean.setOrgRowTotal( orgs.size() );
		
		
		// write the result file 
		orgConnFileWriter.writeResultFile(orgs);
		
		
	    if ( StringUtils.isNotEmpty( configParams.getResultFtpPathOrgConn() ) ){			
			
			if (reportDataBean.getFailedFastException() == null ){
				try {
					
					File resultFile = new File(configParams.getResultFileFullPathOrgConn() );
					
					if ( !resultFile.exists() || resultFile.length() == 0){
						
						throw new Exception("The result file for Organization connector doesn't exist or is empty!");
						
					}
					
					logger.debug("Starting upload of result file for Organization connector...");
										
					resultUploader.uploadFile(resultFile);
															
					logger.debug("Result file uploaded.");
					
				} catch (Exception e) {
					reportDataBean.setFailedFastException(new Exception("Failed to upload the result file for Organization connector",e));					
					reportDataBean.addOrgExceptionWithLimit(e);
					logger.error("Failed to upload the result file for Organization connector", e);
				}
			}
			
		} else {
			logger.debug("Upload of result file for Organization connector was skipped.");
		}
	 
	    logger.debug("End of execution of step generate file for Organization connector");
	}
	

	/*
	public void addException(Exception e) {
		try {
			orgExceptionsQueue.put(e);
		} catch (InterruptedException e1) {
			logger.debug(e1);
		}		
	}*/


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public FTPUploader getResultUploader() {
		return resultUploader;
	}


	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	

	public OrgConnFileWriter getOrgConnFileWriter() {
		return orgConnFileWriter;
	}

	public void setOrgConnFileWriter(OrgConnFileWriter orgConnFileWriter) {
		this.orgConnFileWriter = orgConnFileWriter;
	}

	public OrgDao getOrgDao() {
		return orgDao;
	}

	public void setOrgDao(OrgDao orgDao) {
		this.orgDao = orgDao;
	}

	public UniReportUploader getUniReportUploader() {
		return uniReportUploader;
	}

	public void setUniReportUploader(UniReportUploader uniReportUploader) {
		this.uniReportUploader = uniReportUploader;
	}
	
	
}
