package eu.unicreditgroup.cn.org.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import eu.unicreditgroup.cn.org.bean.OrgData;



public class OrgDao extends SimpleJdbcDaoSupport {

	private final String DEFAULT_PARENT_OU = "'UNICREDIT'";
	private final String DEFAULT_PARENT_OU_4_REMOVED_OU = "'REMOVED_OU'";
	
	//private final String DEFAULT_TREE_ROOT_DESCR = "'Root OU'";

	// private final String orgsSql =
	// "SELECT DISTINCT OU_SAPCODE AS ORG_ID, ORG_SHORT AS ORG_ABBR, ORG_LONG AS ORG_DESC, OUFATHER_H AS ORG_ID_PARENT FROM HR_ORG_STRUCTURE";


	/**
	 * This is the main query which gets the data for the Org Connector
	 * The result of the query contains a complete and actual list of organizations, their parent org and descriptions.
	 * The result combines data from HR_CONN_RES_FILE (SAP user data) and HR_ORG_STRUCTURE tables. 
	 * Note 1. Not all OUs are present in HR_CONN_RES_FILE and there are many empty (with no users) OUs in hierarchy.  
	 * Some OU appears to be only logic structure and there have never been assigned with users (like tree roots).
	 * Note 2. It's important to send a complete list of OUs because OU with non existing parent OU won't be updated or created. 
	 * 
	 */
	private final String orgsSql = 
			/* If description is available in SAP user data it will be used  
			 * otherwise the descriptions is taken from HR_ORG_STRCTURE table.
			 * In that case for non UBIS OU the first four abbreviation code is replaced with org code.
			 * (The same logic which is applied to SAP user data.)
			 * 
			 */
			
			//TODO check if a this select is actually unique
			//" SELECT DISTINCT JOB_LOC_ID AS ORG_ID, JOB_LOC_DSCR AS ORG_DESC  FROM HR_CONN_RES_FILE "
			
			" WITH D AS "
			+ " ( "
			+ " SELECT DISTINCT JOB_LOC_ID AS ORG_ID, JOB_LOC_DSCR AS ORG_DESC  FROM HR_CONN_RES_FILE "
			+ " UNION "
			+ " SELECT "
			+ "   OU_SAPCODE AS ORG_ID, "
			+ "   CASE "
			+ "       WHEN BUKRS IN ( SELECT DISTINCT ORG_ID FROM HR_CONN_ORG_LST WHERE ORG_DESC LIKE 'UBIS%' ) THEN TO_CHAR(ORG_SHORT || '-' || ORG_LONG) "
			+ "       ELSE TO_CHAR(BUKRS || SUBSTR(ORG_SHORT, 5) || '-' || ORG_LONG) "
			+ "   END AS ORG_DESC "
			+ "   FROM HR_ORG_STRUCTURE  "
			+ "   WHERE OU_SAPCODE NOT IN (SELECT DISTINCT JOB_LOC_ID FROM HR_CONN_RES_FILE WHERE JOB_LOC_ID IS NOT NULL) "			
			+ " UNION " //Descriptions for removed OU but still present in LMS
			+ " SELECT ORG_ID, ORG_DESC FROM ORG_CONN_PA_ORG "
			+ " WHERE "
			+ " ORG_ID NOT IN (SELECT DISTINCT JOB_LOC_ID FROM HR_CONN_RES_FILE WHERE JOB_LOC_ID IS NOT NULL) " // OU not present in SAP data 
			+ " AND ORG_ID NOT IN ( SELECT OU_SAPCODE FROM HR_ORG_STRUCTURE ) " // and OU not present in Org Strucutre
			+ " ), " 			
			+ /* LIST OF ALL EXISITING FATHERS */ 
			" EF AS ( SELECT OU_SAPCODE FROM HR_ORG_STRUCTURE S WHERE OU_SAPCODE IN (SELECT OUFATHER_H FROM HR_ORG_STRUCTURE) ) "
			+ " SELECT " /* OFFICES PRESENT IN THE LMS WHICH HAVE PARENT ORG */
			+ " PO.ORG_ID, "
			+ " ( SELECT D.ORG_DESC FROM D WHERE D.ORG_ID = PO.ORG_ID ) ORG_DESC, "
			+ " NVL( (SELECT EF.OU_SAPCODE FROM EF WHERE EF.OU_SAPCODE = OS.OUFATHER_H) , " + DEFAULT_PARENT_OU + " ) AS ORG_PARENT_ID " //OU with non exisiting parents will be put directly under UNICREDIT  			
			+ " FROM ORG_CONN_PA_ORG PO, HR_ORG_STRUCTURE OS "
			+ " WHERE OS.OU_SAPCODE = PO.ORG_ID "
			+ " AND PO.ORG_ID IS NOT NULL "
			+ " AND PO.ORG_ID <> '00000000' "
			+ " UNION "
			+ " SELECT DISTINCT " /* THE REST OF OFFICES NOT PRESENT IN LMS */
			+ " TO_CHAR(OS.OU_SAPCODE) AS ORG_ID, "
			+ " ( SELECT D.ORG_DESC FROM D WHERE D.ORG_ID = OS.OU_SAPCODE ) ORG_DESC, "
			+ " NVL( (SELECT EF.OU_SAPCODE FROM EF WHERE EF.OU_SAPCODE = OS.OUFATHER_H) , " + DEFAULT_PARENT_OU + " ) AS ORG_PARENT_ID " //OU with non exisiting parents will be put directly under UNICREDIT
			+ " FROM HR_ORG_STRUCTURE OS "
			+ " WHERE "
			+ " OS.OU_SAPCODE IS NOT NULL "
			+ " AND OS.OU_SAPCODE NOT IN (SELECT ORG_ID FROM ORG_CONN_PA_ORG WHERE ORG_ID IS NOT NULL AND ORG_ID <> '00000000') "			
			// 2015-03-25 We do not create non existing parents 
			//+ " UNION "			
			//+ // NOTE It's important to include non-existing parents from org structure because, deps with non existing parents will be not created!
			//" SELECT DISTINCT TO_CHAR(OUFATHER_H) AS ORG_ID, " + DEFAULT_TREE_ROOT_DESCR + " AS ORG_DESC, "
			//+ DEFAULT_PARENT_OU
			//+ " AS ORG_PARENT_ID "
			//+ /* NON EXISTING PARENTS FROM ORG STRUCTURE */
			//" FROM HR_ORG_STRUCTURE WHERE OUFATHER_H  NOT IN (SELECT OU_SAPCODE FROM HR_ORG_STRUCTURE) "		
			// " AND OUFATHER_H NOT IN (SELECT ORG_ID FROM ORG_CONN_PA_ORG WHERE
			// ORG_ID IS NOT NULL AND ORG_ID <> '00000000')			
			+ " UNION "
			+ // Change Parent Org ID for organizations which are no longer
				// present in the HR_ORG_STRUCTURE
			" SELECT PO.ORG_ID, "
			+ " ( SELECT D.ORG_DESC FROM D WHERE D.ORG_ID = PO.ORG_ID ) ORG_DESC, "
			+ DEFAULT_PARENT_OU_4_REMOVED_OU
			+ " AS ORG_ID_PARENT FROM ORG_CONN_PA_ORG PO "
			+ " WHERE  "
			+ " NOT EXISTS (SELECT OS.OU_SAPCODE FROM HR_ORG_STRUCTURE OS WHERE OS.OU_SAPCODE = PO.ORG_ID) "
			+ " AND PO.ORG_ID_PARENT IS NOT NULL "
			+ // exclude roots org
			" AND PO.ORG_ID <> '00000000' "
			+ // exclude it
			" AND PO.ORG_ID NOT IN  ( SELECT DISTINCT TO_CHAR(OUFATHER_H) AS ORG_ID "
			+ // exclude not existing roots from table HR_ORG_STRUCTURE
			" FROM HR_ORG_STRUCTURE WHERE OUFATHER_H  NOT IN (SELECT OU_SAPCODE FROM HR_ORG_STRUCTURE) ) "
			+ " AND LENGTH(PO.ORG_ID) = 8 " // only on staging because of data mess
			+ " AND NVL(PO.ORG_TYP_ID,' ') <> 'LE' "
			+ " AND NVL(PO.ORG_ID_PARENT,' ') <> 'LEGAL_ENTITIES' "
			+ " AND NVL(PO.ORG_ID,' ') <> 'AA' ";
	
	
	
	public List<OrgData> getOrgList() {
		return getJdbcTemplate().query(orgsSql, new RowMapper<OrgData>() {
			@Override
			public OrgData mapRow(ResultSet rs, int rowNum) throws SQLException {

				OrgData od = new OrgData();
				od.setOrgId(rs.getString("ORG_ID"));
				od.setOrgIdParent(rs.getString("ORG_PARENT_ID"));
				od.setOrgDesc(rs.getString("ORG_DESC"));
				return od;

			}
		});
	}
	
	public void cleanTable(String tableName) 
	{
		getSimpleJdbcTemplate().update("TRUNCATE TABLE " + tableName);
	}
	

	public void insertRowsToOrgTable(ArrayList<OrgData> orgs) throws Exception {
		
		cleanTable("ORG_CONN_PA_ORG_TMP");
		
		/*
		ORG_ID VARCHAR2(90), 
		ORG_DESC  VARCHAR2(300),
		ORG_TYP_ID  VARCHAR2(90),
		DMN_ID  VARCHAR2(90),
		ORG_ID_PARENT  VARCHAR2(90),
		*/		

		String sql = "INSERT INTO ORG_CONN_PA_ORG_TMP(ORG_ID,ORG_DESC,ORG_TYP_ID,DMN_ID,ORG_ID_PARENT) VALUES (?,?,?,?,?)";

		PreparedStatement ps = this.getJdbcTemplate().getDataSource()
				.getConnection().prepareStatement(sql);

		for (OrgData orgData : orgs) {

			ps.setString(1, orgData.getOrgId());// ORG_ID,
			ps.setString(2, orgData.getOrgDesc());// ORG_DESC,
			ps.setString(3, orgData.getOrgTypId());// ORG_TYP_ID,
			ps.setString(4, orgData.getDmnId());// DMN_ID,
			ps.setString(5, orgData.getOrgIdParent());// ORG_ID_PARENT,			
			ps.addBatch();
		}

		ps.executeBatch();
		// getSimpleJdbcTemplate().
		
		// get count of rows in the temp table
		
		int orgCount =  getSimpleJdbcTemplate().queryForInt( "SELECT COUNT(*) FROM ORG_CONN_PA_ORG_TMP");
		if ( orgCount > 9000 ){
			
			getSimpleJdbcTemplate().update("TRUNCATE TABLE ORG_CONN_PA_ORG");
			getSimpleJdbcTemplate().update("INSERT INTO ORG_CONN_PA_ORG SELECT * FROM ORG_CONN_PA_ORG_TMP");
			
		} else {
			throw new Exception("Unable to load organizations into ORG_CONN_PA_ORG becasue there number of organizations is too low: " + orgCount + " and the data might be incomplete");
		}
		

	}

}
