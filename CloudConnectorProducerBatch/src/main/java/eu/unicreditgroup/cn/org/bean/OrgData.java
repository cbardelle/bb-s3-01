package eu.unicreditgroup.cn.org.bean;

public class OrgData {
	
	//"ORG_ID","ORG_DESC","ORG_TYP_ID","DMN_ID","ORG_ID_PARENT"
	
	private String orgId;
	
	//private String orgAbbr;
	
	private String orgDesc;
	
	private String orgTypId;
	
	private String dmnId;
	
	private String orgIdParent;
	

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	/*	
	public String getOrgAbbr() {
		return orgAbbr;
	}
	public void setOrgAbbr(String orgAbbr) {
		this.orgAbbr = orgAbbr;
	}
	*/

	public String getOrgDesc() {
		return orgDesc;
	}

	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}

	public String getOrgIdParent() {
		return orgIdParent;
	}

	public void setOrgIdParent(String orgIdParent) {
		this.orgIdParent = orgIdParent;
	}

	public String getOrgTypId() {
		return orgTypId;
	}

	public void setOrgTypId(String orgTypId) {
		this.orgTypId = orgTypId;
	}

	public String getDmnId() {
		return dmnId;
	}

	public void setDmnId(String dmnId) {
		this.dmnId = dmnId;
	}
	

}
