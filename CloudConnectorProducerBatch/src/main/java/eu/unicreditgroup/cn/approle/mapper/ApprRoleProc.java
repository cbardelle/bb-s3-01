package eu.unicreditgroup.cn.approle.mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.unicreditgroup.cn.approle.bean.ApprRole;
import eu.unicreditgroup.cn.approle.bean.ApprRoleColumn;


public class ApprRoleProc {

	
	private List<ApprRoleColumn> addApprRoleColumns;
	
	private List<ApprRoleColumn> removeApprRoleColumns;
	
	private ApprRoleResultWriter apprRoleResultWriter;
	
	
	private void processRoles(List<ApprRole> roles,  List<ApprRoleColumn> colCfg ) throws IOException{
		
		for ( ApprRole ar : roles ){			
			List<String> rowValues = new ArrayList();
			for (ApprRoleColumn arc: colCfg  ){
				rowValues.add( arc.getMapper().map(ar, arc.getDefaultValue()) );				
			}
			apprRoleResultWriter.append(rowValues);			
		}
		
	}
	

	
	public void processNewRoles(List<ApprRole> newRoles) throws IOException {
		processRoles(newRoles,  addApprRoleColumns );
	}

	
	public void processRemovedRoles(List<ApprRole> removedRoles) throws IOException {
		processRoles(removedRoles,  removeApprRoleColumns );
	}
	
	public void closeResultFile() throws IOException{
		
		apprRoleResultWriter.close();
		
	}
	

	public List<ApprRoleColumn> getAddApprRoleColumns() {
		return addApprRoleColumns;
	}

	public void setAddApprRoleColumns(List<ApprRoleColumn> addApprRoleColumns) {
		this.addApprRoleColumns = addApprRoleColumns;
	}

	public List<ApprRoleColumn> getRemoveApprRoleColumns() {
		return removeApprRoleColumns;
	}

	public void setRemoveApprRoleColumns(List<ApprRoleColumn> removeApprRoleColumns) {
		this.removeApprRoleColumns = removeApprRoleColumns;
	}


	public ApprRoleResultWriter getApprRoleResultWriter() {
		return apprRoleResultWriter;
	}


	public void setApprRoleResultWriter(ApprRoleResultWriter apprRoleResultWriter) {
		this.apprRoleResultWriter = apprRoleResultWriter;
	}



	public void writeHeader() throws IOException {
		
		List<String> columns = new ArrayList<String>();
		for ( ApprRoleColumn arc : addApprRoleColumns ){			
			columns.add(arc.getColName());						
		}
		
		apprRoleResultWriter.appendHeader(columns);
		
		
	}
	
	
	

}
