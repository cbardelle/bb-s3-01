package eu.unicreditgroup.cn.approle.mapper;

import eu.unicreditgroup.cn.approle.bean.ApprRole;

/**
 * Mapping to a fixed value
 * 
 * @author UV00074
 *
 */
public class FixedValueMapper implements IRoleMapper {

	@Override
	public String map(ApprRole ar, String defaultValue) {

		return defaultValue;
	}

}
