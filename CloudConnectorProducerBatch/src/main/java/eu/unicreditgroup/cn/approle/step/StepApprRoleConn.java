package eu.unicreditgroup.cn.approle.step;

import java.io.File;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.step.BatchStep;
import eu.unicreditgroup.cn.approle.bean.ApprRole;
import eu.unicreditgroup.cn.approle.dao.ApprRoleConnDao;
import eu.unicreditgroup.cn.approle.mapper.ApprRoleProc;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.BackupUtil;


/**
 * This is the step responsible for generating the source file 
 * for the Approval Role Assignment Connector 
 * 
 * 
 */
public class StepApprRoleConn implements BatchStep {
	
	Logger logger = Logger.getLogger(StepApprRoleConn.class);
	
	private ApprRoleConnDao apprRoleConnDao; 
	
	private ConfigParams configParams;
	
	private ApprRoleProc apprRoleProc;
	
	private FTPUploader resultUploader;
		
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<Exception> apprRoleExceptionsQueue;
	

	@Override
	public void execute() throws Exception {
		
		logger.debug("Start of execution of step generate file for Approval role connector");
		
		try {
			BackupUtil.backupFile(configParams.getResultFileFullPathApprRoleConn());
		} catch (Exception e2) {
			logger.error("Failed to backup the result file for Approval Role connector!");
			apprRoleExceptionsQueue.add(e2);
		}
		
		
		// current roles count
		int currRolesCnt = 0;
		
		try{		
			
			currRolesCnt = apprRoleConnDao.getCurrRolesCnt();
			
			apprRoleConnDao.loadAppRolesTmpTable();
			
			
		} catch(Exception e){			
			logger.debug("Failed to load Approval Roles Assignments into the temp table cannot proceed!", e);
			throw(e);			
		}
		
		List<ApprRole> newRoles = apprRoleConnDao.getNewRoles();
		
		List<ApprRole> removedRoles = apprRoleConnDao.getRemovedRoles();
		
		// count of new roles
		int newRolesCnt = newRoles == null ? 0 : newRoles.size();
		
		//count of removed roles
		int rmRolesCnt = removedRoles == null ? 0 : removedRoles.size(); 
		
		float newRowsRatio = 0;
		
		float rmRowsRatio = 0;
		
		// dont't calculate when loading the data for the 1st time
		if ( currRolesCnt > 0){			
			newRowsRatio = (float) (((float)newRolesCnt * 100.0) / (float)currRolesCnt);			
			rmRowsRatio = (float) (((float)rmRolesCnt * 100.0) / (float)currRolesCnt);			
		}
		
		reportDataBean.setAppRoleConnNewRolesCount(newRolesCnt);
		reportDataBean.setAppRoleConnRemovedCount(rmRolesCnt);
		
		// write file header
		apprRoleProc.writeHeader();
		
		
		// do not process new roles if ratio is exceeded
		
		if ( newRowsRatio <= configParams.getApproRoleConnNewRowsMaxCnt() ){			
			apprRoleProc.processNewRoles(newRoles);
		} else {
			Exception ex = new Exception("The ratio of NEW records is too high " + newRowsRatio );
						
			apprRoleExceptionsQueue.add(ex);
						
			logger.error("TOO MANY NEW RECORDS", ex);	
		}
		
		
		// do not process removed roles if ratio is exceeded
		if ( rmRowsRatio <= configParams.getApproRoleConnRmRowsMaxCnt() ){
			apprRoleProc.processRemovedRoles(removedRoles);			
		} else {			
			
			Exception ex = new Exception("The ratio of REMOVED records is too high " + rmRowsRatio );
			
			apprRoleExceptionsQueue.add(ex);
			
			logger.error("TOO MANY REMOVED RECORDS!!!", ex);
			
		}
		
		apprRoleProc.closeResultFile();
				
		apprRoleConnDao.replaceMainApprRoleTable();		
		
		if ( StringUtils.isNotEmpty( configParams.getResultFtpPathApprRoleConn() ) ){			
			
			if (reportDataBean.getFailedFastException() == null ){
				try {
					
					File resultFile = new File(configParams.getResultFileFullPathApprRoleConn());
					
					if ( !resultFile.exists() || resultFile.length() == 0){
						
						throw new Exception("The result file for Approval role connector doesn't exist or is empty!");
					}
					
					logger.debug("Starting upload of result file for Approval role connector...");
					
					
					resultUploader.uploadFile(resultFile);
										
					
					logger.debug("Result file uploaded.");
				} catch (Exception e) {
					reportDataBean.setFailedFastException(new Exception("Failed to upload the result file for Approval role connector",e));					
					apprRoleExceptionsQueue.add(e);
					logger.error("Failed to upload the result file for Approval role connector", e);
				}
			}
			
		} else {
			logger.debug("Upload of result file for Approval role connector was skipped.");
		}
		
		logger.debug("End of execution of step generate file for Approval role connector");
		
	}


	public ApprRoleConnDao getApprRoleConnDao() {
		return apprRoleConnDao;
	}


	public void setApprRoleConnDao(ApprRoleConnDao apprRoleConnDao) {
		this.apprRoleConnDao = apprRoleConnDao;
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public ApprRoleProc getApprRoleProc() {
		return apprRoleProc;
	}


	public void setApprRoleProc(ApprRoleProc apprRoleProc) {
		this.apprRoleProc = apprRoleProc;
	}


	public FTPUploader getResultUploader() {
		return resultUploader;
	}


	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}


	public BlockingQueue<Exception> getApprRoleExceptionsQueue() {
		return apprRoleExceptionsQueue;
	}


	public void setApprRoleExceptionsQueue(
			BlockingQueue<Exception> apprRoleExceptionsQueue) {
		this.apprRoleExceptionsQueue = apprRoleExceptionsQueue;
	}


	public void addException(Exception e) {
		
		try {
			apprRoleExceptionsQueue.put(e);
		} catch (InterruptedException e1) {
			logger.debug(e1);
		}
	}

	
}
