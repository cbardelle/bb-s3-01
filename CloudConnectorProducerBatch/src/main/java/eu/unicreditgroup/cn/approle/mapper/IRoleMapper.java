package eu.unicreditgroup.cn.approle.mapper;

import eu.unicreditgroup.cn.approle.bean.ApprRole;


/**
 * Interface for all approvale role mapper clesses
 * 
 * @author UV00074
 * 
 */

public interface IRoleMapper {
	
	
	String map( ApprRole ar, String defaultValue  );
		
	
	
}
