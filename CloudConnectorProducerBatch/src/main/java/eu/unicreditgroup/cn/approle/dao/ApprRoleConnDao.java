package eu.unicreditgroup.cn.approle.dao;

 import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.cn.approle.bean.ApprRole;
import eu.unicreditgroup.cn.approle.bean.User;
import eu.unicreditgroup.config.ConfigParams;

public class ApprRoleConnDao extends SimpleJdbcDaoSupport {

	private static final Logger logger = Logger.getLogger(ApprRoleConnDao.class);
	
	//private static final String USR_DICT_TABLE = "hr_conn_usr_attr_dict";
	
	private final String DEFAULT_ORG_APPROVER_USER = "DEFAULT_ORG_APPROVER";
		
	private java.sql.Timestamp toSqlTimeStamp(java.util.Date javaDate){
		return new java.sql.Timestamp(javaDate.getTime());
	}
	
	
	/**
	 * Loads the data into the temporary table
	 * 
	 * 
	 */
	public void loadAppRolesTmpTable(){
		
		// truncate the temporary table
		getJdbcTemplate().execute("TRUNCATE TABLE APPR_CONN_ASGN_TMP");
		
		// load the approval roles into the temp table
		String loadApprRoleSql = " INSERT INTO APPR_CONN_ASGN_TMP " + 
		" SELECT LINE_MANAGER_ID, USER_OFFICE_CODE , 'LINE_MANAGER' AS USER_TYPE FROM ( " +
		" WITH SVR AS ( SELECT DISTINCT SUPERVISOR FROM HR_CONN_RES_FILE WHERE SUPERVISOR IS NOT NULL AND SUPERVISOR IN (SELECT USER_ID FROM HR_CONN_RES_FILE WHERE NOT_ACTIVE ='N') AND NOT_ACTIVE = 'N' ) " + // list of exisiting LMs and of ACTIVE USERS
		" SELECT RF.SUPERVISOR AS LINE_MANAGER_ID, RF.ORG_ID AS USER_OFFICE_CODE FROM  HR_CONN_RES_FILE RF, SVR " + // offices of subordinates who are not a LM
		" WHERE RF.SUPERVISOR = SVR.SUPERVISOR " + 
		" AND RF.USER_ID NOT IN (SELECT * FROM SVR) AND RF.ORG_ID IS NOT NULL" + // as already said not a LM
		" UNION " + // attach office of each LM (LM might not be a LM to anybody in his office)
		" SELECT RF.USER_ID AS LINE_MANAGER_ID , RF.ORG_ID AS USER_OFFICE_CODE FROM  HR_CONN_RES_FILE RF, SVR WHERE " + 
		" RF.USER_ID = SVR.SUPERVISOR AND RF.ORG_ID IS NOT NULL )";

		
		getJdbcTemplate().execute(loadApprRoleSql);
		
		// After loading the data identify the offices with no LM
		// and for those offices assign HRBP users 		
		String officeNoLineMgr =		
		
		" INSERT INTO APPR_CONN_ASGN_TMP " +
		" WITH ONLM AS ( SELECT DISTINCT ORG_ID FROM HR_CONN_RES_FILE WHERE ORG_ID NOT IN   " + // offices with no line manager
		" ( SELECT DISTINCT USER_OFFICE_CODE FROM APPR_CONN_ASGN_TMP WHERE USER_TYPE = 'LINE_MANAGER') " + 
		" AND ORG_ID IS NOT NULL   " +
		" AND ORG_ID <> '00000000' )  " +
		" SELECT DISTINCT  RF.HRBP_ID_CC_70 AS LINE_MANAGER_ID,  RF.ORG_ID AS USER_OFFICE_CODE, 'HRBP' AS USER_TYPE FROM HR_CONN_RES_FILE RF , ONLM " + 
		" WHERE RF.ORG_ID = ONLM.ORG_ID  " +
		" AND RF.HRBP_ID_CC_70 IN (SELECT USER_ID FROM HR_CONN_RES_FILE WHERE NOT_ACTIVE ='N')  " + // hrbp is an active user
		" ORDER BY RF.HRBP_ID_CC_70 ";

		
		getJdbcTemplate().execute(officeNoLineMgr);
		
		String officeWithDefaultApprover =
		" INSERT INTO APPR_CONN_ASGN_TMP " + 
		 " WITH THEREST AS (SELECT DISTINCT ORG_ID FROM HR_CONN_RES_FILE WHERE ORG_ID NOT IN " +
		 " ( SELECT DISTINCT USER_OFFICE_CODE FROM APPR_CONN_ASGN_TMP WHERE USER_TYPE = 'LINE_MANAGER' OR USER_TYPE = 'HRBP') " + 
		 " AND ORG_ID IS NOT NULL ) " +
		 " SELECT '" + DEFAULT_ORG_APPROVER_USER + "' AS LINE_MANAGER_ID,  THEREST.ORG_ID AS USER_OFFICE_CODE, 'DEFAULT_APPROVER' AS USER_TYPE FROM THEREST "; 
		
		getJdbcTemplate().execute(officeWithDefaultApprover);
		
		
	}
	
/*
	public String getDefaultBixzUserRole() {
		
		String sql = "SELECT ROLE_NAME FROM BIZX_CONN_ROLES WHERE ROLE_ID = 'LMSU'";
		
		return getJdbcTemplate().query(sql, new ResultSetExtractor<String>(){

			@Override
			public String extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				rs.next();
				return rs.getString(1);
			}
		});
		
	}
	
	
	public List<User> getAdminWithRole() {
		String sql = "SELECT A.USER_ID, R.ROLE_NAME  FROM BIZX_CONN_ADMINS A, BIZX_CONN_ROLES R WHERE A.ROLE_ID = R.ROLE_ID";

		return getJdbcTemplate().query(sql, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				User user = new User();
				
				user.setUserId(rs.getString("USER_ID"));
				user.setBizxRole(rs.getString("ROLE_NAME"));
				
				return user;
			}
		});
	}
*/

	public List<ApprRole> getNewRoles() {
		
		String sql = "SELECT LINE_MANAGER_ID, USER_OFFICE_CODE FROM APPR_CONN_ASGN_TMP MINUS SELECT LINE_MANAGER_ID, USER_OFFICE_CODE FROM APPR_CONN_ASGN";

		return getJdbcTemplate().query(sql, new RowMapper<ApprRole>() {
			@Override
			public ApprRole mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ApprRole apprRole = new ApprRole();
				
				apprRole.setLineManagerId(rs.getString("LINE_MANAGER_ID"));
				apprRole.setOfficeCode(rs.getString("USER_OFFICE_CODE"));
				
				return apprRole;
			}
		});			
	}


	public List<ApprRole> getRemovedRoles() {

		String sql = "SELECT LINE_MANAGER_ID, USER_OFFICE_CODE FROM APPR_CONN_ASGN MINUS SELECT LINE_MANAGER_ID, USER_OFFICE_CODE FROM APPR_CONN_ASGN_TMP";

		return getJdbcTemplate().query(sql, new RowMapper<ApprRole>() {
			@Override
			public ApprRole mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ApprRole apprRole = new ApprRole();
				
				apprRole.setLineManagerId(rs.getString("LINE_MANAGER_ID"));
				apprRole.setOfficeCode(rs.getString("USER_OFFICE_CODE"));
				
				return apprRole;
			}
		});		
	}


	public int getCurrRolesCnt() {
		// TODO Auto-generated method stub		
		String sql = "SELECT COUNT(*) FROM APPR_CONN_ASGN";		
		return getJdbcTemplate().queryForInt(sql);		
	}


	
	public void replaceMainApprRoleTable(){		
		getJdbcTemplate().update("TRUNCATE TABLE APPR_CONN_ASGN");
		getJdbcTemplate().execute("INSERT INTO APPR_CONN_ASGN SELECT * FROM APPR_CONN_ASGN_TMP");		
	}
	
	
	
	
}
