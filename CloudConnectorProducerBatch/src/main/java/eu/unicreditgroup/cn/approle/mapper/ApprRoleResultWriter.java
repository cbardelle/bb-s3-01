package eu.unicreditgroup.cn.approle.mapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import eu.unicreditgroup.config.ConfigParams;

public class ApprRoleResultWriter {

	private ConfigParams configParams;
	
	private File resultFile;
		
	private BufferedWriter bufferedWriter;
	
		
	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}
	
	
	public void append( List<String>  rowValues ) throws IOException {
		if (resultFile == null){
			initResultFile();
		}		
		
			
			bufferedWriter.write( getResultRow( rowValues ));			
			bufferedWriter.newLine();
	
		
	}

	private String getResultRow(List<String> rowValues) {

		StringBuilder sb = new StringBuilder();
		
		boolean notFirst = false;
		for ( String s : rowValues ){
			
			if ( notFirst ){				
				sb.append("|");
			} else {
				notFirst = true;
			}			
			//sb.append("\"").append(s).append("\"");			
			sb.append(s);
		}
		
		sb.append("!##!");
		
		return sb.toString();
	}
	
	
	private String getHeaderRow(List<String> columns) {
		StringBuilder sb = new StringBuilder();
		
		boolean notFirst = false;
		for ( String s : columns ){
			
			if ( notFirst ){				
				sb.append("|");
			} else {
				notFirst = true;
			}			
			sb.append(s);			
		} 
		sb.append("!##!");
		
		return sb.toString();
	}
	
	

	private void initResultFile() throws IOException {
		resultFile = new File(configParams.getResultFileFullPathApprRoleConn());
		
		bufferedWriter = new BufferedWriter(new FileWriter(resultFile));
		
		
		
	}
	
	public void close() throws IOException{
		bufferedWriter.close();
	}

	public void appendHeader(List<String> columns) throws IOException {

		if (resultFile == null){
			initResultFile();
		}		
					
		bufferedWriter.write( getHeaderRow( columns ));			
		bufferedWriter.newLine();
	
		
		
	}

	
	
	
	
	
	
	
	
	
	
}
