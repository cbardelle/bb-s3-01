package eu.unicreditgroup.cn.custcol.proc;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;
import eu.unicreditgroup.cn.custcol.conf.CustColConf;
import eu.unicreditgroup.mailer.ReportDataBean;

public class CustColProc {
	
	Logger logger = Logger.getLogger(CustColProc.class);
	
	/**
	 * The queue for processed custom column values
	 */
	private BlockingQueue<CustColValue> custColQueue; 
	
	private BlockingQueue<Exception> custColExceptionsQueue;
	
	private ReportDataBean reportDataBean;  
	
	
	/**
	 * Get the description with max occurrence count
	 * 
	 * @return
	 */
	private String getTheMostWantedDescr(HashMap<String,Integer> descriptions){
		
		if (descriptions == null){			
			return null;			
		}		
		if (descriptions.size() == 0){
			return null;
		}
		if (descriptions.size() == 1){
			return descriptions.keySet().iterator().next();
		}
					
		int countMax = 0;
		
		// if all values are null it will return null anyway
		String itemMax = null;		
					
		for( String desc : descriptions.keySet() ) {
			
			if (desc == null){ continue;}
				
			int count = descriptions.get(desc);
			
			if ( count > countMax ){
				countMax = count;
				itemMax = desc; 
			}
		}
		return itemMax;
	}
	
	
	/**
	 * Process custom column values
	 * put processed values to the queue
	 * @throws Exception 
	 *  
	 * 
	 */
	public void processValues( List<CustColValue> data, CustColConf custColConf ){
		
		try {
			// K - custom column ID
			// V - HashMap: K - cust col descr, V - count 
			HashMap<String, HashMap<String,Integer>> custColValues = new HashMap<String, HashMap<String,Integer>>();
					
			for ( CustColValue ccv : data){
				
				if ( custColValues.containsKey( ccv.getId() ) ){
					
					HashMap<String,Integer> descrCount = custColValues.get( ccv.getId() );

					// there's more than one description for the same cust col Id					
					if ( descrCount.containsKey( ccv.getDesc() ) == false ){						
						descrCount.put( ccv.getDesc(), ccv.getCount() );						
					}				
					
				} else {				
					HashMap<String,Integer> descrCount = new HashMap<String,Integer>();				
					descrCount.put( ccv.getDesc(), ccv.getCount() );							
					custColValues.put(ccv.getId(), descrCount);				
				}
				
			}	
			
			
			for ( String ccid :  custColValues.keySet() ){
				
				HashMap<String,Integer> descrCountMap = custColValues.get( ccid );
				
				
				CustColValue ccv = new CustColValue();
				
				if ( descrCountMap.size() == 1 ){ // there's one description				
					ccv.setId(ccid);
					ccv.setDesc( descrCountMap.keySet().iterator().next() ); // the only description
					ccv.setLabel(custColConf.getLabel());
					ccv.setColNum(custColConf.getColNum());
				} else { 
					if (descrCountMap.size() == 0){
						throw new Exception("There's No description for the custom column: " + custColConf.getLabel() + ", cc id: " + ccid);
					}
					// there's more descriptions
					
					
					ccv.setId(ccid);
					String theMostWantedDescr =getTheMostWantedDescr( descrCountMap ); 
					ccv.setDesc( theMostWantedDescr ); 
					
					ccv.setLabel(custColConf.getLabel());
					ccv.setColNum(custColConf.getColNum());
					
					reportDataBean.incCustColumConnDiffDescr();
					
					StringBuilder sb = new StringBuilder();
					
					for ( String value : descrCountMap.keySet() ){
						sb.append(value).append(",");						
					}					
					
					logger.debug("The custom colum " + custColConf.getLabel() + " has !!!DIFFERENT!!! values: " + sb.toString() + "  for the cust col: " + ccv.getLabel() + ", (CC " + ccv.getColNum() + "), the used description is: " + theMostWantedDescr);					
				}		
				
				custColQueue.put(ccv);
				
			}
			
		} catch (Exception e) {
			logger.debug("Exception when processing cusotm column: " + custColConf.getLabel(), e);
			
			//TODO add to the report bean!
			
		}
	}	
	
	
	public BlockingQueue<CustColValue> getCustColQueue() {
		return custColQueue;
	}

	public void setCustColQueue(BlockingQueue<CustColValue> custColQueue) {
		this.custColQueue = custColQueue;
	}


	public BlockingQueue<Exception> getCustColExceptionsQueue() {
		return custColExceptionsQueue;
	}


	public void setCustColExceptionsQueue(
			BlockingQueue<Exception> custColExceptionsQueue) {
		this.custColExceptionsQueue = custColExceptionsQueue;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

}
