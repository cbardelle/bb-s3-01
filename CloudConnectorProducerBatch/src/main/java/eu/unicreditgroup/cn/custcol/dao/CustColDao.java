package eu.unicreditgroup.cn.custcol.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;

public class CustColDao extends SimpleJdbcDaoSupport {

		
	
	private List<CustColValue>  getCustomColumnValues( String sql ){
		return getJdbcTemplate().query(sql, new RowMapper<CustColValue>() {
			@Override
			public CustColValue mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CustColValue value = new CustColValue();				
				value.setId(rs.getString("ID"));
				value.setDesc(rs.getString("DESCR"));	
				value.setCount(rs.getInt("CNT"));
				return value;
			}
		});		
	}	
	
	public List<CustColValue> getJobSeniorityValues() {
		String sql = "SELECT DISTINCT JOB_SENIOR_CC_200 AS ID, JOB_SENIOR_CC_200_DSCR AS DESCR, COUNT(*) AS CNT FROM HR_CONN_RES_FILE WHERE JOB_SENIOR_CC_200 IS NOT NULL AND JOB_SENIOR_CC_200_DSCR IS NOT NULL group by JOB_SENIOR_CC_200, JOB_SENIOR_CC_200_DSCR";
		return getCustomColumnValues(sql);		
	}
	
	
	public List<CustColValue> getServiceLineValues() {
		String sql = "SELECT DISTINCT SERVICE_LINE_CC_210 AS ID, SERVICE_LINE_CC_210_DSCR AS DESCR, COUNT(*) AS CNT FROM HR_CONN_RES_FILE WHERE SERVICE_LINE_CC_210 IS NOT NULL AND SERVICE_LINE_CC_210_DSCR IS NOT NULL group by SERVICE_LINE_CC_210, SERVICE_LINE_CC_210_DSCR";
		return getCustomColumnValues(sql);
	}
	
	
	public List<CustColValue> getBusinessLineValues() {
		String sql = "SELECT DISTINCT BUS_LINE_CC_220 AS ID, BUS_LINE_CC_220_DSCR AS DESCR, COUNT(*) AS CNT FROM HR_CONN_RES_FILE WHERE BUS_LINE_CC_220 IS NOT NULL AND BUS_LINE_CC_220_DSCR IS NOT NULL group by BUS_LINE_CC_220, BUS_LINE_CC_220_DSCR";
		return getCustomColumnValues(sql);
	}
	
	
	public List<CustColValue> getOrgUnitLocationTypeValues() {
		String sql = "SELECT DISTINCT CC_240 AS ID, CC_240_DSCR AS DESCR, COUNT(*) AS CNT FROM HR_CONN_RES_FILE WHERE CC_240 IS NOT NULL AND CC_240_DSCR IS NOT NULL group by CC_240, CC_240_DSCR";
		return getCustomColumnValues(sql);
	}

	
	public List<CustColValue> getHostLegalEntityValues() {
		String sql = "SELECT CC_270 AS ID, CC_270_DSCR AS DESCR, COUNT(*) AS CNT FROM HR_CONN_RES_FILE WHERE CC_270 IS NOT NULL AND CC_270_DSCR IS NOT NULL GROUP BY CC_270, CC_270_DSCR";
		return getCustomColumnValues(sql);
	}


}
