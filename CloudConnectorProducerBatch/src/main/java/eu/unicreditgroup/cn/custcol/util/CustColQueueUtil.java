package eu.unicreditgroup.cn.custcol.util;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;


public class CustColQueueUtil {
	
	public static final String SHUTDOWN_STRING = "***********SHUTDOWN_VALUE***********";
	
	private static CustColValue shutDownValue = new CustColValue();
	static	{
		shutDownValue.setId(SHUTDOWN_STRING);
	}
		
	public static CustColValue generateShutDownVal(){
		return shutDownValue;
	}
	
	public static boolean isShutdownValue(CustColValue ccv){
		
		if (ccv == null){
			return false;
		} else {
			return (
				SHUTDOWN_STRING.equals( ccv.getId() ) == true
			);	  
				 
		}
	}
}
