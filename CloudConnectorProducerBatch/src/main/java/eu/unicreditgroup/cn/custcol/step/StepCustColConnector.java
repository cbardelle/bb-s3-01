package eu.unicreditgroup.cn.custcol.step;

import java.io.File;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.step.BatchStep;
import eu.unicreditgroup.cn.bizx.step.StepGenerateFileForBizx;
import eu.unicreditgroup.cn.custcol.proc.CustColConsumer;
import eu.unicreditgroup.cn.custcol.proc.ICustColDataProvider;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.BackupUtil;

public class StepCustColConnector implements BatchStep {
	
	//private List<ICustColDataProvider> customColumns;	
	private List<Runnable> customColumns;
	
	private CustColConsumer custColConsumer;
			
	private BlockingQueue<Exception> custColExceptionsQueue;
	
	private ConfigParams configParams;
	
	private FTPUploader resultUploader;
	
	private ReportDataBean reportDataBean;
	
	
	Logger logger = Logger.getLogger(StepCustColConnector.class);	
	
	
	
	
	@Override
	public void execute() throws Exception {
		
		logger.debug("Start of execution of step generate file for Custom Column connector");
		
		try {
			BackupUtil.backupFile(configParams.getResultFileFullPathCustColConn());
		} catch (Exception e2) {
			logger.error("Failed to backup the result file for Custom column connector!");
			custColExceptionsQueue.add(e2);
		}

		int corePoolSize = 5;
		int maximumPoolSize = 10;
		int keepAliveTime = 5;
				
		ThreadFactory threadFactory = Executors.defaultThreadFactory();	    
	    ThreadPoolExecutor executorPool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(corePoolSize), threadFactory );
	    
	    Thread custColConsumerTh = new Thread(custColConsumer);
	    custColConsumerTh.start();
	    
	    
	    for (Runnable col : customColumns ){	    	
	    	executorPool.execute(col);	    	
	    }	    
	    
	    executorPool.shutdown();
	    executorPool.awaitTermination(15, TimeUnit.MINUTES);
	    
	    custColConsumer.shutdown();
	    
	    
	    if ( StringUtils.isNotEmpty( configParams.getResultFtpPathCustColConn() ) ){			
			
			if (reportDataBean.getFailedFastException() == null ){
				try {
					
					File resultFile = new File(configParams.getResultFileFullPathCustColConn() );
					
					if ( !resultFile.exists() || resultFile.length() == 0){
						
						throw new Exception("The result file for Custom Column connector doesn't exist or is empty!");
						
					}
					
					logger.debug("Starting upload of result file for Custom Column connector...");
										
					resultUploader.uploadFile(resultFile);
															
					logger.debug("Result file uploaded.");
					
				} catch (Exception e) {
					reportDataBean.setFailedFastException(new Exception("Failed to upload the result file for Custom Column connector",e));					
					custColExceptionsQueue.add(e);
					logger.error("Failed to upload the result file for Custom Column connector", e);
				}
			}
			
		} else {
			logger.debug("Upload of result file for Custom Column connector was skipped.");
		}
	 
	    logger.debug("End of execution of step generate file for Custom Column connector");
	}
	

	public List<Runnable> getCustomColumns() {
		return customColumns;
	}

	public void setCustomColumns(List<Runnable> customColumns) {
		this.customColumns = customColumns;
	}
 
	
	public CustColConsumer getCustColConsumer() {
		return custColConsumer;
	}


	public void setCustColConsumer(CustColConsumer custColConsumer) {
		this.custColConsumer = custColConsumer;
	}


	public void addException(Exception e) {
		
		try {
			custColExceptionsQueue.put(e);
		} catch (InterruptedException e1) {
			logger.debug(e1);
		}
		
	}


	public BlockingQueue<Exception> getCustColExceptionsQueue() {
		return custColExceptionsQueue;
	}


	public void setCustColExceptionsQueue(
			BlockingQueue<Exception> custColExceptionsQueue) {
		this.custColExceptionsQueue = custColExceptionsQueue;
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public FTPUploader getResultUploader() {
		return resultUploader;
	}


	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
	
	

}
