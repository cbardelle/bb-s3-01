package eu.unicreditgroup.cn.custcol.proc;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;
import eu.unicreditgroup.cn.custcol.util.CustColQueueUtil;
import eu.unicreditgroup.cn.custcol.util.ResultFileWriter;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;


/**
 * 
 * Consumer of the queue of processes custom columns values
 * 
 * @author UV00074
 *
 */
public class CustColConsumer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(CustColConsumer.class);
	
	/**
	 * The queue for processed custom column values
	 */
	private BlockingQueue<CustColValue> custColQueue;
	
	private ConfigParams configParams;
	
	
	private final String CC_COLUMN_DELIMITER = "|";	
	
	private final String CC_LINE_END = "!##!";
	
	
	private ResultFileWriter resultFileWriter;
	
	private ReportDataBean reportDataBean; 
	
	 final Lock lock = new ReentrantLock();
    final Condition resultFileClosed  = lock.newCondition(); 

	

	public void run() {
		
		
		CustColValue custColValue = new CustColValue();
		try {	
			
			
			
			resultFileWriter = new ResultFileWriter(CC_COLUMN_DELIMITER, CC_LINE_END, configParams.getResultFileFullPathCustColConn());
			
			while(true){
				Thread.yield();
				
				custColValue = custColQueue.take();
				
				if (CustColQueueUtil.isShutdownValue(custColValue)){
					logger.debug(String.format("Shutdown value was found. Consumer will stop its work."));
					custColQueue.put(custColValue);
					
					resultFileWriter.closeFile();
					
					lock.lock();
					
					resultFileClosed.signal();
										
					lock.unlock();
					
					return;
				}
				
				resultFileWriter.writeResultRow(custColValue);
				
				reportDataBean.incCustColumConnTotalWrite();
							
			}
			
		} catch (Exception ex) {
			
			logger.debug("Exception when processing custom column values", ex);		
			
		} 
		
		
	}
	
	
	public void shutdown() throws Exception{		
		
		try {
			//lock.lock();
			
			custColQueue.put( CustColQueueUtil.generateShutDownVal() );
			
			lock.lock();
			
			resultFileClosed.await(60, TimeUnit.SECONDS);		
			
			

			
		} catch (Exception e) {
			lock.unlock();
			logger.debug("Exception when shutdown", e);
		}
		
		
		
				
		
	}


	public BlockingQueue<CustColValue> getCustColQueue() {
		return custColQueue;
	}


	public void setCustColQueue(BlockingQueue<CustColValue> custColQueue) {
		this.custColQueue = custColQueue;
	}


	public ResultFileWriter getResultFileWriter() {
		return resultFileWriter;
	}


	public void setResultFileWriter(ResultFileWriter resultFileWriter) {
		this.resultFileWriter = resultFileWriter;
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	

}
