package eu.unicreditgroup.cn.custcol.conf;

public class CustColConf {
	
	private String colNum;
	
	private String label;
	
	private String referenced;

	public String getColNum() {
		return colNum;
	}

	public void setColNum(String colNum) {
		this.colNum = colNum;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getReferenced() {
		return referenced;
	}

	public void setReferenced(String referenced) {
		this.referenced = referenced;
	} 
	
	

}
