package eu.unicreditgroup.cn.bizx.mapper;


import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;



public class UserNameMapper implements IMapper {
	
		
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String userId = srcVal[0]; 

		MappedColumn mappedCol = new MappedColumn();
		
		if (StringUtils.isNotEmpty(userId)){
			userId = userId.toLowerCase();
		}
		
		mappedCol.setValue(userId);
		return mappedCol;
	}
	
}
