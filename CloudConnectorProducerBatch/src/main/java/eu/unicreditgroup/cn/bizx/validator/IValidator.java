package eu.unicreditgroup.cn.bizx.validator;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;

/**
 * 
 * Interface for all validator objects
 * 
 * @author UV00074
 *
 */

public interface IValidator {
	/**
	 * 
	 * @param value 	value  to be validated
	 * @param srcValues 	source values used to create the value
	 * @param resColIndex  result column index
	 * @return
	 */
	public boolean validate(String value, String[] srcValues, Integer resColIndex, SourceRow sourceRow);
	
	
	/**
	 * Should a row with invalid value be rejected?
	 * 
	 * @return
	 */
	public boolean isRowRejectable();

}
