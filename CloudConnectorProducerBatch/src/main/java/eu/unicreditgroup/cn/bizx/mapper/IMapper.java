package eu.unicreditgroup.cn.bizx.mapper;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;


/**
 * Interface for all mapper objects
 * 
 * @author UV00074
 * 
 */

public interface IMapper {
	
	/**
	 * 
	 * @param srcVal	list of source values 
	 * @param defaultVal	default value is returned when source values
	 * 	cannot be properly mapped 
	 * When srcVal is null the defaultVal is returned
	 * 
	 * @return the mapped value
	 */
	MappedColumn map(String[] srcVal, String defaultVal , SourceRow sourceRow);
		
	
	
}
