package eu.unicreditgroup.cn.bizx.mapper.exception;

public class InvalidColumnException extends Exception{
	
	public InvalidColumnException(String errMsg){
		super(errMsg);
	}
	
		
}
