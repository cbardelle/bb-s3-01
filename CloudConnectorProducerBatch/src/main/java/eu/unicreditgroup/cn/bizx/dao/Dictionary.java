package eu.unicreditgroup.cn.bizx.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.cn.bizx.bean.User;

public class Dictionary implements InitializingBean{
	
	
	private DictionaryDao dictionaryDao;
	
	private String defaultBizxUserRole;
	
	/**
	 * Hashmap with BizX admins
	 * K - user_id
	 * V - role_name in BizX
	 */
	private HashMap<String,String> bizxAdmins;
	
	
	
	/**
	 * List of active line managers
	 * 
	 */
	private List<String> activeLineManagers;
	


	private void initBizxAdmins(){
		
		bizxAdmins = new HashMap<String,String>();
		
		List<User> admins = dictionaryDao.getAdminWithRole();
		
		for (User admin : admins){
			bizxAdmins.put(admin.getUserId(), admin.getBizxRole());
		}
	}
	
	private void initLineManagerList(){
		activeLineManagers = dictionaryDao.getActiveLineManagers();
		
		
	}
	
	

	
	public String getAdminOrUserRole(String  adminId){
		
		if ( bizxAdmins.containsKey( adminId )){			
			// get a role name for an administrator
			return bizxAdmins.get( adminId );
			
		} else {
			
			return defaultBizxUserRole;
		}
	}


	public DictionaryDao getDictionaryDao() {
		return dictionaryDao;
	}

	
	public void setDictionaryDao(DictionaryDao dictionaryDao) {
		this.dictionaryDao = dictionaryDao;
	}

	
	@Override
	public void afterPropertiesSet() throws Exception {
		initBizxAdmins();
		initLineManagerList();
		defaultBizxUserRole = dictionaryDao.getDefaultBixzUserRole();
	}
	
	public String getDefaultBizxUserRole() {
		return defaultBizxUserRole;
	}

	public void setDefaultBizxUserRole(String defaultBizxUserRole) {
		this.defaultBizxUserRole = defaultBizxUserRole;
	}


	public boolean isActiveLineManager(String lineManager) {
		return activeLineManagers.contains( lineManager.toUpperCase() );
	}
	
}
