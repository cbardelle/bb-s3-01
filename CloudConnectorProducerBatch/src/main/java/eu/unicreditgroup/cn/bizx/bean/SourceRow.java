package eu.unicreditgroup.cn.bizx.bean;

import org.apache.commons.lang.StringUtils;


public class SourceRow {
	
	String sourceFileName;

	String[] data;
	
	Boolean valid;
	
	final int USER_ID = 1;
	
	final int FIRST_NAME = 2;
	
	final int LAST_NAME = 3;
	
	final int HOST_LEGAL_ENTITY = 73; // custom column 270
	
	public String getUserId()
	{
		if (data == null){
			return "";
		}		
		return data[USER_ID];		
	}
	
	
	public String getHostLegalEntity(){
		if (data == null){
			return "";
		}		
		return data[HOST_LEGAL_ENTITY];		
		
	}
	
	public String getIdOrUserName()
	{
		if (data == null){
			return "";
		}
		
		if (StringUtils.isNotEmpty(data[USER_ID])){
			return data[USER_ID];
		} else {
			// if there's no user Id return fname + last name
			return data[FIRST_NAME] + " " + data[LAST_NAME];	
		}	
		
	}
	
	public String getValue(int index){	
		
		if (data == null){
			return null;
		}
		if (index >= data.length){
			return null;
		}
		
		return data[index];
	}
	
	public void setValue(int index, String val){		
		data[index] = val;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

}
