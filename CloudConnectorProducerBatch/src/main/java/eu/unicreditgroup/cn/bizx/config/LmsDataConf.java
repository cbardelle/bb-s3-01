package eu.unicreditgroup.cn.bizx.config;

import java.util.List;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;

/**
 * Configuration of the source file
 * (spring configured)
 * 
 * @author UV00074
 *
 */
public class LmsDataConf {
	
	private List<LmsColumn> lmsColumns;
	
	private LmsColumn userId;
	
	private LmsColumn firstName;
			
	private LmsColumn lastName;


	public List<LmsColumn> getLmsColumns() {
		return lmsColumns;
	}

	public void setLmsColumns(List<LmsColumn> lmsColumns) {
		this.lmsColumns = lmsColumns;
	}

	public LmsColumn getUserId() {
		return userId;
	}

	public void setUserId(LmsColumn userId) {
		this.userId = userId;
	}

	public LmsColumn getFirstName() {
		return firstName;
	}

	public void setFirstName(LmsColumn firstName) {
		this.firstName = firstName;
	}

	public LmsColumn getLastName() {
		return lastName;
	}

	public void setLastName(LmsColumn lastName) {
		this.lastName = lastName;
	}

}
