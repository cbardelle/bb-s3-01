package eu.unicreditgroup.cn.bizx.queue;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.bizx.bean.MappedRow;
import eu.unicreditgroup.cn.bizx.config.BizxColumn;
import eu.unicreditgroup.cn.bizx.util.ResultFileWriter;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;


/**
 * 
 * The consumer of mapped rows from the mappedRowsQueue queue.
 * Rows are write down to the result file which will be sent to the BizX connector. 
 * 
 * @author UV00074
 *
 */
public class BizxDataConsumer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(BizxDataConsumer.class);
	
	private ConfigParams configParams;
			
	private int consumerNumber;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<MappedRow> mappedRowsQueue;
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
		 
	private final String BIZX_COLUMN_DELIMITER = ",";	
	
	private final String BIZX_LINE_END = "\r\n";
	
	private ResultFileWriter bizxFileWriter = null;
	
	private List<BizxColumn> resultColumns;
		
	
	
	public void closeResultFile(){
		if ( bizxFileWriter!= null ){
			try {				
				bizxFileWriter.close();
			} catch (Exception e) {
				logger.error("Failed to close result file", e);
			}
		}		
	}
	
	@Override
	public void run() {
		
		
		try {
			
			//bizxFileWriter = new ResultFileWriter(PLATEAU_COLUMN_DELIMITER, PLATEAU_LINE_END, configParams.getPlateauFullFilePath());
			
			bizxFileWriter = new ResultFileWriter(BIZX_COLUMN_DELIMITER, BIZX_LINE_END, configParams.getResultFileFullPathBizxConn());
			
			bizxFileWriter.writeHeader(resultColumns);
			
			bizxFileWriter.writeHeaderDescriptions(resultColumns);
			
			
						
			while(true){
				Thread.yield();
				
				MappedRow mappedRow = mappedRowsQueue.take();								
				
				if (BizxRowsQueueUtil.isShutdownRow(mappedRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					mappedRowsQueue.put(mappedRow);
					
					return;					
				}
								
				//reportDataBean.incResultFileRowsCount();				
				bizxFileWriter.writeResultRow( mappedRow.getData() );
				reportDataBean.incBizxRowTotal();
				
				/*
				if (configParams.isGenearteCsvFile())
				{
					csvFileWriter.writeResultRow( mappedRow.getData() );
				}*/
								
			}
			
		} catch (Exception ex) {
			logger.error(String.format("Exception during processing mapped rows" ), ex);			
			bizxExceptionsQueue.add(ex);
			
			// when exception occurs during writing the result the file is not sent
			//reportDataBean.setFailedFastException(ex);
		} finally {
			
			try {
				
				bizxFileWriter.close();
				
			} catch (Exception e) {				
				logger.error(e);
				logger.debug("Exception during closing result file writer", e);				
			}			
		}
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}


	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	
	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}
	

	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}
	

	public BlockingQueue<MappedRow> getMappedRowsQueue() {
		return mappedRowsQueue;
	}
	

	public void setMappedRowsQueue(BlockingQueue<MappedRow> mappedRowsQueue) {
		this.mappedRowsQueue = mappedRowsQueue;
	}
	

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}
	

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public List<BizxColumn> getResultColumns() {
		return resultColumns;
	}

	public void setResultColumns(List<BizxColumn> resultColumns) {
		this.resultColumns = resultColumns;
	}
	
	

}
