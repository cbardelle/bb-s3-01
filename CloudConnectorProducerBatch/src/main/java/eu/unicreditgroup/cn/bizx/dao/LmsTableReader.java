package eu.unicreditgroup.cn.bizx.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.config.LmsDataConf;

public class LmsTableReader {
	
	private static final Logger logger = Logger.getLogger(LmsTableReader.class);
	
	private LmsDataConf lmsDataConf;	
			
	private class LmsRowMapper implements RowMapper<SourceRow> {
		@Override
		public SourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			SourceRow srcRow = new SourceRow();
			
			int lmsColsCount = lmsDataConf.getLmsColumns().size(); 
					
			String[] row = new String[lmsColsCount];					
			
			for ( int i = 0; i < lmsColsCount ; i++ ){
				row[i] = rs.getString(i+1);
			}
			srcRow.setData(row);			
			return srcRow;
		}
	}
		
	private SimpleJdbcTemplate simpleJdbcTemplate;
		
	private DataSource dataSource;
	
	
	// ???
	//private boolean runOnce = true;

	private static int bunchSize = 1000;
	
	private static int bunchRowMin = 1;
	
	private static int bunchRowMax;
	
	//private AtomicBoolean atomicEmpty = new AtomicBoolean(false);
	
	private static  AtomicBoolean atomicEmpty = new AtomicBoolean(false);
	//private static  boolean empty = false;
		
	
	// count of all rows in a table
	private int rowsCount = -1;
	
	private String SQL_ROWS_COUNT = "SELECT COUNT(*) FROM HR_CONN_RES_FILE";
	
	private String SQL_GET_NEXT_BUNCH = 
	"	SELECT * FROM															" +	
	"	  ( SELECT /*+ FIRST_ROWS(%d) */ src.*, ROWNUM rnum  FROM	" +
	"	    ( SELECT * FROM HR_CONN_RES_FILE ORDER BY user_id ) src				" +
	"	    WHERE ROWNUM <= %d  )								" +
	"	WHERE rnum >= %d											";
	
	
	public synchronized String getNextBunchSql(){

		bunchRowMax = bunchRowMin + bunchSize - 1;        
        if (bunchRowMax > getTotalRowsCount()) {            
            bunchRowMax = getTotalRowsCount();        }        
                
        logger.debug(String.format("Getting rows %d to %d", bunchRowMin , bunchRowMax ));                
        String sql = String.format(SQL_GET_NEXT_BUNCH, bunchSize,  bunchRowMax , bunchRowMin);        
		bunchRowMin += bunchSize;		
		if ( bunchRowMin > getTotalRowsCount() ) {			
			
			atomicEmpty.set(true);
			//empty = true;
		}					

		return sql;
	}
	
	public List<SourceRow> getNextBunch(){
		if ( atomicEmpty.get() == true ) {			
			return null;
		}		
		return simpleJdbcTemplate.query(getNextBunchSql(), new LmsRowMapper());	
	}

	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }	

	public int getTotalRowsCount(){
		if (rowsCount < 0){			
			rowsCount = simpleJdbcTemplate.queryForInt(SQL_ROWS_COUNT);			 
		}		
		return rowsCount;		
	}
			
	/*
	public int getSourceColCount() {
		return sourceColCount;
	}

	public void setSourceColCount(int sourceColCount) {
		this.sourceColCount = sourceColCount;
	}
	*/

	public long getBunchSize() {
		return bunchSize;
	}

	public void setBunchSize(int bunchSize) {
		this.bunchSize = bunchSize;
	}
	
	/*
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public boolean isEmpty() {
		//return atomicEmpty.get();
		return empty;
	}*/

	public LmsDataConf getLmsDataConf() {
		return lmsDataConf;
	}

	public void setLmsDataConf(LmsDataConf lmsDataConf) {
		this.lmsDataConf = lmsDataConf;
	}
		
}
