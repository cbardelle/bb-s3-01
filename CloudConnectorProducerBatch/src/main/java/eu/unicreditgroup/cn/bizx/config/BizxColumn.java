package eu.unicreditgroup.cn.bizx.config;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.mapper.IMapper;
import eu.unicreditgroup.cn.bizx.validator.IValidator;

/**
 * Metadata of the result column
 * 
 * 
 * @author UV00074
 *
 */

public class BizxColumn {
	
	private String resColName;
	
	private int resColIndex;
	
	/**
	 * source column(s) that will be  used for mapping � (sometimes more then one)
	 */
	private List<LmsColumn> srcCol;

	/**
	 * column mapper
	 */
	private IMapper mapper;
	
	/**
	 * validators list
	 */
	private List<IValidator> validators;
	
	/**
	 * validators that will allow to reject a row 
	 * 
	 * For example for external users columns firstName, lastName are mandatory,
	 * so external users without firstName or/and lastName will be rejected 
	 * 
	 */
	private List<IValidator> rejectableValidators;
	
	
	/**
	 * Is the column mandatory
	 */
	private boolean mandatory = false;
	
	/**
	 * Description of the column
	 * for non custom columns: column name in the result file 
	 * for custom columns: columns description (label) in Plateau
	 */
	private String resColDesc;
	
	/**
	 * LMS attribute id used in assignment profiles in Plateau  
	 * 
	 */
	private String lmsAttrName;
	
		
	String type;
	
	int length;
	
	String defaultValue = null;
	
	
	

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getResColName() {
		return resColName;
	}

	public void setResColName(String resColName) {
		this.resColName = resColName;
	}

	public int getResColIndex() {
		return resColIndex;
	}

	public void setResColIndex(int resColIndex) {
		this.resColIndex = resColIndex;
	}

	

	public IMapper getMapper() {
		return mapper;
	}

	public void setMapper(IMapper mapper) {
		this.mapper = mapper;
	}

	public List<IValidator> getValidators() {
		return validators;
	}

	public void setValidators(List<IValidator> validators) {
		this.validators = validators;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	

	public String getResColDesc() {
		return resColDesc;
	}

	public void setResColDesc(String resColDesc) {
		this.resColDesc = resColDesc;
	}

	public String getLmsAttrName() {
		if (StringUtils.isEmpty(lmsAttrName) ){
			return "";
		}
		else{
			return lmsAttrName;
		}
	}

	public void setLmsAttrName(String lmsAttrName) {
		this.lmsAttrName = lmsAttrName;
	}

	public List<IValidator> getRejectableValidators() {
		return rejectableValidators;
	}

	public void setRejectableValidators(List<IValidator> rejectableValidators) {
		this.rejectableValidators = rejectableValidators;
	}

	public List<LmsColumn> getSrcCol() {
		return srcCol;
	}

	public void setSrcCol(List<LmsColumn> srcCol) {
		this.srcCol = srcCol;
	}

	
	
	
}
