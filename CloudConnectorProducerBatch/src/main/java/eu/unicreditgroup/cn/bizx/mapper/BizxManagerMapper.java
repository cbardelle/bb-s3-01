package eu.unicreditgroup.cn.bizx.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.dao.Dictionary;

public class BizxManagerMapper implements IMapper {
	
	private Dictionary dictionary;
	
	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
				
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {			
			
			// check line manager exist is not empty
			String lineManager = srcVal[0];

			// if empty use the default value
			if (StringUtils.isEmpty( lineManager )) {
				mappedCol.setValue(defaultVal);
				return mappedCol; 
			} else { // check is LM is and exisiting an active user
				if (dictionary.isActiveLineManager( lineManager ) ){
					mappedCol.setValue( lineManager );
					return mappedCol;
				} else {
					mappedCol.setValue(defaultVal);
					return mappedCol;
				}
			}
		}
	}


	public Dictionary getDictionary() {
		return dictionary;
	}


	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	

	

}
