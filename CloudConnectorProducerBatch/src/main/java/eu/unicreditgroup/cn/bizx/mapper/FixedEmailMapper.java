package eu.unicreditgroup.cn.bizx.mapper;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;

public class FixedEmailMapper implements IMapper {

	private String defaultEmail; 
	
	public FixedEmailMapper(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		return new MappedColumn(defaultEmail);
	}

}
