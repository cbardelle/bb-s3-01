package eu.unicreditgroup.cn.bizx.validator;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;

//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

public class EmailValidator implements IValidator {

	/*
	private static Pattern pattern;
	private Matcher matcher;
	 
	private static final String EMAIL_PATTERN =  "^\\w+[\\w-\\.]*\\@\\w+((-\\w+)|(\\w*))\\.[a-z]{2,3}$";
	 
	static{
		pattern = Pattern.compile(EMAIL_PATTERN);
	}*/

	@Override
	public boolean validate(String value, String[] srcValues, Integer resColIndex , SourceRow sourceRow) {
		
		if (StringUtils.isEmpty(value)){
			return true;
		}
		
		int position = value.indexOf("@"); 
		
		if ( position > 0 && position < value.length()  ){
			return true;
		} else {
			return false;
		}
		
		
		
		//matcher = pattern.matcher(value);
		//return matcher.matches();
		
	}

	@Override
	public boolean isRowRejectable() {
		return false;
	}
}
