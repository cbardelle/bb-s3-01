package eu.unicreditgroup.cn.bizx.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import eu.unicreditgroup.cn.bizx.bean.User;

public class DictionaryDao extends SimpleJdbcDaoSupport {

	private static final Logger logger = Logger.getLogger(DictionaryDao.class);
	
	private static final String USR_DICT_TABLE = "hr_conn_usr_attr_dict";
	
	
		
	private java.sql.Timestamp toSqlTimeStamp(java.util.Date javaDate){
		return new java.sql.Timestamp(javaDate.getTime());		
	}
	

	public String getDefaultBixzUserRole() {
		
		String sql = "SELECT ROLE_NAME FROM BIZX_CONN_ROLES WHERE ROLE_ID = 'LMSU'";
		
		return getJdbcTemplate().query(sql, new ResultSetExtractor<String>(){

			@Override
			public String extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				rs.next();
				return rs.getString(1);
			}
		});
		
	}
	
	
	public List<User> getAdminWithRole() {
		String sql = "SELECT A.USER_ID, R.ROLE_NAME  FROM BIZX_CONN_ADMINS A, BIZX_CONN_ROLES R WHERE A.ROLE_ID = R.ROLE_ID";

		return getJdbcTemplate().query(sql, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				User user = new User();
				
				user.setUserId(rs.getString("USER_ID"));
				user.setBizxRole(rs.getString("ROLE_NAME"));
				
				return user;
			}
		});
	}


	public List<String> getActiveLineManagers() {
		
		String sql = "SELECT DISTINCT UPPER(SUPERVISOR) AS USER_ID FROM HR_CONN_RES_FILE WHERE SUPERVISOR IS NOT NULL AND SUPERVISOR IN ( SELECT USER_ID FROM HR_CONN_RES_FILE WHERE NOT_ACTIVE = 'N' )";
				
		return getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				
				return rs.getString("USER_ID");
				
			}
		});
	}
	
	
	
	
	
	
	
}
