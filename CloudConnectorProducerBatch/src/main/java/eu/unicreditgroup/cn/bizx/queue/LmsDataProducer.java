package eu.unicreditgroup.cn.bizx.queue;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.dao.LmsTableReader;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.cn.bizx.queue.SourceRowQueueUtil;

/**
 * 
 * 
 * 
 * This class is responsible for reading rows from the HR_CONN_RES_FILE
 * (a table with users data which is imported to LMS) 
 *  * Each row is put into a queue, which acts like a read buffer.
 * When all rows from the table are processed termination object is put into table.
 *   
 * 
 * @author UV00074
 *
 */
public class LmsDataProducer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(LmsDataProducer.class);
	
	private ConfigParams configParams;
			
	private BlockingQueue<SourceRow> lmsDataQueue;
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	private LmsTableReader lmsTableReader;
	
	private int consumerNumber;
	
	@Override
	public void run() {

		try {			
			
			while( true ){
				
				Thread.yield();				
			

				if ( SourceRowQueueUtil.isShutdownRow( lmsDataQueue.peek()) ){
					
					logger.debug(String.format("[LMS data procuder %d] will stop its work ", consumerNumber));
					 
					return;
				}
				
				List<SourceRow> buffer = lmsTableReader.getNextBunch();		

				// no more data in table...
				if (buffer == null || buffer.isEmpty()){
					
					lmsDataQueue.put(SourceRowQueueUtil.generateShutdownRow());
					
					return;
				}			
				for (SourceRow srcRow : buffer){									
					lmsDataQueue.put(srcRow);
				}				
			}
			
		} catch (Exception e) {
			
			logger.error("Exception in source tabale producer.", e);
			bizxExceptionsQueue.add(e);
			
		} finally {
			
			try{				
				// put end of the queue marker
				lmsDataQueue.put(SourceRowQueueUtil.generateShutdownRow());		
				
				reportDataBean.setLmsSourceRowTotal(lmsTableReader.getTotalRowsCount());
				
			} catch(Exception e){
				logger.error("Exception occured : " + e);
			}			
		}
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}
	
	

	

	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}

	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public void setConsumerNumber(int i) {
		consumerNumber = i;
		
	}

	public BlockingQueue<SourceRow> getLmsDataQueue() {
		return lmsDataQueue;
	}

	public void setLmsDataQueue(BlockingQueue<SourceRow> lmsDataQueue) {
		this.lmsDataQueue = lmsDataQueue;
	}

	public LmsTableReader getLmsTableReader() {
		return lmsTableReader;
	}

	public void setLmsTableReader(LmsTableReader lmsTableReader) {
		this.lmsTableReader = lmsTableReader;
	}

	public int getConsumerNumber() {
		return consumerNumber;
	}

	
	
}
