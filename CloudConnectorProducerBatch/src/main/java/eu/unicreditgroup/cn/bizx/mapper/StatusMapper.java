package eu.unicreditgroup.cn.bizx.mapper;


import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;



public class StatusMapper implements IMapper {
	
	private final String LMS_ACTIVE_NO = "N";
	private final String LMS_ACTIVE_YES = "Y";
	private final String BIZX_ACTIVE = "active";
	private final String BIZX_INACTIVE = "inactive";
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String notActive = srcVal[0]; 

		MappedColumn mappedCol = new MappedColumn();
		
		String bizxStatus = "";
		
		if (LMS_ACTIVE_NO.equalsIgnoreCase(notActive)){
			bizxStatus = BIZX_ACTIVE;
		} else if (LMS_ACTIVE_YES.equalsIgnoreCase(notActive)){
			bizxStatus = BIZX_INACTIVE;			
		} else {
			mappedCol.setErrorMsg(	String.format("Failed to map user status, lsm not_active value : %s", notActive) );
			return mappedCol; 
		}
		mappedCol.setValue(bizxStatus);
		return mappedCol;
	}
	
}
