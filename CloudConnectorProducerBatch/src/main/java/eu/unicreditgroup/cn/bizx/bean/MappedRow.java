package eu.unicreditgroup.cn.bizx.bean;

public class MappedRow {

	String[] data;
	
	int t;
	
	Boolean valid;	
		
	public String getValue(int index){		
		return data[index];
	}
	
	public void setValue(int index, String val){		
		data[index] = val;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	
}
