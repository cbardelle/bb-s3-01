package eu.unicreditgroup.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.annotation.Resource;

import org.junit.Test;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;
import eu.unicreditgroup.cn.custcol.conf.CustColConf;
import eu.unicreditgroup.cn.custcol.proc.CustColProc;
import eu.unicreditgroup.config.ConfigParams;

public class CustColProcTest extends BaseSpringTest {

	@Resource(name = "custColProc")
	private CustColProc custColProc;

	@Test
	public void testProcessValues() {

		// public void processValues( List<CustColValue> data, CustColConf
		// custColConf ){
		List<CustColValue> data = new ArrayList<CustColValue>();

		CustColValue v1 = new CustColValue();
		v1.setColNum("1");
		v1.setLabel("COLUMN_1");
		v1.setId("AAA");
		v1.setDesc("AAA-DESC-111");
		v1.setCount(22);

		CustColValue v2 = new CustColValue();
		v2.setColNum("1");
		v2.setLabel("COLUMN_1");
		v2.setId("BBB");
		v2.setDesc("BBB-DESC-111");
		v2.setCount(1);

		CustColValue v3 = new CustColValue();
		v3.setColNum("1");
		v3.setLabel("COLUMN_1");
		v3.setId("AAA");
		v3.setDesc("AAA-DESC-222");
		v3.setCount(11);

		CustColValue v4 = new CustColValue();
		v4.setColNum("1");
		v4.setLabel("COLUMN_1");
		v4.setId("AAA");
		v4.setDesc("AAA-DESC-333");
		v4.setCount(42);

		data.add(v1);

		data.add(v2);

		data.add(v3);

		data.add(v4);

		CustColConf custColConf = new CustColConf();
		custColConf.setColNum("1");
		custColConf.setLabel("COLUMN_1");
		custColProc.processValues(data, custColConf);

		BlockingQueue<CustColValue> values = custColProc.getCustColQueue();

		while (true) {

			CustColValue val = values.poll();
			if (val == null)
				break;

			System.out.println(">>>>>>>>>>> " + val.getId() + " "
					+ val.getDesc());

		}
	}

}