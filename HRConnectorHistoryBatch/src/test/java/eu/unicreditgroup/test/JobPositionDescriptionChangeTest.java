package eu.unicreditgroup.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

import eu.unicreditgroup.batch.processor.JobLocationDescriptionProcessor;
import eu.unicreditgroup.batch.processor.JobPositionDescriptionProcessor;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.utils.PlateauUtil;



public class JobPositionDescriptionChangeTest { //  extends BaseSpringTest{
	
		
	//@Resource(name = "jobPositionDescriptionProcessor")
	private JobPositionDescriptionProcessor jobPositionDescriptionProcessor;
	
	
	//@Resource(name = "configParams")
	private ConfigParams configParams;
	
	
	//@Test
	public void changeJobPositionDescr(){

		List lst = new ArrayList<Map<String, Object>>();
		Map jobPos = new HashMap<String,Object>();
		jobPos.put("CODE","TEST01");
		jobPos.put("DESCR","TEST 01 BRAND NEW NEW NEW");
		
		lst.add(jobPos);
		
		try {

			//note the proxy is REQUIRED for the staging!
			
			
			String proxyUrl = "l7fwout.usinet.it";
			int proxyPort = 3128;
			WebClient webClient= PlateauUtil.getLoggedWebClient( configParams.getStrPlateauLoginPageUrl(),
											configParams.getStrPlateauLoginUsr(),
											configParams.getStrPlateauLoginPwd(), 
											BrowserVersion.FIREFOX_3,
											proxyUrl, proxyPort );
			
			
			jobPositionDescriptionProcessor.updateJobPositionDescriptionInPlateau(lst, webClient);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
}
