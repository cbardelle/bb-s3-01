
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.plateausystems.elms.webservices.axis package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CurriculumCreationDate_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "CreationDate");
    private final static QName _CurriculumLastUpdateTimestamp_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "LastUpdateTimestamp");
    private final static QName _CurriculumBasisDateInDBTimezone_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "BasisDateInDBTimezone");
    private final static QName _ItemsSearchCriteriaRevisionDate_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "RevisionDate");
    private final static QName _ItemContactHours_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "ContactHours");
    private final static QName _ItemMinEnrollment_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "MinEnrollment");
    private final static QName _ItemCreditHours_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "CreditHours");
    private final static QName _ItemDefaultRetrainingNumber_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "DefaultRetrainingNumber");
    private final static QName _ItemCreateTimestamp_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "CreateTimestamp");
    private final static QName _ItemWaitlistRemainderSentTimestamp_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "WaitlistRemainderSentTimestamp");
    private final static QName _ItemEnrollmentThresDays_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "EnrollmentThresDays");
    private final static QName _ItemApprovalTimestamp_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "ApprovalTimestamp");
    private final static QName _ItemRating_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "Rating");
    private final static QName _ItemTotalLength_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "TotalLength");
    private final static QName _ItemDefaultInitialNumber_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "DefaultInitialNumber");
    private final static QName _ItemCpeHours_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "CpeHours");
    private final static QName _ItemPrepLength_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "PrepLength");
    private final static QName _ItemMaxEnrollment_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "MaxEnrollment");
    private final static QName _ItemsToReviseItemToReviseSourceItemRevisionDate_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "SourceItemRevisionDate");
    private final static QName _ItemsToReviseItemToReviseDestinationItemRevisionDate_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "DestinationItemRevisionDate");
    private final static QName _LearnerTerminationDate_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "TerminationDate");
    private final static QName _LearnerHireDate_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "HireDate");
    private final static QName _LearnerResetPin_QNAME = new QName("http://axis.webservices.elms.plateausystems.com", "ResetPin");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.plateausystems.elms.webservices.axis
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateLearners }
     * 
     */
    public UpdateLearners createUpdateLearners() {
        return new UpdateLearners();
    }

    /**
     * Create an instance of {@link Caller }
     * 
     */
    public Caller createCaller() {
        return new Caller();
    }

    /**
     * Create an instance of {@link Learner }
     * 
     */
    public Learner createLearner() {
        return new Learner();
    }

    /**
     * Create an instance of {@link Organizations }
     * 
     */
    public Organizations createOrganizations() {
        return new Organizations();
    }

    /**
     * Create an instance of {@link Organization }
     * 
     */
    public Organization createOrganization() {
        return new Organization();
    }

    /**
     * Create an instance of {@link LearnersSearchCriteria }
     * 
     */
    public LearnersSearchCriteria createLearnersSearchCriteria() {
        return new LearnersSearchCriteria();
    }

    /**
     * Create an instance of {@link UpdateOrganizations }
     * 
     */
    public UpdateOrganizations createUpdateOrganizations() {
        return new UpdateOrganizations();
    }

    /**
     * Create an instance of {@link AddItems }
     * 
     */
    public AddItems createAddItems() {
        return new AddItems();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link DeleteCurricula }
     * 
     */
    public DeleteCurricula createDeleteCurricula() {
        return new DeleteCurricula();
    }

    /**
     * Create an instance of {@link Curriculum }
     * 
     */
    public Curriculum createCurriculum() {
        return new Curriculum();
    }

    /**
     * Create an instance of {@link UpdateGeneralReferences }
     * 
     */
    public UpdateGeneralReferences createUpdateGeneralReferences() {
        return new UpdateGeneralReferences();
    }

    /**
     * Create an instance of {@link JobLocation }
     * 
     */
    public JobLocation createJobLocation() {
        return new JobLocation();
    }

    /**
     * Create an instance of {@link CurriculaSearchCriteria }
     * 
     */
    public CurriculaSearchCriteria createCurriculaSearchCriteria() {
        return new CurriculaSearchCriteria();
    }

    /**
     * Create an instance of {@link UpdateCurricula }
     * 
     */
    public UpdateCurricula createUpdateCurricula() {
        return new UpdateCurricula();
    }

    /**
     * Create an instance of {@link DeleteLearners }
     * 
     */
    public DeleteLearners createDeleteLearners() {
        return new DeleteLearners();
    }

    /**
     * Create an instance of {@link AddCurricula }
     * 
     */
    public AddCurricula createAddCurricula() {
        return new AddCurricula();
    }

    /**
     * Create an instance of {@link Learners }
     * 
     */
    public Learners createLearners() {
        return new Learners();
    }

    /**
     * Create an instance of {@link Items }
     * 
     */
    public Items createItems() {
        return new Items();
    }

    /**
     * Create an instance of {@link ItemsToRevise }
     * 
     */
    public ItemsToRevise createItemsToRevise() {
        return new ItemsToRevise();
    }

    /**
     * Create an instance of {@link ItemsToReviseItemToRevise }
     * 
     */
    public ItemsToReviseItemToRevise createItemsToReviseItemToRevise() {
        return new ItemsToReviseItemToRevise();
    }

    /**
     * Create an instance of {@link OrganizationsSearchCriteria }
     * 
     */
    public OrganizationsSearchCriteria createOrganizationsSearchCriteria() {
        return new OrganizationsSearchCriteria();
    }

    /**
     * Create an instance of {@link DeleteOrganizations }
     * 
     */
    public DeleteOrganizations createDeleteOrganizations() {
        return new DeleteOrganizations();
    }

    /**
     * Create an instance of {@link AddLearners }
     * 
     */
    public AddLearners createAddLearners() {
        return new AddLearners();
    }

    /**
     * Create an instance of {@link GeneralReferences }
     * 
     */
    public GeneralReferences createGeneralReferences() {
        return new GeneralReferences();
    }

    /**
     * Create an instance of {@link Curricula }
     * 
     */
    public Curricula createCurricula() {
        return new Curricula();
    }

    /**
     * Create an instance of {@link DeleteItems }
     * 
     */
    public DeleteItems createDeleteItems() {
        return new DeleteItems();
    }

    /**
     * Create an instance of {@link UpdateItems }
     * 
     */
    public UpdateItems createUpdateItems() {
        return new UpdateItems();
    }

    /**
     * Create an instance of {@link UpdateLearnersPasswords }
     * 
     */
    public UpdateLearnersPasswords createUpdateLearnersPasswords() {
        return new UpdateLearnersPasswords();
    }

    /**
     * Create an instance of {@link LearnerPassword }
     * 
     */
    public LearnerPassword createLearnerPassword() {
        return new LearnerPassword();
    }

    /**
     * Create an instance of {@link GeneralReferencesSearchCriteria }
     * 
     */
    public GeneralReferencesSearchCriteria createGeneralReferencesSearchCriteria() {
        return new GeneralReferencesSearchCriteria();
    }

    /**
     * Create an instance of {@link ItemsToCopy }
     * 
     */
    public ItemsToCopy createItemsToCopy() {
        return new ItemsToCopy();
    }

    /**
     * Create an instance of {@link ItemsToCopyItemToCopy }
     * 
     */
    public ItemsToCopyItemToCopy createItemsToCopyItemToCopy() {
        return new ItemsToCopyItemToCopy();
    }

    /**
     * Create an instance of {@link AddOrganizations }
     * 
     */
    public AddOrganizations createAddOrganizations() {
        return new AddOrganizations();
    }

    /**
     * Create an instance of {@link ItemsSearchCriteria }
     * 
     */
    public ItemsSearchCriteria createItemsSearchCriteria() {
        return new ItemsSearchCriteria();
    }

    /**
     * Create an instance of {@link StudentPhonesStudentPhone }
     * 
     */
    public StudentPhonesStudentPhone createStudentPhonesStudentPhone() {
        return new StudentPhonesStudentPhone();
    }

    /**
     * Create an instance of {@link CustomColumnVOList }
     * 
     */
    public CustomColumnVOList createCustomColumnVOList() {
        return new CustomColumnVOList();
    }

    /**
     * Create an instance of {@link ItemCustomColumnWSVO }
     * 
     */
    public ItemCustomColumnWSVO createItemCustomColumnWSVO() {
        return new ItemCustomColumnWSVO();
    }

    /**
     * Create an instance of {@link CustomColumnWSVO }
     * 
     */
    public CustomColumnWSVO createCustomColumnWSVO() {
        return new CustomColumnWSVO();
    }

    /**
     * Create an instance of {@link ItemCustomColumnVOList }
     * 
     */
    public ItemCustomColumnVOList createItemCustomColumnVOList() {
        return new ItemCustomColumnVOList();
    }

    /**
     * Create an instance of {@link StudentPhones }
     * 
     */
    public StudentPhones createStudentPhones() {
        return new StudentPhones();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "CreationDate", scope = Curriculum.class)
    public JAXBElement<XMLGregorianCalendar> createCurriculumCreationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumCreationDate_QNAME, XMLGregorianCalendar.class, Curriculum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "LastUpdateTimestamp", scope = Curriculum.class)
    public JAXBElement<XMLGregorianCalendar> createCurriculumLastUpdateTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumLastUpdateTimestamp_QNAME, XMLGregorianCalendar.class, Curriculum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "BasisDateInDBTimezone", scope = Curriculum.class)
    public JAXBElement<XMLGregorianCalendar> createCurriculumBasisDateInDBTimezone(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumBasisDateInDBTimezone_QNAME, XMLGregorianCalendar.class, Curriculum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "RevisionDate", scope = ItemsSearchCriteria.class)
    public JAXBElement<XMLGregorianCalendar> createItemsSearchCriteriaRevisionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemsSearchCriteriaRevisionDate_QNAME, XMLGregorianCalendar.class, ItemsSearchCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "RevisionDate", scope = Item.class)
    public JAXBElement<XMLGregorianCalendar> createItemRevisionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemsSearchCriteriaRevisionDate_QNAME, XMLGregorianCalendar.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "ContactHours", scope = Item.class)
    public JAXBElement<Double> createItemContactHours(Double value) {
        return new JAXBElement<Double>(_ItemContactHours_QNAME, Double.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "MinEnrollment", scope = Item.class)
    public JAXBElement<Long> createItemMinEnrollment(Long value) {
        return new JAXBElement<Long>(_ItemMinEnrollment_QNAME, Long.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "CreditHours", scope = Item.class)
    public JAXBElement<Double> createItemCreditHours(Double value) {
        return new JAXBElement<Double>(_ItemCreditHours_QNAME, Double.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "DefaultRetrainingNumber", scope = Item.class)
    public JAXBElement<Long> createItemDefaultRetrainingNumber(Long value) {
        return new JAXBElement<Long>(_ItemDefaultRetrainingNumber_QNAME, Long.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "CreateTimestamp", scope = Item.class)
    public JAXBElement<XMLGregorianCalendar> createItemCreateTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemCreateTimestamp_QNAME, XMLGregorianCalendar.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "WaitlistRemainderSentTimestamp", scope = Item.class)
    public JAXBElement<XMLGregorianCalendar> createItemWaitlistRemainderSentTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemWaitlistRemainderSentTimestamp_QNAME, XMLGregorianCalendar.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "EnrollmentThresDays", scope = Item.class)
    public JAXBElement<Long> createItemEnrollmentThresDays(Long value) {
        return new JAXBElement<Long>(_ItemEnrollmentThresDays_QNAME, Long.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "ApprovalTimestamp", scope = Item.class)
    public JAXBElement<XMLGregorianCalendar> createItemApprovalTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemApprovalTimestamp_QNAME, XMLGregorianCalendar.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "Rating", scope = Item.class)
    public JAXBElement<Double> createItemRating(Double value) {
        return new JAXBElement<Double>(_ItemRating_QNAME, Double.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "TotalLength", scope = Item.class)
    public JAXBElement<Double> createItemTotalLength(Double value) {
        return new JAXBElement<Double>(_ItemTotalLength_QNAME, Double.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "LastUpdateTimestamp", scope = Item.class)
    public JAXBElement<XMLGregorianCalendar> createItemLastUpdateTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumLastUpdateTimestamp_QNAME, XMLGregorianCalendar.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "DefaultInitialNumber", scope = Item.class)
    public JAXBElement<Long> createItemDefaultInitialNumber(Long value) {
        return new JAXBElement<Long>(_ItemDefaultInitialNumber_QNAME, Long.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "CpeHours", scope = Item.class)
    public JAXBElement<Double> createItemCpeHours(Double value) {
        return new JAXBElement<Double>(_ItemCpeHours_QNAME, Double.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "PrepLength", scope = Item.class)
    public JAXBElement<Double> createItemPrepLength(Double value) {
        return new JAXBElement<Double>(_ItemPrepLength_QNAME, Double.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "MaxEnrollment", scope = Item.class)
    public JAXBElement<Long> createItemMaxEnrollment(Long value) {
        return new JAXBElement<Long>(_ItemMaxEnrollment_QNAME, Long.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "LastUpdateTimestamp", scope = JobLocation.class)
    public JAXBElement<XMLGregorianCalendar> createJobLocationLastUpdateTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumLastUpdateTimestamp_QNAME, XMLGregorianCalendar.class, JobLocation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "SourceItemRevisionDate", scope = ItemsToReviseItemToRevise.class)
    public JAXBElement<XMLGregorianCalendar> createItemsToReviseItemToReviseSourceItemRevisionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemsToReviseItemToReviseSourceItemRevisionDate_QNAME, XMLGregorianCalendar.class, ItemsToReviseItemToRevise.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "DestinationItemRevisionDate", scope = ItemsToReviseItemToRevise.class)
    public JAXBElement<XMLGregorianCalendar> createItemsToReviseItemToReviseDestinationItemRevisionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemsToReviseItemToReviseDestinationItemRevisionDate_QNAME, XMLGregorianCalendar.class, ItemsToReviseItemToRevise.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "SourceItemRevisionDate", scope = ItemsToCopyItemToCopy.class)
    public JAXBElement<XMLGregorianCalendar> createItemsToCopyItemToCopySourceItemRevisionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemsToReviseItemToReviseSourceItemRevisionDate_QNAME, XMLGregorianCalendar.class, ItemsToCopyItemToCopy.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "DestinationItemRevisionDate", scope = ItemsToCopyItemToCopy.class)
    public JAXBElement<XMLGregorianCalendar> createItemsToCopyItemToCopyDestinationItemRevisionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ItemsToReviseItemToReviseDestinationItemRevisionDate_QNAME, XMLGregorianCalendar.class, ItemsToCopyItemToCopy.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "TerminationDate", scope = Learner.class)
    public JAXBElement<XMLGregorianCalendar> createLearnerTerminationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_LearnerTerminationDate_QNAME, XMLGregorianCalendar.class, Learner.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "HireDate", scope = Learner.class)
    public JAXBElement<XMLGregorianCalendar> createLearnerHireDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_LearnerHireDate_QNAME, XMLGregorianCalendar.class, Learner.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "ResetPin", scope = Learner.class)
    public JAXBElement<Boolean> createLearnerResetPin(Boolean value) {
        return new JAXBElement<Boolean>(_LearnerResetPin_QNAME, Boolean.class, Learner.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "LastUpdateTimestamp", scope = Learner.class)
    public JAXBElement<XMLGregorianCalendar> createLearnerLastUpdateTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumLastUpdateTimestamp_QNAME, XMLGregorianCalendar.class, Learner.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://axis.webservices.elms.plateausystems.com", name = "LastUpdateTimestamp", scope = Organization.class)
    public JAXBElement<XMLGregorianCalendar> createOrganizationLastUpdateTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CurriculumLastUpdateTimestamp_QNAME, XMLGregorianCalendar.class, Organization.class, value);
    }

}
