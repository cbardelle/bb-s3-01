
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Item complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Item">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComponentID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComponentTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RevisionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RevisionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DomainID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DeliveryMethodID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequirementTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SafetyRelated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Approved" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NotActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ReviserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApprovedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApprovalTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LessionPlanFilename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TargetAudience" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactHours" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="CreditHours" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="CpeHours" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InstructorMaterials" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StudentMaterials" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TotalLength" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="PrepLength" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="MinEnrollment" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="MaxEnrollment" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="EnrollmentThresDays" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="AutoFillEnrollment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="WaitlistRemainderSentTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AutoCompetency" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Classification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductionReady" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ComponentGoals" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StudentCanRecordLearningEvents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SupervisorCanRecordLearningEvents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ProcessDefinitionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApprovalRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="LastUpdateUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DefaultInitialNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DefaultInitialPeriodTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DefaultInitialBasisTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DefaultRetrainingNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DefaultRetrainingPeriodTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DefaultRetrainingBasisTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rating" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="GradingOption" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ChargebackMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CatalogSKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ScheduleCanOverridePrice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ShippingRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsBlendedLearningRegisterRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsUserRequestsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UserCanWaitlist" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ComponentKey" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SelfEnrollment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CostCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ESigEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="itemCustomColVO" type="{http://axis.webservices.elms.plateausystems.com}ItemCustomColumnVOList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Item", propOrder = {
    "componentID",
    "componentTypeID",
    "revisionDate",
    "title",
    "description",
    "revisionNo",
    "sourceID",
    "domainID",
    "deliveryMethodID",
    "requirementTypeID",
    "safetyRelated",
    "approved",
    "notActive",
    "reviserName",
    "approvedBy",
    "approvalTimestamp",
    "lessionPlanFilename",
    "targetAudience",
    "contactName",
    "contactHours",
    "creditHours",
    "cpeHours",
    "comments",
    "instructorMaterials",
    "studentMaterials",
    "createTimestamp",
    "totalLength",
    "prepLength",
    "minEnrollment",
    "maxEnrollment",
    "enrollmentThresDays",
    "autoFillEnrollment",
    "waitlistRemainderSentTimestamp",
    "autoCompetency",
    "classification",
    "productionReady",
    "componentGoals",
    "studentCanRecordLearningEvents",
    "supervisorCanRecordLearningEvents",
    "processDefinitionID",
    "approvalRequired",
    "lastUpdateUser",
    "lastUpdateTimestamp",
    "defaultInitialNumber",
    "defaultInitialPeriodTypeID",
    "defaultInitialBasisTypeID",
    "defaultRetrainingNumber",
    "defaultRetrainingPeriodTypeID",
    "defaultRetrainingBasisTypeID",
    "rating",
    "gradingOption",
    "chargebackMethod",
    "catalogSKU",
    "scheduleCanOverridePrice",
    "shippingRequired",
    "isBlendedLearningRegisterRequired",
    "isUserRequestsEnabled",
    "userCanWaitlist",
    "componentKey",
    "selfEnrollment",
    "costCurrencyCode",
    "eSigEnabled",
    "itemCustomColVO"
})
public class Item {

    @XmlElement(name = "ComponentID", required = true, nillable = true)
    protected String componentID;
    @XmlElement(name = "ComponentTypeID", required = true, nillable = true)
    protected String componentTypeID;
    @XmlElementRef(name = "RevisionDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> revisionDate;
    @XmlElement(name = "Title", required = true, nillable = true)
    protected String title;
    @XmlElement(name = "Description", required = true, nillable = true)
    protected String description;
    @XmlElement(name = "RevisionNo", required = true, nillable = true)
    protected String revisionNo;
    @XmlElement(name = "SourceID", required = true, nillable = true)
    protected String sourceID;
    @XmlElement(name = "DomainID", required = true, nillable = true)
    protected String domainID;
    @XmlElement(name = "DeliveryMethodID", required = true, nillable = true)
    protected String deliveryMethodID;
    @XmlElement(name = "RequirementTypeID", required = true, nillable = true)
    protected String requirementTypeID;
    @XmlElement(name = "SafetyRelated", required = true, type = Boolean.class, nillable = true)
    protected Boolean safetyRelated;
    @XmlElement(name = "Approved", required = true, type = Boolean.class, nillable = true)
    protected Boolean approved;
    @XmlElement(name = "NotActive", required = true, type = Boolean.class, nillable = true)
    protected Boolean notActive;
    @XmlElement(name = "ReviserName", required = true, nillable = true)
    protected String reviserName;
    @XmlElement(name = "ApprovedBy", required = true, nillable = true)
    protected String approvedBy;
    @XmlElementRef(name = "ApprovalTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> approvalTimestamp;
    @XmlElement(name = "LessionPlanFilename", required = true, nillable = true)
    protected String lessionPlanFilename;
    @XmlElement(name = "TargetAudience", required = true, nillable = true)
    protected String targetAudience;
    @XmlElement(name = "ContactName", required = true, nillable = true)
    protected String contactName;
    @XmlElementRef(name = "ContactHours", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Double> contactHours;
    @XmlElementRef(name = "CreditHours", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Double> creditHours;
    @XmlElementRef(name = "CpeHours", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Double> cpeHours;
    @XmlElement(name = "Comments", required = true, nillable = true)
    protected String comments;
    @XmlElement(name = "InstructorMaterials", required = true, nillable = true)
    protected String instructorMaterials;
    @XmlElement(name = "StudentMaterials", required = true, nillable = true)
    protected String studentMaterials;
    @XmlElementRef(name = "CreateTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> createTimestamp;
    @XmlElementRef(name = "TotalLength", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Double> totalLength;
    @XmlElementRef(name = "PrepLength", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Double> prepLength;
    @XmlElementRef(name = "MinEnrollment", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Long> minEnrollment;
    @XmlElementRef(name = "MaxEnrollment", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Long> maxEnrollment;
    @XmlElementRef(name = "EnrollmentThresDays", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Long> enrollmentThresDays;
    @XmlElement(name = "AutoFillEnrollment", required = true, type = Boolean.class, nillable = true)
    protected Boolean autoFillEnrollment;
    @XmlElementRef(name = "WaitlistRemainderSentTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> waitlistRemainderSentTimestamp;
    @XmlElement(name = "AutoCompetency", required = true, type = Boolean.class, nillable = true)
    protected Boolean autoCompetency;
    @XmlElement(name = "Classification", required = true, nillable = true)
    protected String classification;
    @XmlElement(name = "ProductionReady", required = true, type = Boolean.class, nillable = true)
    protected Boolean productionReady;
    @XmlElement(name = "ComponentGoals", required = true, nillable = true)
    protected String componentGoals;
    @XmlElement(name = "StudentCanRecordLearningEvents", required = true, type = Boolean.class, nillable = true)
    protected Boolean studentCanRecordLearningEvents;
    @XmlElement(name = "SupervisorCanRecordLearningEvents", required = true, type = Boolean.class, nillable = true)
    protected Boolean supervisorCanRecordLearningEvents;
    @XmlElement(name = "ProcessDefinitionID", required = true, nillable = true)
    protected String processDefinitionID;
    @XmlElement(name = "ApprovalRequired", required = true, type = Boolean.class, nillable = true)
    protected Boolean approvalRequired;
    @XmlElement(name = "LastUpdateUser", required = true, nillable = true)
    protected String lastUpdateUser;
    @XmlElementRef(name = "LastUpdateTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> lastUpdateTimestamp;
    @XmlElementRef(name = "DefaultInitialNumber", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Long> defaultInitialNumber;
    @XmlElement(name = "DefaultInitialPeriodTypeID", required = true, nillable = true)
    protected String defaultInitialPeriodTypeID;
    @XmlElement(name = "DefaultInitialBasisTypeID", required = true, nillable = true)
    protected String defaultInitialBasisTypeID;
    @XmlElementRef(name = "DefaultRetrainingNumber", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Long> defaultRetrainingNumber;
    @XmlElement(name = "DefaultRetrainingPeriodTypeID", required = true, nillable = true)
    protected String defaultRetrainingPeriodTypeID;
    @XmlElement(name = "DefaultRetrainingBasisTypeID", required = true, nillable = true)
    protected String defaultRetrainingBasisTypeID;
    @XmlElementRef(name = "Rating", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Double> rating;
    @XmlElement(name = "GradingOption", required = true, type = Long.class, nillable = true)
    protected Long gradingOption;
    @XmlElement(name = "ChargebackMethod", required = true, nillable = true)
    protected String chargebackMethod;
    @XmlElement(name = "CatalogSKU", required = true, nillable = true)
    protected String catalogSKU;
    @XmlElement(name = "ScheduleCanOverridePrice", required = true, type = Boolean.class, nillable = true)
    protected Boolean scheduleCanOverridePrice;
    @XmlElement(name = "ShippingRequired", required = true, type = Boolean.class, nillable = true)
    protected Boolean shippingRequired;
    @XmlElement(name = "IsBlendedLearningRegisterRequired", required = true, type = Boolean.class, nillable = true)
    protected Boolean isBlendedLearningRegisterRequired;
    @XmlElement(name = "IsUserRequestsEnabled", required = true, type = Boolean.class, nillable = true)
    protected Boolean isUserRequestsEnabled;
    @XmlElement(name = "UserCanWaitlist", required = true, type = Boolean.class, nillable = true)
    protected Boolean userCanWaitlist;
    @XmlElement(name = "ComponentKey", required = true, type = Long.class, nillable = true)
    protected Long componentKey;
    @XmlElement(name = "SelfEnrollment", required = true, type = Boolean.class, nillable = true)
    protected Boolean selfEnrollment;
    @XmlElement(name = "CostCurrencyCode", required = true, nillable = true)
    protected String costCurrencyCode;
    @XmlElement(name = "ESigEnabled", required = true, type = Boolean.class, nillable = true)
    protected Boolean eSigEnabled;
    @XmlElement(required = true, nillable = true)
    protected ItemCustomColumnVOList itemCustomColVO;

    /**
     * Gets the value of the componentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentID() {
        return componentID;
    }

    /**
     * Sets the value of the componentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentID(String value) {
        this.componentID = value;
    }

    /**
     * Gets the value of the componentTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentTypeID() {
        return componentTypeID;
    }

    /**
     * Sets the value of the componentTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentTypeID(String value) {
        this.componentTypeID = value;
    }

    /**
     * Gets the value of the revisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getRevisionDate() {
        return revisionDate;
    }

    /**
     * Sets the value of the revisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setRevisionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.revisionDate = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the revisionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevisionNo() {
        return revisionNo;
    }

    /**
     * Sets the value of the revisionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevisionNo(String value) {
        this.revisionNo = value;
    }

    /**
     * Gets the value of the sourceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceID() {
        return sourceID;
    }

    /**
     * Sets the value of the sourceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceID(String value) {
        this.sourceID = value;
    }

    /**
     * Gets the value of the domainID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainID() {
        return domainID;
    }

    /**
     * Sets the value of the domainID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainID(String value) {
        this.domainID = value;
    }

    /**
     * Gets the value of the deliveryMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryMethodID() {
        return deliveryMethodID;
    }

    /**
     * Sets the value of the deliveryMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryMethodID(String value) {
        this.deliveryMethodID = value;
    }

    /**
     * Gets the value of the requirementTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequirementTypeID() {
        return requirementTypeID;
    }

    /**
     * Sets the value of the requirementTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequirementTypeID(String value) {
        this.requirementTypeID = value;
    }

    /**
     * Gets the value of the safetyRelated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSafetyRelated() {
        return safetyRelated;
    }

    /**
     * Sets the value of the safetyRelated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSafetyRelated(Boolean value) {
        this.safetyRelated = value;
    }

    /**
     * Gets the value of the approved property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApproved() {
        return approved;
    }

    /**
     * Sets the value of the approved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApproved(Boolean value) {
        this.approved = value;
    }

    /**
     * Gets the value of the notActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotActive() {
        return notActive;
    }

    /**
     * Sets the value of the notActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotActive(Boolean value) {
        this.notActive = value;
    }

    /**
     * Gets the value of the reviserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReviserName() {
        return reviserName;
    }

    /**
     * Sets the value of the reviserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReviserName(String value) {
        this.reviserName = value;
    }

    /**
     * Gets the value of the approvedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * Sets the value of the approvedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovedBy(String value) {
        this.approvedBy = value;
    }

    /**
     * Gets the value of the approvalTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getApprovalTimestamp() {
        return approvalTimestamp;
    }

    /**
     * Sets the value of the approvalTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setApprovalTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.approvalTimestamp = value;
    }

    /**
     * Gets the value of the lessionPlanFilename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLessionPlanFilename() {
        return lessionPlanFilename;
    }

    /**
     * Sets the value of the lessionPlanFilename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLessionPlanFilename(String value) {
        this.lessionPlanFilename = value;
    }

    /**
     * Gets the value of the targetAudience property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetAudience() {
        return targetAudience;
    }

    /**
     * Sets the value of the targetAudience property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetAudience(String value) {
        this.targetAudience = value;
    }

    /**
     * Gets the value of the contactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the value of the contactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Gets the value of the contactHours property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getContactHours() {
        return contactHours;
    }

    /**
     * Sets the value of the contactHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setContactHours(JAXBElement<Double> value) {
        this.contactHours = value;
    }

    /**
     * Gets the value of the creditHours property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getCreditHours() {
        return creditHours;
    }

    /**
     * Sets the value of the creditHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setCreditHours(JAXBElement<Double> value) {
        this.creditHours = value;
    }

    /**
     * Gets the value of the cpeHours property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getCpeHours() {
        return cpeHours;
    }

    /**
     * Sets the value of the cpeHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setCpeHours(JAXBElement<Double> value) {
        this.cpeHours = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the instructorMaterials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstructorMaterials() {
        return instructorMaterials;
    }

    /**
     * Sets the value of the instructorMaterials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstructorMaterials(String value) {
        this.instructorMaterials = value;
    }

    /**
     * Gets the value of the studentMaterials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudentMaterials() {
        return studentMaterials;
    }

    /**
     * Sets the value of the studentMaterials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudentMaterials(String value) {
        this.studentMaterials = value;
    }

    /**
     * Gets the value of the createTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCreateTimestamp() {
        return createTimestamp;
    }

    /**
     * Sets the value of the createTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCreateTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.createTimestamp = value;
    }

    /**
     * Gets the value of the totalLength property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getTotalLength() {
        return totalLength;
    }

    /**
     * Sets the value of the totalLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setTotalLength(JAXBElement<Double> value) {
        this.totalLength = value;
    }

    /**
     * Gets the value of the prepLength property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPrepLength() {
        return prepLength;
    }

    /**
     * Sets the value of the prepLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPrepLength(JAXBElement<Double> value) {
        this.prepLength = value;
    }

    /**
     * Gets the value of the minEnrollment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMinEnrollment() {
        return minEnrollment;
    }

    /**
     * Sets the value of the minEnrollment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMinEnrollment(JAXBElement<Long> value) {
        this.minEnrollment = value;
    }

    /**
     * Gets the value of the maxEnrollment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMaxEnrollment() {
        return maxEnrollment;
    }

    /**
     * Sets the value of the maxEnrollment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMaxEnrollment(JAXBElement<Long> value) {
        this.maxEnrollment = value;
    }

    /**
     * Gets the value of the enrollmentThresDays property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getEnrollmentThresDays() {
        return enrollmentThresDays;
    }

    /**
     * Sets the value of the enrollmentThresDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setEnrollmentThresDays(JAXBElement<Long> value) {
        this.enrollmentThresDays = value;
    }

    /**
     * Gets the value of the autoFillEnrollment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoFillEnrollment() {
        return autoFillEnrollment;
    }

    /**
     * Sets the value of the autoFillEnrollment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoFillEnrollment(Boolean value) {
        this.autoFillEnrollment = value;
    }

    /**
     * Gets the value of the waitlistRemainderSentTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getWaitlistRemainderSentTimestamp() {
        return waitlistRemainderSentTimestamp;
    }

    /**
     * Sets the value of the waitlistRemainderSentTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setWaitlistRemainderSentTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.waitlistRemainderSentTimestamp = value;
    }

    /**
     * Gets the value of the autoCompetency property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoCompetency() {
        return autoCompetency;
    }

    /**
     * Sets the value of the autoCompetency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoCompetency(Boolean value) {
        this.autoCompetency = value;
    }

    /**
     * Gets the value of the classification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassification() {
        return classification;
    }

    /**
     * Sets the value of the classification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassification(String value) {
        this.classification = value;
    }

    /**
     * Gets the value of the productionReady property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProductionReady() {
        return productionReady;
    }

    /**
     * Sets the value of the productionReady property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProductionReady(Boolean value) {
        this.productionReady = value;
    }

    /**
     * Gets the value of the componentGoals property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentGoals() {
        return componentGoals;
    }

    /**
     * Sets the value of the componentGoals property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentGoals(String value) {
        this.componentGoals = value;
    }

    /**
     * Gets the value of the studentCanRecordLearningEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStudentCanRecordLearningEvents() {
        return studentCanRecordLearningEvents;
    }

    /**
     * Sets the value of the studentCanRecordLearningEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStudentCanRecordLearningEvents(Boolean value) {
        this.studentCanRecordLearningEvents = value;
    }

    /**
     * Gets the value of the supervisorCanRecordLearningEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupervisorCanRecordLearningEvents() {
        return supervisorCanRecordLearningEvents;
    }

    /**
     * Sets the value of the supervisorCanRecordLearningEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupervisorCanRecordLearningEvents(Boolean value) {
        this.supervisorCanRecordLearningEvents = value;
    }

    /**
     * Gets the value of the processDefinitionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessDefinitionID() {
        return processDefinitionID;
    }

    /**
     * Sets the value of the processDefinitionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessDefinitionID(String value) {
        this.processDefinitionID = value;
    }

    /**
     * Gets the value of the approvalRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApprovalRequired() {
        return approvalRequired;
    }

    /**
     * Sets the value of the approvalRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApprovalRequired(Boolean value) {
        this.approvalRequired = value;
    }

    /**
     * Gets the value of the lastUpdateUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the value of the lastUpdateUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateUser(String value) {
        this.lastUpdateUser = value;
    }

    /**
     * Gets the value of the lastUpdateTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    /**
     * Sets the value of the lastUpdateTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastUpdateTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.lastUpdateTimestamp = value;
    }

    /**
     * Gets the value of the defaultInitialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getDefaultInitialNumber() {
        return defaultInitialNumber;
    }

    /**
     * Sets the value of the defaultInitialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setDefaultInitialNumber(JAXBElement<Long> value) {
        this.defaultInitialNumber = value;
    }

    /**
     * Gets the value of the defaultInitialPeriodTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultInitialPeriodTypeID() {
        return defaultInitialPeriodTypeID;
    }

    /**
     * Sets the value of the defaultInitialPeriodTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultInitialPeriodTypeID(String value) {
        this.defaultInitialPeriodTypeID = value;
    }

    /**
     * Gets the value of the defaultInitialBasisTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultInitialBasisTypeID() {
        return defaultInitialBasisTypeID;
    }

    /**
     * Sets the value of the defaultInitialBasisTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultInitialBasisTypeID(String value) {
        this.defaultInitialBasisTypeID = value;
    }

    /**
     * Gets the value of the defaultRetrainingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getDefaultRetrainingNumber() {
        return defaultRetrainingNumber;
    }

    /**
     * Sets the value of the defaultRetrainingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setDefaultRetrainingNumber(JAXBElement<Long> value) {
        this.defaultRetrainingNumber = value;
    }

    /**
     * Gets the value of the defaultRetrainingPeriodTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRetrainingPeriodTypeID() {
        return defaultRetrainingPeriodTypeID;
    }

    /**
     * Sets the value of the defaultRetrainingPeriodTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRetrainingPeriodTypeID(String value) {
        this.defaultRetrainingPeriodTypeID = value;
    }

    /**
     * Gets the value of the defaultRetrainingBasisTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRetrainingBasisTypeID() {
        return defaultRetrainingBasisTypeID;
    }

    /**
     * Sets the value of the defaultRetrainingBasisTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRetrainingBasisTypeID(String value) {
        this.defaultRetrainingBasisTypeID = value;
    }

    /**
     * Gets the value of the rating property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getRating() {
        return rating;
    }

    /**
     * Sets the value of the rating property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setRating(JAXBElement<Double> value) {
        this.rating = value;
    }

    /**
     * Gets the value of the gradingOption property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGradingOption() {
        return gradingOption;
    }

    /**
     * Sets the value of the gradingOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGradingOption(Long value) {
        this.gradingOption = value;
    }

    /**
     * Gets the value of the chargebackMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargebackMethod() {
        return chargebackMethod;
    }

    /**
     * Sets the value of the chargebackMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargebackMethod(String value) {
        this.chargebackMethod = value;
    }

    /**
     * Gets the value of the catalogSKU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogSKU() {
        return catalogSKU;
    }

    /**
     * Sets the value of the catalogSKU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogSKU(String value) {
        this.catalogSKU = value;
    }

    /**
     * Gets the value of the scheduleCanOverridePrice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isScheduleCanOverridePrice() {
        return scheduleCanOverridePrice;
    }

    /**
     * Sets the value of the scheduleCanOverridePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScheduleCanOverridePrice(Boolean value) {
        this.scheduleCanOverridePrice = value;
    }

    /**
     * Gets the value of the shippingRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShippingRequired() {
        return shippingRequired;
    }

    /**
     * Sets the value of the shippingRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShippingRequired(Boolean value) {
        this.shippingRequired = value;
    }

    /**
     * Gets the value of the isBlendedLearningRegisterRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBlendedLearningRegisterRequired() {
        return isBlendedLearningRegisterRequired;
    }

    /**
     * Sets the value of the isBlendedLearningRegisterRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBlendedLearningRegisterRequired(Boolean value) {
        this.isBlendedLearningRegisterRequired = value;
    }

    /**
     * Gets the value of the isUserRequestsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsUserRequestsEnabled() {
        return isUserRequestsEnabled;
    }

    /**
     * Sets the value of the isUserRequestsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsUserRequestsEnabled(Boolean value) {
        this.isUserRequestsEnabled = value;
    }

    /**
     * Gets the value of the userCanWaitlist property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUserCanWaitlist() {
        return userCanWaitlist;
    }

    /**
     * Sets the value of the userCanWaitlist property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUserCanWaitlist(Boolean value) {
        this.userCanWaitlist = value;
    }

    /**
     * Gets the value of the componentKey property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getComponentKey() {
        return componentKey;
    }

    /**
     * Sets the value of the componentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setComponentKey(Long value) {
        this.componentKey = value;
    }

    /**
     * Gets the value of the selfEnrollment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelfEnrollment() {
        return selfEnrollment;
    }

    /**
     * Sets the value of the selfEnrollment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelfEnrollment(Boolean value) {
        this.selfEnrollment = value;
    }

    /**
     * Gets the value of the costCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostCurrencyCode() {
        return costCurrencyCode;
    }

    /**
     * Sets the value of the costCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostCurrencyCode(String value) {
        this.costCurrencyCode = value;
    }

    /**
     * Gets the value of the eSigEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isESigEnabled() {
        return eSigEnabled;
    }

    /**
     * Sets the value of the eSigEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setESigEnabled(Boolean value) {
        this.eSigEnabled = value;
    }

    /**
     * Gets the value of the itemCustomColVO property.
     * 
     * @return
     *     possible object is
     *     {@link ItemCustomColumnVOList }
     *     
     */
    public ItemCustomColumnVOList getItemCustomColVO() {
        return itemCustomColVO;
    }

    /**
     * Sets the value of the itemCustomColVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemCustomColumnVOList }
     *     
     */
    public void setItemCustomColVO(ItemCustomColumnVOList value) {
        this.itemCustomColVO = value;
    }

}
