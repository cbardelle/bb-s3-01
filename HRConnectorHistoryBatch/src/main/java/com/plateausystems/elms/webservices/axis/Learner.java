
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Learner complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Learner">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudentID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MiddleInitial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NotActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasAccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmployeeStatusID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmployeeTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JobLocationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JobPositionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DomainID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrganizationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Postal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SupervisorID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HireDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TerminationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResumeLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingAddress1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingPhoneNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingFaxNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingAddress1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingPhoneNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingFaxNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ShippingContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsLocked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ResetPin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StudentPhones" type="{http://axis.webservices.elms.plateausystems.com}StudentPhones"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Esig" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EsigDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Encrypted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RegionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TargetJobPositionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RoleID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SecretQuestion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SecretAnswer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoachID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SelfRegistration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SelfRegistrationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ManageSubSuccessionPlanning" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ManageOwnSuccessionPlanning" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PlateauTalentGatewayUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PasswordExpirationTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="HasAccessToOrgFinancialAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NotifyWhenItemAddedToLearningPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NotifyWhenItemModifiedInLearningPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NotifyWhenItemRemovedFromLearningPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NotifyWhenSubordinatesCompleteItem" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NotifyWhenSubordinatesFailItem" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ImageID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="JobTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PastService" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="customColumnValueForLearner" type="{http://axis.webservices.elms.plateausystems.com}CustomColumnVOList"/>
 *         &lt;element name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Timezone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Locale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JobPositionEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MappedAdminID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MappedInstructorID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AltSuperID1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AltSuperID2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AltSuperID3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShoppingAccountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EnableShoppingAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PositionNumberID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IncludeInGovtReporting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="LegalEntityID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmployeeClassID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HourlyRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="HourlyRateCurrency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegularTempID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Fulltime" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NativeDeeplinkUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Learner", propOrder = {
    "studentID",
    "lastName",
    "firstName",
    "middleInitial",
    "notActive",
    "hasAccess",
    "employeeStatusID",
    "employeeTypeID",
    "jobLocationID",
    "jobPositionID",
    "domainID",
    "organizationID",
    "address",
    "city",
    "state",
    "postal",
    "country",
    "supervisorID",
    "hireDate",
    "terminationDate",
    "emailAddress",
    "resumeLocation",
    "comments",
    "shippingName",
    "shippingAddress",
    "shippingAddress1",
    "shippingCity",
    "shippingState",
    "shippingPostal",
    "shippingCountry",
    "shippingPhoneNo",
    "shippingFaxNo",
    "shippingEmailAddress",
    "billingAddress",
    "billingAddress1",
    "billingCity",
    "billingState",
    "billingPostal",
    "billingCountry",
    "billingPhoneNo",
    "billingFaxNo",
    "billingEmailAddress",
    "lastUpdateUser",
    "lastUpdateTimestamp",
    "shippingContactName",
    "billingName",
    "billingContactName",
    "isLocked",
    "resetPin",
    "studentPhones",
    "password",
    "esig",
    "esigDate",
    "encrypted",
    "regionID",
    "targetJobPositionID",
    "roleID",
    "secretQuestion",
    "secretAnswer",
    "coachID",
    "selfRegistration",
    "selfRegistrationDate",
    "manageSubSuccessionPlanning",
    "manageOwnSuccessionPlanning",
    "plateauTalentGatewayUser",
    "passwordExpirationTimestamp",
    "hasAccessToOrgFinancialAccount",
    "notifyWhenItemAddedToLearningPlan",
    "notifyWhenItemModifiedInLearningPlan",
    "notifyWhenItemRemovedFromLearningPlan",
    "notifyWhenSubordinatesCompleteItem",
    "notifyWhenSubordinatesFailItem",
    "imageID",
    "jobTitle",
    "gender",
    "pastService",
    "customColumnValueForLearner",
    "accountCode",
    "timezone",
    "locale",
    "currencyID",
    "jobPositionEffectiveDate",
    "mappedAdminID",
    "mappedInstructorID",
    "altSuperID1",
    "altSuperID2",
    "altSuperID3",
    "shoppingAccountType",
    "enableShoppingAccount",
    "positionNumberID",
    "includeInGovtReporting",
    "legalEntityID",
    "employeeClassID",
    "hourlyRate",
    "hourlyRateCurrency",
    "regularTempID",
    "fulltime",
    "nativeDeeplinkUser"
})
public class Learner {

    @XmlElement(name = "StudentID", required = true, nillable = true)
    protected String studentID;
    @XmlElement(name = "LastName", required = true, nillable = true)
    protected String lastName;
    @XmlElement(name = "FirstName", required = true, nillable = true)
    protected String firstName;
    @XmlElement(name = "MiddleInitial", required = true, nillable = true)
    protected String middleInitial;
    @XmlElement(name = "NotActive", required = true, type = Boolean.class, nillable = true)
    protected Boolean notActive;
    @XmlElement(name = "HasAccess", required = true, type = Boolean.class, nillable = true)
    protected Boolean hasAccess;
    @XmlElement(name = "EmployeeStatusID", required = true, nillable = true)
    protected String employeeStatusID;
    @XmlElement(name = "EmployeeTypeID", required = true, nillable = true)
    protected String employeeTypeID;
    @XmlElement(name = "JobLocationID", required = true, nillable = true)
    protected String jobLocationID;
    @XmlElement(name = "JobPositionID", required = true, nillable = true)
    protected String jobPositionID;
    @XmlElement(name = "DomainID", required = true, nillable = true)
    protected String domainID;
    @XmlElement(name = "OrganizationID", required = true, nillable = true)
    protected String organizationID;
    @XmlElement(name = "Address", required = true, nillable = true)
    protected String address;
    @XmlElement(name = "City", required = true, nillable = true)
    protected String city;
    @XmlElement(name = "State", required = true, nillable = true)
    protected String state;
    @XmlElement(name = "Postal", required = true, nillable = true)
    protected String postal;
    @XmlElement(name = "Country", required = true, nillable = true)
    protected String country;
    @XmlElement(name = "SupervisorID", required = true, nillable = true)
    protected String supervisorID;
    @XmlElementRef(name = "HireDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> hireDate;
    @XmlElementRef(name = "TerminationDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> terminationDate;
    @XmlElement(name = "EmailAddress", required = true, nillable = true)
    protected String emailAddress;
    @XmlElement(name = "ResumeLocation", required = true, nillable = true)
    protected String resumeLocation;
    @XmlElement(name = "Comments", required = true, nillable = true)
    protected String comments;
    @XmlElement(name = "ShippingName", required = true, nillable = true)
    protected String shippingName;
    @XmlElement(name = "ShippingAddress", required = true, nillable = true)
    protected String shippingAddress;
    @XmlElement(name = "ShippingAddress1", required = true, nillable = true)
    protected String shippingAddress1;
    @XmlElement(name = "ShippingCity", required = true, nillable = true)
    protected String shippingCity;
    @XmlElement(name = "ShippingState", required = true, nillable = true)
    protected String shippingState;
    @XmlElement(name = "ShippingPostal", required = true, nillable = true)
    protected String shippingPostal;
    @XmlElement(name = "ShippingCountry", required = true, nillable = true)
    protected String shippingCountry;
    @XmlElement(name = "ShippingPhoneNo", required = true, nillable = true)
    protected String shippingPhoneNo;
    @XmlElement(name = "ShippingFaxNo", required = true, nillable = true)
    protected String shippingFaxNo;
    @XmlElement(name = "ShippingEmailAddress", required = true, nillable = true)
    protected String shippingEmailAddress;
    @XmlElement(name = "BillingAddress", required = true, nillable = true)
    protected String billingAddress;
    @XmlElement(name = "BillingAddress1", required = true, nillable = true)
    protected String billingAddress1;
    @XmlElement(name = "BillingCity", required = true, nillable = true)
    protected String billingCity;
    @XmlElement(name = "BillingState", required = true, nillable = true)
    protected String billingState;
    @XmlElement(name = "BillingPostal", required = true, nillable = true)
    protected String billingPostal;
    @XmlElement(name = "BillingCountry", required = true, nillable = true)
    protected String billingCountry;
    @XmlElement(name = "BillingPhoneNo", required = true, nillable = true)
    protected String billingPhoneNo;
    @XmlElement(name = "BillingFaxNo", required = true, nillable = true)
    protected String billingFaxNo;
    @XmlElement(name = "BillingEmailAddress", required = true, nillable = true)
    protected String billingEmailAddress;
    @XmlElement(name = "LastUpdateUser", required = true, nillable = true)
    protected String lastUpdateUser;
    @XmlElementRef(name = "LastUpdateTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> lastUpdateTimestamp;
    @XmlElement(name = "ShippingContactName", required = true, nillable = true)
    protected String shippingContactName;
    @XmlElement(name = "BillingName", required = true, nillable = true)
    protected String billingName;
    @XmlElement(name = "BillingContactName", required = true, nillable = true)
    protected String billingContactName;
    @XmlElement(name = "IsLocked", required = true, type = Boolean.class, nillable = true)
    protected Boolean isLocked;
    @XmlElementRef(name = "ResetPin", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<Boolean> resetPin;
    @XmlElement(name = "StudentPhones", required = true, nillable = true)
    protected StudentPhones studentPhones;
    @XmlElement(name = "Password", required = true, nillable = true)
    protected String password;
    @XmlElement(name = "Esig", required = true, nillable = true)
    protected String esig;
    @XmlElement(name = "EsigDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar esigDate;
    @XmlElement(name = "Encrypted", required = true, type = Boolean.class, nillable = true)
    protected Boolean encrypted;
    @XmlElement(name = "RegionID", required = true, nillable = true)
    protected String regionID;
    @XmlElement(name = "TargetJobPositionID", required = true, nillable = true)
    protected String targetJobPositionID;
    @XmlElement(name = "RoleID", required = true, nillable = true)
    protected String roleID;
    @XmlElement(name = "SecretQuestion", required = true, nillable = true)
    protected String secretQuestion;
    @XmlElement(name = "SecretAnswer", required = true, nillable = true)
    protected String secretAnswer;
    @XmlElement(name = "CoachID", required = true, nillable = true)
    protected String coachID;
    @XmlElement(name = "SelfRegistration", required = true, type = Boolean.class, nillable = true)
    protected Boolean selfRegistration;
    @XmlElement(name = "SelfRegistrationDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar selfRegistrationDate;
    @XmlElement(name = "ManageSubSuccessionPlanning", required = true, type = Boolean.class, nillable = true)
    protected Boolean manageSubSuccessionPlanning;
    @XmlElement(name = "ManageOwnSuccessionPlanning", required = true, type = Boolean.class, nillable = true)
    protected Boolean manageOwnSuccessionPlanning;
    @XmlElement(name = "PlateauTalentGatewayUser", required = true, type = Boolean.class, nillable = true)
    protected Boolean plateauTalentGatewayUser;
    @XmlElement(name = "PasswordExpirationTimestamp", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar passwordExpirationTimestamp;
    @XmlElement(name = "HasAccessToOrgFinancialAccount", required = true, type = Boolean.class, nillable = true)
    protected Boolean hasAccessToOrgFinancialAccount;
    @XmlElement(name = "NotifyWhenItemAddedToLearningPlan", required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyWhenItemAddedToLearningPlan;
    @XmlElement(name = "NotifyWhenItemModifiedInLearningPlan", required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyWhenItemModifiedInLearningPlan;
    @XmlElement(name = "NotifyWhenItemRemovedFromLearningPlan", required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyWhenItemRemovedFromLearningPlan;
    @XmlElement(name = "NotifyWhenSubordinatesCompleteItem", required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyWhenSubordinatesCompleteItem;
    @XmlElement(name = "NotifyWhenSubordinatesFailItem", required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyWhenSubordinatesFailItem;
    @XmlElement(name = "ImageID", required = true, type = Long.class, nillable = true)
    protected Long imageID;
    @XmlElement(name = "JobTitle", required = true, nillable = true)
    protected String jobTitle;
    @XmlElement(name = "Gender", required = true, nillable = true)
    protected String gender;
    @XmlElement(name = "PastService", required = true, type = Long.class, nillable = true)
    protected Long pastService;
    @XmlElement(required = true, nillable = true)
    protected CustomColumnVOList customColumnValueForLearner;
    @XmlElement(name = "AccountCode", required = true, nillable = true)
    protected String accountCode;
    @XmlElement(name = "Timezone", required = true, nillable = true)
    protected String timezone;
    @XmlElement(name = "Locale", required = true, nillable = true)
    protected String locale;
    @XmlElement(name = "CurrencyID", required = true, nillable = true)
    protected String currencyID;
    @XmlElement(name = "JobPositionEffectiveDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar jobPositionEffectiveDate;
    @XmlElement(name = "MappedAdminID", required = true, nillable = true)
    protected String mappedAdminID;
    @XmlElement(name = "MappedInstructorID", required = true, nillable = true)
    protected String mappedInstructorID;
    @XmlElement(name = "AltSuperID1", required = true, nillable = true)
    protected String altSuperID1;
    @XmlElement(name = "AltSuperID2", required = true, nillable = true)
    protected String altSuperID2;
    @XmlElement(name = "AltSuperID3", required = true, nillable = true)
    protected String altSuperID3;
    @XmlElement(name = "ShoppingAccountType", required = true, nillable = true)
    protected String shoppingAccountType;
    @XmlElement(name = "EnableShoppingAccount", required = true, type = Boolean.class, nillable = true)
    protected Boolean enableShoppingAccount;
    @XmlElement(name = "PositionNumberID", required = true, nillable = true)
    protected String positionNumberID;
    @XmlElement(name = "IncludeInGovtReporting", required = true, type = Boolean.class, nillable = true)
    protected Boolean includeInGovtReporting;
    @XmlElement(name = "LegalEntityID", required = true, nillable = true)
    protected String legalEntityID;
    @XmlElement(name = "EmployeeClassID", required = true, nillable = true)
    protected String employeeClassID;
    @XmlElement(name = "HourlyRate", required = true, type = Double.class, nillable = true)
    protected Double hourlyRate;
    @XmlElement(name = "HourlyRateCurrency", required = true, nillable = true)
    protected String hourlyRateCurrency;
    @XmlElement(name = "RegularTempID", required = true, nillable = true)
    protected String regularTempID;
    @XmlElement(name = "Fulltime", required = true, type = Boolean.class, nillable = true)
    protected Boolean fulltime;
    @XmlElement(name = "NativeDeeplinkUser", required = true, type = Boolean.class, nillable = true)
    protected Boolean nativeDeeplinkUser;

    /**
     * Gets the value of the studentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudentID() {
        return studentID;
    }

    /**
     * Sets the value of the studentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudentID(String value) {
        this.studentID = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleInitial() {
        return middleInitial;
    }

    /**
     * Sets the value of the middleInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleInitial(String value) {
        this.middleInitial = value;
    }

    /**
     * Gets the value of the notActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotActive() {
        return notActive;
    }

    /**
     * Sets the value of the notActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotActive(Boolean value) {
        this.notActive = value;
    }

    /**
     * Gets the value of the hasAccess property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasAccess() {
        return hasAccess;
    }

    /**
     * Sets the value of the hasAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasAccess(Boolean value) {
        this.hasAccess = value;
    }

    /**
     * Gets the value of the employeeStatusID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeStatusID() {
        return employeeStatusID;
    }

    /**
     * Sets the value of the employeeStatusID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeStatusID(String value) {
        this.employeeStatusID = value;
    }

    /**
     * Gets the value of the employeeTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeTypeID() {
        return employeeTypeID;
    }

    /**
     * Sets the value of the employeeTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeTypeID(String value) {
        this.employeeTypeID = value;
    }

    /**
     * Gets the value of the jobLocationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobLocationID() {
        return jobLocationID;
    }

    /**
     * Sets the value of the jobLocationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobLocationID(String value) {
        this.jobLocationID = value;
    }

    /**
     * Gets the value of the jobPositionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobPositionID() {
        return jobPositionID;
    }

    /**
     * Sets the value of the jobPositionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobPositionID(String value) {
        this.jobPositionID = value;
    }

    /**
     * Gets the value of the domainID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainID() {
        return domainID;
    }

    /**
     * Sets the value of the domainID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainID(String value) {
        this.domainID = value;
    }

    /**
     * Gets the value of the organizationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganizationID() {
        return organizationID;
    }

    /**
     * Sets the value of the organizationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganizationID(String value) {
        this.organizationID = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the postal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostal() {
        return postal;
    }

    /**
     * Sets the value of the postal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostal(String value) {
        this.postal = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the supervisorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupervisorID() {
        return supervisorID;
    }

    /**
     * Sets the value of the supervisorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupervisorID(String value) {
        this.supervisorID = value;
    }

    /**
     * Gets the value of the hireDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getHireDate() {
        return hireDate;
    }

    /**
     * Sets the value of the hireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setHireDate(JAXBElement<XMLGregorianCalendar> value) {
        this.hireDate = value;
    }

    /**
     * Gets the value of the terminationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTerminationDate() {
        return terminationDate;
    }

    /**
     * Sets the value of the terminationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTerminationDate(JAXBElement<XMLGregorianCalendar> value) {
        this.terminationDate = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the resumeLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResumeLocation() {
        return resumeLocation;
    }

    /**
     * Sets the value of the resumeLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResumeLocation(String value) {
        this.resumeLocation = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the shippingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingName() {
        return shippingName;
    }

    /**
     * Sets the value of the shippingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingName(String value) {
        this.shippingName = value;
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingAddress(String value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the shippingAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingAddress1() {
        return shippingAddress1;
    }

    /**
     * Sets the value of the shippingAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingAddress1(String value) {
        this.shippingAddress1 = value;
    }

    /**
     * Gets the value of the shippingCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingCity() {
        return shippingCity;
    }

    /**
     * Sets the value of the shippingCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingCity(String value) {
        this.shippingCity = value;
    }

    /**
     * Gets the value of the shippingState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingState() {
        return shippingState;
    }

    /**
     * Sets the value of the shippingState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingState(String value) {
        this.shippingState = value;
    }

    /**
     * Gets the value of the shippingPostal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingPostal() {
        return shippingPostal;
    }

    /**
     * Sets the value of the shippingPostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingPostal(String value) {
        this.shippingPostal = value;
    }

    /**
     * Gets the value of the shippingCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingCountry() {
        return shippingCountry;
    }

    /**
     * Sets the value of the shippingCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingCountry(String value) {
        this.shippingCountry = value;
    }

    /**
     * Gets the value of the shippingPhoneNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingPhoneNo() {
        return shippingPhoneNo;
    }

    /**
     * Sets the value of the shippingPhoneNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingPhoneNo(String value) {
        this.shippingPhoneNo = value;
    }

    /**
     * Gets the value of the shippingFaxNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingFaxNo() {
        return shippingFaxNo;
    }

    /**
     * Sets the value of the shippingFaxNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingFaxNo(String value) {
        this.shippingFaxNo = value;
    }

    /**
     * Gets the value of the shippingEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingEmailAddress() {
        return shippingEmailAddress;
    }

    /**
     * Sets the value of the shippingEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingEmailAddress(String value) {
        this.shippingEmailAddress = value;
    }

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAddress(String value) {
        this.billingAddress = value;
    }

    /**
     * Gets the value of the billingAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAddress1() {
        return billingAddress1;
    }

    /**
     * Sets the value of the billingAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAddress1(String value) {
        this.billingAddress1 = value;
    }

    /**
     * Gets the value of the billingCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCity() {
        return billingCity;
    }

    /**
     * Sets the value of the billingCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCity(String value) {
        this.billingCity = value;
    }

    /**
     * Gets the value of the billingState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingState() {
        return billingState;
    }

    /**
     * Sets the value of the billingState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingState(String value) {
        this.billingState = value;
    }

    /**
     * Gets the value of the billingPostal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingPostal() {
        return billingPostal;
    }

    /**
     * Sets the value of the billingPostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingPostal(String value) {
        this.billingPostal = value;
    }

    /**
     * Gets the value of the billingCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCountry() {
        return billingCountry;
    }

    /**
     * Sets the value of the billingCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCountry(String value) {
        this.billingCountry = value;
    }

    /**
     * Gets the value of the billingPhoneNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingPhoneNo() {
        return billingPhoneNo;
    }

    /**
     * Sets the value of the billingPhoneNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingPhoneNo(String value) {
        this.billingPhoneNo = value;
    }

    /**
     * Gets the value of the billingFaxNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingFaxNo() {
        return billingFaxNo;
    }

    /**
     * Sets the value of the billingFaxNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingFaxNo(String value) {
        this.billingFaxNo = value;
    }

    /**
     * Gets the value of the billingEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingEmailAddress() {
        return billingEmailAddress;
    }

    /**
     * Sets the value of the billingEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingEmailAddress(String value) {
        this.billingEmailAddress = value;
    }

    /**
     * Gets the value of the lastUpdateUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the value of the lastUpdateUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateUser(String value) {
        this.lastUpdateUser = value;
    }

    /**
     * Gets the value of the lastUpdateTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    /**
     * Sets the value of the lastUpdateTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastUpdateTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.lastUpdateTimestamp = value;
    }

    /**
     * Gets the value of the shippingContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingContactName() {
        return shippingContactName;
    }

    /**
     * Sets the value of the shippingContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingContactName(String value) {
        this.shippingContactName = value;
    }

    /**
     * Gets the value of the billingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingName() {
        return billingName;
    }

    /**
     * Sets the value of the billingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingName(String value) {
        this.billingName = value;
    }

    /**
     * Gets the value of the billingContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingContactName() {
        return billingContactName;
    }

    /**
     * Sets the value of the billingContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingContactName(String value) {
        this.billingContactName = value;
    }

    /**
     * Gets the value of the isLocked property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLocked() {
        return isLocked;
    }

    /**
     * Sets the value of the isLocked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLocked(Boolean value) {
        this.isLocked = value;
    }

    /**
     * Gets the value of the resetPin property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getResetPin() {
        return resetPin;
    }

    /**
     * Sets the value of the resetPin property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setResetPin(JAXBElement<Boolean> value) {
        this.resetPin = value;
    }

    /**
     * Gets the value of the studentPhones property.
     * 
     * @return
     *     possible object is
     *     {@link StudentPhones }
     *     
     */
    public StudentPhones getStudentPhones() {
        return studentPhones;
    }

    /**
     * Sets the value of the studentPhones property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudentPhones }
     *     
     */
    public void setStudentPhones(StudentPhones value) {
        this.studentPhones = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the esig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsig() {
        return esig;
    }

    /**
     * Sets the value of the esig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsig(String value) {
        this.esig = value;
    }

    /**
     * Gets the value of the esigDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEsigDate() {
        return esigDate;
    }

    /**
     * Sets the value of the esigDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEsigDate(XMLGregorianCalendar value) {
        this.esigDate = value;
    }

    /**
     * Gets the value of the encrypted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEncrypted() {
        return encrypted;
    }

    /**
     * Sets the value of the encrypted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEncrypted(Boolean value) {
        this.encrypted = value;
    }

    /**
     * Gets the value of the regionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionID() {
        return regionID;
    }

    /**
     * Sets the value of the regionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionID(String value) {
        this.regionID = value;
    }

    /**
     * Gets the value of the targetJobPositionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetJobPositionID() {
        return targetJobPositionID;
    }

    /**
     * Sets the value of the targetJobPositionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetJobPositionID(String value) {
        this.targetJobPositionID = value;
    }

    /**
     * Gets the value of the roleID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleID() {
        return roleID;
    }

    /**
     * Sets the value of the roleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleID(String value) {
        this.roleID = value;
    }

    /**
     * Gets the value of the secretQuestion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecretQuestion() {
        return secretQuestion;
    }

    /**
     * Sets the value of the secretQuestion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecretQuestion(String value) {
        this.secretQuestion = value;
    }

    /**
     * Gets the value of the secretAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecretAnswer() {
        return secretAnswer;
    }

    /**
     * Sets the value of the secretAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecretAnswer(String value) {
        this.secretAnswer = value;
    }

    /**
     * Gets the value of the coachID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoachID() {
        return coachID;
    }

    /**
     * Sets the value of the coachID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoachID(String value) {
        this.coachID = value;
    }

    /**
     * Gets the value of the selfRegistration property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelfRegistration() {
        return selfRegistration;
    }

    /**
     * Sets the value of the selfRegistration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelfRegistration(Boolean value) {
        this.selfRegistration = value;
    }

    /**
     * Gets the value of the selfRegistrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSelfRegistrationDate() {
        return selfRegistrationDate;
    }

    /**
     * Sets the value of the selfRegistrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSelfRegistrationDate(XMLGregorianCalendar value) {
        this.selfRegistrationDate = value;
    }

    /**
     * Gets the value of the manageSubSuccessionPlanning property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isManageSubSuccessionPlanning() {
        return manageSubSuccessionPlanning;
    }

    /**
     * Sets the value of the manageSubSuccessionPlanning property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setManageSubSuccessionPlanning(Boolean value) {
        this.manageSubSuccessionPlanning = value;
    }

    /**
     * Gets the value of the manageOwnSuccessionPlanning property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isManageOwnSuccessionPlanning() {
        return manageOwnSuccessionPlanning;
    }

    /**
     * Sets the value of the manageOwnSuccessionPlanning property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setManageOwnSuccessionPlanning(Boolean value) {
        this.manageOwnSuccessionPlanning = value;
    }

    /**
     * Gets the value of the plateauTalentGatewayUser property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlateauTalentGatewayUser() {
        return plateauTalentGatewayUser;
    }

    /**
     * Sets the value of the plateauTalentGatewayUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlateauTalentGatewayUser(Boolean value) {
        this.plateauTalentGatewayUser = value;
    }

    /**
     * Gets the value of the passwordExpirationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPasswordExpirationTimestamp() {
        return passwordExpirationTimestamp;
    }

    /**
     * Sets the value of the passwordExpirationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPasswordExpirationTimestamp(XMLGregorianCalendar value) {
        this.passwordExpirationTimestamp = value;
    }

    /**
     * Gets the value of the hasAccessToOrgFinancialAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasAccessToOrgFinancialAccount() {
        return hasAccessToOrgFinancialAccount;
    }

    /**
     * Sets the value of the hasAccessToOrgFinancialAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasAccessToOrgFinancialAccount(Boolean value) {
        this.hasAccessToOrgFinancialAccount = value;
    }

    /**
     * Gets the value of the notifyWhenItemAddedToLearningPlan property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyWhenItemAddedToLearningPlan() {
        return notifyWhenItemAddedToLearningPlan;
    }

    /**
     * Sets the value of the notifyWhenItemAddedToLearningPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyWhenItemAddedToLearningPlan(Boolean value) {
        this.notifyWhenItemAddedToLearningPlan = value;
    }

    /**
     * Gets the value of the notifyWhenItemModifiedInLearningPlan property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyWhenItemModifiedInLearningPlan() {
        return notifyWhenItemModifiedInLearningPlan;
    }

    /**
     * Sets the value of the notifyWhenItemModifiedInLearningPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyWhenItemModifiedInLearningPlan(Boolean value) {
        this.notifyWhenItemModifiedInLearningPlan = value;
    }

    /**
     * Gets the value of the notifyWhenItemRemovedFromLearningPlan property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyWhenItemRemovedFromLearningPlan() {
        return notifyWhenItemRemovedFromLearningPlan;
    }

    /**
     * Sets the value of the notifyWhenItemRemovedFromLearningPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyWhenItemRemovedFromLearningPlan(Boolean value) {
        this.notifyWhenItemRemovedFromLearningPlan = value;
    }

    /**
     * Gets the value of the notifyWhenSubordinatesCompleteItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyWhenSubordinatesCompleteItem() {
        return notifyWhenSubordinatesCompleteItem;
    }

    /**
     * Sets the value of the notifyWhenSubordinatesCompleteItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyWhenSubordinatesCompleteItem(Boolean value) {
        this.notifyWhenSubordinatesCompleteItem = value;
    }

    /**
     * Gets the value of the notifyWhenSubordinatesFailItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyWhenSubordinatesFailItem() {
        return notifyWhenSubordinatesFailItem;
    }

    /**
     * Sets the value of the notifyWhenSubordinatesFailItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyWhenSubordinatesFailItem(Boolean value) {
        this.notifyWhenSubordinatesFailItem = value;
    }

    /**
     * Gets the value of the imageID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getImageID() {
        return imageID;
    }

    /**
     * Sets the value of the imageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setImageID(Long value) {
        this.imageID = value;
    }

    /**
     * Gets the value of the jobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the value of the jobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the pastService property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPastService() {
        return pastService;
    }

    /**
     * Sets the value of the pastService property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPastService(Long value) {
        this.pastService = value;
    }

    /**
     * Gets the value of the customColumnValueForLearner property.
     * 
     * @return
     *     possible object is
     *     {@link CustomColumnVOList }
     *     
     */
    public CustomColumnVOList getCustomColumnValueForLearner() {
        return customColumnValueForLearner;
    }

    /**
     * Sets the value of the customColumnValueForLearner property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomColumnVOList }
     *     
     */
    public void setCustomColumnValueForLearner(CustomColumnVOList value) {
        this.customColumnValueForLearner = value;
    }

    /**
     * Gets the value of the accountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Sets the value of the accountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Gets the value of the timezone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimezone() {
        return timezone;
    }

    /**
     * Sets the value of the timezone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimezone(String value) {
        this.timezone = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

    /**
     * Gets the value of the currencyID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyID() {
        return currencyID;
    }

    /**
     * Sets the value of the currencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyID(String value) {
        this.currencyID = value;
    }

    /**
     * Gets the value of the jobPositionEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJobPositionEffectiveDate() {
        return jobPositionEffectiveDate;
    }

    /**
     * Sets the value of the jobPositionEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJobPositionEffectiveDate(XMLGregorianCalendar value) {
        this.jobPositionEffectiveDate = value;
    }

    /**
     * Gets the value of the mappedAdminID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMappedAdminID() {
        return mappedAdminID;
    }

    /**
     * Sets the value of the mappedAdminID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMappedAdminID(String value) {
        this.mappedAdminID = value;
    }

    /**
     * Gets the value of the mappedInstructorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMappedInstructorID() {
        return mappedInstructorID;
    }

    /**
     * Sets the value of the mappedInstructorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMappedInstructorID(String value) {
        this.mappedInstructorID = value;
    }

    /**
     * Gets the value of the altSuperID1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltSuperID1() {
        return altSuperID1;
    }

    /**
     * Sets the value of the altSuperID1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltSuperID1(String value) {
        this.altSuperID1 = value;
    }

    /**
     * Gets the value of the altSuperID2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltSuperID2() {
        return altSuperID2;
    }

    /**
     * Sets the value of the altSuperID2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltSuperID2(String value) {
        this.altSuperID2 = value;
    }

    /**
     * Gets the value of the altSuperID3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltSuperID3() {
        return altSuperID3;
    }

    /**
     * Sets the value of the altSuperID3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltSuperID3(String value) {
        this.altSuperID3 = value;
    }

    /**
     * Gets the value of the shoppingAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShoppingAccountType() {
        return shoppingAccountType;
    }

    /**
     * Sets the value of the shoppingAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShoppingAccountType(String value) {
        this.shoppingAccountType = value;
    }

    /**
     * Gets the value of the enableShoppingAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableShoppingAccount() {
        return enableShoppingAccount;
    }

    /**
     * Sets the value of the enableShoppingAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableShoppingAccount(Boolean value) {
        this.enableShoppingAccount = value;
    }

    /**
     * Gets the value of the positionNumberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositionNumberID() {
        return positionNumberID;
    }

    /**
     * Sets the value of the positionNumberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositionNumberID(String value) {
        this.positionNumberID = value;
    }

    /**
     * Gets the value of the includeInGovtReporting property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeInGovtReporting() {
        return includeInGovtReporting;
    }

    /**
     * Sets the value of the includeInGovtReporting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeInGovtReporting(Boolean value) {
        this.includeInGovtReporting = value;
    }

    /**
     * Gets the value of the legalEntityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalEntityID() {
        return legalEntityID;
    }

    /**
     * Sets the value of the legalEntityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalEntityID(String value) {
        this.legalEntityID = value;
    }

    /**
     * Gets the value of the employeeClassID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeClassID() {
        return employeeClassID;
    }

    /**
     * Sets the value of the employeeClassID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeClassID(String value) {
        this.employeeClassID = value;
    }

    /**
     * Gets the value of the hourlyRate property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getHourlyRate() {
        return hourlyRate;
    }

    /**
     * Sets the value of the hourlyRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setHourlyRate(Double value) {
        this.hourlyRate = value;
    }

    /**
     * Gets the value of the hourlyRateCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHourlyRateCurrency() {
        return hourlyRateCurrency;
    }

    /**
     * Sets the value of the hourlyRateCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHourlyRateCurrency(String value) {
        this.hourlyRateCurrency = value;
    }

    /**
     * Gets the value of the regularTempID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegularTempID() {
        return regularTempID;
    }

    /**
     * Sets the value of the regularTempID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegularTempID(String value) {
        this.regularTempID = value;
    }

    /**
     * Gets the value of the fulltime property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFulltime() {
        return fulltime;
    }

    /**
     * Sets the value of the fulltime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFulltime(Boolean value) {
        this.fulltime = value;
    }

    /**
     * Gets the value of the nativeDeeplinkUser property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNativeDeeplinkUser() {
        return nativeDeeplinkUser;
    }

    /**
     * Sets the value of the nativeDeeplinkUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNativeDeeplinkUser(Boolean value) {
        this.nativeDeeplinkUser = value;
    }

}
