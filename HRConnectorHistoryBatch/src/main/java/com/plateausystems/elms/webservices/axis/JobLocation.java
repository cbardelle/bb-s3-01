
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for JobLocation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobLocation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Action" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JobLocationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JobLocationDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobLocation", propOrder = {
    "action",
    "jobLocationID",
    "jobLocationDescription",
    "lastUpdateUser",
    "lastUpdateTimestamp"
})
public class JobLocation {

    @XmlElement(name = "Action", required = true, nillable = true)
    protected String action;
    @XmlElement(name = "JobLocationID", required = true, nillable = true)
    protected String jobLocationID;
    @XmlElement(name = "JobLocationDescription", required = true, nillable = true)
    protected String jobLocationDescription;
    @XmlElement(name = "LastUpdateUser", required = true, nillable = true)
    protected String lastUpdateUser;
    @XmlElementRef(name = "LastUpdateTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> lastUpdateTimestamp;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the jobLocationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobLocationID() {
        return jobLocationID;
    }

    /**
     * Sets the value of the jobLocationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobLocationID(String value) {
        this.jobLocationID = value;
    }

    /**
     * Gets the value of the jobLocationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobLocationDescription() {
        return jobLocationDescription;
    }

    /**
     * Sets the value of the jobLocationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobLocationDescription(String value) {
        this.jobLocationDescription = value;
    }

    /**
     * Gets the value of the lastUpdateUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the value of the lastUpdateUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateUser(String value) {
        this.lastUpdateUser = value;
    }

    /**
     * Gets the value of the lastUpdateTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    /**
     * Sets the value of the lastUpdateTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastUpdateTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.lastUpdateTimestamp = value;
    }

}
