
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Curriculum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Curriculum">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QualificationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NotActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="QualificationTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QualificationDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QualificationTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ForceIncomplete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EnableESig" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DomainID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BasisDateInDBTimezone" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LastUpdateUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Curriculum", propOrder = {
    "qualificationID",
    "notActive",
    "creationDate",
    "qualificationTitle",
    "qualificationDesc",
    "qualificationTypeID",
    "forceIncomplete",
    "enableESig",
    "domainID",
    "basisDateInDBTimezone",
    "lastUpdateUser",
    "lastUpdateTimestamp"
})
public class Curriculum {

    @XmlElement(name = "QualificationID", required = true, nillable = true)
    protected String qualificationID;
    @XmlElement(name = "NotActive", required = true, type = Boolean.class, nillable = true)
    protected Boolean notActive;
    @XmlElementRef(name = "CreationDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> creationDate;
    @XmlElement(name = "QualificationTitle", required = true, nillable = true)
    protected String qualificationTitle;
    @XmlElement(name = "QualificationDesc", required = true, nillable = true)
    protected String qualificationDesc;
    @XmlElement(name = "QualificationTypeID", required = true, nillable = true)
    protected String qualificationTypeID;
    @XmlElement(name = "ForceIncomplete", required = true, type = Boolean.class, nillable = true)
    protected Boolean forceIncomplete;
    @XmlElement(name = "EnableESig", required = true, type = Boolean.class, nillable = true)
    protected Boolean enableESig;
    @XmlElement(name = "DomainID", required = true, nillable = true)
    protected String domainID;
    @XmlElementRef(name = "BasisDateInDBTimezone", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> basisDateInDBTimezone;
    @XmlElement(name = "LastUpdateUser", required = true, nillable = true)
    protected String lastUpdateUser;
    @XmlElementRef(name = "LastUpdateTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> lastUpdateTimestamp;

    /**
     * Gets the value of the qualificationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualificationID() {
        return qualificationID;
    }

    /**
     * Sets the value of the qualificationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualificationID(String value) {
        this.qualificationID = value;
    }

    /**
     * Gets the value of the notActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotActive() {
        return notActive;
    }

    /**
     * Sets the value of the notActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotActive(Boolean value) {
        this.notActive = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCreationDate(JAXBElement<XMLGregorianCalendar> value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the qualificationTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualificationTitle() {
        return qualificationTitle;
    }

    /**
     * Sets the value of the qualificationTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualificationTitle(String value) {
        this.qualificationTitle = value;
    }

    /**
     * Gets the value of the qualificationDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualificationDesc() {
        return qualificationDesc;
    }

    /**
     * Sets the value of the qualificationDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualificationDesc(String value) {
        this.qualificationDesc = value;
    }

    /**
     * Gets the value of the qualificationTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualificationTypeID() {
        return qualificationTypeID;
    }

    /**
     * Sets the value of the qualificationTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualificationTypeID(String value) {
        this.qualificationTypeID = value;
    }

    /**
     * Gets the value of the forceIncomplete property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceIncomplete() {
        return forceIncomplete;
    }

    /**
     * Sets the value of the forceIncomplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceIncomplete(Boolean value) {
        this.forceIncomplete = value;
    }

    /**
     * Gets the value of the enableESig property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableESig() {
        return enableESig;
    }

    /**
     * Sets the value of the enableESig property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableESig(Boolean value) {
        this.enableESig = value;
    }

    /**
     * Gets the value of the domainID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainID() {
        return domainID;
    }

    /**
     * Sets the value of the domainID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainID(String value) {
        this.domainID = value;
    }

    /**
     * Gets the value of the basisDateInDBTimezone property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getBasisDateInDBTimezone() {
        return basisDateInDBTimezone;
    }

    /**
     * Sets the value of the basisDateInDBTimezone property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setBasisDateInDBTimezone(JAXBElement<XMLGregorianCalendar> value) {
        this.basisDateInDBTimezone = value;
    }

    /**
     * Gets the value of the lastUpdateUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the value of the lastUpdateUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateUser(String value) {
        this.lastUpdateUser = value;
    }

    /**
     * Gets the value of the lastUpdateTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    /**
     * Sets the value of the lastUpdateTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastUpdateTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.lastUpdateTimestamp = value;
    }

}
