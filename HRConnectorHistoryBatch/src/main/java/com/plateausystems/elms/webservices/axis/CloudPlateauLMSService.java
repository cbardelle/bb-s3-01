
package com.plateausystems.elms.webservices.axis;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;



@WebServiceClient(name = "PlateauLMSService", targetNamespace = "http://axis.webservices.elms.plateausystems.com", wsdlLocation = "https://unicreditspa-stage.plateau.com/learning/services/PlateauLMSService?wsdl")
public class CloudPlateauLMSService
    extends Service
{
	
    protected CloudPlateauLMSService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /*
    public PlateauLMSService() {    	
        super(PLATEAULMSSERVICE_WSDL_LOCATION, new QName("http://axis.webservices.elms.plateausystems.com", "PlateauLMSService"));
    }*/

    /**
     * 
     * @return
     *     returns PlateauLMS
     */
    /*
    @WebEndpoint(name = "PlateauLMSService")
    public PlateauLMS getPlateauLMSService() {
        return super.getPort(new QName("http://axis.webservices.elms.plateausystems.com", "PlateauLMSService"), PlateauLMS.class);
    }*/

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PlateauLMS
     */
    /*
    @WebEndpoint(name = "PlateauLMSService")
    public PlateauLMS getPlateauLMSService(WebServiceFeature... features) {
        return super.getPort(new QName("http://axis.webservices.elms.plateausystems.com", "PlateauLMSService"), PlateauLMS.class, features);
    }*/

}
