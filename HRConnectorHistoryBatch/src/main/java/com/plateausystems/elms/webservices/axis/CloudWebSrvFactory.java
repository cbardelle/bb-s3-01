package com.plateausystems.elms.webservices.axis;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;

public class CloudWebSrvFactory {
	
	
	private final static Logger logger = Logger.getLogger(CloudWebSrvFactory.class.getName());
	
	
	private  ConfigParams configParams;
	
	private CloudPlateauLMSService cloudPlateauLMSService;
	
	private PlateauLMS cloudWS;
	
	private Caller caller;

	
	public CloudWebSrvFactory(){}
	
	public  Caller getCaller(){
		if (caller == null){
			caller = new Caller();
			caller.setCallerID(configParams.getStrPlateauWsUsr());
		}
		return caller;
	}
	
	
	public  PlateauLMS getCloudWS() throws Exception{
		
		if (cloudWS == null){
			
			if (cloudPlateauLMSService == null ){
				cloudPlateauLMSService = new CloudPlateauLMSService(getPlateauWsUrl(),new QName("http://axis.webservices.elms.plateausystems.com", "PlateauLMSService") );
			}
			
			cloudWS = cloudPlateauLMSService.getPort(PlateauLMS.class);
			
			((BindingProvider)cloudWS).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configParams.getStrPlateauWsUsr());
			((BindingProvider)cloudWS).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, configParams.getStrPlateauWsPwd());
			
		}
		return cloudWS;
	}
	

	private  URL getPlateauWsUrl() throws Exception{
		
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = CloudPlateauLMSService.class.getResource(".");
            url = new URL(baseUrl, configParams.getStrPlateauWsWsdlUrl());
        } catch (MalformedURLException e) {
            logger.debug("Failed to create URL for the wsdl Location: '" + configParams.getStrPlateauWsWsdlUrl());
            throw new Exception("Failed to create URL for the wsdl Location: '" + configParams.getStrPlateauWsWsdlUrl(),e);	            
        }
        return url;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}
	
	
	

}
