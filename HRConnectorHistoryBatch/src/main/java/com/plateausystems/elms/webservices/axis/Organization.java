
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Organization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Organization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrgID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrgTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingAddress1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShippingEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingAddress1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillingEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastUpdateTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DomainID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Style" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsRootOrg" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ParentOrganizationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PromoteToRootOrganization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="InheritParentOrgType" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Organization", propOrder = {
    "orgID",
    "orgTypeID",
    "description",
    "shippingName",
    "shippingContactName",
    "shippingAddress",
    "shippingAddress1",
    "shippingCity",
    "shippingState",
    "shippingPostal",
    "shippingCountryCode",
    "shippingPhoneNumber",
    "shippingFaxNumber",
    "shippingEmail",
    "billingName",
    "billingContactName",
    "billingAddress",
    "billingAddress1",
    "billingCity",
    "billingState",
    "billingPostal",
    "billingCountryCode",
    "billingPhoneNumber",
    "billingFaxNumber",
    "billingEmail",
    "lastUpdateUser",
    "lastUpdateTimestamp",
    "email",
    "comments",
    "domainID",
    "style",
    "isRootOrg",
    "parentOrganizationID",
    "promoteToRootOrganization",
    "inheritParentOrgType"
})
public class Organization {

    @XmlElement(name = "OrgID", required = true, nillable = true)
    protected String orgID;
    @XmlElement(name = "OrgTypeID", required = true, nillable = true)
    protected String orgTypeID;
    @XmlElement(name = "Description", required = true, nillable = true)
    protected String description;
    @XmlElement(name = "ShippingName", required = true, nillable = true)
    protected String shippingName;
    @XmlElement(name = "ShippingContactName", required = true, nillable = true)
    protected String shippingContactName;
    @XmlElement(name = "ShippingAddress", required = true, nillable = true)
    protected String shippingAddress;
    @XmlElement(name = "ShippingAddress1", required = true, nillable = true)
    protected String shippingAddress1;
    @XmlElement(name = "ShippingCity", required = true, nillable = true)
    protected String shippingCity;
    @XmlElement(name = "ShippingState", required = true, nillable = true)
    protected String shippingState;
    @XmlElement(name = "ShippingPostal", required = true, nillable = true)
    protected String shippingPostal;
    @XmlElement(name = "ShippingCountryCode", required = true, nillable = true)
    protected String shippingCountryCode;
    @XmlElement(name = "ShippingPhoneNumber", required = true, nillable = true)
    protected String shippingPhoneNumber;
    @XmlElement(name = "ShippingFaxNumber", required = true, nillable = true)
    protected String shippingFaxNumber;
    @XmlElement(name = "ShippingEmail", required = true, nillable = true)
    protected String shippingEmail;
    @XmlElement(name = "BillingName", required = true, nillable = true)
    protected String billingName;
    @XmlElement(name = "BillingContactName", required = true, nillable = true)
    protected String billingContactName;
    @XmlElement(name = "BillingAddress", required = true, nillable = true)
    protected String billingAddress;
    @XmlElement(name = "BillingAddress1", required = true, nillable = true)
    protected String billingAddress1;
    @XmlElement(name = "BillingCity", required = true, nillable = true)
    protected String billingCity;
    @XmlElement(name = "BillingState", required = true, nillable = true)
    protected String billingState;
    @XmlElement(name = "BillingPostal", required = true, nillable = true)
    protected String billingPostal;
    @XmlElement(name = "BillingCountryCode", required = true, nillable = true)
    protected String billingCountryCode;
    @XmlElement(name = "BillingPhoneNumber", required = true, nillable = true)
    protected String billingPhoneNumber;
    @XmlElement(name = "BillingFaxNumber", required = true, nillable = true)
    protected String billingFaxNumber;
    @XmlElement(name = "BillingEmail", required = true, nillable = true)
    protected String billingEmail;
    @XmlElement(name = "LastUpdateUser", required = true, nillable = true)
    protected String lastUpdateUser;
    @XmlElementRef(name = "LastUpdateTimestamp", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> lastUpdateTimestamp;
    @XmlElement(name = "Email", required = true, nillable = true)
    protected String email;
    @XmlElement(name = "Comments", required = true, nillable = true)
    protected String comments;
    @XmlElement(name = "DomainID", required = true, nillable = true)
    protected String domainID;
    @XmlElement(name = "Style", required = true, nillable = true)
    protected String style;
    @XmlElement(name = "IsRootOrg", required = true, type = Boolean.class, nillable = true)
    protected Boolean isRootOrg;
    @XmlElement(name = "ParentOrganizationID", required = true, nillable = true)
    protected String parentOrganizationID;
    @XmlElement(name = "PromoteToRootOrganization", required = true, type = Boolean.class, nillable = true)
    protected Boolean promoteToRootOrganization;
    @XmlElement(name = "InheritParentOrgType", required = true, type = Boolean.class, nillable = true)
    protected Boolean inheritParentOrgType;

    /**
     * Gets the value of the orgID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgID() {
        return orgID;
    }

    /**
     * Sets the value of the orgID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgID(String value) {
        this.orgID = value;
    }

    /**
     * Gets the value of the orgTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgTypeID() {
        return orgTypeID;
    }

    /**
     * Sets the value of the orgTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgTypeID(String value) {
        this.orgTypeID = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the shippingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingName() {
        return shippingName;
    }

    /**
     * Sets the value of the shippingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingName(String value) {
        this.shippingName = value;
    }

    /**
     * Gets the value of the shippingContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingContactName() {
        return shippingContactName;
    }

    /**
     * Sets the value of the shippingContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingContactName(String value) {
        this.shippingContactName = value;
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingAddress(String value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the shippingAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingAddress1() {
        return shippingAddress1;
    }

    /**
     * Sets the value of the shippingAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingAddress1(String value) {
        this.shippingAddress1 = value;
    }

    /**
     * Gets the value of the shippingCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingCity() {
        return shippingCity;
    }

    /**
     * Sets the value of the shippingCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingCity(String value) {
        this.shippingCity = value;
    }

    /**
     * Gets the value of the shippingState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingState() {
        return shippingState;
    }

    /**
     * Sets the value of the shippingState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingState(String value) {
        this.shippingState = value;
    }

    /**
     * Gets the value of the shippingPostal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingPostal() {
        return shippingPostal;
    }

    /**
     * Sets the value of the shippingPostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingPostal(String value) {
        this.shippingPostal = value;
    }

    /**
     * Gets the value of the shippingCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingCountryCode() {
        return shippingCountryCode;
    }

    /**
     * Sets the value of the shippingCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingCountryCode(String value) {
        this.shippingCountryCode = value;
    }

    /**
     * Gets the value of the shippingPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingPhoneNumber() {
        return shippingPhoneNumber;
    }

    /**
     * Sets the value of the shippingPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingPhoneNumber(String value) {
        this.shippingPhoneNumber = value;
    }

    /**
     * Gets the value of the shippingFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingFaxNumber() {
        return shippingFaxNumber;
    }

    /**
     * Sets the value of the shippingFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingFaxNumber(String value) {
        this.shippingFaxNumber = value;
    }

    /**
     * Gets the value of the shippingEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingEmail() {
        return shippingEmail;
    }

    /**
     * Sets the value of the shippingEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingEmail(String value) {
        this.shippingEmail = value;
    }

    /**
     * Gets the value of the billingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingName() {
        return billingName;
    }

    /**
     * Sets the value of the billingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingName(String value) {
        this.billingName = value;
    }

    /**
     * Gets the value of the billingContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingContactName() {
        return billingContactName;
    }

    /**
     * Sets the value of the billingContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingContactName(String value) {
        this.billingContactName = value;
    }

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAddress(String value) {
        this.billingAddress = value;
    }

    /**
     * Gets the value of the billingAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAddress1() {
        return billingAddress1;
    }

    /**
     * Sets the value of the billingAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAddress1(String value) {
        this.billingAddress1 = value;
    }

    /**
     * Gets the value of the billingCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCity() {
        return billingCity;
    }

    /**
     * Sets the value of the billingCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCity(String value) {
        this.billingCity = value;
    }

    /**
     * Gets the value of the billingState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingState() {
        return billingState;
    }

    /**
     * Sets the value of the billingState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingState(String value) {
        this.billingState = value;
    }

    /**
     * Gets the value of the billingPostal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingPostal() {
        return billingPostal;
    }

    /**
     * Sets the value of the billingPostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingPostal(String value) {
        this.billingPostal = value;
    }

    /**
     * Gets the value of the billingCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCountryCode() {
        return billingCountryCode;
    }

    /**
     * Sets the value of the billingCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCountryCode(String value) {
        this.billingCountryCode = value;
    }

    /**
     * Gets the value of the billingPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingPhoneNumber() {
        return billingPhoneNumber;
    }

    /**
     * Sets the value of the billingPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingPhoneNumber(String value) {
        this.billingPhoneNumber = value;
    }

    /**
     * Gets the value of the billingFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingFaxNumber() {
        return billingFaxNumber;
    }

    /**
     * Sets the value of the billingFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingFaxNumber(String value) {
        this.billingFaxNumber = value;
    }

    /**
     * Gets the value of the billingEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingEmail() {
        return billingEmail;
    }

    /**
     * Sets the value of the billingEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingEmail(String value) {
        this.billingEmail = value;
    }

    /**
     * Gets the value of the lastUpdateUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the value of the lastUpdateUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateUser(String value) {
        this.lastUpdateUser = value;
    }

    /**
     * Gets the value of the lastUpdateTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    /**
     * Sets the value of the lastUpdateTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastUpdateTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.lastUpdateTimestamp = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the domainID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainID() {
        return domainID;
    }

    /**
     * Sets the value of the domainID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainID(String value) {
        this.domainID = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStyle() {
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStyle(String value) {
        this.style = value;
    }

    /**
     * Gets the value of the isRootOrg property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRootOrg() {
        return isRootOrg;
    }

    /**
     * Sets the value of the isRootOrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRootOrg(Boolean value) {
        this.isRootOrg = value;
    }

    /**
     * Gets the value of the parentOrganizationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentOrganizationID() {
        return parentOrganizationID;
    }

    /**
     * Sets the value of the parentOrganizationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentOrganizationID(String value) {
        this.parentOrganizationID = value;
    }

    /**
     * Gets the value of the promoteToRootOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPromoteToRootOrganization() {
        return promoteToRootOrganization;
    }

    /**
     * Sets the value of the promoteToRootOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromoteToRootOrganization(Boolean value) {
        this.promoteToRootOrganization = value;
    }

    /**
     * Gets the value of the inheritParentOrgType property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInheritParentOrgType() {
        return inheritParentOrgType;
    }

    /**
     * Sets the value of the inheritParentOrgType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInheritParentOrgType(Boolean value) {
        this.inheritParentOrgType = value;
    }

}
