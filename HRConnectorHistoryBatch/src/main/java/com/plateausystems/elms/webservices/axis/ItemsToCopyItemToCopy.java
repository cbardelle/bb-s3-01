
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ItemsToCopyItemToCopy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemsToCopyItemToCopy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SourceItemTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceItemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceItemRevisionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DestinationItemTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DestinationItemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DestinationItemRevisionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CopyAuthorized" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyCollateralCredit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyRelatedCompetencies" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyRelatedDocuments" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyRelatedTasks" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyRequests" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyOnlineSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NewTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NewRevisionNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CopynIstructorCosts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyCatalogAssociations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NewSKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CopyPrerequisites" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemsToCopyItemToCopy", propOrder = {
    "sourceItemTypeID",
    "sourceItemID",
    "sourceItemRevisionDate",
    "destinationItemTypeID",
    "destinationItemID",
    "destinationItemRevisionDate",
    "copyAuthorized",
    "copyCollateralCredit",
    "copyRelatedCompetencies",
    "copyRelatedDocuments",
    "copyRelatedTasks",
    "copyRequests",
    "copyOnlineSettings",
    "newTitle",
    "newRevisionNum",
    "user",
    "copynIstructorCosts",
    "copyCatalogAssociations",
    "newSKU",
    "copyPrerequisites"
})
public class ItemsToCopyItemToCopy {

    @XmlElement(name = "SourceItemTypeID", required = true, nillable = true)
    protected String sourceItemTypeID;
    @XmlElement(name = "SourceItemID", required = true, nillable = true)
    protected String sourceItemID;
    @XmlElementRef(name = "SourceItemRevisionDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> sourceItemRevisionDate;
    @XmlElement(name = "DestinationItemTypeID", required = true, nillable = true)
    protected String destinationItemTypeID;
    @XmlElement(name = "DestinationItemID", required = true, nillable = true)
    protected String destinationItemID;
    @XmlElementRef(name = "DestinationItemRevisionDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> destinationItemRevisionDate;
    @XmlElement(name = "CopyAuthorized", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyAuthorized;
    @XmlElement(name = "CopyCollateralCredit", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyCollateralCredit;
    @XmlElement(name = "CopyRelatedCompetencies", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyRelatedCompetencies;
    @XmlElement(name = "CopyRelatedDocuments", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyRelatedDocuments;
    @XmlElement(name = "CopyRelatedTasks", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyRelatedTasks;
    @XmlElement(name = "CopyRequests", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyRequests;
    @XmlElement(name = "CopyOnlineSettings", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyOnlineSettings;
    @XmlElement(name = "NewTitle", required = true, nillable = true)
    protected String newTitle;
    @XmlElement(name = "NewRevisionNum", required = true, nillable = true)
    protected String newRevisionNum;
    @XmlElement(required = true, nillable = true)
    protected String user;
    @XmlElement(name = "CopynIstructorCosts", required = true, type = Boolean.class, nillable = true)
    protected Boolean copynIstructorCosts;
    @XmlElement(name = "CopyCatalogAssociations", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyCatalogAssociations;
    @XmlElement(name = "NewSKU", required = true, nillable = true)
    protected String newSKU;
    @XmlElement(name = "CopyPrerequisites", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyPrerequisites;

    /**
     * Gets the value of the sourceItemTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceItemTypeID() {
        return sourceItemTypeID;
    }

    /**
     * Sets the value of the sourceItemTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceItemTypeID(String value) {
        this.sourceItemTypeID = value;
    }

    /**
     * Gets the value of the sourceItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceItemID() {
        return sourceItemID;
    }

    /**
     * Sets the value of the sourceItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceItemID(String value) {
        this.sourceItemID = value;
    }

    /**
     * Gets the value of the sourceItemRevisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getSourceItemRevisionDate() {
        return sourceItemRevisionDate;
    }

    /**
     * Sets the value of the sourceItemRevisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setSourceItemRevisionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.sourceItemRevisionDate = value;
    }

    /**
     * Gets the value of the destinationItemTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationItemTypeID() {
        return destinationItemTypeID;
    }

    /**
     * Sets the value of the destinationItemTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationItemTypeID(String value) {
        this.destinationItemTypeID = value;
    }

    /**
     * Gets the value of the destinationItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationItemID() {
        return destinationItemID;
    }

    /**
     * Sets the value of the destinationItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationItemID(String value) {
        this.destinationItemID = value;
    }

    /**
     * Gets the value of the destinationItemRevisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDestinationItemRevisionDate() {
        return destinationItemRevisionDate;
    }

    /**
     * Sets the value of the destinationItemRevisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDestinationItemRevisionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.destinationItemRevisionDate = value;
    }

    /**
     * Gets the value of the copyAuthorized property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyAuthorized() {
        return copyAuthorized;
    }

    /**
     * Sets the value of the copyAuthorized property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyAuthorized(Boolean value) {
        this.copyAuthorized = value;
    }

    /**
     * Gets the value of the copyCollateralCredit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyCollateralCredit() {
        return copyCollateralCredit;
    }

    /**
     * Sets the value of the copyCollateralCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyCollateralCredit(Boolean value) {
        this.copyCollateralCredit = value;
    }

    /**
     * Gets the value of the copyRelatedCompetencies property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyRelatedCompetencies() {
        return copyRelatedCompetencies;
    }

    /**
     * Sets the value of the copyRelatedCompetencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyRelatedCompetencies(Boolean value) {
        this.copyRelatedCompetencies = value;
    }

    /**
     * Gets the value of the copyRelatedDocuments property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyRelatedDocuments() {
        return copyRelatedDocuments;
    }

    /**
     * Sets the value of the copyRelatedDocuments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyRelatedDocuments(Boolean value) {
        this.copyRelatedDocuments = value;
    }

    /**
     * Gets the value of the copyRelatedTasks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyRelatedTasks() {
        return copyRelatedTasks;
    }

    /**
     * Sets the value of the copyRelatedTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyRelatedTasks(Boolean value) {
        this.copyRelatedTasks = value;
    }

    /**
     * Gets the value of the copyRequests property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyRequests() {
        return copyRequests;
    }

    /**
     * Sets the value of the copyRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyRequests(Boolean value) {
        this.copyRequests = value;
    }

    /**
     * Gets the value of the copyOnlineSettings property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyOnlineSettings() {
        return copyOnlineSettings;
    }

    /**
     * Sets the value of the copyOnlineSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyOnlineSettings(Boolean value) {
        this.copyOnlineSettings = value;
    }

    /**
     * Gets the value of the newTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewTitle() {
        return newTitle;
    }

    /**
     * Sets the value of the newTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewTitle(String value) {
        this.newTitle = value;
    }

    /**
     * Gets the value of the newRevisionNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewRevisionNum() {
        return newRevisionNum;
    }

    /**
     * Sets the value of the newRevisionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewRevisionNum(String value) {
        this.newRevisionNum = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Gets the value of the copynIstructorCosts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopynIstructorCosts() {
        return copynIstructorCosts;
    }

    /**
     * Sets the value of the copynIstructorCosts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopynIstructorCosts(Boolean value) {
        this.copynIstructorCosts = value;
    }

    /**
     * Gets the value of the copyCatalogAssociations property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyCatalogAssociations() {
        return copyCatalogAssociations;
    }

    /**
     * Sets the value of the copyCatalogAssociations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyCatalogAssociations(Boolean value) {
        this.copyCatalogAssociations = value;
    }

    /**
     * Gets the value of the newSKU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSKU() {
        return newSKU;
    }

    /**
     * Sets the value of the newSKU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSKU(String value) {
        this.newSKU = value;
    }

    /**
     * Gets the value of the copyPrerequisites property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyPrerequisites() {
        return copyPrerequisites;
    }

    /**
     * Sets the value of the copyPrerequisites property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyPrerequisites(Boolean value) {
        this.copyPrerequisites = value;
    }

}
