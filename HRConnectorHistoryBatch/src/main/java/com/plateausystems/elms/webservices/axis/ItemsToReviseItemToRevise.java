
package com.plateausystems.elms.webservices.axis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ItemsToReviseItemToRevise complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemsToReviseItemToRevise">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SourceItemTypeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceItemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceItemRevisionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DestinationItemRevisionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InactivePreviousRevisions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyDevelopmentPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CopyInstructorAndSchedule" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AllowStudentAccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NewTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NewRevisionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemsToReviseItemToRevise", propOrder = {
    "sourceItemTypeID",
    "sourceItemID",
    "sourceItemRevisionDate",
    "destinationItemRevisionDate",
    "inactivePreviousRevisions",
    "copyDevelopmentPlan",
    "copyInstructorAndSchedule",
    "userName",
    "allowStudentAccess",
    "newTitle",
    "newRevisionNo"
})
public class ItemsToReviseItemToRevise {

    @XmlElement(name = "SourceItemTypeID", required = true, nillable = true)
    protected String sourceItemTypeID;
    @XmlElement(name = "SourceItemID", required = true, nillable = true)
    protected String sourceItemID;
    @XmlElementRef(name = "SourceItemRevisionDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> sourceItemRevisionDate;
    @XmlElementRef(name = "DestinationItemRevisionDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> destinationItemRevisionDate;
    @XmlElement(name = "InactivePreviousRevisions", required = true, type = Boolean.class, nillable = true)
    protected Boolean inactivePreviousRevisions;
    @XmlElement(name = "CopyDevelopmentPlan", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyDevelopmentPlan;
    @XmlElement(name = "CopyInstructorAndSchedule", required = true, type = Boolean.class, nillable = true)
    protected Boolean copyInstructorAndSchedule;
    @XmlElement(name = "UserName", required = true, nillable = true)
    protected String userName;
    @XmlElement(name = "AllowStudentAccess", required = true, type = Boolean.class, nillable = true)
    protected Boolean allowStudentAccess;
    @XmlElement(name = "NewTitle", required = true, nillable = true)
    protected String newTitle;
    @XmlElement(name = "NewRevisionNo", required = true, nillable = true)
    protected String newRevisionNo;

    /**
     * Gets the value of the sourceItemTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceItemTypeID() {
        return sourceItemTypeID;
    }

    /**
     * Sets the value of the sourceItemTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceItemTypeID(String value) {
        this.sourceItemTypeID = value;
    }

    /**
     * Gets the value of the sourceItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceItemID() {
        return sourceItemID;
    }

    /**
     * Sets the value of the sourceItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceItemID(String value) {
        this.sourceItemID = value;
    }

    /**
     * Gets the value of the sourceItemRevisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getSourceItemRevisionDate() {
        return sourceItemRevisionDate;
    }

    /**
     * Sets the value of the sourceItemRevisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setSourceItemRevisionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.sourceItemRevisionDate = value;
    }

    /**
     * Gets the value of the destinationItemRevisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDestinationItemRevisionDate() {
        return destinationItemRevisionDate;
    }

    /**
     * Sets the value of the destinationItemRevisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDestinationItemRevisionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.destinationItemRevisionDate = value;
    }

    /**
     * Gets the value of the inactivePreviousRevisions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInactivePreviousRevisions() {
        return inactivePreviousRevisions;
    }

    /**
     * Sets the value of the inactivePreviousRevisions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInactivePreviousRevisions(Boolean value) {
        this.inactivePreviousRevisions = value;
    }

    /**
     * Gets the value of the copyDevelopmentPlan property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyDevelopmentPlan() {
        return copyDevelopmentPlan;
    }

    /**
     * Sets the value of the copyDevelopmentPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyDevelopmentPlan(Boolean value) {
        this.copyDevelopmentPlan = value;
    }

    /**
     * Gets the value of the copyInstructorAndSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCopyInstructorAndSchedule() {
        return copyInstructorAndSchedule;
    }

    /**
     * Sets the value of the copyInstructorAndSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCopyInstructorAndSchedule(Boolean value) {
        this.copyInstructorAndSchedule = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the allowStudentAccess property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowStudentAccess() {
        return allowStudentAccess;
    }

    /**
     * Sets the value of the allowStudentAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowStudentAccess(Boolean value) {
        this.allowStudentAccess = value;
    }

    /**
     * Gets the value of the newTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewTitle() {
        return newTitle;
    }

    /**
     * Sets the value of the newTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewTitle(String value) {
        this.newTitle = value;
    }

    /**
     * Gets the value of the newRevisionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewRevisionNo() {
        return newRevisionNo;
    }

    /**
     * Sets the value of the newRevisionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewRevisionNo(String value) {
        this.newRevisionNo = value;
    }

}
