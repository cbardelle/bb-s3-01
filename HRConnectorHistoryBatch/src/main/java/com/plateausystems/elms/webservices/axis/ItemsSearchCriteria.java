
package com.plateausystems.elms.webservices.axis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Caller" type="{http://axis.webservices.elms.plateausystems.com}Caller"/>
 *         &lt;element name="ItemTypes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *         &lt;element name="ItemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RevisionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="RevisionNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ItemTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ItemClassifications" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *         &lt;element name="HasOnlineContent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DomainIDs" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "caller",
    "itemTypes",
    "itemID",
    "revisionDate",
    "revisionNumber",
    "itemTitle",
    "isActive",
    "itemClassifications",
    "hasOnlineContent",
    "domainIDs"
})
@XmlRootElement(name = "ItemsSearchCriteria")
public class ItemsSearchCriteria {

    @XmlElement(name = "Caller", required = true, nillable = true)
    protected Caller caller;
    @XmlElement(name = "ItemTypes", required = true, nillable = true)
    protected List<String> itemTypes;
    @XmlElement(name = "ItemID", required = true, nillable = true)
    protected String itemID;
    @XmlElementRef(name = "RevisionDate", namespace = "http://axis.webservices.elms.plateausystems.com", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> revisionDate;
    @XmlElement(name = "RevisionNumber", required = true, nillable = true)
    protected String revisionNumber;
    @XmlElement(name = "ItemTitle", required = true, nillable = true)
    protected String itemTitle;
    @XmlElement(name = "IsActive", required = true, type = Boolean.class, nillable = true)
    protected Boolean isActive;
    @XmlElement(name = "ItemClassifications", required = true, nillable = true)
    protected List<String> itemClassifications;
    @XmlElement(name = "HasOnlineContent", required = true, type = Boolean.class, nillable = true)
    protected Boolean hasOnlineContent;
    @XmlElement(name = "DomainIDs", required = true, nillable = true)
    protected List<String> domainIDs;

    /**
     * Gets the value of the caller property.
     * 
     * @return
     *     possible object is
     *     {@link Caller }
     *     
     */
    public Caller getCaller() {
        return caller;
    }

    /**
     * Sets the value of the caller property.
     * 
     * @param value
     *     allowed object is
     *     {@link Caller }
     *     
     */
    public void setCaller(Caller value) {
        this.caller = value;
    }

    /**
     * Gets the value of the itemTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getItemTypes() {
        if (itemTypes == null) {
            itemTypes = new ArrayList<String>();
        }
        return this.itemTypes;
    }

    /**
     * Gets the value of the itemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Sets the value of the itemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Gets the value of the revisionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getRevisionDate() {
        return revisionDate;
    }

    /**
     * Sets the value of the revisionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setRevisionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.revisionDate = value;
    }

    /**
     * Gets the value of the revisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevisionNumber() {
        return revisionNumber;
    }

    /**
     * Sets the value of the revisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevisionNumber(String value) {
        this.revisionNumber = value;
    }

    /**
     * Gets the value of the itemTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTitle() {
        return itemTitle;
    }

    /**
     * Sets the value of the itemTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTitle(String value) {
        this.itemTitle = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the itemClassifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemClassifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemClassifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getItemClassifications() {
        if (itemClassifications == null) {
            itemClassifications = new ArrayList<String>();
        }
        return this.itemClassifications;
    }

    /**
     * Gets the value of the hasOnlineContent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasOnlineContent() {
        return hasOnlineContent;
    }

    /**
     * Sets the value of the hasOnlineContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasOnlineContent(Boolean value) {
        this.hasOnlineContent = value;
    }

    /**
     * Gets the value of the domainIDs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the domainIDs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDomainIDs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDomainIDs() {
        if (domainIDs == null) {
            domainIDs = new ArrayList<String>();
        }
        return this.domainIDs;
    }

}
