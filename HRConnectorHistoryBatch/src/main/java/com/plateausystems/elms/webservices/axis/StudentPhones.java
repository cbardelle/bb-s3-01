
package com.plateausystems.elms.webservices.axis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StudentPhones complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StudentPhones">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudentPhone" type="{http://axis.webservices.elms.plateausystems.com}StudentPhonesStudentPhone" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudentPhones", propOrder = {
    "studentPhone"
})
public class StudentPhones {

    @XmlElement(name = "StudentPhone", required = true, nillable = true)
    protected List<StudentPhonesStudentPhone> studentPhone;

    /**
     * Gets the value of the studentPhone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studentPhone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudentPhone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudentPhonesStudentPhone }
     * 
     * 
     */
    public List<StudentPhonesStudentPhone> getStudentPhone() {
        if (studentPhone == null) {
            studentPhone = new ArrayList<StudentPhonesStudentPhone>();
        }
        return this.studentPhone;
    }

}
