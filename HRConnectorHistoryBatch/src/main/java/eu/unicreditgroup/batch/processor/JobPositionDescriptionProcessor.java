package eu.unicreditgroup.batch.processor;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.JobPositionDao;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.PlateauUtil;

public class JobPositionDescriptionProcessor {
	
	Logger logger = Logger.getLogger(JobPositionDescriptionProcessor.class);
	
	private JobPositionDao jobPositionDao;
	private ConfigParams configParams;
	private ReportDataBean reportDataBean;

	
	public void execute(){
	
		//1. Display errors when there are different descriptions for the same code
		List<Map<String, Object>> differentDescErrorList = jobPositionDao.getDifferentDescErrorList();
		reportDataBean.setNumOfJobPosDescDifferentForTheSameCode(differentDescErrorList.size());
		if(differentDescErrorList.size()>0){
			logger.warn("There are "+differentDescErrorList.size()+" different descriptions for the same code:");
			Iterator<Map<String, Object>> differentDescErrorIterator = differentDescErrorList.iterator();
			Map<String, Object> differentDescError;
			while( differentDescErrorIterator.hasNext() ){
				differentDescError = differentDescErrorIterator.next();
				logger.warn("ID: "+differentDescError.get("GLOBAL_JOB_CODE")+", ABBR: "+differentDescError.get("GLOBAL_JOB_ABBR")+", DESC: "+ differentDescError.get("GLOBAL_JOB_DESC"));
			}
		}
		
		boolean canUpdateDictTable = false;
		try {
			//2. Update Job Location Description in Plateau
			
			List<Map<String, Object>> jobPositionList = jobPositionDao.getModifiedDescForSendToPlateau();
			logger.info(jobPositionList.size()+" Job Position Descriptions need to be updated in Plateau.");

			
			
			
			WebClient webClient= PlateauUtil.getLoggedWebClient( configParams.getStrPlateauLoginPageUrl(),
											configParams.getStrPlateauLoginUsr(),
											configParams.getStrPlateauLoginPwd(), 
											BrowserVersion.FIREFOX_3, null, 0 );
			
			
			updateJobPositionDescriptionInPlateau( jobPositionList, webClient );

			//After successful update in Plateau we can update dict table.
			canUpdateDictTable = true;
			
		} catch (Exception e) {
			logger.error("Error in updateJobPositionDescriptionInPlateau! Must skip update of old Job Position Descriptions in dict table!", e);
		}
		
		if(canUpdateDictTable){
			
			//3. Update old Job Position Descriptions in dict table with new values
			int updatedDesc = jobPositionDao.updateOldDescInDictTable();
			logger.info(updatedDesc+" old Job Position Descriptions have been updated in dict table.");
			reportDataBean.setNumOfJobPosDescUpdatesInDictTable(updatedDesc);
		}
		

		//4. Add new Job Position Desc to dict table
		int insertedDesc = jobPositionDao.insertNewDescToDictTable();
		logger.info(insertedDesc+" new Job Position Descriptions have been inserted to dict table.");
		reportDataBean.setNumOfJobPosDescInsertsToDictTable(insertedDesc);
	}

	public void updateJobPositionDescriptionInPlateau( List<Map<String, Object>> jobPositionList, WebClient webClient ) throws Exception{
		
	
		if(jobPositionList.size()>0){
			
			Iterator<Map<String, Object>> iterator = jobPositionList.iterator();
			Map<String, Object> jobPositionMap;
			String jobPositionId;
			String jobPositionDesc;
						

			HtmlPage htmlPage;
			HtmlForm htmlForm;

			while( iterator.hasNext() ){
				jobPositionMap = iterator.next();
				
				jobPositionId = jobPositionMap.get("CODE").toString();
				jobPositionDesc = jobPositionMap.get("DESCR").toString();
				
				try {
					htmlPage = webClient.getPage(configParams.getStrPlateauJobPositionEditPageUrl()+"?jobPositionId="+jobPositionId);
					htmlForm  = htmlPage.getFormByName("jobPositionForm");
					htmlForm.getInputByName("jobPositionDesc").setValueAttribute( jobPositionDesc );
					htmlForm.getInputByName("submitbutton").click();
					logger.info("ID: "+jobPositionId+", DESC: "+ jobPositionDesc);
				} catch (FailingHttpStatusCodeException e) {
					logger.error("Can't update by HtmlUnit. ID: "+jobPositionId+", DESC: "+ jobPositionDesc, e);
				}
				//System.out.println(htmlPage.asXml());
			}

		}
		logger.info(jobPositionList.size()+" Job Position Descriptions have been updated in Plateau by HTMLUnit.");
		reportDataBean.setNumOfJobPosDescUpdatesSentByHtmlUnit(jobPositionList.size());
		
	}

	public JobPositionDao getJobPositionDao() {
		return jobPositionDao;
	}

	public void setJobPositionDao(JobPositionDao jobPositionDao) {
		this.jobPositionDao = jobPositionDao;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

}
