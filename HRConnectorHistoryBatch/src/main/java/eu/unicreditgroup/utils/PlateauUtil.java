package eu.unicreditgroup.utils;

import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class PlateauUtil {
		
	static Logger logger = Logger.getLogger(PlateauUtil.class);
		
	
	
	
	public static WebClient getLoggedWebClient(String loginPageUrl, String login, String pwd, BrowserVersion browserVersion, String proxyUrl, int proxyPort ) throws Exception{
		
		
		WebClient webClient;
		if (proxyUrl != null ){		
			webClient = new WebClient(browserVersion, proxyUrl, proxyPort);
		} else {
			webClient = new WebClient(browserVersion);
		}
		
		logger.debug("Loging to plateau...");

		// get the loging page		
		HtmlPage loginPage = webClient.getPage(loginPageUrl);

		final HtmlForm form = loginPage.getFormByName("authenticateForm");

		final HtmlSubmitInput submit = form.getInputByName("submit");
		final HtmlTextInput userName = form.getInputByName("userName");
		final HtmlPasswordInput password = form.getInputByName("password");

		
		// set credentials
		userName.setValueAttribute(login);
		password.setValueAttribute(pwd);
		

		// submit the loging form
		HtmlPage result = submit.click();
		 
		HtmlElement welcomeCheck = result.getElementById("welcomeText");
		if (welcomeCheck == null){
			throw new Exception("Login failed becase it seems that admin is not welcomed by anyone ...");
		}

		logger.debug("Logged!");
		return webClient;
		
	}
	
	
	

	
	
}
