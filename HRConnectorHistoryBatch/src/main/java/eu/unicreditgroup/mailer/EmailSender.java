package eu.unicreditgroup.mailer;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;


/**
 * Simplifies configuration for sending email with usage of
 * Jakarta commons-email.
 *
 */
public class EmailSender {
	
	private static final String EMAIL_LIST_DELIMITERS = " ;,";
	
	private static final String EMAIL_VALIDATION_REGEXP = "[a-zA-Z0-9]+[a-zA-Z0-9_\\.\\-\\*\\+]*@(([a-zA-Z0-9]+[a-zA-Z0-9_\\.\\-\\+]*[a-zA-Z0-9])|([a-zA-Z0-9]+))";
	
	private static Pattern emailValidationPattern;
	static {
		emailValidationPattern = Pattern.compile(EMAIL_VALIDATION_REGEXP);
	}
	
	private String hostName;
	
	private String fromAddress;
	
	private String subject;
	
	private String message;
	
	private Map<String, ByteArrayDataSource> attachmentsMap;
	
	private Set<String> toAddresses;
	
	private Set<String> ccAddresses;
	
	private Set<String> bccAddresses;

	
	public EmailSender() {
		attachmentsMap = new HashMap<String, ByteArrayDataSource>();
		
		toAddresses = new HashSet<String>();
		ccAddresses = new HashSet<String>();
		bccAddresses = new HashSet<String>();
		
	}
	
	
	
	
	public static boolean validateEmail(String email){
		Matcher matcher = emailValidationPattern.matcher(email);
		return matcher.matches();
	}
	
	
	
	/**
	 * Reads given configFiles and sets up all found properties.
	 * 
	 * @param configFile
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public EmailSender(String configFile) throws FileNotFoundException, IOException{
		//default constructor
		this();
		
		//initialize fields
		Properties props = new Properties();
		props.load( new FileInputStream(configFile) );
		
		//hostName
		if(StringUtils.isNotBlank( props.getProperty("hostName"))){
			hostName = props.getProperty("hostName");
		}
		
		//fromAddress
		if(StringUtils.isNotBlank( props.getProperty("fromAddress"))){
			fromAddress = props.getProperty("fromAddress");
		}

		//subject
		if(StringUtils.isNotBlank( props.getProperty("subject"))){
			subject = props.getProperty("subject");
		}
		
		//message
		if(StringUtils.isNotBlank( props.getProperty("message"))){
			message = props.getProperty("message");
		}
		
		//toAddresses
		if(StringUtils.isNotBlank( props.getProperty("toAddresses"))){
			String toAddrList = props.getProperty("toAddresses");
			
			StringTokenizer st = new StringTokenizer( toAddrList, EMAIL_LIST_DELIMITERS );
			while(st.hasMoreTokens()){
				String email = StringUtils.trim(st.nextToken());
				if (validateEmail(email)){
					toAddresses.add(email);
				}
			}
			
		}
		
		//ccAddresses
		if(StringUtils.isNotBlank( props.getProperty("ccAddresses"))){
			String ccAddrList = props.getProperty("ccAddresses");
			
			StringTokenizer st = new StringTokenizer( ccAddrList, EMAIL_LIST_DELIMITERS );
			while(st.hasMoreTokens()){
				String email = StringUtils.trim(st.nextToken());
				if (validateEmail(email)){
					ccAddresses.add(email);
				}
			}
			
		}
		
		
		//bccAddresses
		if(StringUtils.isNotBlank( props.getProperty("bccAddresses"))){
			String bccAddrList = props.getProperty("bccAddresses");
			
			StringTokenizer st = new StringTokenizer( bccAddrList, EMAIL_LIST_DELIMITERS );
			while(st.hasMoreTokens()){
				String email = StringUtils.trim(st.nextToken());
				if (validateEmail(email)){
					bccAddresses.add(email);
				}
			}
			
		}
		
	}


	
	/**
	 * Adds file to be attached in the email.
	 * 
	 * @param filePath path to the file
	 * @param attachmentName name of attachment representing given file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void addAttachmentFromFile(String filePath, String attachmentName) throws FileNotFoundException, IOException{
		attachmentsMap.put(attachmentName, new ByteArrayDataSource( new FileInputStream(filePath), null));
	}

	
	
	/**
	 * Adds file to be attached in the email.
	 * 
	 * @param filePath path to the file
	 * @param attachmentName name of attachment representing given file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void addZippedAttachmentFromFile(String filePath, String attachmentName) throws FileNotFoundException, IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream( baos );
		
		ZipEntry ze = new ZipEntry(attachmentName);
		zos.putNextEntry(ze);
		
		IOUtils.copy(new FileInputStream(filePath), zos);

		zos.closeEntry();
		zos.close();
		baos.close();
		
		String zipAttachmentName = StringUtils.replace(attachmentName, ".", "_") + ".zip";
		
		attachmentsMap.put(zipAttachmentName, new ByteArrayDataSource( baos.toByteArray(), null));
	}
	
	
	
	/**
	 * Adds attachment with content from
	 * <code>attachmentContent</code>
	 * and name 
	 * <code>attachmentName</code>
	 * 
	 * @param attachmentContent
	 * @param attachmentName
	 * @throws IOException 
	 */
	public void addAttachmentFromString(String attachmentContent, String attachmentName) throws IOException{
		attachmentsMap.put(attachmentName, new ByteArrayDataSource(attachmentContent, null));
	}
	
	
	
	/**
	 * Sends an email.
	 * 
	 * @throws EmailException
	 */
	public void sendEmail() throws EmailException{
		HtmlEmail email = new HtmlEmail();

		if(StringUtils.isNotBlank(hostName)){
			email.setHostName(hostName);
		}
		
		if(StringUtils.isNotBlank(fromAddress)){
			email.setFrom(fromAddress);
		}
		
		if(StringUtils.isNotBlank(subject)){
			email.setSubject(subject);
		}
		
		if(StringUtils.isNotBlank(message)){
			email.setHtmlMsg(message);
		}
		
		//TO addresses
		for (String toAddr : toAddresses) {
			email.addTo(toAddr);
		}
		
		//CC addresses
		for (String ccAddr : ccAddresses) {
			email.addCc(ccAddr);
		}
		
		//BCC addresses
		for (String bccAddr : bccAddresses) {
			email.addBcc(bccAddr);
		}
		
		//attachments
		Set<Entry<String, ByteArrayDataSource>> attEntrSet = attachmentsMap.entrySet();
		for (Entry<String, ByteArrayDataSource> entry : attEntrSet) {
			email.attach(entry.getValue(), entry.getKey(), entry.getKey());
		}
		
		email.send();
		
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Map<String, ByteArrayDataSource> getAttachmentsMap() {
		return attachmentsMap;
	}

	public void setAttachmentsMap(Map<String, ByteArrayDataSource> attachmentsMap) {
		this.attachmentsMap = attachmentsMap;
	}

	public Set<String> getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(Set<String> toAddresses) {
		this.toAddresses = toAddresses;
	}

	public Set<String> getCcAddresses() {
		return ccAddresses;
	}

	public void setCcAddresses(Set<String> ccAddresses) {
		this.ccAddresses = ccAddresses;
	}

	public Set<String> getBccAddresses() {
		return bccAddresses;
	}

	public void setBccAddresses(Set<String>bccAddresses) {
		this.bccAddresses = bccAddresses;
	}
	
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}	
	
}
