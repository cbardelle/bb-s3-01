package eu.unicreditgroup.mailer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ConfigParams;

public class ReportDataBean {
	
	private Date stepProcessHistoryDataStartDate;
	
	private Date stepProcessHistoryDataEndDate;
	
	private Date batchStartDate;
		
	private String srcDataDeliveryDateAsString;
	
	private Date srcDataDeliveryDateAsDate;
	
	private Date batchEndDate;
	
	private long newUsersCount;
	
	private long removedUsersCount;
	
	private long updatedUsersCount;
	
	private List<String> usersWithInvalidLineManager;
	
	// 2012-06-11 - refactoring processing of users with invalid line managers simplified
	//private List<String> usersWithNonExistingLineManager;
	
	private List<String> usersWithInvalidLineManagerWS;
	
	private int usersWithInvalidLineManagerCount = 0;
		
	// 2012-06-11 - refactoring processing of users with invalid line managers simplified
	//private int usersWithWithNonExistingLineManagerCount = 0;
	
	
	private int numOfJobLocDescDifferentForTheSameCode 	= 0;
	private int numOfJobLocDescUpdatesSentByWS			= 0;
	private int numOfJobLocDescUpdatesInDictTable		= 0;
	private int numOfJobLocDescInsertsToDictTable		= 0;
	private int numOfJobPosDescDifferentForTheSameCode 	= 0;
	private int numOfJobPosDescUpdatesSentByHtmlUnit	= 0;
	private int numOfJobPosDescUpdatesInDictTable		= 0;
	private int numOfJobPosDescInsertsToDictTable		= 0;
	
	public final String JAVA_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public final String SQL_DATE_FORMAT = "YYYY-MM-DD HH24:MI:SS";
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private ConfigParams configParams;
	
	private static final Logger logger = Logger.getLogger(ReportDataBean.class);
		
	synchronized public void incNewUsersCount(){
		newUsersCount++;
	}

	synchronized public void addNewUsersCount(int count){
		newUsersCount+= count;
	}
	

	synchronized public void incRemovedUsersCount(){
		removedUsersCount++;
	}

	synchronized public void addRemovedUsersCount(int count){
		removedUsersCount+= count;
	}

	
	synchronized public void incUpdatedUsersCount(){
		updatedUsersCount++;
	}

	synchronized public void addUpdatedUsersCount(int count){
		updatedUsersCount+= count;
	}

	

		
	public Date getStepProcessHistoryDataStartDate() {
		return stepProcessHistoryDataStartDate;
	}

	public void setStepProcessHistoryDataStartDate(
			Date stepProcessHistoryDataStartDate) {
		this.stepProcessHistoryDataStartDate = stepProcessHistoryDataStartDate;
	}

	public Date getStepProcessHistoryDataEndDate() {
		return stepProcessHistoryDataEndDate;
	}

	public void setStepProcessHistoryDataEndDate(Date stepProcessHistoryDataEndDate) {
		this.stepProcessHistoryDataEndDate = stepProcessHistoryDataEndDate;
	}

	public Date getBatchStartDate() {
		return batchStartDate;
	}

	public void setBatchStartDate(Date batchStartDate) {
		this.batchStartDate = batchStartDate;	
				
		DateFormat df = new SimpleDateFormat(JAVA_DATE_FORMAT);
			    
		// Was source data delivery date specified in config file?
		Date startDate = batchStartDate;
		
		if(StringUtils.isNotEmpty(configParams.getStrSourceDataDeliveryDate())){
			try {
				startDate = df.parse(configParams.getStrSourceDataDeliveryDate());
			} catch (ParseException e) {
				
				logger.error("Unable to parse source data delivery date: " + configParams.getStrSourceDataDeliveryDate(),e);
				startDate = batchStartDate;
				
			}	
		}
		srcDataDeliveryDateAsDate = startDate;
		srcDataDeliveryDateAsString = df.format(startDate);
		
	}

	public Date getBatchEndDate() {
		return batchEndDate;
	}

	public void setBatchEndDate(Date batchEndDate) {
		this.batchEndDate = batchEndDate;
	}
	

	public List<String> getUsersWithInvalidLineManager() {
		return usersWithInvalidLineManager;
	}

	public void setUsersWithInvalidLineManager(
			List<String> usersWithInvalidLineManager) {
		this.usersWithInvalidLineManager = usersWithInvalidLineManager;
	}

	public List<String> getUsersWithInvalidLineManagerWS() {
		return usersWithInvalidLineManagerWS;
	}

	public void setUsersWithInvalidLineManagerWS(
			List<String> usersWithInvalidLineManagerWS) {
		this.usersWithInvalidLineManagerWS = usersWithInvalidLineManagerWS;
	}

	public int getUsersWithInvalidLineManagerCount() {
		return usersWithInvalidLineManagerCount;
	}

	public void setUsersWithInvalidLineManagerCount(
			int usersWithInvalidLineManagerCount) {
		this.usersWithInvalidLineManagerCount = usersWithInvalidLineManagerCount;
	}

	public long getNewUsersCount() {
		return newUsersCount;
	}

	public void setNewUsersCount(long newUsersCount) {
		this.newUsersCount = newUsersCount;
	}

	public long getRemovedUsersCount() {
		return removedUsersCount;
	}

	public void setRemovedUsersCount(long removedUsersCount) {
		this.removedUsersCount = removedUsersCount;
	}

	public long getUpdatedUsersCount() {
		return updatedUsersCount;
	}

	public void setUpdatedUsersCount(long updatedUsersCount) {
		this.updatedUsersCount = updatedUsersCount;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public String getSrcDataDeliveryDateAsString() {
		return srcDataDeliveryDateAsString;
	}

	public Date getSrcDataDeliveryDateAsDate() {
		return srcDataDeliveryDateAsDate;
	}

	public void setSrcDataDeliveryDateAsDate(Date srcDataDeliveryDateAsDate) {
		this.srcDataDeliveryDateAsDate = srcDataDeliveryDateAsDate;
	}

	// 2012-06-11 - refactoring processing of users with invalid line managers simplified
	/*
	public List<String> getUsersWithNonExistingLineManager() {
		return usersWithNonExistingLineManager;
	}

	public void setUsersWithNonExistingLineManager(
			List<String> usersWithNonExistingLineManager) {
		this.usersWithNonExistingLineManager = usersWithNonExistingLineManager;
	}

	public int getUsersWithWithNonExistingLineManagerCount() {
		return usersWithWithNonExistingLineManagerCount;
	}

	public void setUsersWithWithNonExistingLineManagerCount(
			int usersWithWithNonExistingLineManagerCount) {
		this.usersWithWithNonExistingLineManagerCount = usersWithWithNonExistingLineManagerCount;
	}*/

	

	public int getNumOfJobLocDescDifferentForTheSameCode() {
		return numOfJobLocDescDifferentForTheSameCode;
	}

	public void setNumOfJobLocDescDifferentForTheSameCode(
			int numOfJobLocDescDifferentForTheSameCode) {
		this.numOfJobLocDescDifferentForTheSameCode = numOfJobLocDescDifferentForTheSameCode;
	}

	public int getNumOfJobLocDescUpdatesSentByWS() {
		return numOfJobLocDescUpdatesSentByWS;
	}

	public void setNumOfJobLocDescUpdatesSentByWS(int numOfJobLocDescUpdatesSentByWS) {
		this.numOfJobLocDescUpdatesSentByWS = numOfJobLocDescUpdatesSentByWS;
	}

	public int getNumOfJobLocDescUpdatesInDictTable() {
		return numOfJobLocDescUpdatesInDictTable;
	}

	public void setNumOfJobLocDescUpdatesInDictTable(
			int numOfJobLocDescUpdatesInDictTable) {
		this.numOfJobLocDescUpdatesInDictTable = numOfJobLocDescUpdatesInDictTable;
	}

	public int getNumOfJobLocDescInsertsToDictTable() {
		return numOfJobLocDescInsertsToDictTable;
	}

	public void setNumOfJobLocDescInsertsToDictTable(
			int numOfJobLocDescInsertsToDictTable) {
		this.numOfJobLocDescInsertsToDictTable = numOfJobLocDescInsertsToDictTable;
	}

	public int getNumOfJobPosDescDifferentForTheSameCode() {
		return numOfJobPosDescDifferentForTheSameCode;
	}

	public void setNumOfJobPosDescDifferentForTheSameCode(
			int numOfJobPosDescDifferentForTheSameCode) {
		this.numOfJobPosDescDifferentForTheSameCode = numOfJobPosDescDifferentForTheSameCode;
	}
	
	public int getNumOfJobPosDescUpdatesSentByHtmlUnit() {
		return numOfJobPosDescUpdatesSentByHtmlUnit;
	}

	public void setNumOfJobPosDescUpdatesSentByHtmlUnit(
			int numOfJobPosDescUpdatesSentByHtmlUnit) {
		this.numOfJobPosDescUpdatesSentByHtmlUnit = numOfJobPosDescUpdatesSentByHtmlUnit;
	}

	public int getNumOfJobPosDescUpdatesInDictTable() {
		return numOfJobPosDescUpdatesInDictTable;
	}

	public void setNumOfJobPosDescUpdatesInDictTable(
			int numOfJobPosDescUpdatesInDictTable) {
		this.numOfJobPosDescUpdatesInDictTable = numOfJobPosDescUpdatesInDictTable;
	}

	public int getNumOfJobPosDescInsertsToDictTable() {
		return numOfJobPosDescInsertsToDictTable;
	}

	public void setNumOfJobPosDescInsertsToDictTable(
			int numOfJobPosDescInsertsToDictTable) {
		this.numOfJobPosDescInsertsToDictTable = numOfJobPosDescInsertsToDictTable;
	}
	
	
	
	
}
