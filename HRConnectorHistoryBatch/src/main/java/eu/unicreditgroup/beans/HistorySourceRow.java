package eu.unicreditgroup.beans;

import org.apache.commons.lang.StringUtils;

public class HistorySourceRow {
	

	String[] data;
	
	Boolean valid;
	
	public String getIdOrUserName()
	{
		if (data == null){
			return "";
		}
		
		if (StringUtils.isNotEmpty(data[2])){
			return data[2];
		} else {
			// if there's no user Id return fname + last name
			return data[3] + " " + data[4];	
		}	
		
	}
	
	public String getValue(int index){	
		
		if (data == null){
			return null;
		}
		if (index >= data.length){
			return null;
		}
		
		return data[index];
	}
	
	public void setValue(int index, String val){		
		data[index] = val;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}


}
