package eu.unicreditgroup.beans;

import java.util.Comparator;

/**
 * 
 * 
 */
public class TrackedAttr implements Comparable<TrackedAttr>, Comparator<TrackedAttr>
{
	private String attrName;
	private String attrCode;
	private String mappedValue;
	private String dictValue;
	private String assignProfId;
	
	//seters / getters
	
	public String getAssignProfId() {
		if( assignProfId != null ) return assignProfId;
		return "";
	}
	public void setAssignProfId(String assignProfId) {
		this.assignProfId = assignProfId;
	}
	public String getAttrName() {
		if(attrName != null) return attrName;
		return "";
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrCode() {
		if( attrCode != null ) return attrCode;
		return "";
	}
	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}
	public String getMappedValue() {
		if( mappedValue != null ) return mappedValue;
		return "";
	}
	public void setMappedValue(String mappedValue) {
		this.mappedValue = mappedValue;
	}
	public String getDictValue() {
		if (dictValue != null) return dictValue;
		return "";
	}
	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}
	
	
	// presenters
	
	@Override
	public String toString() {
		return attrName.concat(",").concat(attrCode).concat(",").concat(dictValue).concat(",").concat(mappedValue);
	}
	
	
	// comperators
	
	@Override
	public int compareTo(TrackedAttr trackedComper) {
		
		if(	attrName.equals(trackedComper.attrName) &&
			attrCode.equals(trackedComper.attrCode) &&
			dictValue.equals(trackedComper.dictValue) ) {
			return 0;
		}
			
		return 1;
	}
	
	@Override
	public int compare(TrackedAttr compareFrom, TrackedAttr compareTo) 
	{
		return compareFrom.compareTo(compareTo);
	}
	
	/**If we contains 2 diferent assignment profiles that are connectet to the same attribute, 
	 * we can marge this assingment profile to one value */
	public boolean updateAssignProf(TrackedAttr trackedAttr)
	{
		if( compareTo(trackedAttr) ==0 )
		{
			if(getAssignProfId().contains(trackedAttr.getAssignProfId()) == false)
			{
				if(!getAssignProfId().isEmpty()) assignProfId = assignProfId.concat(", ");
				assignProfId = assignProfId.concat(trackedAttr.getAssignProfId());
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return attrCode.concat(attrName).concat(dictValue).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TrackedAttr)
		{
			return compareTo((TrackedAttr)obj) == 0;
		}
		return false;
	}
}
