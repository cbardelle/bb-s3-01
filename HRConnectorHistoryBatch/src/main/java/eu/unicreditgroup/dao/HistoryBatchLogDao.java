package eu.unicreditgroup.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import eu.unicreditgroup.mailer.ReportDataBean;

public class HistoryBatchLogDao {

	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;
	
	private ReportDataBean reportDataBean;
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }	

	/**
	 * Add information about batch execution to the log table: HR_CONN_HST_BATCH_LOG
	 * 
	 * @param reportDataBean
	 */
	public void addLogInfo(){
		
		String sql = "insert into HR_CONN_HST_BATCH_LOG values(?, ?, ?, ?, ?, ?, ?)";
		
		
				
	  	String execResult = "";
	  	if (reportDataBean.getExceptionsQueue().size() > 0){
	  		execResult = "EXCEPTIONS";
	  	} else {
	  		execResult = "SUCCESS";
	  	}
	  	
	  	Object[] args = new Object[7]; 
	  	
	    args[0] = toSqlTimeStamp(reportDataBean.getBatchStartDate()); 
	    args[1] = toSqlTimeStamp(reportDataBean.getBatchEndDate()); 
	    args[2] = toSqlTimeStamp(reportDataBean.getSrcDataDeliveryDateAsDate()); 	
	    args[3] = execResult;
	    args[4] = reportDataBean.getNewUsersCount();
	    args[5] = reportDataBean.getUpdatedUsersCount();
	    args[6] = reportDataBean.getRemovedUsersCount();
	    
	    
	    simpleJdbcTemplate.update(sql, args );
		
		
	}
	
	
	private java.sql.Timestamp toSqlTimeStamp(java.util.Date javaDate){
			
		return new java.sql.Timestamp(javaDate.getTime());
		
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
	
	
}
