package eu.unicreditgroup.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;


public class JobPositionDao {

	private SimpleJdbcTemplate simpleJdbcTemplate;
	private DataSource dataSource;
	
	private final String JOB_POS_DESC_RES_ATTR_IDX = "8"; 
	

	private final String SQL_CODES_WITH_DUPLICATES =
		" select GLOBAL_JOB_CODE																			"+
		" from ( select distinct GLOBAL_JOB_CODE, GLOBAL_JOB_ABBR, GLOBAL_JOB_DESC from HR_CONN_SAP_SRC )	"+
		" group by GLOBAL_JOB_CODE																			"+
		" having count(*)>1																					";
	
	//error list with different descriptions & abbreviations for the same code
	private final String SQL_GET_DIFFERENT_DESC_ERROR_LIST =	
		"	select distinct GLOBAL_JOB_CODE, GLOBAL_JOB_ABBR, GLOBAL_JOB_DESC	"+						
		"	from HR_CONN_SAP_SRC												"+
		"	where GLOBAL_JOB_CODE in ("+SQL_CODES_WITH_DUPLICATES+")			"+
		"	order by GLOBAL_JOB_CODE											";
//		THIS IS SLOWER VERSION OF ABOVE QUERY
//		"	select distinct a.GLOBAL_JOB_CODE, a.GLOBAL_JOB_ABBR, a.GLOBAL_JOB_DESC						"+
//		"	from HR_CONN_SAP_SRC a,																		"+
//		"	     HR_CONN_SAP_SRC b																		"+
//		"	where a.GLOBAL_JOB_CODE = b.GLOBAL_JOB_CODE													"+
//		"	and   ( a.GLOBAL_JOB_DESC!= b.GLOBAL_JOB_DESC or a.GLOBAL_JOB_ABBR!= b.GLOBAL_JOB_ABBR )	"+
//		"	order by a.GLOBAL_JOB_CODE																	";

	
	// new Job Position Descriptions to send to PLATEAU by HtmlUnit
	private final String SQL_GET_MODIFIED_DESC_FOR_SEND_TO_PLATEAU = 
		"	select distinct GLOBAL_JOB_CODE as CODE, GLOBAL_JOB_ABBR||'-'||GLOBAL_JOB_DESC as DESCR			"+
		"	from HR_CONN_SAP_SRC sap, HR_CONN_USR_ATTR_DESCR_DICT dict										"+
		"	where sap.GLOBAL_JOB_CODE =  dict.SRC_ATTR_CODE													"+
		"	and   sap.GLOBAL_JOB_ABBR || '-' || sap.GLOBAL_JOB_DESC != dict.RES_ATTR_VAL					"+
		"	and   dict.RES_ATTR_IDX = "+JOB_POS_DESC_RES_ATTR_IDX+"											"+
		"	/* protection from different descriptions & abbreviations for the same code */					"+
		"	and   GLOBAL_JOB_CODE not in ("+SQL_CODES_WITH_DUPLICATES+")									";

		
	// insert new Job Position Descriptions
	private final String SQL_INSERT_NEW_DESC =  
		"	insert into HR_CONN_USR_ATTR_DESCR_DICT(SRC_ATTR_CODE, RES_ATTR_VAL, RES_ATTR_IDX, ADDED_DATE, LAST_UPDATED_DATE)	"+
		"	select  GLOBAL_JOB_CODE as SRC_ATTR_CODE,																			"+
		"	        GLOBAL_JOB_ABBR || '-' || GLOBAL_JOB_DESC  as RES_ATTR_VAL,													"+
		"	        "+ JOB_POS_DESC_RES_ATTR_IDX +" as RES_ATTR_IDX,															"+
		"	        sysdate          as ADDED_DATE,																				"+
		"	        null             as LAST_UPDATED_DATE																		"+
		"	from        																										"+
		"	(																													"+
		"	    select distinct GLOBAL_JOB_CODE, GLOBAL_JOB_DESC, GLOBAL_JOB_ABBR												"+
		"	    from HR_CONN_SAP_SRC																							"+
		"	    where GLOBAL_JOB_CODE not in (select SRC_ATTR_CODE from HR_CONN_USR_ATTR_DESCR_DICT where RES_ATTR_IDX = "+JOB_POS_DESC_RES_ATTR_IDX+")	"+
		"	    /* protection from different descriptions & abbreviations for the same code */									"+
		"	    and   GLOBAL_JOB_CODE not in ("+SQL_CODES_WITH_DUPLICATES+")													"+
		"	)																													";
	
	
	
	// update old Job Position Descriptions
	private final String SQL_UPDATE_OLD_DESC =  
		"	update HR_CONN_USR_ATTR_DESCR_DICT dict															"+
		"	set dict.RES_ATTR_VAL = (																		"+
		"	                            select distinct GLOBAL_JOB_ABBR || '-' || GLOBAL_JOB_DESC			"+
		"	                            from HR_CONN_SAP_SRC												"+
		"	                            where GLOBAL_JOB_CODE = dict.SRC_ATTR_CODE							"+
		"	                        ),																		"+
		"	    dict.LAST_UPDATED_DATE = sysdate															"+
		"	where dict.SRC_ATTR_CODE in																		"+
		"	(																								"+
		"	    select distinct GLOBAL_JOB_CODE																"+
		"	    from HR_CONN_SAP_SRC sap, HR_CONN_USR_ATTR_DESCR_DICT dict									"+
		"	    where sap.GLOBAL_JOB_CODE =  dict.SRC_ATTR_CODE												"+
		"	    and   sap.GLOBAL_JOB_ABBR||'-'||GLOBAL_JOB_DESC != dict.RES_ATTR_VAL						"+
		"		and   RES_ATTR_IDX = "+JOB_POS_DESC_RES_ATTR_IDX+"											"+
		"	)																								"+
		"	and dict.RES_ATTR_IDX = "+JOB_POS_DESC_RES_ATTR_IDX+"											"+
		"	/* protection from different descriptions & abbreviations for the same code */					"+
		"	and dict.SRC_ATTR_CODE not in ("+SQL_CODES_WITH_DUPLICATES+")									";
	
	
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }
	public SimpleJdbcTemplate getSimpleJdbcTemplate() {
		return simpleJdbcTemplate;
	}
	public void setSimpleJdbcTemplate(SimpleJdbcTemplate simpleJdbcTemplate) {
		this.simpleJdbcTemplate = simpleJdbcTemplate;
	}
	
	
	/*
	 * This method adds new Job Position Descriptions
	 * from SAP (HR_CONN_SAP_SRC table) to HR_CONN_USR_ATTR_DESCR_DICT table.
	 */
	public int insertNewDescToDictTable(){
		return simpleJdbcTemplate.update( SQL_INSERT_NEW_DESC );
	}

	/*
	 * This method updates old Job Position Descriptions in HR_CONN_USR_ATTR_DESCR_DICT
	 * with new values from SAP (HR_CONN_SAP_SRC table)
	 */
	public int updateOldDescInDictTable(){
		return simpleJdbcTemplate.update( SQL_UPDATE_OLD_DESC );
	}

	/*
	 * This method gets Job Position Descriptions from HR_CONN_SAP_SRC table
	 * which have been modified in SAP and need to be updated in PLATEAU.
	 */
	public List<Map<String, Object>> getModifiedDescForSendToPlateau(){
		
		return simpleJdbcTemplate.queryForList( SQL_GET_MODIFIED_DESC_FOR_SEND_TO_PLATEAU );
	}
	
	/*
	 * This method gets error list with different Job Position Descriptions for the same code
	 */
	public List<Map<String, Object>> getDifferentDescErrorList(){
		
		return simpleJdbcTemplate.queryForList( SQL_GET_DIFFERENT_DESC_ERROR_LIST );
	}
}
