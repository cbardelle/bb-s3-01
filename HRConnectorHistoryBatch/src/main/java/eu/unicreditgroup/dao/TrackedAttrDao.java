package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import eu.unicreditgroup.beans.TrackedAttr;

/**
 * Fetch data from database about users attributes that has been changed in last 7 days.
 * 
 * @author C150385
 */
public class TrackedAttrDao 
{
	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	/**
	 *  Sets connection to database.
	 * 
	 * NOTICE : this one is tricky.
	 * During profiling it become fact that dataSource isn't 
	 * use at all except for creating simplpleJbdbTemplate.
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource) 
	{
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
    }
	
	/**
	 * Fetch list of changes to User Assignment Profiles Attributes 
	 * that took place in last 7 days.
	 * 
	 * @return changes stored in List<TrackedAttr>
	 */
	public List<TrackedAttr> fetchTrackedAttrsSinceLastWeek()
	{
		
		String sql = 	"select "+
						"	distinct "+
						"	plt_attr_name, "+
						"	attr_sap_code, "+
						"	new_val, "+
						"	old_val, "+
						""+
						//"-- concatenated list of ap_id's related to changed value "+
						"	(	select "+
						"			listagg( ap_id, ', ') within group ( order by ap_id) as instr  "+
						"        from "+
					    //"           --get only unique data from last 7 days "+
						"           ( select distinct res_attr_idx, attr_sap_code, new_val, old_val, ap_id  from  hr_conn_plt_attr_log where trunc( created_date ) > trunc(current_date)-7 ) inner_pal "+ 
						"        where "+
						"            inner_pal.res_attr_idx = pal.res_attr_idx and "+
						"            inner_pal.attr_sap_code = pal.attr_sap_code and "+
						"            inner_pal.new_val = pal.new_val and "+
						"            inner_pal.old_val = pal.old_val  "+
						"	) ap_id "+
						//"-- end of concatenated list  "+
						"from "+
						"    (	 "+
						//"    -- get list of changes with related plateau attribute name "+
						"        select "+
						"            distinct  "+
						"            hcpal.res_attr_idx, "+
						"            rad.res_attr_desc as plt_attr_name, "+
						"            hcpal.attr_sap_code, "+
						"            hcpal.new_val, "+
						"            hcpal.old_val "+
						"        from "+
						"            hr_conn_plt_attr_log hcpal "+
						"        join hr_conn_res_attr_dict rad on hcpal.res_attr_idx = rad.res_attr_idx "+
						//"        --data for last 7 days "+
						"        where trunc(hcpal.created_date) > trunc(current_date) - 7 "+
						//"    --end of list of changes "+
						"    )pal ";
			
			
		//important: since we want to mapped resultSet row on 2 different class objects - we have decide to not use mapping.
		List<TrackedAttr> results = simpleJdbcTemplate.query(sql, new TrackedAttrMapper());
		
		//restore historical data.
		return results;
		 		
	}
	
	/**
	 * Simple mapper that converts result set for single row 
	 * from HR_CONN_PLT_ATTR_LOG table into TrackedAttr object.
	 * 
	 * @see fetchTrackedAttrsSinceLastWeek
	 * */
	public class TrackedAttrMapper implements RowMapper<TrackedAttr> 
	{
		public TrackedAttr mapRow(ResultSet resSet, int index) throws SQLException 
		{
			//response
			TrackedAttr response = new TrackedAttr();
			
			//Attribute SAP code
			response.setAttrCode(resSet.getString("ATTR_SAP_CODE"));
			
			//Plateau attr desc 
			response.setAttrName(resSet.getString("PLT_ATTR_NAME"));
			
			//New value
			response.setDictValue(resSet.getString("NEW_VAL"));
			
			//Old value
			response.setMappedValue(resSet.getString("OLD_VAL"));
			
			//List of assignment profiles
			response.setAssignProfId(resSet.getString("AP_ID"));
				
			return response;
		}
	}
}
