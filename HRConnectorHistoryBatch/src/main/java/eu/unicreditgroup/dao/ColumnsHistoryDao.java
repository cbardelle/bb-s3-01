package eu.unicreditgroup.dao;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import eu.unicreditgroup.beans.ChangedColInf;
import eu.unicreditgroup.mailer.ReportDataBean;


public class ColumnsHistoryDao {

	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;
	
	private String sqlInsertRow = " INSERT INTO HR_CONN_COLS_HST  VALUES ( ?, ?, ?, ?, ? )";
	
	private int[] columnTypes;
	
	private ReportDataBean reportDataBean;
	
	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
        
        columnTypes = new int[5];
        columnTypes[0] = java.sql.Types.VARCHAR;
        columnTypes[1] = java.sql.Types.VARCHAR;
        columnTypes[2] = java.sql.Types.VARCHAR;
        columnTypes[3] = java.sql.Types.VARCHAR;
        columnTypes[4] = java.sql.Types.TIMESTAMP;        
        
    }	
	
	public void addChangedColInf(List<ChangedColInf> changes){
		
	    List<Object[]> rows = new ArrayList<Object[]>();
		
	    for (ChangedColInf changedCol : changes){
	    	String[] rowData = new String[5];
	    	rowData[0] = changedCol.getUserId();
	    	rowData[1] = changedCol.getColumnName();
	    	
	    	
	    	String oldValue = changedCol.getOldValue();
	    	if(StringUtils.isEmpty(oldValue)){
	    		oldValue = "";
	    	}
	    	rowData[2] = oldValue;
	    	
	    	String newValue = changedCol.getNewValue();
	    	if(StringUtils.isEmpty(newValue)){
	    		newValue = "";
	    	}	    	
	    	rowData[3] = newValue;
	    	
	    	// time stamp as source data delivery date	    	
	    	rowData[4] = reportDataBean.getSrcDataDeliveryDateAsString();
	    	        
	    	rows.add(rowData);
	    }
								
    	simpleJdbcTemplate.batchUpdate( sqlInsertRow, rows, columnTypes);
			
	}

	public SimpleJdbcTemplate getSimpleJdbcTemplate() {
		return simpleJdbcTemplate;
	}

	public void setSimpleJdbcTemplate(SimpleJdbcTemplate simpleJdbcTemplate) {
		this.simpleJdbcTemplate = simpleJdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	
	
}
