package eu.unicreditgroup.config;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.utils.ParametersListParser;


public class ConfigParams implements InitializingBean{
	
	private static final Logger logger = Logger.getLogger(ConfigParams.class);

		
	private int sourceRowProcessThreadsNumber;
	
	private String strSourceRowProcessThreadsNumber;
	
	private String strColumnsToTrack;
	
	private String[] columnsToTrack;
	
	private boolean stepProcessUsersWithInvalidLineManager;
	                
	private boolean stepProcessHistoryData;

	private boolean stepProcessUsersRemovedFromSapSource;
	
	private boolean stepProcessJobLocationDescriptionChanges;
	
	private boolean stepProcessJobPositionDescriptionChanges;

	private boolean stepSendUsersApChangedReport;
	
	private int sendUsersApChangesAtWeekDay;
	
	private String strStepProcessUsersWithInvalidLineManager;
	

	private String strStepProcessHistoryData;

	private String strStepProcessUsersRemovedFromSapSource;
	
	private String strStepProcessJobLocationDescriptionChanges;
	
	private String strStepSendUsersApChangedReport;
	
	private String strStepProcessJobPositionDescriptionChanges;

	private String strSourceDataDeliveryDate;
	
	private String strSendUsersApChangesAtWeekDay;
	
	Date sourceDataDeliveryDate = null;
	
	private String strPlateauWsWsdlUrl;
	private String strPlateauWsUsr;
	private String strPlateauWsPwd;
	
	private String strPlateauLoginPageUrl;
	private String strPlateauLoginUsr;
	private String strPlateauLoginPwd;				

	// Used by StepProcessJobPositionDescriptionChanges
	private String strPlateauJobPositionEditPageUrl;
	
	
	
	@Override
	public void afterPropertiesSet() throws Exception {		
		
		ParametersListParser listParser = new ParametersListParser(); 
			
		logger.debug("ConfigParams object: " + toString());
		
		sourceRowProcessThreadsNumber = NumberUtils.toInt(strSourceRowProcessThreadsNumber, -1);
		if (sourceRowProcessThreadsNumber < 1){
			throw new IllegalStateException(String.format("Number of threads must be positive number but has value: '%s'.", strSourceRowProcessThreadsNumber));
		}
		
		columnsToTrack = listParser.parse(strColumnsToTrack);
		
		if (columnsToTrack.length == 0){
			throw new IllegalStateException("At least one column to track has to be specified!");
		}
		
		stepProcessUsersWithInvalidLineManager = processBooleanParameter(strStepProcessUsersWithInvalidLineManager);
		
		stepProcessHistoryData = processBooleanParameter(strStepProcessHistoryData);

		stepProcessUsersRemovedFromSapSource = processBooleanParameter(strStepProcessUsersRemovedFromSapSource);
		
		stepProcessJobLocationDescriptionChanges = processBooleanParameter(strStepProcessJobLocationDescriptionChanges);

		stepProcessJobPositionDescriptionChanges = processBooleanParameter(strStepProcessJobPositionDescriptionChanges);
		
		stepSendUsersApChangedReport = processBooleanParameter(strStepSendUsersApChangedReport);
	
		sendUsersApChangesAtWeekDay = processDayOfWeekParameter(strSendUsersApChangesAtWeekDay);
	}

	
	/** Parse short names of days into integer value. Return value is compatible with java.util.Calendar 
	 * day of week numeration. If input value doesn't match any day of week function return index 1 (Sunday). */
	
	public int processDayOfWeekParameter(String dayOfWeek)
	{
		String day = dayOfWeek.toLowerCase();
		
		if("sun".equals(day)) return 1;
		if("mon".equals(day)) return 2;
		if("tues".equals(day)) return 3;
		if("wed".equals(day)) return 4;
		if("thur".equals(day)) return 5;
		if("fri".equals(day)) return 6;
		if("sat".equals(day)) return 7;
		
		//by default return sunday.
		return 1;
	}

	public boolean processBooleanParameter(String parameter){
		if (StringUtils.equalsIgnoreCase(parameter, "y")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "1")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "tak")){
			parameter = "true";
		}
		
		if (StringUtils.equalsIgnoreCase(parameter, "si")){
			parameter = "true";
		}
		
		return BooleanUtils.toBoolean(parameter);
	}
		
	public String getFileNameWithoutExt(String fileName){
		if (fileName == null) return null;
		int dotPos = fileName.lastIndexOf('.');	
		return dotPos < 0 ? fileName : fileName.substring(0, dotPos);		
	}


	public int getSourceRowProcessThreadsNumber() {
		return sourceRowProcessThreadsNumber;
	}


	public void setSourceRowProcessThreadsNumber(int sourceRowProcessThreadsNumber) {
		this.sourceRowProcessThreadsNumber = sourceRowProcessThreadsNumber;
	}


	public String getStrSourceRowProcessThreadsNumber() {
		return strSourceRowProcessThreadsNumber;
	}


	public void setStrSourceRowProcessThreadsNumber(
			String strSourceRowProcessThreadsNumber) {
		this.strSourceRowProcessThreadsNumber = strSourceRowProcessThreadsNumber;
	}


	public String getStrColumnsToTrack() {
		return strColumnsToTrack;
	}


	public void setStrColumnsToTrack(String strColumnsToTrack) {
		this.strColumnsToTrack = strColumnsToTrack;
	}


	public String[] getColumnsToTrack() {
		return columnsToTrack;
	}


	public void setColumnsToTrack(String[] columnsToTrack) {
		this.columnsToTrack = columnsToTrack;
	}

	
	public boolean isStepProcessHistoryData() {
		return stepProcessHistoryData;
	}


	public void setStepProcessHistoryData(boolean stepProcessHistoryData) {
		this.stepProcessHistoryData = stepProcessHistoryData;
	}


	public boolean isStepProcessUsersRemovedFromSapSource() {
		return stepProcessUsersRemovedFromSapSource;
	}


	public void setStepProcessUsersRemovedFromSapSource(
			boolean stepProcessUsersRemovedFromSapSource) {
		this.stepProcessUsersRemovedFromSapSource = stepProcessUsersRemovedFromSapSource;
	}


	


	public String getStrStepProcessHistoryData() {
		return strStepProcessHistoryData;
	}


	public void setStrStepProcessHistoryData(String strStepProcessHistoryData) {
		this.strStepProcessHistoryData = strStepProcessHistoryData;
	}


	public String getStrStepProcessUsersRemovedFromSapSource() {
		return strStepProcessUsersRemovedFromSapSource;
	}


	public void setStrStepProcessUsersRemovedFromSapSource(
			String strStepProcessUsersRemovedFromSapSource) {
		this.strStepProcessUsersRemovedFromSapSource = strStepProcessUsersRemovedFromSapSource;
	}


	public String getStrSourceDataDeliveryDate() {
		return strSourceDataDeliveryDate;
	}


	public void setStrSourceDataDeliveryDate(String strSourceDataDeliveryDate) {
		this.strSourceDataDeliveryDate = strSourceDataDeliveryDate;
	}

	
	public String getStrStepProcessUsersWithInvalidLineManager() {
		return strStepProcessUsersWithInvalidLineManager;
	}


	public void setStrStepProcessUsersWithInvalidLineManager(
			String strStepProcessUsersWithInvalidLineManager) {
		this.strStepProcessUsersWithInvalidLineManager = strStepProcessUsersWithInvalidLineManager;
	}


	public boolean isStepProcessUsersWithInvalidLineManager() {
		return stepProcessUsersWithInvalidLineManager;
	}


	public void setStrStepSendUsersApChangedReport(
			String strStepSendUsersApChangedReport) {
		this.strStepSendUsersApChangedReport = strStepSendUsersApChangedReport;
	}
	
	public boolean isStepSendUsersApChangedReport() {
		return stepSendUsersApChangedReport;
	}
	
	public String getStrStepSendUsersApChangedReport() {
		return strStepSendUsersApChangedReport;
	}
	
	
	public String getStrPlateauWsWsdlUrl() {
		return strPlateauWsWsdlUrl;
	}

	public void setStrPlateauWsWsdlUrl(String strPlateauWsWsdlUrl) {
		this.strPlateauWsWsdlUrl = strPlateauWsWsdlUrl;
	}


	public boolean isStepProcessJobLocationDescriptionChanges() {
		return stepProcessJobLocationDescriptionChanges;
	}


	public void setStepProcessJobLocationDescriptionChanges(
			boolean stepProcessJobLocationDescriptionChanges) {
		this.stepProcessJobLocationDescriptionChanges = stepProcessJobLocationDescriptionChanges;
	}


	public String getStrStepProcessJobLocationDescriptionChanges() {
		return strStepProcessJobLocationDescriptionChanges;
	}


	public void setStrStepProcessJobLocationDescriptionChanges(
			String strStepProcessJobLocationDescriptionChanges) {
		this.strStepProcessJobLocationDescriptionChanges = strStepProcessJobLocationDescriptionChanges;
	}


	public boolean isStepProcessJobPositionDescriptionChanges() {
		return stepProcessJobPositionDescriptionChanges;
	}


	public void setStepProcessJobPositionDescriptionChanges(
			boolean stepProcessJobPositionDescriptionChanges) {
		this.stepProcessJobPositionDescriptionChanges = stepProcessJobPositionDescriptionChanges;
	}


	public String getStrStepProcessJobPositionDescriptionChanges() {
		return strStepProcessJobPositionDescriptionChanges;
	}


	public void setStrStepProcessJobPositionDescriptionChanges(
			String strStepProcessJobPositionDescriptionChanges) {
		this.strStepProcessJobPositionDescriptionChanges = strStepProcessJobPositionDescriptionChanges;
	}


	public String getStrPlateauWsUsr() {
		return strPlateauWsUsr;
	}


	public void setStrPlateauWsUsr(String strPlateauWsUsr) {
		this.strPlateauWsUsr = strPlateauWsUsr;
	}


	public String getStrPlateauWsPwd() {
		return strPlateauWsPwd;
	}


	public void setStrPlateauWsPwd(String strPlateauWsPwd) {
		this.strPlateauWsPwd = strPlateauWsPwd;
	}


	public String getStrPlateauLoginPageUrl() {
		return strPlateauLoginPageUrl;
	}


	public void setStrPlateauLoginPageUrl(String strPlateauLoginPageUrl) {
		this.strPlateauLoginPageUrl = strPlateauLoginPageUrl;
	}


	public String getStrPlateauLoginUsr() {
		return strPlateauLoginUsr;
	}


	public void setStrPlateauLoginUsr(String strPlateauLoginUsr) {
		this.strPlateauLoginUsr = strPlateauLoginUsr;
	}


	public String getStrPlateauLoginPwd() {
		return strPlateauLoginPwd;
	}


	public void setStrPlateauLoginPwd(String strPlateauLoginPwd) {
		this.strPlateauLoginPwd = strPlateauLoginPwd;
	}


	public String getStrPlateauJobPositionEditPageUrl() {
		return strPlateauJobPositionEditPageUrl;
	}


	public void setStrPlateauJobPositionEditPageUrl(
			String strPlateauJobPositionEditPageUrl) {
		this.strPlateauJobPositionEditPageUrl = strPlateauJobPositionEditPageUrl;
	}
	
	public String getStrSendUsersApChangesAtWeekDay() {
		return strSendUsersApChangesAtWeekDay;
	}


	public void setStrSendUsersApChangesAtWeekDay(
			String strSendUsersApChangesAtWeekDay) {
		this.strSendUsersApChangesAtWeekDay = strSendUsersApChangesAtWeekDay;
	}


	public int getSendUsersApChangesAtWeekDay() {
		return sendUsersApChangesAtWeekDay;
	}

}
