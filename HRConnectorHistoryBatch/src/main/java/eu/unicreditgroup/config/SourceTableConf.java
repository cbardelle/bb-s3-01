package eu.unicreditgroup.config;

import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

/**
 * @author UV00074
 *
 */
public class SourceTableConf implements InitializingBean {

	
	//source table columns information
	Map sourceColumnsMap; 
	
	//number of columns in the historical source table
	int sourceColumnsCount;
	
	
	//TODO  THIS SETTINGS HAS TO BE REVISED EVERY TIME THE TABLE HR_CONN_SAP_SRC_HST IS CHANGED!!! 
	
	private final String USER_ID_COL_NAME = "USER_ID";
	
	//private final int USER_ID_COLUMN_INDEX = 2;
	
	private final String COL_REMOVED_DATE_NAME = "REMOVED_DATE";
	
	//private final int COL_REMOVED_DATE_INDEX = 86;
	
	private final int ADDITIONAL_COLUMNS_OF_HR_CONN_SAP_SRC_HST = 4;
	
	
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if (sourceColumnsMap.size() == 0 ){			
			throw new Exception("Source table info was not properly initialized!");			
		}
		// source + additional columns count
		sourceColumnsCount = sourceColumnsMap.size();		
	}


	public Map getSourceColumnsMap() {
		return sourceColumnsMap;
	}


	public void setSourceColumnsMap(Map sourceColumnsMap) {
		this.sourceColumnsMap = sourceColumnsMap;
	}


	/**
	 * Get count of all columns (source cols and cols specific to HR_CONN_SAP_SRC_HST)
	 * @return
	 */
	public int getSourceAndAdditionalColumnsCount() {
		return sourceColumnsCount;
	}
	
	/**
	 * Get count of only source columns (identical to HR_CONN_SAP_SRC)
	 * @return
	 */
	public int getOnlySourceColumnsCount() {
		return sourceColumnsCount - ADDITIONAL_COLUMNS_OF_HR_CONN_SAP_SRC_HST;
	}	
	
	public int getUserIdIndex(){
		SourceColumn sc = (SourceColumn) sourceColumnsMap.get(USER_ID_COL_NAME);
		return sc.getSrcColIndex();		
	}


	/**
	 * returns the index of the column 'REMOVED_DATE'
	 * (starting from 0!)  
	 * @return
	 */
	public int getColRemovedDateIndex() {
		SourceColumn sc = (SourceColumn) sourceColumnsMap.get(COL_REMOVED_DATE_NAME);
		return sc.getSrcColIndex();
	}


	public String getColRemovedDateName() {
		return COL_REMOVED_DATE_NAME;		
	}

	public int getAdditionalColsConut() {
		return  ADDITIONAL_COLUMNS_OF_HR_CONN_SAP_SRC_HST;
	}
	
	
}
