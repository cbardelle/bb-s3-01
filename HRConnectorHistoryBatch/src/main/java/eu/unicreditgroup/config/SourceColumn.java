package eu.unicreditgroup.config;

public class SourceColumn {
	
	String srcColName;
	
	int srcColIndex;
	
	String type;
	
	int length;
	
	boolean mandatory = false;
	

	public String getSrcColName() {
		return srcColName;
	}

	public void setSrcColName(String srcColName) {
		this.srcColName = srcColName;
	}

	public int getSrcColIndex() {
		return srcColIndex;
	}

	public void setSrcColIndex(int srcColIndex) {
		this.srcColIndex = srcColIndex;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	
	


}
