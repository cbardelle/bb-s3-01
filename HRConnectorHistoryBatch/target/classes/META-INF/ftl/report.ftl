<html>

<head>
	
	<style>
		body {
			font-family: verdana, sans-serif;
			font-size: 10pt;
			padding: 6px;
		}
		
		.headInfoDoneWithoutErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #3db00b;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoDoneWithErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #FF8C00;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoFailedFast {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #d8001d;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		
		
		
		.resultTable {
			width: 100%;
			border: 1px #6a8546 solid;
			text-align: center;
		}
		
		.resultTableName {
			background-color: #6a8546;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
			padding: 3px;
			width: 280px;
			margin-top: 10px;
		}
		
		.resultName {
			font-weight: bolder;
			width: 300px;
			text-align: left;
			vertical-align: top;
			font-size: 10pt;
		}
		
		.resultValue {
			text-align: left;
			padding-left: 5px;
			font-size: 10pt;
		}
		
		.errorValue {
			color: #d8001d;
			font-weight: bolder;
		}

		.warningValue {
			text-align: left;
			color: #ed872c;
			font-weight: bolder;
		}

	</style>

</head>

<body>
	
	<#-- General exit code  -->
	<#if (rb.exceptionsQueue?size == 0)>
		<div class="headInfoDoneWithoutErrors"> 
			&radic; Batch finished successfully.
		</div>				
	<#else>	
		<div class="headInfoDoneWithErrors"> 
			Batch finisched but there were exceptions during execution of the batch.
		</div>
	</#if>

	
	
	<#-- Generic execution info  -->
	<div class="resultTableName">Execution</div>
	<table class="resultTable">
		<tr>
			<td class="resultName">Start</td>
			<td class="resultValue">
				<#if rb.batchStartDate??>
					${rb.batchStartDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
		<tr>
			<td class="resultName">End</td>
			<td  class="resultValue">
				<#if rb.batchEndDate?? >
					${rb.batchEndDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
	</table>


	<#-- Process users with invalid line manager -->
	<div class="resultTableName">Process users with invalid line manager</div>
	<table class="resultTable">
		<#if (rb.configParams.stepProcessUsersWithInvalidLineManager == true)>						
			<tr>			
				<td class="resultName">Total count of users with invalid line manager in the source file</td>
				<td  class="resultValue">
					${rb.usersWithInvalidLineManagerCount}
				</td>
			</tr>			
			<tr>			
				<td class="resultName">Users with invalid line manager reseted by Plateau Webservice</td>
				<td  class="resultValue">
					${rb.usersWithInvalidLineManagerWS}
				</td>
			</tr>			
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
	<#-- Process history data -->
	<div class="resultTableName">Process history data</div>
	<table class="resultTable">
		<#if (rb.configParams.stepProcessHistoryData == true)>		
			<tr>
				<td class="resultName">Start</td>
				<td class="resultValue">
					<#if rb.stepProcessHistoryDataStartDate?? >
						${rb.stepProcessHistoryDataStartDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
				<td class="resultName">End</td>
				<td  class="resultValue">
					<#if rb.stepProcessHistoryDataEndDate?? >
						${rb.stepProcessHistoryDataEndDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>				
			<tr>			
				<td class="resultName">Number of added users</td>
				<td  class="resultValue">
					${rb.newUsersCount}
				</td>
			</tr>
			<tr>			
				<td class="resultName">Number of updated users</td>
				<td  class="resultValue">
					${rb.updatedUsersCount}
				</td>
			</tr>		
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
	<#-- Process removed users -->
	<div class="resultTableName">Process users removed from SAP source</div>
	<table class="resultTable">
		<#if (rb.configParams.stepProcessUsersRemovedFromSapSource == true)>
			<tr>			
				<td class="resultName">Number of removed users</td>
				<td  class="resultValue">
					${rb.removedUsersCount}
				</td>
			</tr>
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
	
	<#-- Process Job Location Description changes -->
	<div class="resultTableName">Process Job Location Description changes</div>
	<table class="resultTable">
		<#if (rb.configParams.stepProcessJobLocationDescriptionChanges == true)>
			<tr>			
				<td class="resultName">Different descriptions for the same code</td>
				<#if (rb.numOfJobLocDescDifferentForTheSameCode == 0)>
					<td  class="resultValue">
						0
					</td>
				<#else>
					<td  class="warningValue">
						${rb.numOfJobLocDescDifferentForTheSameCode}
					</td>
				</#if>
			</tr>
			<tr>			
				<td class="resultName">Updates sent by WS</td>
				<td  class="resultValue">
					${rb.numOfJobLocDescUpdatesSentByWS}
				</td>
			</tr>
			<tr>			
				<td class="resultName">Updates in dict table</td>
				<td  class="resultValue">
					${rb.numOfJobLocDescUpdatesInDictTable}
				</td>
			</tr>
			<tr>			
				<td class="resultName">Inserts to dict table</td>
				<td  class="resultValue">
					${rb.numOfJobLocDescInsertsToDictTable}
				</td>
			</tr>
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>

	<#-- Process Job Position Description changes -->
	<div class="resultTableName">Process Job Position Description changes</div>
	<table class="resultTable">
		<#if (rb.configParams.stepProcessJobPositionDescriptionChanges == true)>
			<tr>			
				<td class="resultName">Different descriptions for the same code</td>
				<#if (rb.numOfJobPosDescDifferentForTheSameCode == 0)>
					<td  class="resultValue">
						0
					</td>
				<#else>
					<td  class="warningValue">
						${rb.numOfJobPosDescDifferentForTheSameCode}
					</td>
				</#if>
			</tr>
			<tr>			
				<td class="resultName">Updates sent by HtmlUnit</td>
				<td  class="resultValue">
					${rb.numOfJobPosDescUpdatesSentByHtmlUnit}
				</td>
			</tr>
			<tr>			
				<td class="resultName">Updates in dict table</td>
				<td  class="resultValue">
					${rb.numOfJobPosDescUpdatesInDictTable}
				</td>
			</tr>
			<tr>			
				<td class="resultName">Inserts to dict table</td>
				<td  class="resultValue">
					${rb.numOfJobPosDescInsertsToDictTable}
				</td>
			</tr>
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
	
	
</body>

</html>
